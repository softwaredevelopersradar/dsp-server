﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using Settings;
using Correlator;
using DspDataModel;
using DspDataModel.Hardware;
using Phases;
using RadioRecorder;
using SimulatorProtocols;

namespace Simulator
{
    public class SignalSimulator
    {
        public float NoiseLevel
        {
            get => _noiseLevel;
            set
            {
                _noiseLevel = value;
                _simulatorDo?.Save();
                if (_simulatorDo != null)
                {
                    _simulatorDo.NoiseLevel = value;
                    _simulatorDo.Save();
                }
            }
        }

        public float FuzzinessLevel
        {
            get => _fuzzinessLevel;
            set
            {
                if (value < 0)
                {
                    return;
                }
                _fuzzinessLevel = value;
                if (_simulatorDo != null)
                {
                    _simulatorDo.FuzzinessLevel = value;
                    _simulatorDo.Save();
                }
            }
        }

        public bool GeneratePhaseNoise
        {
            get => _generatePhaseNoise;
            set
            {
                _generatePhaseNoise = value; 
                _simulatorDo?.Save();
                if (_simulatorDo != null)
                {
                    _simulatorDo.GeneratePhaseNoise = value;
                    _simulatorDo.Save();
                }
            }
        }

        public ReceiverChannel Channel { get; set; }

        public bool PlayRecord { get; set; }

        public Signal SignalGeneratorSignal { get; set; }

        public string RecordName
        {
            get => _recordName;
            set
            {
                _recordName = value;
                _recordPlayer.OpenRecord(RecordName);
            }
        }

        public ObservableCollection<Signal> Signals { get; set; }

        public ObservableCollection<FhssSignal> FhssSignals { get; set; }

        private readonly TheoreticalTable _theoreticalTable;
        private readonly RadioPathTable _radioPathTable;
        private readonly Random _random;

        private readonly SimulatorDo _simulatorDo;

        private readonly RecordPlayer _recordPlayer;
        private string _recordName;

        private static SignalSimulator _instance;

        private int _currentShift;

        /// <summary>
        /// Additional noise level to imitate noise level deviation in one band
        /// </summary>
        private readonly float[][] _bandNoiseProfiles;

        /// <summary>
        /// Additional noise level to imitate noise level deviation in one band that changes per time
        /// </summary>
        private readonly float[][] _extraBandNoiseProfiles;

        /// <summary>
        /// profile offsets for each channel (profile is shifted for each channel by different value)
        /// </summary>
        private readonly int[][] _bandNoiseProfileOffsets;

        private bool _generatePhaseNoise;
        private float _noiseLevel;
        private float _fuzzinessLevel;

        private int _profileUpdateIndex = 0;

        private const float NoiseGeneratorAdditionalLevel = 30;

        private const int PhaseNoiseDeviation = 20;

        private const int BandProfileParts = 30;
        private const int ProfilePartSize = Constants.BandSampleCount / BandProfileParts + 1;

        private const int BandExtraProfileParts = 10;
        private const int ExtraProfilePartSize = Constants.BandSampleCount / BandExtraProfileParts + 1;

        public SignalSimulator(bool saveSimulatedSignals = true, string radioPathTableFilename = "") : this(new SimulatorDo(), saveSimulatedSignals, radioPathTableFilename)
        { }

        internal SignalSimulator(SimulatorDo simulatorDo, bool saveSimulatedSignals = true, string radioPathTableFilename = "")
        {
            _theoreticalTable = new TheoreticalTable();
            simulatorDo.IgnoreSave = !saveSimulatedSignals;

            try
            {
                _radioPathTable = new RadioPathTable(radioPathTableFilename);
            }
            catch
            {
                _radioPathTable = new RadioPathTable();
            }

            _random = new Random(DateTime.Now.Millisecond);

            Signals = new ObservableCollection<Signal>(simulatorDo.Signals);
            foreach (var s in Signals)
            {
                s.Simulator = this;
                UpdateSignalPhases(s);
            }
            Signals.CollectionChanged += UpdateSignals;

            FhssSignals = new ObservableCollection<FhssSignal>(simulatorDo.FhssSignals);
            foreach (var s in FhssSignals)
            {
                s.SetSimulator(this);
            }
            FhssSignals.CollectionChanged += UpdateFhssSignals;

            _recordPlayer = new RecordPlayer();

            NoiseLevel = simulatorDo.NoiseLevel;
            FuzzinessLevel = simulatorDo.FuzzinessLevel;
            GeneratePhaseNoise = simulatorDo.GeneratePhaseNoise;
            PlayRecord = false;

            _bandNoiseProfiles = new float[Config.Instance.BandSettings.BandCount][];
            _extraBandNoiseProfiles = new float[Config.Instance.BandSettings.BandCount][];
            _bandNoiseProfileOffsets = new int[Config.Instance.BandSettings.BandCount][];
            var maxSampleOffset = Constants.BandSampleCount / BandProfileParts;

            for (var i = 0; i < Config.Instance.BandSettings.BandCount; ++i)
            {
                _bandNoiseProfiles[i] = GenerateBandProfile(-10);
                _extraBandNoiseProfiles[i] = new float[BandExtraProfileParts];
                _bandNoiseProfileOffsets[i] = new int[Constants.ReceiversCount];
                for (var j = 0; j < Constants.ReceiversCount; ++j)
                {
                    _bandNoiseProfileOffsets[i][j] = _random.Next(0, maxSampleOffset);
                }
            }

            _simulatorDo = simulatorDo;
        }

        private void UpdateExtraBandNoiseProfile(int bandNumber)
        {
            if (_profileUpdateIndex++ != 13)
            {
                return;
            }
            _profileUpdateIndex = 0;

            var profile = _extraBandNoiseProfiles[bandNumber];
            var index = _random.Next(profile.Length);
            profile[index] = (float) -_random.NextDouble() * 2;
        }

        private float[] GenerateBandProfile(int minLevelDecay)
        {
            const int maxLevelStep = 2;

            var profile = new float[BandProfileParts];
            for (var i = 3; i < BandProfileParts - 2; ++i)
            {
                profile[i] = _random.Next(minLevelDecay, 0);
                if (i > 3)
                {
                    var now = profile[i];
                    var prev = profile[i - 1];
                    if (Math.Abs(now - prev) > maxLevelStep)
                    {
                        if (now > prev)
                        {
                            profile[i] = prev + maxLevelStep;
                        }
                        else
                        {
                            profile[i] = prev - maxLevelStep;
                        }
                    }
                }
            }
            var startValue = profile[3];
            profile[1] = startValue / 3;
            profile[2] = startValue * 2 / 3;

            var endValue = profile[profile.Length - 3];
            profile[profile.Length - 1] = endValue / 3;
            profile[profile.Length - 2] = endValue * 2 / 3;

            return profile;
        }

        private void UpdateFhssSignals(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.Action == NotifyCollectionChangedAction.Remove)
            {
                UpdateFhssSignalsDo();
                return;
            }
            if (e.NewItems == null)
            {
                return;
            }
            foreach (FhssSignal fhssSignal in e.NewItems)
            {
                fhssSignal.SetSimulator(this);
                foreach (var signal in fhssSignal.CurrentSignals)
                {
                    UpdateSignalPhases(signal);
                }
            }
            UpdateFhssSignalsDo();
        }

        private void UpdateSignals(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.Action == NotifyCollectionChangedAction.Remove)
            {
                UpdateSignalsDo();
                return;
            }
            if (e.NewItems == null)
            {
                return;
            }
            foreach (Signal signal in e.NewItems)
            {
                UpdateSignalPhases(signal);
                signal.Simulator = this;
            }
            UpdateSignalsDo();
        }

        private void UpdateSignalsDo()
        {
            if (_simulatorDo != null)
            {
                _simulatorDo.Signals = Signals.ToArray();
                _simulatorDo.Save();
            }
        }

        private void UpdateFhssSignalsDo()
        {
            if (_simulatorDo != null)
            {
                _simulatorDo.FhssSignals = FhssSignals.ToArray();
                _simulatorDo.Save();
            }
        }

        public void UpdateSignal(Signal signal)
        {
            UpdateSignalPhases(signal);
            UpdateSignalsDo();
        }

        public void UpdateFhssSignal(FhssSignal signal)
        {
            UpdateFhssSignalsDo();
        }

        public void UpdateSignalPhases(Signal signal)
        {
            signal.CalculateOffsets();
            signal.Phases = new float[signal.EndOffset - signal.StartOffset + 1, 5];
            for (var i = signal.StartOffset; i <= signal.EndOffset; ++i)
            {
                var frequency = Utilities.GetFrequencyKhz(signal.BandNumber, i);
                var phases = GetPhases(frequency, signal.Direction);
                for (int j = 0; j < 5; ++j)
                {
                    signal.Phases[i - signal.StartOffset, j] = phases[j];
                }
            }
        }

        private DataResponse ReadResponseFromPlayer(IReadOnlyList<byte> bandNumbers)
        {
            var scan = _recordPlayer.ReadScan(bandNumbers);
            if (scan == null)
            {
                var channels = Enumerable.Repeat(new ChannelData(), Constants.ReceiversCount).ToArray();
                scan = new DataResponse(channels, PlayRecord);
            }
            for (var i = 0; i < scan.Channels.Length; i++)
            {
                var channel = scan.Channels[i];
                AddSignalsToChannel(channel, bandNumbers[i], i);
            }
            return scan;
        }

        public DataResponse GetResponse(IReadOnlyList<byte> bandNumbers)
        {
            if (_recordPlayer.IsRecordOpened && PlayRecord)
            {
                return ReadResponseFromPlayer(bandNumbers);
            }
            var channels = new ChannelData[Constants.ReceiversCount];
            UpdateFhssSignals();
            _currentShift = _random.Next(0, 360);
            for (var i = 0; i < channels.Length; ++i)
            {
                channels[i] = GetChannel(bandNumbers[i], i);
            }
            return new DataResponse(channels, PlayRecord);
        }

        private float[] GetPhases(float frequencyKhz, int direction)
        {
            var theoreticalPhases = _theoreticalTable.GetPhases(frequencyKhz)[direction].Phases;
            var radioPathPhases = _radioPathTable.GetPhases(frequencyKhz).Phases;
            var phases = new float[5];

            for (int i = 1; i < phases.Length; ++i)
            {
                phases[i] = (720 - theoreticalPhases[i - 1] - radioPathPhases[i - 1] / 3) % 360;
            }
            return phases;
        }

        private void UpdateFhssSignals()
        {
            foreach (var signal in FhssSignals)
            {
                signal.Update();
            }
        }

        public void AddFhssSignal(FhssSignal signal)
        {
            FhssSignals.Add(signal);
            signal.SetSimulator(this);
        }

        private ChannelData GetChannel(int bandNumber, int index)
        {
            if (Channel == ReceiverChannel.NoiseGenerator)
            {
                return GetNoiseGeneratorChannel(bandNumber, index);
            }
            else
            {
                return GetAirChannel(bandNumber, index);
            }
        }

        private ChannelData GetNoiseGeneratorChannel(int bandNumber, int index)
        {
            var amplitudes = new byte[Constants.BandSampleCount];
            var phases = new short[Constants.BandSampleCount];

            var channelFactors = new float[] {2, 3, 4, 7, 9, 11};
            var factor = channelFactors[index] * 2e-4f;

            for (var i = 0; i < amplitudes.Length; ++i)
            {
                var fuzz = _random.Next((int)(-1 * FuzzinessLevel), (int)FuzzinessLevel);
                var amplitude = NoiseLevel - Constants.ReceiverMinAmplitude + fuzz + NoiseGeneratorAdditionalLevel;
                var frequency = Utilities.GetFrequencyKhz(bandNumber, i);
                var shift = GeneratePhaseNoise ? _random.Next(0, PhaseNoiseDeviation) : 0;
                var phase = (short) ((bandNumber * 10 + frequency * factor + shift) % 3600);
                amplitudes[i] = (byte)amplitude;
                phases[i] = phase;
            }
            return new ChannelData(amplitudes, phases);
        }

        private void AddSignalsToChannel(ChannelData channel, int bandNumber, int index)
        {
            var amplitudes = channel.Amplitudes;
            var phases = channel.Phases;

            var allSignals = Signals.Concat(FhssSignals.SelectMany(s => s.CurrentSignals)).ToList();
            if (SignalGeneratorSignal != null)
            {
                allSignals.Add(SignalGeneratorSignal);
            }

            foreach (var signal in allSignals)
            {
                if (signal.Phases == null)
                {
                    continue;
                }
                if (signal.BandNumber == bandNumber)
                {
                    var maxAmplitude = (byte)(signal.Amplitude - Constants.ReceiverMinAmplitude);
                    var minAmplitude = maxAmplitude - 4;
                    var length = signal.EndOffset - signal.StartOffset + 1;
                    var center = (length + 1) / 2;
                    for (var i = signal.StartOffset; i <= signal.EndOffset; ++i)
                    {
                        if (signal.BandwidthKhz > 50)
                        {
                            var x = i - signal.StartOffset < center
                                ? (1f * i - signal.StartOffset) / center
                                : (signal.EndOffset - 1f * i) / center;
                            amplitudes[i] = (byte)(minAmplitude + (maxAmplitude - minAmplitude) * x + 1);
                        }
                        else
                        {
                            amplitudes[i] = maxAmplitude;
                        }

                        if (index < Constants.DfReceiversCount)
                        {
                            var shift = GeneratePhaseNoise ? _random.Next(-PhaseNoiseDeviation, PhaseNoiseDeviation) : 0;
                            var phase = PhaseMath.NormalizePhase(signal.Phases[i - signal.StartOffset, index] + shift + _currentShift);
                            phases[i] = (short)(phase * 10);
                        }
                    }
                }
            }
        }

        private ChannelData GetAirChannel(int bandNumber, int index)
        {
            var amplitudes = new byte[Constants.BandSampleCount];
            var phases = new short[Constants.BandSampleCount];
            var profile = _bandNoiseProfiles[bandNumber];
            UpdateExtraBandNoiseProfile(bandNumber);

            for (var i = 0; i < amplitudes.Length; ++i)
            {
                var fuzz = _random.Next((int)(-1 * FuzzinessLevel), (int)FuzzinessLevel);

                var profileIndex = (i - _bandNoiseProfileOffsets[bandNumber][index]) / ProfilePartSize;
                var profileOffset = profileIndex < profile.Length ? profile[profileIndex] : 0;
                var extraProfileOffset = _extraBandNoiseProfiles[bandNumber][i / ExtraProfilePartSize];

                var amplitude = NoiseLevel - Constants.ReceiverMinAmplitude + fuzz + profileOffset + extraProfileOffset;

                amplitudes[i] = (byte)amplitude;
                phases[i] = (short)_random.Next(0, 3600);
            }

            var channel = new ChannelData(amplitudes, phases);
            AddSignalsToChannel(channel, bandNumber, index);

            return channel;
        }
    }
}

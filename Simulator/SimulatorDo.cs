﻿using System;
using System.IO;
using System.Text;
using YamlDotNet.Serialization;

namespace Simulator
{
    internal class SimulatorDo
    {
        public Signal[] Signals { get; set; } = new Signal[0];

        public FhssSignal[] FhssSignals { get; set; } = new FhssSignal[0];

        public float NoiseLevel { get; set; } = -95;
        public float FuzzinessLevel { get; set; } = 5;

        public bool GeneratePhaseNoise { get; set; } = true;

        public const string DefaultFileName = "SimulatorSave.yaml";

        public bool IgnoreSave { get; set; } = false;

        public void Save(string filename = DefaultFileName)
        {
            if (IgnoreSave)
            {
                return;
            }
            try
            {
                var serializer = new SerializerBuilder()
                    .EmitDefaults()
                    .Build();
                using (var fs = File.Open(filename, FileMode.Create))
                using (var sw = new StreamWriter(fs, Encoding.UTF8))
                {
                    serializer.Serialize(sw, this);
                }
            }
            catch (Exception ex)
            {
                // ignored
            }

        }

        /// <returns> returns true if load is successful </returns>
        public static SimulatorDo Load(string filename = DefaultFileName)
        {
            try
            {
                return LoadInner(filename);
            }
            catch (Exception ex)
            {
                return new SimulatorDo();
            }
        }

        private static SimulatorDo LoadInner(string filename)
        {
            var deserializer = new DeserializerBuilder()
                .Build();
            return deserializer.Deserialize<SimulatorDo>(File.ReadAllText(filename));
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using DspDataModel;
using DspDataModel.Data;
using DspDataModel.Hardware;
using HardwareLibrary.TransmitionChannels;
using Settings;

namespace HardwareLibrary.FpgaDevices
{
    public class FpgaDeviceManager : IFpgaDeviceManager
    {
        private readonly IFpgaDevice[] _devices;
        public IReadOnlyList<IFpgaDevice> Devices => _devices;
        public FpgaMode Mode { get; private set; } = FpgaMode.Intelligence;

        private bool _isInitialized;

        public FpgaOffsetsSetup OffsetsSetup { get; }

        public FpgaDeviceBandsSettingsSource BandsSettingsSource { get; }

        private readonly object _lockObject = new object();

        public FpgaDeviceManager()
        {
            _devices = new IFpgaDevice[Config.Instance.HardwareSettings.FpgaDevicesCount];
            OffsetsSetup = new FpgaOffsetsSetup();
            BandsSettingsSource = new FpgaDeviceBandsSettingsSource();

            for (var i = 0; i < _devices.Length; ++i)
            {
                _devices[i] = new FpgaDevice(BandsSettingsSource, OffsetsSetup, i);
            }
        }

        public ITransmissionChannel GetFpgaTransmissionChannel()
        {
            return new FpgaTransmitionChannel(Devices[0], 2, 0x20000 + 0x18, 0x20000 + 0x58, 0x20000 + 0x7C, 0x20000 + 0x78);
        }

        public bool StartDevices()
        {
            var flags = Devices
                .Select(device => device.GetAdcFlag())
                .ToArray();

            var result = true;
            for (var i = 0; i < Devices.Count; i++)
            {
                result = result && Devices[i].StartAdc(flags[i]);
            }
            return result;
        }

        public bool WaitData()
        {
            foreach (var device in _devices)
            {
                if (!device.WaitData())
                {
                    return false;
                }
            }
            return true;
        }

        public bool StartFhssMode(FhssSetup setup)
        {
            if (Mode == FpgaMode.Fhss)
            {
                return true;
            }
            lock (_lockObject)
            {
                var result = Devices[0].StartFhssMode(setup);
                if (result)
                {
                    Mode = FpgaMode.Fhss;
                }
                return result;
            }
        }

        public bool StopFhssMode()
        {
            if (Mode == FpgaMode.Intelligence)
            {
                return true;
            }
            lock (_lockObject)
            {
                var result = Devices[0].StopFhssMode();
                if (result)
                {
                    Mode = FpgaMode.Intelligence;
                }
                return result;
            }
        }

        public void Close()
        {
            if (!_isInitialized)
            {
                return;
            }
            foreach (var device in _devices)
            {
                device.Close();
            }
            _isInitialized = false;
        }

        public int GetDriverVersion()
        {
            var version = Devices[0].GetDriverVersion();
            Contract.Assert(Devices.Skip(1).All(d => d.GetDriverVersion() == version), "Error! Fpga device driver versions are not the same.");

            return version;
        }

        public IFpgaDataScan GetScan(IReadOnlyList<int> bandNumbers)
        {
            lock (_lockObject)
            {
                if (!StartDevices())
                {
                    return null;
                }
                if (!WaitData())
                {
                    return null;
                }
                return ReadDataFromDevices(bandNumbers);
            }
        }

        private IFpgaDataScan ReadDataFromDevices(IReadOnlyList<int> bandNumbers)
        {
            var scan = new FpgaDataScan();
            foreach (var device in _devices)
            {
                if (!device.ReadData(scan, bandNumbers))
                {
                    return null;
                }
            }
            return scan;
        }

        /// <summary>
        /// Whole cycle of getting scan, but without parsing. Is needed for correct handle of receiver band changing. HACK
        /// </summary>
        public void GetBlankScan()
        {
            lock (_lockObject)
            {
                if (!StartDevices())
                {
                    return;
                }
                if (!WaitData())
                {
                    return;
                }
                foreach (var device in _devices)
                {
                    device.Read();
                }
            }
        }

        private void InitializeNetIfNeeded()
        {
            const string netInitFileName = "NetInit.txt";
            const int netInitPauseMs = 800;

            if (IsNetInitNeeded())
            {
                lock (_lockObject)
                {
                    if (!IsNetInitNeeded())
                    {
                        return;
                    }
                    Thread.Sleep(netInitPauseMs);

                    if (Devices[0].InitializeFpgaNet())
                    {
                        MessageLogger.Log("Fpga net initialized");
                    }
                    else
                    {
                        MessageLogger.Warning("Can't initialize fpga net");
                    }
                    SaveNetInitFile();

                    Thread.Sleep(netInitPauseMs);
                }
            }

            bool IsNetInitNeeded()
            {
                if (!File.Exists(netInitFileName))
                {
                    return true;
                }
                var fileText = File.ReadAllText(netInitFileName);
                if (DateTime.TryParse(fileText, out var lastNetInitTime))
                {
                    if (lastNetInitTime > DateTime.Now)
                    {
                        // something went wrong, maybe a calendar has been changed
                        // so let's suppose that net init is needed
                        return true;
                    }
                    // tricky way to find system boot time found on the internet
                    var systemWorkTimeSpan = TimeSpan.FromMilliseconds(Environment.TickCount);
                    var systemBootTime = DateTime.Now.Subtract(systemWorkTimeSpan);

                    return lastNetInitTime < systemBootTime;
                }
                return true;
            }

            void SaveNetInitFile()
            {
                File.WriteAllText(netInitFileName, DateTime.Now.ToString());
            }
        }

        private void OpenDevices()
        {
            foreach (var device in _devices)
            {
                device.Open();
            }
        }

        public bool Initialize()
        {
            MessageLogger.Trace("Device manager: initialization");

            if (_isInitialized)
            {
                return true;
            }
            OpenDevices();
            Thread.Sleep(10);
            if (!GetModuleOffsets(_devices[0].DeviceHandler))
            {
                return false;
            }
            foreach (var device in _devices)
            {
                if (!device.Initialize())
                {
                    return false;
                }
            }
            foreach (var device in _devices)
            {
                if (!device.InitFpga(ChannelMode.Intelligence, FftResolution.N16384))
                {
                    return false;
                }
                Thread.Sleep(10);
            }
            foreach (var device in _devices)
            {
                if (!device.InitFpgaTacting(TactingMode.External))
                {
                    return false;
                }
                Thread.Sleep(10);
            }
            _isInitialized = true;
            InitializeNetIfNeeded();
            return true;
        }

        private bool GetModuleOffsets(IntPtr hDev)
        {
            MessageLogger.Trace("Device manager: get module offsets");

            byte bar = 0;

            if (!DevApi.pGetPciConfig(hDev, out var config))
            {
                return false;
            }
            if (!DevApi.pEndianness(hDev, out var endianness))
            {
                return false;
            }
            if (!DevApi.pFindBarAndOffset(hDev, bar, (int)TypePt0.AxiCdma, 0, ref OffsetsSetup.AxiCdmaBar, ref OffsetsSetup.AxiCdmaOff))
            {
                OffsetsSetup.AxiCdmaBar = 0x0;
                OffsetsSetup.AxiCdmaOff = 0x40000;
            }
            if (!DevApi.pFindBarAndOffset(hDev, bar, (int)TypePt0.AxiPcieCtrl,
                (int)PcieCtrlE.PcieCtrlDev0Base, ref OffsetsSetup.AxiPcie0Bar, ref OffsetsSetup.AxiPcie0Off))
            {
                OffsetsSetup.AxiPcie0Bar = 0x0;
                OffsetsSetup.AxiPcie0Off = 0x0;
            }
            if (!DevApi.pFindBarAndOffset(hDev, bar, (int)TypePt0.AxiPcieCtrl,
                (int)PcieCtrlE.PcieCtrlDev1Base, ref OffsetsSetup.AxiPcie1Bar, ref OffsetsSetup.AxiPcie1Off))
            {
                OffsetsSetup.AxiPcie1Bar = 0x0;
                OffsetsSetup.AxiPcie1Off = 0x20000;
            }
            if (!DevApi.pFindAddr(hDev, bar, endianness, (int)TypePt0.AxiPcieCtrl,
                (int)PcieCtrlE.PcieCtrlDev0AxiBar0Offset, ref OffsetsSetup.AxiBar0D0Off))
            {
                OffsetsSetup.AxiBar0D0Off = 0x20000000;
            }
            if (!DevApi.pFindAddr(hDev, bar, endianness, (int)TypePt0.AxiPcieCtrl,
                (int)PcieCtrlE.PcieCtrlDev0AxiBar0Size, ref OffsetsSetup.AxiBar0D0Size))
            {
                OffsetsSetup.AxiBar0D0Size = 256 * 1024 * 1024;
            }
            if (!DevApi.pFindAddr(hDev, bar, endianness, (int)TypePt0.AxiPcieCtrl,
                (int)PcieCtrlE.PcieCtrlDev1AxiBar0Offset, ref OffsetsSetup.AxiBar0D1Off))
            {
                OffsetsSetup.AxiBar0D1Off = 0x20000000;
            }
            if (!DevApi.pFindAddr(hDev, bar, endianness, (int)TypePt0.AxiPcieCtrl,
                (int)PcieCtrlE.PcieCtrlDev1AxiBar0Size, ref OffsetsSetup.AxiBar0D1Size))
            {
                OffsetsSetup.AxiBar0D1Size = 256 * 1024 * 1024;
            }
            if (!DevApi.pFindBarAndOffset(hDev, bar, (int)TypePt0.InterfaceTbl, 0, ref OffsetsSetup.CmdTblBar, ref OffsetsSetup.CmdTblOff))
            {
                return false;
            }

            OffsetsSetup.Adc1Base = 0xC0000000;

            switch ((config.SubVendorID & 0xF00) >> 8)
            {
                case 0:
                    OffsetsSetup.AxiPcieBar = OffsetsSetup.AxiPcie0Bar;
                    OffsetsSetup.AxiPcieOff = OffsetsSetup.AxiPcie0Off;
                    OffsetsSetup.AxiBarOff = OffsetsSetup.AxiBar0D0Off;
                    OffsetsSetup.AxiBarSize = OffsetsSetup.AxiBar0D0Size;
                    break;
                case 1:
                    OffsetsSetup.AxiPcieBar = OffsetsSetup.AxiPcie1Bar;
                    OffsetsSetup.AxiPcieOff = OffsetsSetup.AxiPcie1Off;
                    OffsetsSetup.AxiBarOff = OffsetsSetup.AxiBar0D1Off;
                    OffsetsSetup.AxiBarSize = OffsetsSetup.AxiBar0D1Size;
                    break;
                default:
                    return false;
            }

            return true;
        }

    }
}

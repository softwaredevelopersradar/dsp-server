﻿using System;
using System.Collections.Generic;
using System.Linq;
using DspDataModel;
using DspDataModel.RadioJam;
using Settings;

namespace HardwareLibrary.RadioJam
{
    public class RadioJamShaperSimulator : IRadioJamShaper
    {
        private bool _isConnected;
        public const int DefaultLiterPower = 1000;

        public bool IsConnected
        {
            get => _isConnected;
            private set
            {
                _isConnected = value; 
                ConnectionStateChangedEvent?.Invoke(this, IsConnected);
            }
        }

        public event EventHandler<bool> ConnectionStateChangedEvent;
        public event EventHandler ShaperNotRespondingEvent;

        public bool Connect(string host, int port, string clientHost, int clientPort)
        {
            if (IsConnected)
            {
                return true;
            }
            IsConnected = true;
            return true;
        }

        public ShaperResponse StartFrsJamming(TimeSpan duration, IEnumerable<IRadioJamTarget> targets)
        {
            Contract.Assert(duration.TotalMilliseconds > 0);
            MessageLogger.ShaperLog($">> Start jamming: emition duration: {duration.TotalMilliseconds:F0}, targets count: {targets.Count()}");
            MessageLogger.ShaperLog($"<< Start jamming: error code: 0");
            return new ShaperResponse(0);
        }

        public ShaperResponse StartFhssJamming(uint fftCountCode, int duration, IEnumerable<IRadioJamFhssTarget> targets)
        {
            return new ShaperResponse(0);
        }

        public ShaperResponse StartFhssDurationMeasurement(int duration, IRadioJamFhssTarget target)
        {
            return new ShaperResponse(0);
        }

        public void Disconnect()
        {
            if (!IsConnected)
            {
                return;
            }
            IsConnected = false;
        }

        public ShaperResponse StopJamming()
        {
            MessageLogger.ShaperLog(">> Stop jamming");
            MessageLogger.ShaperLog("<< Stop jamming: error code: 0");
            return new ShaperResponse(0);
        }

        public ShaperResponse<int[]> GetPower()
        {
            MessageLogger.ShaperLog(">> Get power");
            MessageLogger.ShaperLog("<< Get power: error code: 0");
            return new ShaperResponse<int[]>(Enumerable.Repeat(DefaultLiterPower, Constants.LiterCount).ToArray(), 0);
        }

        public ShaperResponse<byte> GetState()
        {
            MessageLogger.ShaperLog(">> Get state");
            MessageLogger.ShaperLog("<< Get state: error code: 0");
            return new ShaperResponse<byte>(255,0);
        }
    }
}

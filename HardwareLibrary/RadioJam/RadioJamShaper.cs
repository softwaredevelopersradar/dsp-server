﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using DspDataModel;
using DspDataModel.RadioJam;
using NoiseShaper;
using Settings;
using ShaperCommandCodes = NoiseShaper.ModuleSHS.CodeogramCiphers;

namespace HardwareLibrary.RadioJam
{
    // be careful - all shaper's methods are not concurrent!
    public class RadioJamShaper : IRadioJamShaper
    {
        private readonly ModuleSHS _shaper;
        private readonly ManualResetEvent _commandResponseEvent;

        private byte[] _lastError;
        private object _lastResponse;
        private byte _lastCommandCode;

        private const int MaxResponseDelayMs = 500;//todo : to config

        private bool _isConnected;

        private RadioJamConfig _config;

        private readonly DecodingAllTypesError _errorDecoder;

        public bool IsConnected
        {
            get => _isConnected;
            private set
            {
                _isConnected = value;
                ConnectionStateChangedEvent?.Invoke(this, IsConnected);
            }
        }

        public event EventHandler<bool> ConnectionStateChangedEvent;
        public event EventHandler ShaperNotRespondingEvent;

        public RadioJamShaper() : this(Config.Instance.HardwareSettings.JammingConfig)
        { }

        public RadioJamShaper(RadioJamConfig config)
        {
            _config = config;
            _shaper = new ModuleSHS(config.ShaperErrorByteCount);
            _errorDecoder = new DecodingAllTypesError(config.ShaperErrorByteCount);
            _shaper.ReceiveCmd += CommandResponse;
            _commandResponseEvent = new ManualResetEvent(false);
        }

        private void CommandResponse(byte bCode, object obj)
        {
            _lastResponse = obj;
            _lastCommandCode = bCode;
            if (obj is TCodeErrorInform errorInform)
            {
                _lastError = errorInform.bCodeError;
            }
            _commandResponseEvent.Set();
        }

        public bool Connect(string host, int port, string clientHost, int clientPort)
        {
            if (IsConnected)
            {
                return true;
            }
            var result = _shaper.ConnectClient(host, clientHost, port, clientPort);
            // some magic numbers that supposed to be as they are, just don't touch it :(
            // feelsbadman.jpg
            _shaper.InitConst(4, 2);
            if (result)
            {
                IsConnected = true;
            }
            return result;
        }

        public void Disconnect()
        {
            if (!IsConnected)
            {
                return;
            }
            _shaper.DisconnectClient();
            IsConnected = false;
        }

        private int GetIntErrorCode(byte[] errorCode)
        {
            if (errorCode == null || errorCode.Length == 0)
                return -1;
            return errorCode.Last();
        }

        private string GetStringErrorCode(byte[] errorCode)
        {
            return errorCode.Aggregate("", (s, e) => s + ", " + e);
        }

        public ShaperResponse StartFrsJamming(TimeSpan duration, IEnumerable<IRadioJamTarget> targets)
        {
            Contract.Assert(duration.TotalMilliseconds > 0);
            var protocolTargets = targets.Select(t => new TOneSourceSupress
                {
                    bDeviation = t.DeviationCode,
                    bDuration = t.DurationCode,
                    bManipulation = t.ManipulationCode,
                    bModulation = t.ModulationCode,
                    uiFreq = (uint) (t.FrequencyKhz * 10)
                })
                .ToArray();

            var args = new TDurationParamFWS
            {
                bCount = (byte) protocolTargets.Length,
                iDuration = (int) duration.TotalMilliseconds,
                SourceSupress = protocolTargets
            };
            Contract.Assert(protocolTargets.Length > 0);

            if (!SecureCommandResponse(
                (StartFrsJammingDelegate)_shaper.SendDurationParamFWS, 
                ShaperCommandCodes.DURAT_PARAM_FWS, 
                new object[] { args }))
                return new ShaperResponse(-1);

            var intError = GetIntErrorCode(_lastError);
            if (_errorDecoder.IsNotThereError(_lastError))
            {
                intError = 0;
            }
            return new ShaperResponse(intError);
        }

        public ShaperResponse StartFhssJamming(uint fftCountCode, int duration, IEnumerable<IRadioJamFhssTarget> targets)
        {
            // mock while shaper doesn't support multi fhss request
            var target = targets.First();
            var frequency = (((int)target.MinFrequencyKhz / 1000) * 1000 - 10000);
            var args = new TDurationParamFHSS
            {
                bCodeFFT = (byte) fftCountCode,
                bDeviation = target.DeviationCode,
                bManipulation = target.ManipulationCode,
                bModulation = target.ModulationCode,
                iFreqMin = frequency,
                iDuration = duration
            };

            var isCommandProcessed = SecureCommandResponse(
                (StartFhssJammingDelegate)_shaper.SendParamFHSS,
                ShaperCommandCodes.PARAM_FHSS,
                new object[] { args });
            return isCommandProcessed ? new ShaperResponse(GetIntErrorCode(_lastError)) : new ShaperResponse(-1);
        }

        public ShaperResponse StartFhssDurationMeasurement(int duration, IRadioJamFhssTarget target)
        {
            // mock while shaper doesn't support multi fhss request
            var frequency = (int)(((int)target.MinFrequencyKhz / 1000) * 1000 - 10000);
            var args = new DurationMeasurement()
            {
                iFreqMin = frequency,
                iCycleDuration = Config.Instance.FhssSearchSettings.FhssDurationMeasureCycleDuration,//TODO : remove from config?
                iRadiationDuration = duration
            };

            var isCommandProcessed = SecureCommandResponse(
                (StartFhssDurationMeasurementDelegate)_shaper.SendFHSSParamDur,
                ShaperCommandCodes.SET_FHSS_PARAMS_DURAT,
                new object[] { args });
            return isCommandProcessed ? new ShaperResponse(GetIntErrorCode(_lastError)) : new ShaperResponse(-1);
        }

        public ShaperResponse StopJamming()
        {
            var isCommandProcessed = SecureCommandResponse((NoArgumentsDelegate) _shaper.SendRadiatOff, ShaperCommandCodes.RADIAT_OFF);
            return isCommandProcessed ? new ShaperResponse(GetIntErrorCode(_lastError)) : new ShaperResponse(-1);
        }

        public ShaperResponse<int[]> GetPower()
        {
            if (!SecureCommandResponse(
                (GetPowerDelegate)_shaper.SendRequestPower, 
                ShaperCommandCodes.REQUEST_POWER, 
                new object[]{(byte)0}))
                return new ShaperResponse<int[]>(null, -1);
            
            if (!_errorDecoder.IsNotThereError(_lastError))
            {
                return new ShaperResponse<int[]>(null, GetIntErrorCode(_lastError));
            }

            var result = ((TCodeErrorInform)_lastResponse).bInform
                .Select(power => power * 10)
                .ToArray();
            return new ShaperResponse<int[]>(result, GetIntErrorCode(_lastError));
        }

        public ShaperResponse<byte> GetState()
        {
            if (!SecureCommandResponse((NoArgumentsDelegate)_shaper.SendFPSState, ShaperCommandCodes.REQUEST_FPS_STATE))
                return new ShaperResponse<byte>(255, -1);

            try
            {
                var response = (TCodeErrorInform) _lastResponse;
                var result = response.bInform.Last();
                return new ShaperResponse<byte>(result, GetIntErrorCode(_lastError));
            }
            catch(Exception e)
            {
                MessageLogger.Error("Error during Get state command : " + e);
            }
            return new ShaperResponse<byte>(255, -1);
            /* todo : test this with shaper
                var isCommandProcessed = SecureCommandResponse((NoArgumentsDelegate) _shaper.SendFPSState, ShaperCommandCodes.REQUEST_FPS_STATE);
                var result = ((TCodeErrorInform)_lastResponse).bInform?.Last();
                return 
                    result != null && result.HasValue && isCommandProcessed ? 
                    new ShaperResponse<byte>(result.Value, GetIntErrorCode(_lastError)) : 
                    new ShaperResponse<byte>(255, -1);
             */
        }

        private bool SecureCommandResponse(Delegate command, ShaperCommandCodes code) => SecureCommandResponse(command, code, new object[] { });

        private bool SecureCommandResponse(Delegate command, ShaperCommandCodes code, object[] args)
        {
            var commandCounter = _config.SecureCommandResponseCount;
            while (commandCounter > 0)
            {
                if (MessageLogger.IsShaperLogEnabled)
                {
                    MessageLogger.ShaperLog($">> {command.Method.Name}");
                }
                command.Method.Invoke(_shaper, args);
                if (_commandResponseEvent.WaitOne(MaxResponseDelayMs))
                {
                    if (_lastCommandCode != (byte) code)
                    {
                        commandCounter--;
                        ResetCommandResponseEvent();
                        continue; // reply has the wrong command code
                    }
                    ResetCommandResponseEvent();
                    if (MessageLogger.IsShaperLogEnabled)
                        MessageLogger.ShaperLog($"<< {command.Method.Name}: error code: {GetStringErrorCode(_lastError)}");
                    return true;
                }
                commandCounter--;
            }
            MessageLogger.Error($"Shaper is not responding to {command.Method.Name}");
            MessageLogger.ShaperLog($"<< Shaper is not responding to {command.Method.Name}");
            ShaperNotRespondingEvent?.Invoke(this, EventArgs.Empty);
            return false;

            void ResetCommandResponseEvent()
            {
                _commandResponseEvent.Reset();
                _lastCommandCode = 0;
            }
        }
    }
}

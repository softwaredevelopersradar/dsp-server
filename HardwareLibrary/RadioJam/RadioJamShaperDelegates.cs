﻿using NoiseShaper;

namespace HardwareLibrary.RadioJam
{
    public delegate bool StartFrsJammingDelegate(TDurationParamFWS args);
    public delegate bool StartFhssJammingDelegate(TDurationParamFHSS args);
    public delegate bool NoArgumentsDelegate();
    public delegate bool StartFhssDurationMeasurementDelegate(DurationMeasurement args);
    public delegate bool GetPowerDelegate(byte args);
}

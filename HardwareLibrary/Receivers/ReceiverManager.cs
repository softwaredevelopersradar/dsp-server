﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using DspDataModel;
using DspDataModel.Hardware;
using HardwareLibrary.TransmitionChannels;
using System.IO;
using ReceiversMessages;
using Settings;
using GetTemperatureRequest = ReceiversMessages.GetTemperatureRequest;
using GetTemperatureResponse = ReceiversMessages.GetTemperatureResponse;
using SetSynchronizationShiftRequest = ReceiversMessages.SetSynchronizationShiftRequest;

namespace HardwareLibrary.Receivers
{
    public class ReceiverManager : IReceiverManager
    {
        public ITransmissionChannel DataChannel { get; }

        public IReadOnlyList<IReceiver> Receivers => _receivers;

        public ReceiverChannel Channel { get; private set; }

        private readonly IReceiver[] _receivers;

        public FpgaDeviceBandsSettingsSource BandsSettingsSource => _deviceManager.BandsSettingsSource;

        public bool IsNoiseGeneratorEnabled => Channel == ReceiverChannel.NoiseGenerator;

        public bool IsExternalCalibrationEnabled => Channel == ReceiverChannel.ExternalCalibration;

        private readonly IFpgaDeviceManager _deviceManager;

        private bool _isBandChanged = false;

        private bool _attenuatorValuesLoaded = false;

        private const int PauseMs = 1;

        private const bool CheckReceiverResponses = true;

        private readonly object _lockObject = new object();

        public ReceiverManager(IFpgaDeviceManager deviceManager, ITransmissionChannel dataChannel)
        {
            Channel = ReceiverChannel.NoiseGenerator; // little hack to prevent stopping of initializing noise generator
            // after initialization Channel is to changed to Air state
            DataChannel = dataChannel;
            _receivers = new IReceiver[Constants.ReceiversCount];
            _deviceManager = deviceManager;
            CreateReceivers();
        }

        private void CreateReceivers()
        {
            for (int i = 0; i < _receivers.Length; ++i)
            {
                _receivers[i] = new Receiver(number: i, channel: DataChannel);
            }
        }

        public bool Initialize()
        {
            MessageLogger.Trace("Receiver manager: initialization");

            if (!DataChannel.Connect(
                Config.Instance.HardwareSettings.ReceiverComPort,
                Config.Instance.HardwareSettings.ReceiverBaudRate))
            {
                return false;
            }
            SetCycleLength(Config.Instance.HardwareSettings.TactingCycleLengthMs);
            CollectFpgaDeviceBandsInformation();
            ResetReceiversToDefault();
            SetSynchronizationShift(Config.Instance.HardwareSettings.CycleSynchronizationShift);
            LoadAttenuatorValues();
            return true;
        }

        private void GetBlankScanIfNeeded()
        {
            if (_isBandChanged)
            {
                _deviceManager.GetBlankScan();
            }
        }

        public bool SetBandNumbers(int bandNumber)
        {
            return SetBandNumbers(Enumerable.Repeat(bandNumber, Constants.ReceiversCount).ToArray());
        }

        /// <summary>
        /// band numbers for all receivers (there should be 6 numbers)
        /// if i-receiver is not used, -1 should be placed to corresponding array item
        /// </summary>
        public bool SetBandNumbers(IReadOnlyList<int> bandNumbers)
        {
            Contract.Assert(bandNumbers.Count == Constants.ReceiversCount);
            try
            {
                if (CanSkipBandNumbersUpdate(bandNumbers))
                {
                    return true;
                }
                lock (_lockObject)
                {
                    _isBandChanged = false;
                    if (AreDfReceiversHasSameBandNumber(bandNumbers))
                    {
                        SetDfReceiversBandNumber(bandNumbers[0]);
                        Micropause(PauseMs);
                        SetBandNumbersInner(bandNumbers, startIndex: Constants.DfReceiversCount);
                    }
                    else if (AreDfReceiversPlacedInSequence(bandNumbers))
                    {
                        SetDfReceiversInSequence(bandNumbers);
                        SetBandNumbersInner(bandNumbers, startIndex: Constants.DfReceiversCount);
                    }
                    else
                    {
                        SetBandNumbersInner(bandNumbers, startIndex: 0);
                    }
                }
                GetBlankScanIfNeeded();
                return true;
            }
            catch (Exception ex)
            {
                MessageLogger.Error(ex, "Can't set band numbers");
                return false;
            }
        }

        private void SetBandNumbersInner(IReadOnlyList<int> bandNumbers, int startIndex)
        {
            for (var i = startIndex; i < Constants.ReceiversCount; ++i)
            {
                if (bandNumbers[i] != -1)
                {
                    var isBandNumberChanged = Receivers[i].BandNumber != bandNumbers[i];
                    Receivers[i].SetBandNumber(bandNumbers[i]);
                    if (isBandNumberChanged)
                    {
                        Micropause(PauseMs);
                        _isBandChanged = true;
                    }
                }
            }
        }

        private static bool AreDfReceiversHasSameBandNumber(IReadOnlyList<int> bandNumbers)
        {
            var bandNumber0 = bandNumbers[0];
            for (var i = 1; i < Constants.DfReceiversCount; ++i)
            {
                if (bandNumbers[i] != -1 && bandNumbers[i] != bandNumber0)
                {
                    return false;
                }
            }
            return true;
        }

        private bool CanSkipBandNumbersUpdate(IReadOnlyList<int> bandNumbers)
        {
            for (var i = 0; i < Constants.ReceiversCount; ++i)//todo 1 -> 0 ?_?_?_
            {
                if (bandNumbers[i] != -1 && bandNumbers[i] != Receivers[i].BandNumber)
                {
                    return false;
                }
            }
            return true;
        }

        private static bool AreDfReceiversPlacedInSequence(IReadOnlyList<int> bandNumbers)
        {
            for (var i = 1; i < Constants.DfReceiversCount; ++i)
            {
                if (bandNumbers[i] != -1 && bandNumbers[i] != bandNumbers[i - 1] + 1)
                {
                    return false;
                }
            }
            return true;
        }

        private bool CanSkipUpdatingDfReceiversBandNumber(int bandNumber)
        {
            for (var i = 0; i < Constants.DfReceiversCount; ++i)
            {
                if (Receivers[i].BandNumber != bandNumber)
                {
                    return false;
                }
            }
            return true;
        }

        private bool CanSkipUpdatingDfReceiversBandNumber(IReadOnlyList<int> bandNumbers)
        {
            for (var i = 0; i < Constants.DfReceiversCount; ++i)
            {
                if (bandNumbers[i] != -1 && Receivers[i].BandNumber != bandNumbers[i])
                {
                    return false;
                }
            }
            return true;
        }

        private void SetDfReceiversInSequence(IReadOnlyList<int> bandNumbers)
        {
            if (CanSkipUpdatingDfReceiversBandNumber(bandNumbers))
            {
                return;
            }

            var message = SetBandRequest.ToBinary(0x0F, (byte)bandNumbers[0]);
            var response = DataChannel.SendReceive<SetBandResponse>(message, SetBandResponse.BinarySize);
#if DEBUG
            if (CheckReceiverResponses && !(DataChannel is FakeTransmitionChannel))
            {
                if (bandNumbers[0] != response.BandNumber)
                {
                    MessageLogger.Error($"SetDfReceiversInSequence response error: band number should be {bandNumbers[0]} but actually is {response.BandNumber}");
                }
            }
#endif
            _isBandChanged = true;

            for (var i = 0; i < Constants.DfReceiversCount; ++i)
            {
                Receivers[i].BandNumber = bandNumbers[i];
            }
            Micropause(PauseMs);
        }

        private void SetDfReceiversBandNumber(int bandNumber)
        {
            if (CanSkipUpdatingDfReceiversBandNumber(bandNumber))
            {
                return;
            }
            SetDfReceiversBandNumbersInner(bandNumber);

            for (var i = 0; i < Constants.DfReceiversCount; ++i)
            {
                Receivers[i].BandNumber = bandNumber;
            }
        }

        private SetBandResponse SetDfReceiversBandNumbersInner(int bandNumber)
        {
            var message = SetBandRequest.ToBinary(0, (byte)bandNumber);
            var result = DataChannel.SendReceive<SetBandResponse>(message, SetBandResponse.BinarySize);
            _isBandChanged = true;
            Micropause(PauseMs);
            return result;
        }

        public void SetReceiversChannel(ReceiverChannel channel)
        {
            MessageLogger.Trace("Receiver manager: set receivers channel " + channel);

            if (Channel == channel)
            {
                return;
            }
            Channel = channel;

            SetReceiversChannel((byte) channel);
        }

        private void SetReceiversChannel(byte state)
        {
            var message = SetNoiseGeneratorEnabledRequest.ToBinary(state);
            var response = DataChannel.SendReceive<SetNoiseGeneratorEnabledResponse>(message, SetNoiseGeneratorEnabledResponse.BinarySize);
#if DEBUG
            if (CheckReceiverResponses && !(DataChannel is FakeTransmitionChannel))
            {
                if (state != response.IsEnabled)
                {
                    MessageLogger.Error($"Set receivers channel response error: state byte should be {state} but actually is {response.IsEnabled}");
                }
            }
#endif
            _isBandChanged = true;
            Micropause(PauseMs);
        }

        private static FpgaDeviceBandSettings GetBandSettings(SetBandResponse response)
        {
            var isConstantAttenuatorEnabled = ((response.StateByte >> 2) & 1) == 1;
            var shouldBeReversed = (((response.StateByte >> 3) & 1) ^
                                    (response.StateByte & 1)) != 1;
            if (response.BandNumber > 99)
            {
                shouldBeReversed = !shouldBeReversed;
            }
            var amplifierGain = 0.147075f * response.AmplifierValue - 13.75f;

            return new FpgaDeviceBandSettings(amplifierGain, response.AttenuatorValue * 0.5f, isConstantAttenuatorEnabled, shouldBeReversed);
        }

        public void CollectFpgaDeviceBandsInformation()
        {
            MessageLogger.Trace("Receiver manager: collect fpga bands information");

            var result = new FpgaDeviceBandSettings[Config.Instance.BandSettings.BandCount];
            for (var i = 0; i < Config.Instance.BandSettings.BandCount; ++i)
            {
                var response = SetDfReceiversBandNumbersInner(i);
                result[i] = GetBandSettings(response);
            }
            // sometimes there are wrong results with first band, so let's just get it's settings one more time
            var firstBandResponse = SetDfReceiversBandNumbersInner(0);
            result[0] = GetBandSettings(firstBandResponse);

            BandsSettingsSource.Settings = result;
        }
        //TODO : find a way to remove IO from RecMan
        private void SaveAttenuatorValues()
        {
            if (File.Exists(Config.Instance.HardwareSettings.AttenuatorsFilename))
               File.Delete(Config.Instance.HardwareSettings.AttenuatorsFilename);

            for (byte i = 0; i < BandsSettingsSource.Settings.Count; i++)
            {
                if (BandsSettingsSource.Settings[i].IsConstantAttenuatorEnabled == false &&
                    BandsSettingsSource.Settings[i].AttenuatorLevel == 0)
                    continue;
                File.AppendAllLines(
                    Config.Instance.HardwareSettings.AttenuatorsFilename,
                    new List<string>()
                    {
                        $"{i} {BandsSettingsSource.Settings[i].IsConstantAttenuatorEnabled} {BandsSettingsSource.Settings[i].AttenuatorLevel}" // ' ' is a separator
                    });
            }
        }

        private void LoadAttenuatorValues()
        {
            if (File.Exists(Config.Instance.HardwareSettings.AttenuatorsFilename) == false)
            {
                _attenuatorValuesLoaded = true;
                return;
            }

            var lines = File.ReadLines(Config.Instance.HardwareSettings.AttenuatorsFilename);
            foreach (var line in lines)
            {
                var args = line.Split(' ');
                var bandNumber = Int32.Parse(args[0]);
                var attenuatorValue = args[2].Replace('.', ',');
                if (bandNumber > BandsSettingsSource.Settings.Count)
                    continue;
                SetAttenuatorValue(bandNumber, Boolean.Parse(args[1]), Single.Parse(attenuatorValue));
            }
            _attenuatorValuesLoaded = true;
        }

        private void Micropause(int milliseconds)
        {
            Thread.Sleep(milliseconds);
        }

        public void ResetReceiversToDefault()
        {
            SetBandNumbers(0);
            SetReceiversChannel(ReceiverChannel.Air);
        }

        public bool SetAttenuatorValue(int bandNumber, bool isConstAttenuatorEnabled, float value)
        {
            lock (_lockObject)
            {
                SetBandNumbers(bandNumber);
                Micropause(PauseMs);

                SetAttenuator(value);
                BandsSettingsSource.Settings[bandNumber].AttenuatorLevel = value;

                SetConstAttenuator(isConstAttenuatorEnabled);
                BandsSettingsSource.Settings[bandNumber].IsConstantAttenuatorEnabled = isConstAttenuatorEnabled;

                if (_attenuatorValuesLoaded)
                    SaveAttenuatorValues();
            }
            return true;
        }

        public bool SetAttenuatorValue(int bandNumber, float value)
        {
            lock (_lockObject)
            {
                SetBandNumbers(bandNumber);
                Micropause(PauseMs);

                SetAttenuator(value);
                BandsSettingsSource.Settings[bandNumber].AttenuatorLevel = value;

                if (_attenuatorValuesLoaded)
                    SaveAttenuatorValues();
            }
            return true;
        }

        public bool SetConstAttenuatorValue(int bandNumber, bool isConstAttenuatorEnabled)
        {
            lock (_lockObject)
            {
                SetBandNumbers(bandNumber);
                Micropause(PauseMs);

                SetConstAttenuator(isConstAttenuatorEnabled);
                BandsSettingsSource.Settings[bandNumber].IsConstantAttenuatorEnabled = isConstAttenuatorEnabled;

                if(_attenuatorValuesLoaded)
                    SaveAttenuatorValues();
            }
            return true;
        }

        private void SetAttenuator(float value)
        {
            var byteValue = (byte)(value * 2);
            if (byteValue >= 64)
            {
                MessageLogger.Warning($"Can't set attenuator value {value}");
                return;
            }

            // setting attenuator for df receivers
            var message = SetAttenuatorValueRequest.ToBinary(0, byteValue);
            var response = DataChannel.SendReceive<SetAttenuatorValueResponse>(message, SetAttenuatorValueResponse.BinarySize);
#if DEBUG
            if (CheckReceiverResponses && !(DataChannel is FakeTransmitionChannel))
            {
                if (response.Value >= 64)
                {
                    MessageLogger.Warning($"Set attenuator response error: got attenuator value {value * 0.5f}");
                }
                if (byteValue != response.Value)
                {
                    MessageLogger.Warning($"Set attenuator response error: byte value is {response.Value} but it should be {byteValue}");
                }
                if (0 != response.ReceiverNumber)
                {
                    MessageLogger.Warning($"Set attenuator response error: receiver number should be 0 but actually is {response.ReceiverNumber}");
                }
            }
#endif
            Micropause(PauseMs);
            // setting attenuator for rc receiver
            message = SetAttenuatorValueRequest.ToBinary(6, byteValue);
            response = DataChannel.SendReceive<SetAttenuatorValueResponse>(message, SetAttenuatorValueResponse.BinarySize);
#if DEBUG
            if (CheckReceiverResponses && !(DataChannel is FakeTransmitionChannel))
            {
                if (response.Value >= 64)
                {
                    MessageLogger.Warning($"Set attenuator response error: got attenuator value {value * 0.5f}");
                }
                if (byteValue != response.Value)
                {
                    MessageLogger.Warning($"Set attenuator response error: byte value is {response.Value} but it should be {byteValue}");
                }
                if (6 != response.ReceiverNumber)
                {
                    MessageLogger.Warning($"Set attenuator response error: receiver number should be 6 but actually is {response.ReceiverNumber}");
                }
            }
#endif
            Micropause(PauseMs);
        }

        private void SetConstAttenuator(bool isConstAttenuatorEnabled)
        {
            var value = (byte)(isConstAttenuatorEnabled ? 1 : 0);

            // setting const attenuator for df receivers
            var message = SetConstAttenuatorEnabledRequest.ToBinary(0, value);
            var response = DataChannel.SendReceive<SetConstAttenuatorEnabledResponse>(message, SetConstAttenuatorEnabledResponse.BinarySize);
#if DEBUG
            if (CheckReceiverResponses && !(DataChannel is FakeTransmitionChannel))
            {
                if (value != response.IsEnabled)
                {
                    MessageLogger.Warning($"Set const attenuator response error: is enabled flag is in wrong state");
                }
                if (0 != response.ReceiverNumber)
                {
                    MessageLogger.Warning($"Set const attenuator response error: receiver number should be 0 but actually is {response.ReceiverNumber}");
                }
            }
#endif
            Micropause(PauseMs);
            // setting const attenuator for rc receiver
            message = SetConstAttenuatorEnabledRequest.ToBinary(6, value);
            response = DataChannel.SendReceive<SetConstAttenuatorEnabledResponse>(message, SetConstAttenuatorEnabledResponse.BinarySize);
#if DEBUG
            if (CheckReceiverResponses && !(DataChannel is FakeTransmitionChannel))
            {
                if (value != response.IsEnabled)
                {
                    MessageLogger.Warning($"Set const attenuator response error: is enabled flag is in wrong state");
                }
                if (6 != response.ReceiverNumber)
                {
                    MessageLogger.Warning($"Set const attenuator response error: receiver number should be 6 but actually is {response.ReceiverNumber}");
                }
            }
#endif
            Micropause(PauseMs);
        }

        public void SetCycleLength(int shift)
        {
            lock (_lockObject)
            {
                MessageLogger.Trace("Receiver manager: set cycle length");

                var message = SetCycleSynchronizationRequest.ToBinary((short) shift);
                var response = DataChannel.SendReceive<SetCycleSynchronizationResponse>(message, SetCycleSynchronizationResponse.BinarySize);

                // I don't know why but after this command receivers don't response correct without little pause
                Micropause(20);
            }
        }

        public void SetSynchronizationShift(int shift)
        {
            lock (_lockObject)
            {
                MessageLogger.Trace("Receiver manager: set synchronization shift");

                var message = SetSynchronizationShiftRequest.ToBinary((short) shift);
                var response = DataChannel.SendReceive<SetSynchronizationShiftResponse>(message, SetSynchronizationShiftResponse.BinarySize);
                Micropause(PauseMs);
            }
        }

        public short GetAmplitudeLevel(int receiverNumber)
        {
            lock (_lockObject)
            {
                MessageLogger.Trace("Receiver manager: get amplitude level");

                var message = GetAmplitudeLevelRequest.ToBinary((byte) receiverNumber);
                var response = DataChannel.SendReceive<GetAmplitudeLevelResponse>(message, GetAmplitudeLevelResponse.BinarySize);
                Micropause(PauseMs);
                return response.AmplitudeLevel;
            }
        }

        public short GetTemperature(int receiverNumber)
        {
            lock (_lockObject)
            {
                MessageLogger.Trace("Receiver manager: get temperature");

                var message = GetTemperatureRequest.ToBinary((byte) receiverNumber);
                var response = DataChannel.SendReceive<GetTemperatureResponse>(message, GetTemperatureResponse.BinarySize);
                Micropause(PauseMs);
                return response.Temperature;
            }
        }
    }
}

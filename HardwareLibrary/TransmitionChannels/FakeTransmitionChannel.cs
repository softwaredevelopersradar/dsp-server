﻿using System;
using System.Collections.Generic;
using DspDataModel.Hardware;
using NetLib;
using SimulatorProtocols;

namespace HardwareLibrary.TransmitionChannels
{
    public class FakeTransmitionChannel : ITransmissionChannel
    {
        private readonly NetClientSlim _client;

        public bool IsConnected { get; private set; }

        public FakeTransmitionChannel(NetClientSlim client)
        {
            _client = client;
            IsConnected = false;
        }

        public bool Connect(params object[] args)
        {
            IsConnected = true;
            return true;
        }

        public void Disconnect()
        {
            IsConnected = false;
        }

        public void Write(byte[] buffer, int offset, int count)
        {
            // looking for set receivers channel command and transmitting it to the simulator
            if (ReceiversMessages.SetNoiseGeneratorEnabledRequest.IsValid(buffer))
            {
                var receiversRequest = ReceiversMessages.SetNoiseGeneratorEnabledRequest.Parse(buffer);
                var message = SetReceiversChannel.ToBinary((ReceiverChannel) receiversRequest.IsEnabled);
                if (_client?.IsWorking ?? false)
                {
                    _client.Write(message);
                }
            }
        }

        public int Read(byte[] buffer, int offset, int count)
        {
            return count;
        }

        public event EventHandler<IReadOnlyList<byte>> OnDataRead;
        public int Timeout { get; set; }
    }
}

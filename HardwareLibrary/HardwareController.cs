﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;
using DspDataModel;
using DspDataModel.Data;
using DspDataModel.DataProcessor;
using DspDataModel.Hardware;
using HardwareLibrary.FpgaDevices;
using HardwareLibrary.Receivers;
using HardwareLibrary.SignalGenerators;
using HardwareLibrary.TransmitionChannels;
using NetLib;

namespace HardwareLibrary
{
    public class HardwareController : IHardwareController
    {
        public IReceiverManager ReceiverManager { get; }
        public IFpgaDeviceManager DeviceManager { get; }
        public ISignalGenerator SignalGenerator { get; }
        public float ScanReadTimeMs { get; private set; }

        private readonly NetClientSlim _simulatorClient;

        public IReadOnlyList<FpgaDeviceBandSettings> FpgaDeviceBandsSettings => DeviceManager.BandsSettingsSource.Settings;

        public event EventHandler<IFpgaDataScan> ScanReadEvent;

        private readonly Config _config;

        private readonly Stopwatch _stopwatch;

        /// <summary>
        /// Mock
        /// </summary>
        public HardwareController() : this(new DataProcessorMock())
        { }

        public HardwareController(IDataProcessor dataProcessor)
            : this(dataProcessor, Config.Instance)
        { }

        public HardwareController(IDataProcessor dataProcessor, Config config)
        {
            _config = config;
            _stopwatch = new Stopwatch();
            dataProcessor.ScanReadTimer = this;
            if (config.HardwareSettings.SimulateFpgaDevices)
            {
                _simulatorClient = new NetClientSlim();
                DeviceManager = new FakeFpgaDeviceManager(dataProcessor, _simulatorClient, _config);
                ReceiverManager = new ReceiverManager(DeviceManager, DeviceManager.GetFpgaTransmissionChannel());
                SignalGenerator = new SignalGeneratorSimulator(_simulatorClient);
            }
            else
            {
                DeviceManager = new FpgaDeviceManager();
                var transmitionChannel = _config.HardwareSettings.UseRs232TransmitionChannel 
                    ? new Rs232TransmitionChannel()
                    : DeviceManager.GetFpgaTransmissionChannel();
                MessageLogger.Log("Receivers connected with "
                                  + (_config.HardwareSettings.UseRs232TransmitionChannel ? "RS232" : "FPGA"));
                ReceiverManager = new ReceiverManager(DeviceManager, transmitionChannel);

                var generatorType = _config.CalibrationSettings.SignalGeneratorSettings.Type;
                SignalGenerator = (generatorType == SignalGeneratorType.Simulation)
                    ? (ISignalGenerator) new SignalGeneratorSimulator()
                    : new SignalGenerator(generatorType);
            }
        }

        public bool Initialize()
        {
            if (!DeviceManager.Initialize())
            {
                MessageLogger.Error("Can't initialize fpga devices");
                return false;
            }
            if (!_config.HardwareSettings.SimulateFpgaDevices)
            {
                MessageLogger.Log($"Fpga devices initialized, driver version {DeviceManager.GetDriverVersion()}");
            }
            if (!ReceiverManager.Initialize())
            {
                MessageLogger.Error("Can't initialize receivers");
                return false;
            }
            CheckSignalGenerator();
            return true;
        }

        private async Task CheckSignalGenerator()
        {
            await InitializeSignalGeneratorAsync();
            SignalGenerator.Close();
        }

        private async Task InitializeSignalGeneratorAsync()
        {
            MessageLogger.Trace("Hardware controller: initialize signal generator");
            var result = await SignalGenerator.Initialize(_config.CalibrationSettings.SignalGeneratorSettings.Host);
            if (!result)
            {
                MessageLogger.Warning("Can't initialize signal generator");
            }
            else if (!(SignalGenerator is SignalGeneratorSimulator))
            {
                MessageLogger.Log(_config.CalibrationSettings.SignalGeneratorSettings.Type + " signal generator initialized");
            }
        }

        private void UpdateScanReadTime()
        {
            if (ScanReadTimeMs == 0)
            {
                ScanReadTimeMs = _stopwatch.ElapsedMilliseconds;
            }
            else
            {
                const float currentScanSpeedFactor = 0.3f;
                ScanReadTimeMs = ScanReadTimeMs * (1 - currentScanSpeedFactor) +
                                 _stopwatch.ElapsedMilliseconds * currentScanSpeedFactor;
            }
        }

        public IFpgaDataScan GetScan(IReadOnlyList<int> bandNumbers)
        {
            if (!ReceiverManager.SetBandNumbers(bandNumbers))
            {
                return null;
            }
            _stopwatch.Restart();
            var data = DeviceManager.GetScan(bandNumbers);
            ScanReadEvent?.Invoke(this, data);
            _stopwatch.Stop();
            UpdateScanReadTime();
            return data;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using Settings;
using Correlator;
using DllDefineModulation;
using DspDataModel;
using DspDataModel.Correlator;
using DspDataModel.Data;
using DspDataModel.DataProcessor;
using SharpExtensions;

namespace DataProcessor
{
    public class DataProcessor : IDataProcessor
    {
        private readonly PhaseCorrelator _correlator;

        public DataProcessor()
            : this(Config.Instance.CalibrationSettings.RadioPathTableFilename)
        {
        }

        public DataProcessor(string radioPathTableFilename)
        {
            _correlator = new PhaseCorrelator(radioPathTableFilename);
        }

        public void ReloadRadioPathTable(string radioPathTableFilename)
        {
            _correlator.ReloadRadioPathTable(radioPathTableFilename);
        }

        public void SetCorrelationType(int type)
        {
            _correlator.CorrelationProviderType = (PhaseCorrelator.CorrelationType) type;
        }

        public IPhaseCorrelator PhaseCorrelator => _correlator;

        /// <summary>
        /// Could be null
        /// </summary>
        public IScanReadTimer ScanReadTimer { get; set; }

        public ProcessResult GetSignals(IDataScan dataScan, ScanProcessConfig config)
        {
            return config.UseDirectionFinding
                ? GetSignalsWithDirectionFinding(dataScan, config)
                : GetSignals(dataScan, config.Threshold, config.FrequencyRanges);
        }

        /// <summary>
        /// Get signals without direction finding
        /// </summary>
        private ProcessResult GetSignals(IDataScan dataScan, float threshold, IReadOnlyList<FrequencyRange> frequencyRanges)
        {
            var signalsRanges = GetSignalRanges(dataScan, threshold, frequencyRanges);
            var signals = new List<ISignal>();

            foreach (var range in signalsRanges)
            {
                GetAmplitudeAndFrequency(dataScan, range, out var amplitude, out var frequency);
                var centralFrequencyKhz = Utilities.GetFrequencyKhz(dataScan.BandNumber, range.CentralPointNumber);
                var modulation = GetSignalModulation(dataScan, threshold, range);
                var (broadcastTimeSpan, subScanCount) = GetSignalEmitDuration(dataScan, range, threshold);

                var signal = new Signal(
                    frequencyKhz: frequency,
                    centralFrequencyKhz: centralFrequencyKhz,
                    direction: 0,
                    reliability: 0,
                    bandwidthKhz: CalculateBandwidth(dataScan, threshold, range),
                    amplitude: amplitude,
                    disardedDirectionsPart: 0,
                    standardDeviation: 0,
                    phases: null,
                    relativeSubScanCount: subScanCount,
                    modulation: modulation,
                    phaseDeviation: 0,
                    broadcastTimeSpan: broadcastTimeSpan);

                signals.Add(signal);
            }

            return new ProcessResult(signals);
        }

        private static float CalculateBandwidth(IAmplitudeBand dataBand, float threshold, Range range)
        {
            float leftProportion = 0f, rightProportion = 0f;
            var spectrum = dataBand.Amplitudes;

            if (range.From != 0)
            {
                if (Math.Abs(spectrum[range.From] - spectrum[range.From - 1]) < 1e-3f)
                {
                    leftProportion = 0.5f;
                }
                else
                {
                    leftProportion = 1 - (threshold - spectrum[range.From - 1]) / (spectrum[range.From] - spectrum[range.From - 1]);
                }
            }
            if (range.To != dataBand.Count - 1)
            {
                if (Math.Abs(spectrum[range.To + 1] - spectrum[range.To]) < 1e-3f)
                {
                    rightProportion = 0.5f;
                }
                else
                {
                    rightProportion = (threshold - spectrum[range.To]) / (spectrum[range.To + 1] - spectrum[range.To]);
                }
            }
            var bandwidth = (range.To - range.From + leftProportion + rightProportion) * Constants.SamplesGapKhz;
            if (bandwidth < 1e-3f)
            {
                bandwidth = 1;
            }
            Contract.Assert(bandwidth > 0 && !float.IsNaN(bandwidth));
            return bandwidth;
        }

        private ProcessResult GetSignalsWithDirectionFinding(IDataScan dataScan, ScanProcessConfig config)
        {
            var threshold = config.Threshold;
            var directionAveragingCount = config.DirectionAveragingCount;
            var scanAveragingCount = config.ScanAveragingCount;
            var calculateHighQualityPhases = config.CalculateHighQualityPhases;

            var subScans = new IDataScan[directionAveragingCount];
            var signals = new List<ISignal>();

            for (var i = 0; i < directionAveragingCount; ++i)
            {
                subScans[i] = dataScan.SubScan(scanAveragingCount * i, scanAveragingCount);
            }

            foreach (var scan in subScans)
            {
                signals.AddRange(GetSignalsFromSubScan(scan, threshold, config.FrequencyRanges));
            }
            signals.Sort((s1, s2) => s1.CentralFrequencyKhz.CompareTo(s2.CentralFrequencyKhz));
            var mergedSignals = Signal.MergeSubScanSignals(signals, threshold, config.DirectionAveragingCount, calculateHighQualityPhases);
            return new ProcessResult(mergedSignals, signals);
        }

        private List<ISignal> GetRawSignals(IReadOnlyList<ISignal> signals, IDataScan dataScan)
        {
            return null;
        }

        private IReadOnlyList<ISignal> GetSignalsFromSubScan(IDataScan scan, float threshold, IReadOnlyList<FrequencyRange> frequencyRanges)
        {
            var signalsRanges = GetSignalRanges(scan, threshold, frequencyRanges);

            var signals = new List<ISignal>();

            foreach (var range in signalsRanges)
            {
                GetAmplitudeAndFrequency(scan, range, out var amplitude, out var frequency);
                var directionInfo = _correlator.CalculateDirection(threshold, scan, range.From, range.To);
                var centralFrequencyKhz = Utilities.GetFrequencyKhz(scan.BandNumber, range.CentralPointNumber);
                var modulation = GetSignalModulation(scan, threshold, range);
                var reliabilty = directionInfo.Correlation;
                var (broadcastTimeSpan, subScanCount) = GetSignalEmitDuration(scan, range, threshold);
                if (reliabilty < Constants.ReliabilityThreshold)
                    reliabilty = 0;

                var signal = new Signal(
                    frequencyKhz: frequency,
                    centralFrequencyKhz: centralFrequencyKhz,
                    direction: directionInfo.Angle,
                    reliability: reliabilty,
                    bandwidthKhz: CalculateBandwidth(scan, threshold, range),
                    amplitude: amplitude,
                    disardedDirectionsPart: directionInfo.DiscardedPhasesPart,
                    standardDeviation: directionInfo.StandardDeviation,
                    phases: directionInfo.Phases,
                    relativeSubScanCount: subScanCount,
                    modulation: modulation,
                    phaseDeviation: directionInfo.PhasesDeviation,
                    broadcastTimeSpan: broadcastTimeSpan);

                signals.Add(signal);
            }

            return signals;
        }

        private (TimeSpan, float) GetSignalEmitDuration(IDataScan scan, Range range, float threshold)
        {
            var scanCount = 0;
            var totalScanCount = scan.ReceiverScans[0].Count;
            for (var i = 0; i < totalScanCount; ++i)
            {
                if (scan.IsAnySampleAboveThreshold(i, range.From, range.To, threshold))
                {
                    scanCount++;
                }
            }
            var relativeSubScanCount = 1f * scanCount / totalScanCount;
            var scanReadTimeMs = ScanReadTimer?.ScanReadTimeMs ?? 0;
            return (TimeSpan.FromMilliseconds(scanCount * scanReadTimeMs), relativeSubScanCount);
        }

        private SignalModulation GetSignalModulation(IDataScan dataScan, float threshold, Range range)
        {
            try
            {
                var data = dataScan.GetAmplitudeBands(range.From, range.To);
                return (SignalModulation) DefineModulationType.DefineType(data, threshold);
            }
            catch (Exception exception)
            {
                MessageLogger.Error(exception, "Modulation definition exception");
                return SignalModulation.Unknown;
            }
        }

        // TODO: write get signal modulation
        private SignalModulation GetSignalModulation(IAmplitudeScan dataScan, float threshold, Range range)
        {
            //var data = dataAggregator.GetAmplitudeBandsFromDfReceivers(range.From, range.To);
            return SignalModulation.Unknown;
            //return (SignalModulation) DefineModulationType.DefineType(data, threshold);
        }

        private static IEnumerable<Range> GetSignalRanges(IDataScan dataScan, float threshold, IReadOnlyList<FrequencyRange> frequencyRanges)
        {
            const float thresholdMultiplier = 0.7f;

            if (frequencyRanges == null)
            {
                foreach (var range in GetSignalsRanges(0, Constants.BandSampleCount - 1))
                {
                    yield return range;
                }
                yield break;
            }

            foreach (var frequencyRange in frequencyRanges)
            {
                var startIndex = Utilities.GetSampleNumber(frequencyRange.StartFrequencyKhz, dataScan.BandNumber);
                var endIndex = Utilities.GetSampleNumber(frequencyRange.EndFrequencyKhz, dataScan.BandNumber);
                startIndex = startIndex.ToBounds(0, Constants.BandSampleCount - 1);
                endIndex = endIndex.ToBounds(0, Constants.BandSampleCount - 1);

                foreach (var range in GetSignalsRanges(startIndex, endIndex))
                {
                    yield return range;
                }
            }

            IEnumerable<Range> GetSignalsRanges(int startIndex, int endIndex)
            {
                for (var i = startIndex; i < endIndex;)
                {
                    if (dataScan.Amplitudes[i] <= threshold)
                    {
                        ++i;
                        continue;
                    }
                    var end = GetSignalEndPointIndex(dataScan, threshold, i, out var maxAmplitude);
                    // filtering straight signals with low amplitude
                    if ((end - i + 1) * Constants.SamplesGapKhz <
                        Config.Instance.DirectionFindingSettings.StraightSignalWidthKhz)
                    {
                        if (dataScan.Amplitudes[i] - threshold <
                            Config.Instance.DirectionFindingSettings.LowStraightSignalThreshold)
                        {
                            ++i;
                            continue;
                        }
                    }

                    // re-filter range with new threshold based on max amplitude
                    var adaptiveThreshold = maxAmplitude * (1 - thresholdMultiplier) + threshold * thresholdMultiplier;

                    foreach (var range in GetSignalRangesInner(adaptiveThreshold, i, end))
                    {
                        yield return range;
                    }
                    i = end + 1;
                }
            }

            IEnumerable<Range> GetSignalRangesInner(float innerThreshold, int from, int to)
            {
                for (var i = from; i < to;)
                {
                    if (dataScan.Amplitudes[i] <= innerThreshold)
                    {
                        ++i;
                        continue;
                    }
                    var end = GetSignalEndPointIndex(dataScan, innerThreshold, i, out _);
                    
                    yield return new Range(i, end - 1);
                    i = end + 1;
                }
            }
        }

        private static void GetAmplitudeAndFrequency(IAmplitudeScan scan, Range range, out float amplitude, out float frequency)
        {
            var maxIndex = range.From;
            var maxAmplitude = float.MinValue;
            var bestDistanceToSignalCenter = int.MaxValue;
            var centralIndex = range.CentralPointNumber;

            const float maxAmplitudeDeviation = 0.1f;

            for (var i = range.From; i <= range.To; ++i)
            {
                var currentAmplitude = scan.Amplitudes[i];

                if (Math.Abs(currentAmplitude - maxAmplitude) < maxAmplitudeDeviation)
                {
                    var distanceToCenter = Math.Abs(i - centralIndex);
                    if (distanceToCenter < bestDistanceToSignalCenter)
                    {
                        bestDistanceToSignalCenter = distanceToCenter;
                        maxIndex = i;
                        maxAmplitude = currentAmplitude;
                    }
                }
                else if (currentAmplitude > maxAmplitude)
                {
                    bestDistanceToSignalCenter = Math.Abs(i - centralIndex);
                    maxIndex = i;
                    maxAmplitude = currentAmplitude;
                }
            }
            amplitude = maxAmplitude;
            frequency = ClarifyFrequency(scan, maxIndex);
        }

        private static float ClarifyFrequency(IAmplitudeScan scan, int index)
        {
            if (index == 0 || index == scan.Count - 1)
            {
                return Utilities.GetFrequencyKhz(scan.BandNumber, index);
            }
            var leftAmplitude = scan.Amplitudes[index - 1];
            var rightAmplitude = scan.Amplitudes[index + 1];
            var minAmplitude = Math.Min(leftAmplitude, rightAmplitude);
            var maxAmplitude = scan.Amplitudes[index];

            if (Math.Abs(minAmplitude - maxAmplitude) < 1e-3)
            {
                return Utilities.GetFrequencyKhz(scan.BandNumber, index);
            }

            var shift = 0.5f * Constants.SamplesGapKhz * (rightAmplitude - leftAmplitude) / (maxAmplitude - minAmplitude);
            return Utilities.GetFrequencyKhz(scan.BandNumber, index) + shift;
        }

        private static int GetSignalEndPointIndex(IAmplitudeBand dataScan, float threshold, int signalStartPointIndex, out float maxAmplitude)
        {
            var endIndex = signalStartPointIndex + 1;
            maxAmplitude = dataScan.Amplitudes[signalStartPointIndex];
            for (var index = signalStartPointIndex + 1; index < dataScan.Count; ++index)
            {
                var amplitude = dataScan.Amplitudes[index];
                if (amplitude > maxAmplitude)
                {
                    maxAmplitude = amplitude;
                }
                if (amplitude > threshold)
                {
                    endIndex = index;
                    continue;
                }
                var stopSearch = true;
                for (var i = 1; i < Config.Instance.DirectionFindingSettings.SignalsGapPointCount; ++i)
                {
                    if (i + index == dataScan.Count)
                    {
                        return dataScan.Amplitudes[dataScan.Count - 1] > threshold ? dataScan.Count - 1 : endIndex;
                    }
                    if (dataScan.Amplitudes[i + endIndex] > threshold)
                    {
                        endIndex += i;
                        index += i;
                        stopSearch = false;
                        break;
                    }
                }
                if (stopSearch)
                    return endIndex;
            }
            return endIndex;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using DspDataModel;
using DspDataModel.Storages;
using Phases;
using SharpExtensions;

namespace DataProcessor.Fhss
{
    public class FhssProcessor
    {
        private readonly IRadioSourceStorage _radioSourceStorage;
        private readonly FhssSearchConfig _config;
        //TODO : clear this shit
        public FhssProcessor(IRadioSourceStorage radioSourceStorage)
            : this(radioSourceStorage, Config.Instance.FhssSearchSettings)
        { }

        public FhssProcessor(IRadioSourceStorage radioSourceStorage, FhssSearchConfig config)
        {
            _config = config;
            _radioSourceStorage = radioSourceStorage;
        }
        
        public List<IFhssNetwork> FindNetworks(IReadOnlyList<ImpulseSignal> signals)
        {
            var frequencies = signals.Select(s => s.Signal.FrequencyKhz).ToList();
            
            var networkRanges = FhssRangeFinder.FindRanges(
                frequencies,
                _config.RangeSectorKhz,
                _config.RangeRelativeThreshold);

            var now = DateTime.Now;
            var lastTimeActiveTimeThreshold = _config.FixedRadioSourceLastUpdateTimeThreshold;
            var activeTimeThreshold = _config.FixedRadioSourceTimeThreshold;

            var fixedRadioSources = _radioSourceStorage
                .GetRadioSources()
                .Where(r => now - r.LastActiveTime() < lastTimeActiveTimeThreshold && 
                            r.BroadcastTimeSpan >= activeTimeThreshold)
                .ToList();

            var signalsWoFrs = signals.ToList();
            foreach (var frs in fixedRadioSources)
            {
                signalsWoFrs = signalsWoFrs.Where(s => IsClose(s.Signal.FrequencyKhz,frs.FrequencyKhz) == false).ToList();
            }
            return networkRanges
                .Select(r => signalsWoFrs
                    .Where(s => r.Contains(s.Signal.FrequencyKhz))
                    .ToList())
                .SelectMany(ProcessNetworkRangeSignals)
                .Select(n => CreateFhssNetwork(n, fixedRadioSources))
                .ToList();

            bool IsClose(float frequency1, float frequency2)
            {
                return Math.Abs(frequency1 - frequency2) < 50;
            }
        }

        private IFhssNetwork CreateFhssNetwork(Network network, IReadOnlyList<IRadioSource> activeRadioSources)
        {
            var signals = network.Networks
                .SelectMany(s => s.Signals)
                .ToList();
            var minFrequency = (float) Math.Round(signals.Min(s => s.Signal.FrequencyKhz));
            var maxFrequency = (float) Math.Round(signals.Max(s => s.Signal.FrequencyKhz));
            var bandwidths = signals
                .Select(s => s.Signal.BandwidthKhz)
                .ToList();
            var bandwidth = bandwidths.QuickSelect(bandwidths.Count / 2);
            var step = GetStep(signals, _config.StepFrequencyThreshold);
            var frequencyCount = (int) Math.Ceiling((maxFrequency - minFrequency) / step);
            var amplitude = signals.Average(s => s.Signal.Amplitude);
            
            var fixedRadioSources = activeRadioSources
                .Where(r => network.Range.Contains(r.FrequencyKhz))
                .Select(r => new FixedRadioSource(
                        r.CentralFrequencyKhz, 
                        r.Amplitude, 
                        r.Direction, 
                        r.GetIntersectionBandwidth()))
                .ToList();

            return new FhssNetwork(
                minFrequencyKhz: minFrequency,
                maxFrequencyKhz: maxFrequency,
                bandwidthKhz: bandwidth,
                stepKhz: step,
                amplitude: amplitude,
                impulseDurationMs: 0,
                isDurationMeasured: false,
                frequenciesCount: frequencyCount,
                directions: network.Networks.Select(n => n.Direction),
                fixedRadioSources: fixedRadioSources
            );
        }

        private float GetStep(IReadOnlyList<ImpulseSignal> networkSignals, float frequencyThreshold)
        {
            if (networkSignals.Count <= 3)
            {
                throw new ArgumentException("Can't find step for network with less than 3 signals");
            }
            var signals = networkSignals.ToList();
            signals.Sort((s1, s2) => s1.Signal.FrequencyKhz.CompareTo(s2.Signal.FrequencyKhz));

            var frequencies = new List<float>
            {
                signals[0].Signal.FrequencyKhz
            };

            for (var i = 1; i < signals.Count; ++i)
            {
                var delta = signals[i].Signal.FrequencyKhz - frequencies[frequencies.Count - 1];
                if (delta < frequencyThreshold)
                {
                    continue;
                }
                frequencies.Add(signals[i].Signal.FrequencyKhz);
            }

            // replace frequencies with frequency-deltas
            for (var i = 2; i < frequencies.Count; ++i)
            {
                frequencies[i - 2] = frequencies[i] - frequencies[i - 1];
            }
            // remove trash
            frequencies.RemoveRange(frequencies.Count - 2, 2);
            frequencies.Sort();

            // select median
            var step = frequencies.QuickSelect(frequencies.Count / 2);
            
            return ApproximateStep(step);
        }

        private float ApproximateStep(float step)
        {
            var knownSteps = new[]
            {
                6.25f, 12.5f, 25, 50, 100
            };
            const float relativeThreshold = 0.15f;

            foreach (var knownStep in knownSteps)
            {
                var from = knownStep * (1 - relativeThreshold);
                var to = knownStep * (1 + relativeThreshold);
                if (step.IsInBounds(from, to))
                {
                    return knownStep;
                }
            }

            return (float) Math.Round(step);
        }

        private static int count = 0;//deleteme
        private List<Network> ProcessNetworkRangeSignals(IReadOnlyList<ImpulseSignal> signals)
        {
            count = 0;//delete me 
            if (signals.Count < _config.NetworkMinSignalCount)
            {
                return new List<Network>();
            }

            var signalDirections = signals.Select(s => s.Signal.Direction).ToList();
            var directionSector = _config.DirectionSector;
            
            var directions = FhssDirectionFinder.CalculateDirections(
                signalDirections,
                directionSector,
                _config.DirectionGlobalRelativeThreshold,
                _config.DirectionLocalRelativeThreshold);

            var directionscopy = new List<float>();
            foreach (var direction in directions)
            {
                directionscopy.Add(direction);
            }
            while (true)
            {
                var restartFlag = false;
                var newDirection = 0.0f;
                var left = 0;
                var right = 0;
                for (int i = 0; i < directionscopy.Count; i++)
                {
                    for (int j = i + 1; j < directionscopy.Count; j++)
                    {
                        if (Math.Abs(directionscopy[i] - directionscopy[j]) < 3)
                        {
                            left = i;
                            right = j;
                            restartFlag = true;
                            newDirection = (directionscopy[i] + directionscopy[j]) / 2;
                            break;
                        }
                    }
                    if (restartFlag)
                        break;
                }
                if (restartFlag)
                {
                    directionscopy.RemoveAt(left);
                    directionscopy.RemoveAt(right - 1);
                    directionscopy.Add(newDirection);
                    continue;
                }
                break;
            }
            var str = "Iteration : "+count+"Merged directions : ";
            foreach (var copy in directionscopy)
            {
                str += copy+", ";
            }
            str += "\r\nReal directions : ";
            foreach (var copy in directions)
            {
                str += copy + ", ";
            }
            //MessageLogger.Warning(str);
            count++;
            var networks = new List<Network>();
            

            foreach (var direction in directions)
            {
                var directionSignals = signals
                    .Where(s => PhaseMath.Angle(s.Signal.Direction, direction) < directionSector)
                    .ToList();
                
                if (directionSignals.Count < _config.NetworkMinSignalCount)
                {
                    continue;
                }
                var directionNetwork = new DirectionNetwork(directionSignals, direction);

                var network = networks.FirstOrDefault(n => n.IsInThisNetwork(directionNetwork));
                if (network == null)
                {
                    networks.Add(new Network(directionNetwork));
                }
                else
                {
                    network.AddToNetwork(directionNetwork);
                }
            }

            return networks;
        }
    }
}

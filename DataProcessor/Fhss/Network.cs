﻿using System;
using System.Collections.Generic;
using DspDataModel;
using Settings;

namespace DataProcessor.Fhss
{
    internal class Network
    {
        public List<DirectionNetwork> Networks { get; } = new List<DirectionNetwork>();
        public FrequencyRange Range { get; private set; }

        public Network(DirectionNetwork network)
        {
            AddToNetwork(network);
        }

        public void AddToNetwork(DirectionNetwork network)
        {
            if (Networks.Count == 0)
            {
                Range = network.Range;
            }
            else
            {
                Range = new FrequencyRange(
                    Math.Min(Range.StartFrequencyKhz, network.Range.StartFrequencyKhz),
                    Math.Max(Range.EndFrequencyKhz, network.Range.EndFrequencyKhz));
            }
            Networks.Add(network);
        }

        public bool IsInThisNetwork(DirectionNetwork network)
        {
            return FrequencyRange.IntersectionFactor(Range, network.Range) >= Constants.FhssIntersectionThreshold;
        }
    }
}
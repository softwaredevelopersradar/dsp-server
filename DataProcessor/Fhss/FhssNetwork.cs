﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using DspDataModel;
using Settings;

namespace DataProcessor.Fhss
{
    public class FhssNetwork : IFhssNetwork
    {
        public float MinFrequencyKhz { get; private set; }
        public float MaxFrequencyKhz { get; private set; }
        public float BandwidthKhz { get; private set; }
        public float StepKhz { get; private set; }
        public float Amplitude { get; private set; }
        public int FrequenciesCount { get; private set; }
        public float ImpulseDurationMs { get; private set; }
        public bool IsDurationMeasured { get; private set; }
        public int Id { get; }
        public bool IsActive { get; private set; }
        public DateTime LastUpdateTime { get; private set; }

        public IReadOnlyList<float> Directions { get; private set; }
        public IReadOnlyList<FixedRadioSource> FixedRadioSources { get; private set; }

        private static int _idCounter = 0;

        public FhssNetwork(float minFrequencyKhz, float maxFrequencyKhz, float bandwidthKhz, float stepKhz, float amplitude, float impulseDurationMs, bool isDurationMeasured, int frequenciesCount, IEnumerable<float> directions, IEnumerable<FixedRadioSource> fixedRadioSources)
        {
            MinFrequencyKhz = minFrequencyKhz;
            MaxFrequencyKhz = maxFrequencyKhz;
            BandwidthKhz = bandwidthKhz;
            StepKhz = stepKhz;
            Amplitude = amplitude;
            ImpulseDurationMs = impulseDurationMs;
            FrequenciesCount = frequenciesCount;
            Directions = directions?.ToArray() ?? new float[0];
            FixedRadioSources = fixedRadioSources?.ToArray() ?? new FixedRadioSource[0];
            IsActive = true;
            IsDurationMeasured = isDurationMeasured;
            LastUpdateTime = DateTime.Now;
            Id = Interlocked.Increment(ref _idCounter);
        }

        public void Update(IFhssNetwork network)
        {
            if (network == null)
            {
                IsActive = false;
                return;
            }
            IsActive = true;
            MinFrequencyKhz = network.MinFrequencyKhz;
            MaxFrequencyKhz = network.MaxFrequencyKhz;
            StepKhz = network.StepKhz;
            Amplitude = network.Amplitude;
            Directions = network.Directions;
            LastUpdateTime = network.LastUpdateTime;
            FixedRadioSources = network.FixedRadioSources;
            FrequenciesCount = network.FrequenciesCount;
            if (IsDurationMeasured == false) //hack for no remeasuring when network changed
            {
                ImpulseDurationMs = network.ImpulseDurationMs;
                IsDurationMeasured = network.IsDurationMeasured;
            }
        }

        public void RemeasureDuration()
        {
            IsDurationMeasured = false;
        }

        public static bool AreSameNetworks(IFhssNetwork network1, IFhssNetwork network2)
        {
            var range1 = new FrequencyRange(network1.MinFrequencyKhz, network1.MaxFrequencyKhz);
            var range2 = new FrequencyRange(network2.MinFrequencyKhz, network2.MaxFrequencyKhz);

            return FrequencyRange.IntersectionFactor(range1, range2) >= Constants.FhssIntersectionThreshold;
        }
    }
}

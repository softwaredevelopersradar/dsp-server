﻿using System;
using DspDataModel.AmplitudeCalulators;

namespace DataProcessor.AmplitudeCalculators
{
    public class AmplitudeCalculatorFactory : IAmplitudeCalculatorFactory
    {
        public AmplitudeCalculatorFactory()
        { }

        static AmplitudeCalculatorFactory()
        {
            Instance = new AmplitudeCalculatorFactory();
        }

        public static readonly AmplitudeCalculatorFactory Instance;

        public IAmplitudeCalculator CreateCalculator(AmplitudeCalculatorPolicy policy)
        {
            switch (policy)
            {
                case AmplitudeCalculatorPolicy.Average:
                    return new AverageAmplitudeCalculator();
                case AmplitudeCalculatorPolicy.Max:
                    return new MaxAmplitudeCalculator();
                case AmplitudeCalculatorPolicy.MinMax:
                    return new MinMaxAmplitudeCalculator();
                default:
                    throw new ArgumentOutOfRangeException(nameof(policy), policy, null);
            }
        }
    }
}

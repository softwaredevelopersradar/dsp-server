﻿using System;
using System.Collections.Generic;
using DspDataModel.Data;
using DspDataModel.Storages;

namespace DspDataModel
{
    public interface IFilterManager
    {
        IReadOnlyList<Band> Bands { get; }
        IReadOnlyList<Filter> Filters { get; }
        IReadOnlyList<FrequencyRange> ForbiddenFrequencies { get; }
        IReadOnlyList<FrequencyRange> ImportantFrequencies { get; }
        IReadOnlyList<FrequencyRange> KnownFrequencies { get; }
        IReadOnlyList<float> NoiseLevels { get; }
        IReadOnlyList<float> Thresholds { get; }
        IReadOnlyList<int> CalibrationBands { get; }
        IReadOnlyList<Filter> IntelligenceFilters { get; }
        IReadOnlyList<Filter> RadioJamFilters { get; }
        RangeType WorkingRangeType { get; set; }

        event EventHandler<IReadOnlyList<Band>> BandsUpdatedEvent;
        event EventHandler<IReadOnlyList<Filter>> FiltersUpdatedEvent;
        event EventHandler<IReadOnlyList<FrequencyRange>> ForbiddenFrequenciesUpdatedEvent;
        event EventHandler<IReadOnlyList<FrequencyRange>> ImportantFrequenciesUpdatedEvent;
        event EventHandler<IReadOnlyList<FrequencyRange>> KnownFrequenciesUpdatedEvent;
        event EventHandler<IReadOnlyList<float>> NoiseLevelUpdatedEvent;
        event EventHandler<IReadOnlyList<float>> ThresholdsUpdatedEvent;

        float GetAdaptiveThreshold(int bandNumber);
        RadioSourceType GetRadioSourceType(float frequencyKhz);
        RadioSourceType GetRadioSourceType(ISignal signal);
        float GetThreshold(int bandNumber);
        IReadOnlyList<Filter> GetFilters(RangeType rangeType);
        bool IsInFilter(float frequencyKhz, float direction);
        bool IsInFilter(ISignal signal);
        bool IsInFilter(IFhssNetwork network);
        
        /// <summary>
        /// returns if filters has changed
        /// </summary>
        bool SetFilters(RangeType rangeType, IEnumerable<Filter> filters);
        bool SetForbiddenFrequencies(IEnumerable<FrequencyRange> forbiddenFrequencies);        
        bool SetImportantFrequencies(IEnumerable<FrequencyRange> importantFrequencies);
        bool SetKnownFrequencies(IEnumerable<FrequencyRange> knownFrequencies);

        void SetGlobalThreshold(float value);
        void SetThreshold(int bandNumber, float value);
        void UpdateNoiseLevel(IAmplitudeScan scan);
        void UpdateNoiseLevel(int bandNumber, float noiseLevel);
        void ClearCalibrationBands();
        void SetCalibrationBands(List<int> bands);
    }
}
﻿using System;

namespace DspDataModel.Data
{
    public class AmplitudeScan : IAmplitudeScan
    {
        public float[] Amplitudes { get; }
        public int BandNumber { get; }
        public DateTime CreationTime { get; }
        public int ScanIndex { get; }

        public int Count => Amplitudes.Length;

        public AmplitudeScan(float[] amplitudes, int bandNumber, DateTime creationTime, int scanIndex)
        {
            Amplitudes = amplitudes;
            BandNumber = bandNumber;
            CreationTime = creationTime;
            ScanIndex = scanIndex;
        }
    }
}

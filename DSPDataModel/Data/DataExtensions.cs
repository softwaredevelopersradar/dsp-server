﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using DspDataModel.Correlator;
using Phases;
using Settings;

namespace DspDataModel.Data
{
    public static class DataExtensions
    {
        public static float GetAmplitude<T>(this IReadOnlyList<T> scans, int index) where T : IAmplitudeBand
        {
            var max = float.MinValue;
            for (var i = 0; i < scans.Count; ++i)
            {
                var amplitude = scans[i].Amplitudes[index];
                if (max < amplitude)
                {
                    max = amplitude;
                }
            }
            return max;
        }

        public static float GetAverageAmplitude<T>(this IReadOnlyList<T> scans, int index) where T : IAmplitudeBand
        {
            var sum = 0f;
            for (var i = 0; i < scans.Count; ++i)
            {
                sum += scans[i].Amplitudes[index];
            }
            return sum / scans.Count;
        }

        public static float GetAmplitude<T>(this IReadOnlyList<IReadOnlyList<T>> scans, int index) where T : IAmplitudeBand
        {
            var max = float.MinValue;
            for (var i = 0; i < scans.Count; ++i)
            {
                var amplitude = GetAmplitude(scans[i], index);
                if (max < amplitude)
                {
                    max = amplitude;
                }
            }
            return max;
        }

        public static float GetSpectralDensity(this IAmplitudeBand band)
        {
            var sum = band.Amplitudes.Sum(amplitude => Math.Pow(10, 0.1 * amplitude));
            return (float) (10 * Math.Log10(sum));
        }

        public static float GetPhase<T>(this IReadOnlyList<T> scans, int index) where T : IBandData
        {
            var phaseFactors = scans
                .Select(scan => scan.Phases[index])
                .ToArray();

            return PhaseMath.CalculatePhase(phaseFactors);
        }

        public static float GetPhase<T>(this IReadOnlyList<T> scans, int index, float threshold) where T : IBandData
        {
            var phaseFactors = scans
                .Where(scan => scan.Amplitudes[index] > threshold)
                .Select(scan => new PhaseFactor(scan.Amplitudes[index] - threshold, scan.Phases[index]))
                .ToArray();

            return PhaseMath.CalculatePhase(phaseFactors);
        }

        public static float GetPhase<T>(this IEnumerable<T> scans, int index, float threshold, out float phasesDeviation) where T : IBandData
        {
            var phaseFactors = scans.Select(
                    scan => new PhaseFactor(scan.Amplitudes[index] - threshold, scan.Phases[index]))
                .Where(pf => pf.Factor > 0)
                .ToArray();

            phasesDeviation = PhaseMath.StandardDeviation(phaseFactors);
            return PhaseMath.AveragePhase(phaseFactors);
        }

        private static PhaseFactor[][] ExtractPhases(IReadOnlyList<DirectionInfo> directionInfos)
        {
            var phases = new PhaseFactor[10][];
            for (var i = 0; i < phases.Length; i++)
            {
                phases[i] = new PhaseFactor[directionInfos.Count];
            }
            for (var i = 0; i < directionInfos.Count; i++)
            {
                var directionInfo = directionInfos[i];
                for (var j = 0; j < Constants.PhasesDifferencesCount; ++j)
                {
                    phases[j][i] = new PhaseFactor(SignalExtensions.GetMergeFactor(directionInfo.Correlation), directionInfo.Phases[j]);
                }
            }
            return phases;
        }

        public static float[] CalculatePhases(this IReadOnlyList<DirectionInfo> directionInfos)
        {
            if (directionInfos.Count == 1)
            {
                return directionInfos[0].Phases;
            }
            var bestDi = directionInfos[0];
            for (var i = 1; i < directionInfos.Count; ++i)
            {
                if (directionInfos[i].Correlation > bestDi.Correlation)
                {
                    bestDi = directionInfos[i];
                }
            }
            return bestDi.Phases;
        }

        public static float CalculatePhaseDeviation(this IReadOnlyList<DirectionInfo> directionInfos)
        {
            if (directionInfos.Count == 1)
            {
                return 0;
            }
            var result = 0f;
            var phases = ExtractPhases(directionInfos);

            for (var i = 0; i < phases.Length; i++)
            {
                result += PhaseMath.StandardDeviation(phases[i]);
            }
            return result / directionInfos.Count;
        }

        public static float[] AveragePhases(this IReadOnlyList<DirectionInfo> directionInfos)
        {
            if (directionInfos.Count == 1)
            {
                return directionInfos[0].Phases;
            }
            var result = new float[Constants.PhasesDifferencesCount];
            var phases = ExtractPhases(directionInfos);

            for (var i = 0; i < phases.Length; i++)
            {
                result[i] = PhaseMath.AveragePhase(phases[i]);
            }
            return result;
        }

        public static IAmplitudeScan MergeSpectrum<T>(this IReadOnlyList<T> scans) where T : class, IAmplitudeScan
        {
            Contract.Assert(scans.All(s => s.Amplitudes.Length == scans[0].Amplitudes.Length));
            Contract.Assert(scans.All(s => s.BandNumber == scans[0].BandNumber));

            switch (scans.Count)
            {
                case 0:
                    return null;
                case 1:
                    return scans[0];
            }
            var amplitudes = new float[scans[0].Amplitudes.Length];
            var creationTime = scans[0].CreationTime;

            for (var i = 0; i < amplitudes.Length; ++i)
            {
                amplitudes[i] = scans.GetAmplitude(i);
            }
            return new AmplitudeScan(amplitudes, scans[0].BandNumber, creationTime, scans[0].ScanIndex);
        }

        public static IReceiverScan MergeScans<T>(this IReadOnlyList<T> scans, bool useAverageAmplitude = false) where T : class, IReceiverScan
        {
            Contract.Assert(scans.All(s => s.Amplitudes.Length == scans[0].Amplitudes.Length));
            Contract.Assert(scans.All(s => s.BandNumber == scans[0].BandNumber));

            switch (scans.Count)
            {
                case 0:
                    return null;
                case 1:
                    return scans[0];
            }
            var amplitudes = new float[scans[0].Amplitudes.Length];
            var phases = scans[0].Phases;
            var creationTime = scans[0].CreationTime;

            if (useAverageAmplitude)
            {
                for (var i = 0; i < amplitudes.Length; ++i)
                {
                    amplitudes[i] = scans.GetAverageAmplitude(i);
                }
            }
            else
            {
                for (var i = 0; i < amplitudes.Length; ++i)
                {
                    amplitudes[i] = scans.GetAmplitude(i);
                }
            }
            return new ReceiverScan(amplitudes, phases, scans[0].BandNumber, creationTime, scans[0].ScanIndex);
        }

        public static float GetMaxAmplitude(this IDataScan scan, int scanIndex, int startIndex, int endIndex)
        {
            var maxAmplitude = float.MinValue;
            for (var i = startIndex; i <= endIndex; ++i)
            {
                var amplitude = scan.GetAmplitude(scanIndex, i);
                if (amplitude > maxAmplitude)
                {
                    maxAmplitude = amplitude;
                }
            }
            return maxAmplitude;
        }

        public static bool IsAnySampleAboveThreshold(this IDataScan scan, int scanIndex, int startIndex, int endIndex, float threshold)
        {
            for (var i = 0; i < scan.ReceiverScans.Length; i++)
            {
                var receiverScan = scan.ReceiverScans[i][scanIndex];
                for (var j = startIndex; j <= endIndex; ++j)
                {
                    var amplitude = receiverScan.Amplitudes[j];
                    if (amplitude > threshold)
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public static float[] GetPhaseDifferences<T>(this IReadOnlyList<T> bands, int index) where T : IBandData
        {
            var result = new float[bands.Count * (bands.Count - 1) / 2];
            var k = 0;
            for (var i = 0; i < bands.Count; ++i)
            {
                for (var j = i + 1; j < bands.Count; ++j)
                {
                    result[k++] = bands[i].Phases[index] - bands[j].Phases[index];
                }
            }
            return result;
        }

        public static float[] GetPhaseDifferences(this IReadOnlyList<float> phases)
        {
            var result = new float[phases.Count * (phases.Count - 1) / 2];
            var k = 0;
            for (var i = 0; i < phases.Count; ++i)
            {
                for (var j = i + 1; j < phases.Count; ++j)
                {
                    result[k++] = phases[i] - phases[j];
                }
            }
            return result;
        }

        /// <summary>
        /// Intelligent method that tries to guess noise level of the spectrum plus some additional small threshold
        /// </summary>
        public static unsafe float GetNoiseLevel(this float[] spectrum)
        {
            const int averagingCount = 7;
            const int averagingShift = averagingCount / 2;
            const float thresholdMultiplier = 0.3f;
            const float noiseThresholdMultiplier = 0.4f;
            const float minAmountThreshold = 75;

            var windowMultipliers = stackalloc float[averagingCount];
            for (var i = 0; i < averagingCount; ++i)
            {
                windowMultipliers[i] = PhaseMath.Sin((90 + 0.5f * Constants.DirectionsCount * i) / averagingCount);
            }

            var values = stackalloc int[256];
            var cumulativeValues = stackalloc float[256];
            var currentSum = 0f;
            var cumulativeMax = 0f;

            foreach (var amplitude in spectrum)
            {
                values[(byte)-amplitude]++;
            }
            

            for (var i = 0; i < averagingCount; ++i)
            {
                currentSum += values[i];
                if (i >= averagingShift)
                {
                    cumulativeValues[i - averagingShift] = currentSum;
                }
            }
            for (var i = averagingCount; i < 256 - averagingCount; ++i)
            {
                currentSum = CalculateCumulativeValue(i);
                cumulativeValues[i] = currentSum;
                if (currentSum > cumulativeMax)
                {
                    cumulativeMax = currentSum;
                }
            }

            var threshold = (int) (cumulativeMax * thresholdMultiplier);
            var index = 255;
            for (; index >= 0; --index)
            {
                if (cumulativeValues[index] > threshold)
                {
                    // we are inside first peak. we should get to it's end, that's would be adaptive threhsold base level
                    break;
                }
            }
            for (; index >= 0; --index)
            {
                if (cumulativeValues[index] > cumulativeValues[index - 1])
                {
                    // we reach the first peak.
                    // now we update threshold value with the current peak value
                    threshold = (int) (cumulativeValues[index] * noiseThresholdMultiplier);
                    break;
                }
            }
            var minAmount = float.MaxValue;
            for (; index >= 0; --index)
            {
                if (cumulativeValues[index] < minAmount)
                {
                    minAmount = cumulativeValues[index];
                }
                if (cumulativeValues[index] < threshold || cumulativeValues[index] > minAmount + minAmountThreshold)
                {
                    // we reach end of the first peak.
                    break;
                }
            }
            return -index;

            // i should be in range [averagingShift ... 256-averagingShift]
            float CalculateCumulativeValue(int i)
            {
                var sum = 0f;
                for (var j = 0; j < averagingCount; ++j)
                {
                    sum += values[i + j - averagingShift] * windowMultipliers[j];
                }
                return sum;
            }
        }

        /// <summary>
        /// Simple method that returns median level of the spectrum
        /// </summary>
        public static unsafe float GetAverageNoiseLevel(this float[] spectrum)
        {
            const int valuesCount = byte.MaxValue;
            var values = stackalloc int[valuesCount];

            foreach (var amplitude in spectrum)
            {
                values[(byte)-amplitude]++;
            }

            var sum = 0;
            var index = 0;
            var threshold = spectrum.Length / 2;

            for (var i = valuesCount - 1; i >= 0; --i)
            {
                sum += values[i];
                if (sum > threshold)
                {
                    index = i;
                    break;
                }
            }
            return -index;
        }

        /// <summary>
        /// Simple method that returns median level of the spectrum
        /// </summary>
        public static unsafe float GetAverageNoiseLevel(this float[] spectrum, int startIndex, int endIndex)
        {
            const int valuesCount = byte.MaxValue;
            var values = stackalloc int[valuesCount];

            for (var i = startIndex; i < endIndex; ++i)
            {
                values[(byte)-spectrum[i]]++;
            }

            var sum = 0;
            var index = 0;
            var threshold = (endIndex - startIndex) / 2;

            for (var i = valuesCount - 1; i >= 0; --i)
            {
                sum += values[i];
                if (sum > threshold)
                {
                    index = i;
                    break;
                }
            }
            return -index;
        }

        public static IAmplitudeBand StrechSpectrum(this IReadOnlyList<float> inputData, int pointCount)
        {
            // no scaling needed
            if (Math.Abs(inputData.Count - pointCount) <= 1)
            {
                return inputData.Count == pointCount
                    ? new AmplitudeBand(inputData.ToArray())
                    : new AmplitudeBand(inputData.Take(Math.Min(pointCount, inputData.Count)).ToArray());
            }
            var data = new float[pointCount];
            if (inputData.Count < pointCount) // enlarge current data array => using linear extrapolation
            {
                for (var i = 0; i < pointCount; ++i)
                {
                    data[i] = GetExtrapolatedValue(inputData, 1f * i / pointCount);
                }
            }
            else // aggregator.Count > pointCount. reduce current data array => using max on range
            {
                var start = 0;
                var end = start + inputData.Count / pointCount + 1;

                for (var i = 0; i < pointCount; ++i)
                {
                    if (end >= inputData.Count)
                    {
                        end = inputData.Count - 1;
                    }
                    data[i] = GetMaxValueInRange(inputData, start, end);

                    start = end;
                    end = (int)((i + 1f) * inputData.Count / pointCount + 1); // using floats to prevent overflow error
                }
            }
            return new AmplitudeBand(data);
        }

        /// <summary>
        /// calculates maximum in range [offset, offset + step]
        /// </summary>
        private static float GetMaxValueInRange(IReadOnlyList<float> data, int start, int end)
        {
            //var to = Math.Min(offset + step, data.Count);
            var answer = data[start];
            for (var i = start + 1; i < end; ++i)
            {
                if (data[i] > answer)
                {
                    answer = data[i];
                }
            }
            return answer;
        }

        /// <summary> calculates linear extrapolated value in current position </summary>
        /// <param name="position"> place in array from 0 to 1 </param>
        private static float GetExtrapolatedValue(IReadOnlyList<float> data, float position)
        {
            var floatIndex = position * data.Count;
            var index1 = (int)floatIndex;
            if (index1 == data.Count - 1)
            {
                return data[index1];
            }
            var index2 = index1 + 1;
            var pos = floatIndex - index1; // position from 0 to 1 between elements with indeces i1 and i2
            var elem1 = data[index1];
            var elem2 = data[index2];

            return elem1 * (1 - pos) + elem2 * pos;
        }
    }
}

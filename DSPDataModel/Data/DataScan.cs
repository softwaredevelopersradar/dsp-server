﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using DspDataModel.AmplitudeCalulators;
using Settings;
using Phases;

namespace DspDataModel.Data
{
    public class DataScan : IDataScan
    {
        public IReadOnlyList<IReceiverScan>[] ReceiverScans { get; }
        public DateTime CreationTime { get; }

        public float[] Amplitudes { get; }

        public int BandNumber { get; }

        public int Count => Constants.BandSampleCount;

        private readonly int _singleReceiverScanCount;

        private readonly float[][] _amplitudes;
        private readonly float[][][] _phases;

        private readonly IAmplitudeCalculator _amplitudeCalculator;

        public int ScanIndex { get; }

        /// <summary>
        /// Creates data scan without calculating result amplitudes
        /// </summary>
        public DataScan(IReadOnlyList<IReadOnlyList<IReceiverScan>> receiverScans)
        {
            Contract.Assert(receiverScans != null);
            Contract.Assert(receiverScans.All(rs => rs.Count == receiverScans[0].Count));
            Contract.Assert(receiverScans.All(scans =>
                scans.All(scan => scan.Count == receiverScans[0][0].Count)));

            _singleReceiverScanCount = receiverScans[0].Count;
            ReceiverScans = receiverScans.ToArray();
            BandNumber = ReceiverScans[0][0].BandNumber;
            CreationTime = ReceiverScans[0][0].CreationTime;

            _amplitudes = ReceiverScans
                .SelectMany(scans => scans.Select(scan => scan.Amplitudes))
                .ToArray();
            _phases = ReceiverScans
                .Select(scans => scans.Select(scan => scan.Phases).ToArray())
                .ToArray();
        }

        /// <summary>
        /// Creates data scan with calculating result amplitudes
        /// </summary>
        public DataScan(IReadOnlyList<IReadOnlyList<IReceiverScan>> receiverScans, IAmplitudeCalculator amplitudeCalculator)
            : this(receiverScans)
        {
            _amplitudeCalculator = amplitudeCalculator;
            if (amplitudeCalculator != null)
            {
                Amplitudes = amplitudeCalculator.CalculateAmplitudes(_amplitudes);
            }
        }

        private float[][] CreatePhaseDifferencesPartResultsArray(int index, out float[] amplitudes)
        {
            var partResults = new float[Constants.PhasesDifferencesCount][];
            amplitudes = new float[_singleReceiverScanCount];

            for (var i = 0; i < partResults.Length; i++)
            {
                partResults[i] = new float[_singleReceiverScanCount];
            }
            for (var scanIndex = 0; scanIndex < _singleReceiverScanCount; ++scanIndex)
            {
                var differences = GetPhaseDifferences(scanIndex, index);
                for (var j = 0; j < Constants.PhasesDifferencesCount; ++j)
                {
                    partResults[j][scanIndex] = differences[j];
                }
                amplitudes[scanIndex] = GetAmplitude(scanIndex, index);
            }
            return partResults;
        }

        /// <summary>
        /// Get Amplitude by ReceiversScan, not by amplitudes array
        /// Could be useful for work with SubScan 
        /// </summary>
        public float GetAmplitude(int index)
        {
            var maxAmplitude = float.MinValue;

            foreach (var amplitudes in _amplitudes)
            {
                var amplitude = amplitudes[index];
                if (amplitude > maxAmplitude)
                {
                    maxAmplitude = amplitude;
                }
            }
            return maxAmplitude;
        }

        public IDataScan SubScan(int startScanIndex, int scansCount)
        {
            var scans = ReceiverScans
                .Select(s =>
                    s.Skip(startScanIndex)
                        .Take(scansCount)
                        .ToArray())
                .ToArray();

            return new DataScan(scans, _amplitudeCalculator);
        }

        public float GetAmplitude(int scanIndex, int index)
        {
            var maxAmplitude = float.MinValue;
            foreach (var scans in ReceiverScans)
            {
                var amplitude = scans[scanIndex].Amplitudes[index];
                if (amplitude > maxAmplitude)
                {
                    maxAmplitude = amplitude;
                }
            }
            return maxAmplitude;
        }

        public float[] GetPhaseDifferences(float threshold, int index)
        {
            var result = new float[Constants.PhasesDifferencesCount];
            var partResults = CreatePhaseDifferencesPartResultsArray(index, out var amplitudes);

            for (var i = 0; i < result.Length; ++i)
            {
                result[i] = PhaseMath.CalculatePhase(partResults[i], amplitudes, threshold);
            }
            return result;
        }

        public float[] GetPhaseDifferences(float threshold, int index, out float deviation)
        {
            var result = new float[Constants.PhasesDifferencesCount];
            var partResults = CreatePhaseDifferencesPartResultsArray(index, out var amplitudes);
            deviation = float.MinValue;

            for (var i = 0; i < result.Length; ++i)
            {
                result[i] = PhaseMath.CalculatePhase(partResults[i], amplitudes, threshold);
                var currentDeviation = PhaseMath.StandardDeviation(partResults[i]);
                if (currentDeviation > deviation)
                {
                    deviation = currentDeviation;
                }
            }
            return result;
        }

        public IReadOnlyList<float[]> GetAmplitudeBands(int startSectorIndex, int endSectorIndex)
        {
            Contract.Assert(startSectorIndex >= 0 && endSectorIndex < Constants.BandSampleCount);
            Contract.Assert(startSectorIndex <= endSectorIndex);

            var bands = new float[_singleReceiverScanCount][];

            var scans = new Slice<IReceiverScan>(ReceiverScans, 0);

            for (var scanIndex = 0; scanIndex < bands.Length; ++scanIndex)
            {
                var amplitudes = new float[endSectorIndex - startSectorIndex + 1];
                scans.Index = scanIndex;

                for (var i = 0; i < amplitudes.Length; ++i)
                {
                    amplitudes[i] = scans.GetAmplitude(i + startSectorIndex);
                }
                bands[scanIndex] = amplitudes;
            }

            return bands;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public float[] GetPhaseDifferences(int scanIndex, int index)
        {
            var result = new float[Constants.PhasesDifferencesCount];
            var k = 0;
            for (var i = 0; i < Constants.DfReceiversCount; ++i)
            {
                var phasesI = _phases[i][scanIndex];
                for (var j = i + 1; j < Constants.DfReceiversCount; ++j)
                {
                    var phasesJ = _phases[j][scanIndex];
                    result[k++] = PhaseMath.NormalizePhase(phasesI[index] - phasesJ[index]);
                }
            }
            return result;
        }
    }
}

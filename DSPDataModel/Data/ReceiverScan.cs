﻿using System;
using Settings;

namespace DspDataModel.Data
{
    public class ReceiverScan : IReceiverScan
    {
        public float[] Amplitudes { get; }
        public float[] Phases { get; }
        public int BandNumber { get; }
        public DateTime CreationTime { get; }
        public int ScanIndex { get; }

        public int Count => Constants.BandSampleCount;

        public ReceiverScan(float[] amplitudes, float[] phases, int bandNumber, DateTime creationTime, int scanIndex)
        {
            Contract.Assert(amplitudes != null);
            Contract.Assert(phases != null);
            Contract.Assert(amplitudes.Length == phases.Length && amplitudes.Length == Constants.BandSampleCount);
            Contract.Assert(bandNumber >= 0);

            Amplitudes = amplitudes;
            Phases = phases;
            BandNumber = bandNumber;
            CreationTime = creationTime;
            ScanIndex = scanIndex;
        }
    }
}

﻿using System.Collections.Generic;

namespace DspDataModel.Data
{
    public interface IDataAggregator
    {
        void AddScan(IFpgaDataScan fpgaDataScan);
        void Clear();
        int DataCount { get; }
        IDataScan GetDataScan();
        IDataScan GetDataScan(int scanIndex);

        IAmplitudeScan GetSpectrum();
        IReadOnlyList<IDataScan> GetSpectrums();

        BandCalibrationResult GetCalibrationData(float stepKhz);
        BandCalibrationResult GetCalibrationDataBySignal(float startFrequencyKhz, float stepKhz, int scanCount);
    }
}

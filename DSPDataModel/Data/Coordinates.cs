﻿namespace DspDataModel.Data
{
    public interface ICoordinates
    {
        double Latitude { get; }
        double Longitude { get; }
    }

    public interface IPosition : ICoordinates
    {
        int Altitude { get; }
    }

    public struct Coordinates : ICoordinates
    {
        public double Latitude { get; }
        public double Longitude { get; }

        public Coordinates(double latitude, double longitude) : this()
        {
            Latitude = latitude;
            Longitude = longitude;
        }
    }

    public struct Position : IPosition
    {
        public double Latitude { get; }
        public double Longitude { get; }
        public int Altitude { get; }

        public Position(double latitude, double longitude, int attitude) : this()
        {
            Latitude = latitude;
            Longitude = longitude;
            Altitude = attitude;
        }
    }
}

﻿namespace DspDataModel.LinkedStation
{
    public enum StationRole
    {
        Standalone = 0,
        Master = 1,
        Slave = 2
    }

    public interface ILinkedStationManager : ILinkedStationClient, ILinkedStationServer
    {
        StationRole Role { get; set; }
        ConnectionState State { get; }
        void SetupAddresses(int ownAddress, int linkedAddress);
    }
}

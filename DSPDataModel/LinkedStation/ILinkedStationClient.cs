﻿using System.Collections.Generic;
using System.Threading.Tasks;
using DspDataModel.Storages;

namespace DspDataModel.LinkedStation
{
    public interface ILinkedStationClient : ILinkedStationBase
    {
        bool Connect(string hostname, int port);
        bool Connect(string comportName);
        Task<bool> GetFrsTargetsResponse(IReadOnlyCollection<IRadioSource> targets);
    }
}

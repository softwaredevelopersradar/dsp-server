﻿using System.Collections.Generic;
using System.Threading.Tasks;
using DspDataModel.RadioJam;
using DspDataModel.Storages;
using DspDataModel.Tasks;
namespace DspDataModel.LinkedStation
{
    public interface ILinkedStationServer : ILinkedStationBase
    {
        bool StartServer(string hostname, int port);
        bool StartServer(string comPortName);
        Task<bool> SetRangeSectors(RangeType rangesType, IEnumerable<Filter> filters);
        Task<bool> SetMode(DspServerMode mode);
        Task<bool> SetSpecialRanges(FrequencyType rangeType, IEnumerable<FrequencyRange> ranges);
        Task<bool> SetFrsJammingTargets(IReadOnlyCollection<IRadioJamTarget> targets);
        Task<bool> SetFhssJammingTargets(IReadOnlyCollection<IRadioJamFhssTarget> targets);
        Task<bool> GetFrsTargetsRequest();
    }
}

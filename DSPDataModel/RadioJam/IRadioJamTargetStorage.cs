﻿using System;
using System.Collections.Generic;

namespace DspDataModel.RadioJam
{
    public interface IRadioJamTargetStorage
    {
        IReadOnlyList<IRadioJamTarget> FrsTargets { get; }
        IReadOnlyList<IRadioJamFhssTarget> FhssTargets { get; }

        /// <summary>
        /// returns if targets changed
        /// </summary>
        bool UpdateFrsTargets(IReadOnlyList<IRadioJamTarget> targets);
        /// <summary>
        /// returns if targets changed
        /// </summary>
        bool UpdateFhssTargets(IReadOnlyList<IRadioJamFhssTarget> targets);

        void AddFrsTargets(IEnumerable<IRadioJamTarget> targets);
        event EventHandler<IReadOnlyList<IRadioJamTarget>> FrsTargetsUpdateEvent;
    }
}

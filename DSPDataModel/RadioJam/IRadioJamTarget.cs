using System;

namespace DspDataModel.RadioJam
{
    public enum TargetState
    {
        NotActive = 0,
        Active = 1,
        ActiveLongTime = 2,
        NotActiveLongTime = 3
    }

    public interface IRadioJamTarget : IEquatable<IRadioJamTarget>
    {
        float FrequencyKhz { get; set; }
        int Priority { get; }
        float Threshold { get; }
        float Direction { get; }
        bool UseAdaptiveThreshold { get; }
        int Id { get; }
        int Liter { get; }
        float Amplitude { get; set; }
        IRadioJamTargetConfig Config { get; }

        byte ModulationCode { get; }
        byte DeviationCode { get; }
        byte ManipulationCode { get; }
        byte DurationCode { get; }

        TargetState ControlState { get; }
        TargetState JamState { get; }
        bool IsEmitted { get; }

        /// <summary>
        /// returns if control state value changed
        /// </summary>
        bool UpdateControlState(bool isControlled);

        /// <summary>
        /// returns if jam state value changed
        /// </summary>
        bool UpdateJamState(bool isJammed);

        /// <summary>
        /// returns if emition state value changed
        /// </summary>
        bool UpdateEmitionState(bool isEmitted);

        void ClearStateFlags();

        /// <summary>
        /// Updates all fields except all state fields
        /// </summary>
        void UpdateTarget(IRadioJamTarget newValue);
    }

    public static class TargetStateExtensions
    {
        public static bool IsActive(this TargetState state)
        {
            return state == TargetState.Active || state == TargetState.ActiveLongTime;
        }
    }
}
﻿using System.Threading.Tasks;

namespace DspDataModel.RadioJam
{
    public enum RadioJamMode
    {
        Frs, // ФРЧ
        Afrs, // АПРЧ
        FrsAuto, // ФРЧ Авто
        Fhss,
        FhssDurationMeasurement //измерение длительности ППРЧ
    }

    public interface IRadioJamManager : IRadioJamTargetConfig
    {
        IRadioJamTargetStorage Storage { get; }
        IRadioJamTargetStorage LinkedStationStorage { get; }
        IRadioJamShaper Shaper { get; }
        RadioJamMode JamMode { get; }
        FftResolution FhssFftResoultion { get; set; }
        bool IsJammingActive { get; }
        int EmitDuration { get; set; }

        /// <summary>
        /// For Afrs and Frs Auto modes only
        /// </summary>
        int Threshold { get; set; }

        /// <summary>
        /// For Afrs mode only
        /// </summary>
        int DirectionSearchSector { get; set; }

        /// <summary>
        /// For Afrs mode only
        /// </summary>
        int FrequencySearchBandwidthKhz { get; set; }

        /// <summary>
        /// For Frs auto mode only
        /// </summary>
        float MinSignalBandwidthKhz { get; set; }

        /// jam duration in microseconds (10e-6)
        int FhssJamDuration { get; set; }
        int ChannelsInLiter { get; set; }
        int CheckEmitDelayMs { get; }
        int MinPowerLevel { get; }
        bool Connect();
        void Disconnect();
        IRadioJamTarget CreateRadioJamTarget(ISignal signal);
        void Start(RadioJamMode mode);
        Task Stop();
    }
}

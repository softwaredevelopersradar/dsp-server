﻿using System;
using System.Collections.Generic;

namespace DspDataModel.RadioJam
{
    public interface IRadioJamShaper
    {
        bool IsConnected { get; }
        bool Connect(string host, int port, string clientHost, int clientPort);
        void Disconnect();
        ShaperResponse StartFrsJamming(TimeSpan duration, IEnumerable<IRadioJamTarget> targets);
        ShaperResponse StartFhssJamming(uint fftCountCode, int duration, IEnumerable<IRadioJamFhssTarget> targets);
        ShaperResponse StartFhssDurationMeasurement(int duration, IRadioJamFhssTarget target);
        ShaperResponse<byte> GetState();
        ShaperResponse StopJamming();
        ShaperResponse<int[]> GetPower();

        event EventHandler<bool> ConnectionStateChangedEvent;
        event EventHandler ShaperNotRespondingEvent;
    }
}

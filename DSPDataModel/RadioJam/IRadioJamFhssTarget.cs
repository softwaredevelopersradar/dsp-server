using System;
using System.Collections.Generic;

namespace DspDataModel.RadioJam
{
    public interface IRadioJamFhssTarget : IEquatable<IRadioJamFhssTarget>
    {
        float MinFrequencyKhz { get; }
        float MaxFrequencyKhz { get; }
        float Threshold { get; }
        byte ModulationCode { get; }
        byte DeviationCode { get; }
        byte ManipulationCode { get; }
        IReadOnlyList<FrequencyRange> ForbiddenRanges { get; }
        int Id { get; }
    }
}
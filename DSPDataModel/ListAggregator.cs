﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;

namespace DspDataModel
{
    /// <inheritdoc />
    /// <summary>
    /// Provides access for ordered sequence of elements that stored in sequences located one after another
    /// </summary>
    public class ListAggregator<T> : IReadOnlyList<T>
    {
        public IReadOnlyList<T>[] Lists { get; }

        public int Count { get; }

        private readonly bool _allListsHaveSameSize;
        private readonly int _listSize;

        // constant is choosed according to benchmark
        private const int MaxListCountForLinearSearch = 30;

        private readonly bool _useLinearSearch;
        private int _lastRequestedListIndex;
        private readonly int[] _listCumulativeSizes;

        public ListAggregator(params IReadOnlyList<T>[] lists)
        {
            Lists = lists
                .Where(list => list != null && list.Any())
                .ToArray();

            _listCumulativeSizes = new int[Lists.Length + 1];
            
            _allListsHaveSameSize = true;
            _listSize = Lists[0].Count;
            _listCumulativeSizes[1] = Lists[0].Count;

            for (int i = 1; i < Lists.Length; ++i)
            {
                _listCumulativeSizes[i] = _listCumulativeSizes[i - 1] + Lists[i - 1].Count;
                if (Lists[i].Count != _listSize)
                {
                    _allListsHaveSameSize = false;
                }
            }
            _listCumulativeSizes[Lists.Length] = _listCumulativeSizes[Lists.Length - 1] + Lists[Lists.Length - 1].Count;

            Count = _listCumulativeSizes[_listCumulativeSizes.Length - 1];
            _useLinearSearch = Lists.Length <= MaxListCountForLinearSearch;
        }

        public IEnumerator<T> GetEnumerator()
        {
            foreach (var list in Lists)
            {
                foreach (var element in list)
                {
                    yield return element;
                }
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private T GetElementWithLinearSearch(int index)
        {
            var index2 = index - _listCumulativeSizes[_lastRequestedListIndex];
            if (0 <= index2 && index2 < Lists[_lastRequestedListIndex].Count)
            {
                return Lists[_lastRequestedListIndex][index2];
            }

            for (int i = 0; i < Lists.Length; ++i)
            {
                if (index < Lists[i].Count)
                {
                    _lastRequestedListIndex = i;
                    return Lists[i][index];
                }
                index -= Lists[i].Count;
            }
            throw new ArgumentException("index " + index +" is out of range!");
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private T GetElementWithBinarySearch(int index)
        {
            var index2 = index - _listCumulativeSizes[_lastRequestedListIndex];
            if (0 <= index2 && index2 < Lists[_lastRequestedListIndex].Count)
            {
                return Lists[_lastRequestedListIndex][index2];
            }

            var min = 0;
            var max = Lists.Length - 1;

            while (max - min > MaxListCountForLinearSearch)
            {
                var avg = (min + max) / 2;
                if (index - _listCumulativeSizes[avg] < 0)
                {
                    max = avg + 1;
                }
                else if (index - _listCumulativeSizes[avg] >= Lists[avg].Count)
                {
                    min = avg - 1;
                }
                else
                {
                    _lastRequestedListIndex = avg;
                    return Lists[avg][index - _listCumulativeSizes[avg]];
                }
            }
            index -= _listCumulativeSizes[min];
            for (int i = min; i <= max; ++i)
            {
                if (index < Lists[i].Count)
                {
                    _lastRequestedListIndex = i;
                    return Lists[i][index];
                }
                index -= Lists[i].Count;
            }
            throw new ArgumentException("index " + index + " is out of range!");
        }

        public T this[int index]
        {
            get
            {
                if (_allListsHaveSameSize)
                {
                    var listIndex = index / _listSize;
                    return Lists[listIndex][index % _listSize];
                }
                else
                {
                    if (_useLinearSearch)
                    {
                        return GetElementWithLinearSearch(index);
                    }
                    else
                    {
                        return GetElementWithBinarySearch(index);
                    }
                }
            }
        }
    }
}

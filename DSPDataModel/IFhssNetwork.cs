﻿using System;
using System.Collections.Generic;

namespace DspDataModel
{
    public interface IFhssNetwork
    {
        float MinFrequencyKhz { get; }
        float MaxFrequencyKhz { get; }
        float BandwidthKhz { get; }
        float StepKhz { get; }
        float Amplitude { get; }
        int FrequenciesCount { get; }
        float ImpulseDurationMs { get; }
        int Id { get; }
        bool IsActive { get; }
        bool IsDurationMeasured { get; }
        DateTime LastUpdateTime { get; }
        void Update(IFhssNetwork network);
        void RemeasureDuration();
        IReadOnlyList<float> Directions { get; }
        IReadOnlyList<FixedRadioSource> FixedRadioSources { get; }
    }

    /// <summary>
    /// structure holds data for Fhss module working fixed radio source
    /// </summary>
    public struct FixedRadioSource
    {
        public float FrequencyKhz { get; }
        public float Amplitude { get; }
        public float Direction { get; }
        public float Bandwidth { get; }

        public FixedRadioSource(float frequencyKhz, float amplitude, float direction, float bandwidth) : this()
        {
            FrequencyKhz = frequencyKhz;
            Amplitude = amplitude;
            Direction = direction;
            Bandwidth = bandwidth;
        }
    }
}

﻿using System.Collections.Generic;
using DspDataModel.Data;

namespace DspDataModel.Hardware
{
    public interface IFpgaDeviceManager
    {
        IReadOnlyList<IFpgaDevice> Devices { get; }
        FpgaMode Mode { get; }

        bool Initialize();
        IFpgaDataScan GetScan(IReadOnlyList<int> bandNumbers);
        ITransmissionChannel GetFpgaTransmissionChannel();
        FpgaOffsetsSetup OffsetsSetup { get; }
        FpgaDeviceBandsSettingsSource BandsSettingsSource { get; }
        void GetBlankScan();
        bool StartDevices();
        bool WaitData();
        bool StartFhssMode(FhssSetup setup);
        bool StopFhssMode();
        void Close();
        int GetDriverVersion();
    }

    public enum FpgaMode
    {
        Intelligence, Fhss
    }
}

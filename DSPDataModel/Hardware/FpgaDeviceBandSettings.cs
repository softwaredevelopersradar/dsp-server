﻿namespace DspDataModel.Hardware
{
    public class FpgaDeviceBandSettings
    {
        public float AmplifierLevel { get; }
        public float AttenuatorLevel { get; set; }
        public bool IsConstantAttenuatorEnabled { get; set; }
        public bool ShouldBeReversed { get; }

        public FpgaDeviceBandSettings(float amplifierLevel, float attenuatorLevel, bool isConstantAttenuatorEnabled, bool shouldBeReversed)
        {
            AmplifierLevel = amplifierLevel;
            AttenuatorLevel = attenuatorLevel;
            IsConstantAttenuatorEnabled = isConstantAttenuatorEnabled;
            ShouldBeReversed = shouldBeReversed;
        }

        public FpgaDeviceBandSettings Copy()
        {
            return new FpgaDeviceBandSettings(AmplifierLevel, AttenuatorLevel, IsConstantAttenuatorEnabled, ShouldBeReversed);
        }
    }
}

﻿using System;
using System.Runtime.InteropServices;

namespace DspDataModel.Hardware
{
    public enum DevFamily
    {
        Unknown = 0,
        Amc = 1,
        Vpx = 2,
        Cpci = 3,
        Comexpress = 4,
        Vpx2Fp1 = 0x102,
        Vpx2Fp2 = 0x112
    }

    public enum CmdStatus
    {
        Ok = 0,
        RebootReq
    }

    public enum DeviceType
    {
        No = 0,
        Xdsp = 1,
        Xpci = 2,
        Setpci = 3,
        Unknown = 4,
        Samc7134A250 = 0x1000,
        Svp7134A250 = 0x4A25,
        Svp7138A250 = 0x510C,
        Svp7131A2500 = 0x1001,
        Svp7131A5000 = 0x1A50,
        Xdsp5Mckm = 0x5AAC,
        Svp7134D250 = 0x4D25,
        Sfm4D1000 = 0x4D10,
        Sfm2A1800 = 0x2A18,
        Svp7132A10002D1000 = 0x2A2D,
        Sfm2A2502D1000 = 0x2A25,
        Samc706 = 0xA706,
        Fiocoms6 = 0x410C,
        Fmc30Rf = 0xF30F,
        Sfm2D2800 = 0x1D50,
        Vc707 = 0xE707,
        Gsd = 0x1111
    }

    [Serializable]
    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi, Size = 29)]
    public struct SetDevEnum
    {
        public uint DevFam;
        public uint DevType;
        public uint DevNum;
        public uint GA;

        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 13)]
        public string SN;
    }

    public enum RegSize
    {
        RsUnknown = 0,
        Rs8B = 1,
        Rs16B = 2,
        Rs32B = 4
    }

    public enum Endianness
    {
        Unset = 0,
        Unknown = 1,
        Le = 2,
        Be = 3
    }

    public enum DmaEngType
    {
        No = 0,
        Xpscdma,
        AxicdmaSimple,
        Axicdmasg
    }

    public enum WorkMode
    {
        Poll = 1,
        Intr = 2
    }

    public enum OpCode
    {
        ReadBRAM,
        ReadDDR,
        WriteDDR
    }

    public enum EngOptions
    {
        DataBufPcMem = 0x1,
        DataBufModBar = 0x2,
        AddrAutoInc = 0x4,
        AddrWrapAround = 0x8
    }

    [Serializable]
    [StructLayout(LayoutKind.Sequential, Pack = 1, CharSet = CharSet.Ansi, Size = 9)]
    public struct LocalDeviceIo
    {
        public byte Bar;
        public uint Offset;
        public uint Value;
    }

    [Serializable]
    [StructLayout(LayoutKind.Sequential, Pack = 2)]
    public struct PciConfig
    {
        public byte RevisionID;
        public ushort SubVendorID;
        public ushort SubSystemID;
    }

    [Serializable]
    [StructLayout(LayoutKind.Sequential, Pack = 1, CharSet = CharSet.Ansi, Size = 33)]
    public struct DmaInfo
    {
        public byte DmaEngTypeMax;
        public uint DmaBufAlignment;
        public uint DmaBufSize;
        public ulong DmaBufBaseLA;
        public ulong DevWindowBaseLA;
        public ulong InDevWindowOffsetLA;
    }

    [Serializable]
    [StructLayout(LayoutKind.Sequential, Pack = 1, CharSet = CharSet.Ansi, Size = 64)]
    public struct DmaConfigin
    {
        public CmdStatus Status;
        public byte DmaEngType;
        public byte WorkMode;
        public byte OpCode;
        public uint EngOps;
        public byte PCIBar;
        public uint PCIOff;
        public byte DMABar;
        public uint DMAOff;
        public uint AxiBarSize;
        public uint TDListAddr;
        public byte TDListBar;
        public uint TDListOff;
        public uint TDListMaxSize;
        public byte DmaBufNumber;
        public uint DmaBufAlignment;
        public uint DmaSimpleTransferMaxSize;
        public uint DmaSGTransferMaxSize;
        public byte BarForDataBuf;
        public uint FromAddr;
        public uint ToAddr;
        public uint Length;
    }

    public enum ProcessingStatus : uint
    {
        NotFinished = 0,
        FftReady = 1,
        Accepted = 2,
        Finished = 0x80000000
    }

    public enum TypePt0
    {
        Notype,
        NpiStub = 0x0001,
        FreqMet,
        InterfaceTbl,
        AxiPcieCtrl,
        AxiCdma,
        Adc5G,
        Ad9129,
        Afe7225,
        Adc2A1G8,
        Ad9467,
        Dac5681Z,
        Sysmon,
        SfmGpio,
        Ads5400,
        LedGpio,
        MainFpgaRegs,
        CarierIfTbl,
        AxiDma,
        DmaBdBuf,
        AxisChecker,
        AxiI2C,
        AxiEmc,
        Fmc_2A2DSubsys,
        Fmc_4A25Subsys,
        Fmc_4D10Subsys,
        Hwicap,
        BufioCtrl,
        SwGpio
    }

    public enum PcieCtrlE
    { //tpt1 for tpt0_axi_pcie_ctrl
        PcieCtrlDev0PciBar0Offset = 0x0001,
        PcieCtrlDev0PciBar1Offset,
        PcieCtrlDev0PciBar2Offset,
        PcieCtrlDev0PciBar3Offset,
        PcieCtrlDev0PciBar4Offset,
        PcieCtrlDev0PciBar5Offset,
        PcieCtrlDev1PciBar0Offset,
        PcieCtrlDev1PciBar1Offset,
        PcieCtrlDev1PciBar2Offset,
        PcieCtrlDev1PciBar3Offset,
        PcieCtrlDev1PciBar4Offset,
        PcieCtrlDev1PciBar5Offset,
        PcieCtrlDev0PciBar0Width,
        PcieCtrlDev0PciBar1Width,
        PcieCtrlDev0PciBar2Width,
        PcieCtrlDev0PciBar3Width,
        PcieCtrlDev0PciBar4Width,
        PcieCtrlDev0PciBar5Width,
        PcieCtrlDev1PciBar0Width,
        PcieCtrlDev1PciBar1Width,
        PcieCtrlDev1PciBar2Width,
        PcieCtrlDev1PciBar3Width,
        PcieCtrlDev1PciBar4Width,
        PcieCtrlDev1PciBar5Width,
        PcieCtrlDev0AxiBar0Offset,
        PcieCtrlDev0AxiBar0Size,
        PcieCtrlDev1AxiBar0Offset,
        PcieCtrlDev1AxiBar0Size,
        PcieCtrlDev0Base,
        PcieCtrlDev1Base,
    }

    // команды для смены режимов платы ЦОС
    public enum AdcCommands
    {
        // управление количеством точек бпф
        N16384 = 2,//16к точек
        N8192 = 3,//8к точек
        N4096 = 4,//4к точек
        LoadMask = 14,// загрузка маски для канала ППРЧ
        LoadThreshold = 16,// загрузка порога для канала ППРЧ (линейный масштаб)
        NetInit = 18// инициализация сети на плате ЦОС (192.168.100.104)
    }

    public enum TactingMode
    {
        External = 12,// включение внешнего тактирования (перед включением убедится что внешнеетактирование есть!!)
        Internal = 13,// включение внутреннего тактирования (стоит по уполмочанию)
    }

    public enum ChannelMode
    {
        Intelligence = 0, Fhss = 1
    }

    public static class HardwareDataStructuresExtensions
    {
        public static int GetSampleCount(this FftResolution self)
        {
            switch (self)
            {
                case FftResolution.N16384:
                    return 16384;
                case FftResolution.N8192:
                    return 8192;
                case FftResolution.N4096:
                    return 4096;
                default:
                    throw new ArgumentException($"Unknown FftPointsCount value: {self}");
            }
        }
    }
}

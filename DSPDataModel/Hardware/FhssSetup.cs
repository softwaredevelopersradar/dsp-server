using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using DspDataModel.RadioJam;
using Settings;

namespace DspDataModel.Hardware
{
    public struct FhssReceiverSetup
    {
        public int ReceiverStartFrequencyKhz { get; }
        public int ReceiverCentralFrequencyKhz => ReceiverStartFrequencyKhz + Constants.BandwidthKhz / 2;

        public bool ShouldBeReversed { get; }

        public FhssReceiverSetup(int receiverStartFrequencyKhz, bool shouldBeReversed) : this()
        {
            ReceiverStartFrequencyKhz = receiverStartFrequencyKhz;
            ShouldBeReversed = shouldBeReversed;
        }
    }

    public class FhssSetup
    {
        public FftResolution FftSize { get; }
        public IReadOnlyList<float> Thresholds { get; }
        public IReadOnlyList<FhssReceiverSetup> ReceiverSettings { get; }
        public IReadOnlyList<IRadioJamFhssTarget> Targets { get; }

        public FhssSetup(FftResolution fftSize, IReadOnlyList<float> thresholds, IReadOnlyList<IRadioJamFhssTarget> targets, IReadOnlyList<FhssReceiverSetup> receiverSettings)
        {
            FftSize = fftSize;
            Thresholds = thresholds;
            Targets = targets;

            if (receiverSettings.Count > Constants.FhssChannelsCount)
            {
                throw new ArgumentException($"Can't create fhss setup with more than {Constants.FhssChannelsCount} band numbers");
            }
            ReceiverSettings = receiverSettings;
        }

        public uint GetFpgaDeviceThreshold(int channelIndex)
        {
            if (channelIndex >= Thresholds.Count)
            {
                return 0;
            }
            return (uint) (Thresholds[channelIndex] - Constants.ReceiverMinAmplitude);
        }

        public uint[] GetMask(int channelIndex)
        {
            var target = Targets[channelIndex];

            int resultArrayLength = FftSize.GetSampleCount();
            var bandPointCount = Utilities.GetBandSamplesCount(resultArrayLength);
            var arrayOffset = (resultArrayLength - bandPointCount) / 2;
            var arrayMaxVisibleIndex = arrayOffset + bandPointCount;
            var pointsGapKhz = 1f * Constants.ReceiverBandwidthKhz / (resultArrayLength - 1);

            if (channelIndex >= ReceiverSettings.Count)
            {
                return new uint[resultArrayLength];
            }

            var bandSetup = ReceiverSettings[channelIndex];

            var startFrequencyKhz = bandSetup.ReceiverStartFrequencyKhz;
            var endFrequencyKhz = startFrequencyKhz + Constants.BandwidthKhz;

            var lastNetworkSampleIndex = arrayOffset + GetSampleNumber(startFrequencyKhz, target.MaxFrequencyKhz);
            if (lastNetworkSampleIndex > arrayMaxVisibleIndex)
            {
                lastNetworkSampleIndex = arrayMaxVisibleIndex;
            }

            var mask = new BitArray(resultArrayLength, defaultValue: false);

            for (var i = arrayOffset; i < lastNetworkSampleIndex; ++i)
            {
                mask[i] = true;
            }

            // TODO:  remove it when Petya adds hardware realization
            var centerStartIndex = resultArrayLength / 2 - Constants.CenterPeakSampleWidth / 2;
            var centerEndIndex = resultArrayLength / 2 + Constants.CenterPeakSampleWidth / 2;
            for (var i = centerStartIndex; i < centerEndIndex; ++i)
            {
                mask[i] = false;
            }

            foreach (var range in target.ForbiddenRanges)
            {
                // whole band is forbidden
                if (range.StartFrequencyKhz < startFrequencyKhz && endFrequencyKhz < range.EndFrequencyKhz)
                {
                    return new uint[resultArrayLength];
                }
                if (range.StartFrequencyKhz >= startFrequencyKhz && endFrequencyKhz < range.EndFrequencyKhz)
                {
                    var offset = arrayOffset + GetSampleNumber(startFrequencyKhz, range.StartFrequencyKhz);
                    for (var i = offset; i < arrayMaxVisibleIndex; ++i)
                    {
                        mask[i] = false;
                    }
                }
                else if (range.StartFrequencyKhz < startFrequencyKhz && endFrequencyKhz >= range.EndFrequencyKhz)
                {
                    var endOffset = arrayOffset + GetSampleNumber(startFrequencyKhz, range.EndFrequencyKhz);
                    for (var i = arrayOffset; i <= endOffset; ++i)
                    {
                        mask[i] = false;
                    }
                }
                else
                {
                    var startOffset = arrayOffset + GetSampleNumber(startFrequencyKhz, range.StartFrequencyKhz);
                    var endOffset = arrayOffset + GetSampleNumber(startFrequencyKhz, range.EndFrequencyKhz);
                    for (var i = startOffset; i <= endOffset; ++i)
                    {
                        mask[i] = false;
                    }
                }
            }

            if (!bandSetup.ShouldBeReversed)
            {
                ReverseMask();
            }
            SwapMaskParts();

            var result = new int[resultArrayLength];
            mask.CopyTo(result, 0);
            return result.Select(i => (uint) i).ToArray();

            void ReverseMask()
            {
                var length = mask.Count - 1;
                var to = mask.Count / 2;
                for (var i = 0; i < to; ++i)
                {
                    var elementI = mask[i];
                    mask[i] = mask[length - i];
                    mask[length - i] = elementI;
                }
            }

            void SwapMaskParts()
            {
                var to = mask.Count / 2;
                for (var i = 0; i < to; ++i)
                {
                    var elementI = mask[i];
                    mask[i] = mask[to + i];
                    mask[to + i] = elementI;
                }
            }

            int GetSampleNumber(float networkStartFrequencyKhz, float frequencyKhz)
            {
                return Utilities.GetSamplesCount(networkStartFrequencyKhz, frequencyKhz, pointsGapKhz);
            }
        }
    }
}
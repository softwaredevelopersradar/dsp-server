﻿using System.Collections.Generic;

namespace DspDataModel.DataProcessor
{
    public struct ProcessResult
    {
        public IReadOnlyList<ISignal> Signals { get; }

        /// <summary>
        /// Not merged signals from subscans. They Are used for Fhss analysis
        /// </summary>
        public IReadOnlyList<ISignal> RawSignals { get; }

        public ProcessResult(IReadOnlyList<ISignal> signals, IReadOnlyList<ISignal> rawSignals = null) : this()
        {
            Signals = signals;
            RawSignals = rawSignals ?? signals;
        }
    }
}
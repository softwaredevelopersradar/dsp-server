﻿using DspDataModel.Correlator;
using DspDataModel.Data;

namespace DspDataModel.DataProcessor
{
    public class DataProcessorMock : IDataProcessor
    {
        public IPhaseCorrelator PhaseCorrelator { get; }
        public IScanReadTimer ScanReadTimer { get; set; }

        public DataProcessorMock()
        {
            PhaseCorrelator = new PhaseCorrelatorMock();
        }

        public ProcessResult GetSignals(IDataScan dataScan, ScanProcessConfig config)
        {
            return new ProcessResult(new ISignal[0], new ISignal[0]);
        }

        public void ReloadRadioPathTable(string radioPathTableFilename)
        {
        }

        public void SetCorrelationType(int type)
        {
        }
    }
}

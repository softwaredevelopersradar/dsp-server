﻿using DspDataModel.Correlator;
using DspDataModel.Data;

namespace DspDataModel.DataProcessor
{
    public interface IDataProcessor
    {
        IPhaseCorrelator PhaseCorrelator { get; }
        IScanReadTimer ScanReadTimer { get; set; }

        ProcessResult GetSignals(IDataScan dataScan, ScanProcessConfig config);

        void ReloadRadioPathTable(string radioPathTableFilename);

        //TODO: Remove this method after test
        void SetCorrelationType(int type);
    }

    public interface IScanReadTimer
    {
        float ScanReadTimeMs { get; }
    }
}

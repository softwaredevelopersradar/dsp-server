﻿using System.Collections.Generic;

namespace DspDataModel.Storages
{
    /// <summary>
    /// Storage of impulse signals
    /// </summary>
    public interface ISignalStorage
    {
        void Put(IEnumerable<ISignal> signals);
        void Clear();
        IEnumerable<ImpulseSignal> GetSignals();
    }
}
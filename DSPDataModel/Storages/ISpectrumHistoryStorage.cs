﻿using System;
using System.Collections.Generic;
using System.Linq;
using DspDataModel.Data;
using Settings;

namespace DspDataModel.Storages
{
    public interface ISpectrumHistoryStorage
    {
        SpectrumHistory GetSpectrumHistory(float startFrequencyKhz, float endFrequencyKhz, int pointCount, TimeSpan timeSpan);
        void StopSpectrumUpdate();
        TimeSpan Rate { get; set; }
        TimeSpan HistoryLength { get; set; }
    }

    public class SpectrumHistory
    {
        public IReadOnlyList<IAmplitudeBand> Bands { get; }
        public TimeSpan Rate { get; }

        public SpectrumHistory(IReadOnlyList<IAmplitudeBand> bands, TimeSpan rate)
        {
            Bands = bands;
            Rate = rate;
        }

        public static SpectrumHistory CreateEmpty(int bandsCount, int bandSampleCount, TimeSpan rate)
        {
            var emptyBand = Enumerable.Repeat(Constants.ReceiverMinAmplitude, bandSampleCount).ToArray();

            var bands = Enumerable.Repeat(emptyBand, bandsCount)
                .Select(band => new AmplitudeBand(band) as IAmplitudeBand)
                .ToArray();

            return new SpectrumHistory(bands, rate);
        }
    }
}

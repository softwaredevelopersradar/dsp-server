﻿using System;

namespace DspDataModel.Storages
{
    public enum RadioSourceType
    {
        Normal, Important, Forbidden, Known
    }

    public interface IRadioSource : ISignal
    {
        int Id { get; }
        bool IsNew { get; }
        bool IsActive { get; }
        DateTime BroadcastStartTime { get; }
        DateTime FirstBroadcastStartTime { get; }
        RadioSourceType SourceType { get; }

        float LinkedDirection { get; }
        float LinkedReliability { get; }
        float GetIntersectionBandwidth();

        double Latitude { get; }
        double Longitude { get; }

        bool IsSameSource(ISignal signal);
        bool IsSameLinkedStationSource(ISignal signal);
        void Update(ISignal signal);
        void Update();

        void UpdateLinkedStationDirection(float direction, float reliability);
    }
}

using System.Collections.Generic;

namespace DspDataModel.Storages
{
    public interface IFhssNetworkStorage
    {
        void Put(IEnumerable<IFhssNetwork> networks);
        void Clear();
        IEnumerable<IFhssNetwork> GetFhssNetworks();

        IReadOnlyCollection<IFhssNetwork> HidedFhssNetworks { get; }
        void PerformAction(SignalAction action, int[] signalsId);
    }
}
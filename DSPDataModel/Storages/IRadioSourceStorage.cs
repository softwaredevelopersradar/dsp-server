﻿using System.Collections.Generic;

namespace DspDataModel.Storages
{
    public interface IRadioSourceStorage
    {
        void Put(IEnumerable<ISignal> signals);
        void PutLinkedStationSignals(IEnumerable<ISignal> signals);
        IEnumerable<IRadioSource> GetRadioSources();
        bool HasSameRadioSource(ISignal signal);
        void Clear();
        int? GetSignalId(ISignal signal);
        IRadioSource FindSameRadioSource(ISignal signal);

        IReadOnlyCollection<IRadioSource> HidedRadioSources { get; }
        void PerformAction(SignalAction action, int[] signalsId);
    }
}

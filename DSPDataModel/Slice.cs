﻿using System.Collections;
using System.Collections.Generic;

namespace DspDataModel
{
    public struct Slice<T> : IReadOnlyList<T>
    {
        public readonly IReadOnlyList<IReadOnlyList<T>> Lists;
        public int Index;

        public int Count => Lists.Count;

        public Slice(IReadOnlyList<IReadOnlyList<T>> lists, int index)
        {
            Lists = lists;
            Index = index;
        }

        public IEnumerator<T> GetEnumerator()
        {
            foreach (var list in Lists)
            {
                yield return list[Index];
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public T this[int i] => Lists[i][Index];
    }
}

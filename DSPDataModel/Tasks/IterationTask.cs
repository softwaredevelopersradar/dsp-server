﻿namespace DspDataModel.Tasks
{
    /// <summary>
    /// Task for each receiver for current iteration
    /// </summary>
    public struct IterationTask
    {
        public int ReceiverNumber { get; set; }
        public int BandNumber { get; set; }

        public IterationTask(int receiverNumber, int bandNumber) : this()
        {
            ReceiverNumber = receiverNumber;
            BandNumber = bandNumber;
        }
    }
}

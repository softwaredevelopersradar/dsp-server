﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Settings;

namespace DspDataModel.Tasks
{
    public interface IModeController
    {
        ITaskManager TaskManager { get; }

        /// <summary>
        /// Scan speed In GHz per sec
        /// </summary>
        float ScanSpeed { get; }

        Task Initialize();
        void OnActivated();
        void OnStop();
        IEnumerable<IReceiverTask> CreateTasks();
    }

    public enum DspServerMode
    {
        Stop,
        RadioIntelligence,
        RadioIntelligenceWithDf,
        RadioJammingFrs,
        RadioJammingAfrs,
        RadioJammingFrsAuto,
        RadioJammingFhss,
        Calibration,
        FhssDurationMeasuring
    }

    public static class DspServerModeExtensions
    {
        public static bool IsRadioJamMode(this DspServerMode mode)
        {
            return mode == DspServerMode.RadioJammingFrs || 
                   mode == DspServerMode.RadioJammingAfrs || 
                   mode == DspServerMode.RadioJammingFrsAuto || 
                   mode == DspServerMode.RadioJammingFhss ||
                   mode == DspServerMode.FhssDurationMeasuring;
        }

        public static bool IsIntelligenceMode(this DspServerMode mode)
        {
            return mode == DspServerMode.RadioIntelligence ||
                   mode == DspServerMode.RadioIntelligenceWithDf;
        }

        private static float UpdateScanSpeed(this IModeController mode, float currentScanSpeed)
        {
            const float maxScanSpeedGhzSec = 1000;
            if (currentScanSpeed < 0 || currentScanSpeed > maxScanSpeedGhzSec)
            {
                return mode.ScanSpeed;
            }
            // ReSharper disable once CompareOfFloatsByEqualityOperator
            if (mode.ScanSpeed == 0)
            {
                return currentScanSpeed;
            }

            const float currentScanSpeedFactor = 0.5f;
            return mode.ScanSpeed * (1 - currentScanSpeedFactor)
                   + currentScanSpeed * currentScanSpeedFactor;
        }

        public static float UpdateScanSpeed(this IModeController mode, int objectiveCount, ref DateTime lastCycleEndTime)
        {
            var now = DateTime.UtcNow;
            var cycleTimeSpan = now - lastCycleEndTime;
            lastCycleEndTime = now;
            var currentScanSpeed = objectiveCount * Constants.BandwidthKhz /
                                   (1000 * cycleTimeSpan.TotalMilliseconds);
            return UpdateScanSpeed(mode, (float)currentScanSpeed);
        }
    }
}

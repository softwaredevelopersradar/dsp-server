﻿using System;
using System.Collections.Generic;
using DspDataModel.DataProcessor;
using DspDataModel.Hardware;
using DspDataModel.Storages;

namespace DspDataModel.Tasks
{
    public interface ITaskManager
    {
        IModeController CurrentMode { get; }
        IHardwareController HardwareController { get; }
        IDataProcessor DataProcessor { get; }
        IFilterManager FilterManager { get; }
        IFilterManager LinkedStationFilterManager { get; }
        IRadioSourceStorage RadioSourceStorage { get; }
        float ScanSpeed { get; }

        // Storage with all signals for fhss searching
        ISignalStorage SignalStorage { get; }
        ISpectrumStorage SpectrumStorage { get; }
        IFhssNetworkStorage FhssNetworkStorage { get; }
        ISpectrumHistoryStorage SpectrumHistoryStorage { get; }
        IReadOnlyList<IReceiverTask> Tasks { get; }

        IFilterManager GetFilterManager(TargetStation station);

        event EventHandler FhssDurationMeasuringStartEvent;

        bool Initialize();
        void SetMode<TMode>(TMode modeController) where TMode : IModeController;
        void AddTask(IReceiverTask task);
        void AddTasks(IEnumerable<IReceiverTask> tasks);
        void RemoveTask(IReceiverTask task);
        void ClearTasks();
    }
}

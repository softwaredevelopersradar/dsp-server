﻿using System;
using System.Collections.Generic;
using DspDataModel.Storages;
using Settings;

namespace DspDataModel
{
    public interface ISignal
    {
        float FrequencyKhz { get; }
        float CentralFrequencyKhz { get; }
        float Direction { get; }
        float Reliability { get; } // number from 0 to 1 that identifies how reliable direction is
        float BandwidthKhz { get; }
        float Amplitude { get; }
        float StandardDeviation { get; }
        float DisardedDirectionsPart { get; }
        float PhaseDeviation { get; }
        SignalModulation Modulation { get; }
        IReadOnlyList<float> Phases { get; }
        TimeSpan BroadcastTimeSpan { get; }

        // number from 0 to 1 where
        // 1 means that the signal was found in all subscans during last scan
        // 0.3 - in 30% of sub scans and so on
        float RelativeSubScanCount { get; } 
    }

    public static class SignalExtensions
    {
        public static float LeftFrequency(this ISignal self)
        {
            return self.CentralFrequencyKhz - self.BandwidthKhz / 2;
        }

        public static float RightFrequency(this ISignal self)
        {
            return self.CentralFrequencyKhz + self.BandwidthKhz / 2;
        }

        public static FrequencyRange GetFrequencyRange(this ISignal self)
        {
            return new FrequencyRange(self.LeftFrequency(), self.RightFrequency());
        }

        public static bool IsDirectionReliable(this ISignal self)
        {
            return self.StandardDeviation < 30 && self.Reliability > Constants.ReliabilityThreshold;
        }

        public static float Distance(ISignal s1, ISignal s2)
        {
            var left1 = s1.LeftFrequency();
            var right1 = s1.RightFrequency();

            var left2 = s2.LeftFrequency();
            var right2 = s2.RightFrequency();

            return Distance(left1, right1, left2, right2);
        }

        private static float Distance(float left1, float right1, float left2, float right2)
        {
            // checking intesection of signals
            if (left1 <= left2 && left2 <= right1)
            {
                return 0;
            }
            if (left1 <= right2 && right2 <= right1)
            {
                return 0;
            }
            if (left2 <= left1 && left1 <= right2)
            {
                return 0;
            }
            if (left2 <= right1 && right1 <= right2)
            {
                return 0;
            }

            return right1 < left2 
                ? left2 - right1 
                : right1 - left2;
        }

        public static float Distance(FrequencyRange range1, FrequencyRange range2)
        {
            var left1 = range1.StartFrequencyKhz;
            var right1 = range1.EndFrequencyKhz;

            var left2 = range2.StartFrequencyKhz;
            var right2 = range2.EndFrequencyKhz;

            return Distance(left1, right1, left2, right2);
        }

        public static float GetMergeFactor(this ISignal signal)
        {
            return GetMergeFactor(signal.Reliability);
        }

        /// <summary>
        /// Reliability in range from 0 to 1
        /// </summary>
        public static float GetMergeFactor(float reliability)
        {
            Contract.Assert(0 <= reliability && reliability <= 1);

            if (reliability < Constants.ReliabilityThreshold)
            {
                // by returning very small but non-zero result we evade merging errors where all factors are zeros
                return 0.001f;
            }
            const float multiplier = 1 / ((1 - Constants.ReliabilityThreshold) * (1 - Constants.ReliabilityThreshold));
            var x = reliability - Constants.ReliabilityThreshold;
            return multiplier * x * x;
        }

        public static float GetMergeFactor(float reliability, float amplitude, float threshold)
        {
            var amplitudeFactor = amplitude - threshold;
            if (amplitudeFactor < 0)
            {
                // by returning very small but non-zero result we evade merging errors where all factors are zeros
                return 0.001f;
            }
            return amplitudeFactor * GetMergeFactor(reliability);
        }

        public static float GetMergeFactor(this ISignal signal, float threshold)
        {
            return GetMergeFactor(signal.Reliability, signal.Amplitude, threshold);
        }

        public static bool HasSameBandwidth(ISignal s1, ISignal s2)
        {
            return AreSameBandwidths(s1.BandwidthKhz, s2.BandwidthKhz);
        }

        public static bool AreSameBandwidths(float bandwidth1, float bandwidth2)
        {
            const float maxBandwidthRatio = 0.5f;
            const float straightSignalBandwidth = 100;
            const float straightSignalMaxGap = 100;

            var maxBandwidth = Math.Max(bandwidth1, bandwidth2);
            var minBandwidth = Math.Min(bandwidth1, bandwidth2);

            if (minBandwidth < straightSignalBandwidth)
            {
                return minBandwidth / maxBandwidth < maxBandwidthRatio;
            }
            return maxBandwidth - minBandwidth < straightSignalMaxGap;
        }

        public static float GetMaxGap(ISignal signal1, ISignal signal2, float defaultSignalsGapKhz)
        {
            var minBandwidth = Math.Min(signal1.BandwidthKhz, signal2.BandwidthKhz);
            float gap = defaultSignalsGapKhz;

            // TODO: move this params to config
            const float bandwidthToGapMultiplier = 0.25f;
            const float maxGapKhz = 300;

            if (minBandwidth * bandwidthToGapMultiplier > gap)
            {
                gap = minBandwidth * bandwidthToGapMultiplier;
            }
            if (gap > maxGapKhz)
            {
                gap = maxGapKhz;
            }
            return gap;
        }

        public static bool AreRangesIntersected(ISignal signal1, ISignal signal2, float defaultSignalsGapKhz)
        {
            var gap = GetMaxGap(signal1, signal2, defaultSignalsGapKhz);
            return Distance(signal1, signal2) < gap;
        }

        public static DateTime LastActiveTime(this IRadioSource radioSource)
        {
            return radioSource.BroadcastStartTime + radioSource.BroadcastTimeSpan;
        }
    }
}

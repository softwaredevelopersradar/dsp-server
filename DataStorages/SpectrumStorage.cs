﻿using System;
using System.Collections.Generic;
using Settings;
using DspDataModel;
using DspDataModel.Data;
using DspDataModel.Storages;

namespace DataStorages
{
    public class SpectrumStorage : ISpectrumStorage
    {
        private readonly List<IAmplitudeScan> _scans;

        public SpectrumStorage()
        {
            _scans = new List<IAmplitudeScan>(Config.Instance.BandSettings.BandCount);

            var defaultAmplitudes = new float[Constants.BandSampleCount];
            for (var i = 0; i < defaultAmplitudes.Length; ++i)
            {
                defaultAmplitudes[i] = Constants.ReceiverMinAmplitude;
            }
            var now = DateTime.UtcNow;

            for (var i = 0; i < Config.Instance.BandSettings.BandCount; ++i)
            {
                _scans.Add(new AmplitudeScan(defaultAmplitudes, i, now, 0));
            }
        }

        public void Put(IAmplitudeScan scan)
        {
            _scans[scan.BandNumber] = scan;
        }

        public IAmplitudeBand GetSpectrum(int bandNumber)
        {
            return _scans[bandNumber];
        }

        public IAmplitudeBand GetSpectrum(float startFrequencyKhz, float endFrequencyKhz, int pointCount)
        {
            return GetSpectrum(_scans, startFrequencyKhz, endFrequencyKhz, pointCount);
        }

        private IAmplitudeBand GetSpectrum<T>(IReadOnlyList<T> dataList, float startFrequencyKhz, float endFrequencyKhz, int pointCount) where T : IAmplitudeBand
        {
            var aggregator = GetAggregatedData(dataList, startFrequencyKhz, endFrequencyKhz);
            return aggregator.StrechSpectrum(pointCount);
        }

        private ArraySegment<float> GetSegment(IReadOnlyList<float> scan, int offset, int count)
        {
            return new ArraySegment<float>((float[]) scan, offset, count);
        }

        private IReadOnlyList<float> GetAggregatedData<T>(IReadOnlyList<T> dataList, float startFrequency, float endFrequency) where T : IAmplitudeBand
        {
            var bandStartIndex = Utilities.GetBandNumber(startFrequency);
            var bandEndIndex = Utilities.GetBandNumber(endFrequency);

            var start = Utilities.GetSampleNumber(startFrequency);
            var end = Utilities.GetSampleNumber(endFrequency);

            if (end == 0)
            {
                bandEndIndex--;
                end = Constants.BandSampleCount - 1;
            }

            if (bandStartIndex == bandEndIndex)
            {
                return GetSegment(dataList[bandStartIndex].Amplitudes, start, end - start + 1);
            }
            else
            {
                var arrays = new IReadOnlyList<float>[bandEndIndex - bandStartIndex + 1];
                arrays[0] = GetSegment(dataList[bandStartIndex].Amplitudes, start, Constants.BandSampleCount - start);
                arrays[arrays.Length - 1] = GetSegment(dataList[bandEndIndex].Amplitudes, 0, end + 1);
                for (int i = 1; i < arrays.Length - 1; ++i)
                {
                    arrays[i] = dataList[bandStartIndex + i].Amplitudes;
                }
                return new ListAggregator<float>(arrays);
            }
        }
    }
}

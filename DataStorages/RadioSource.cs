﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using DspDataModel;
using DspDataModel.Storages;
using Phases;
using Settings;
using TimeQueue;

namespace DataStorages
{
    public class RadioSource : IRadioSource
    {
        public float FrequencyKhz { get; private set; }
        public float CentralFrequencyKhz { get; private set; }
        public float Direction { get; private set; }
        public float Reliability { get; private set; }
        public float BandwidthKhz { get; private set; }
        public float StandardDeviation { get; private set; }
        public float DisardedDirectionsPart { get; private set; }
        public float PhaseDeviation { get; private set; }
        public float Amplitude { get; private set; }
        public int Id { get; }
        public bool IsNew { get; private set; }
        public bool IsActive { get; private set; }
        public DateTime BroadcastStartTime { get; private set; }
        public DateTime FirstBroadcastStartTime { get; }
        public TimeSpan BroadcastTimeSpan { get; private set; }

        /// <summary>
        /// This field is used only for Signals, not for RadioSources
        /// </summary>
        public float RelativeSubScanCount { get; }
        public IReadOnlyList<float> Phases { get; }

        public RadioSourceType SourceType { get; }

        public float LinkedDirection { get; private set; }
        public float LinkedReliability { get; private set; } = 0;
        
        /// <summary>
        /// returns intersection of all saved bandwidths for radio source
        /// </summary>
        public float GetIntersectionBandwidth()
        {
            var maxLeftFrequency = _dataQueue.Max(s => s.CentralFrequencyKhz - s.BandwidthKhz * 0.5f);
            var minRightFrequency = _dataQueue.Min(s => s.CentralFrequencyKhz + s.BandwidthKhz * 0.5f);
            if (maxLeftFrequency >= minRightFrequency)
            {
                return 0;
            }
            return minRightFrequency - maxLeftFrequency;
        }

        public double Latitude { get; private set; } = -1;
        public double Longitude { get; private set; } = -1;

        public SignalModulation Modulation { get; private set; }

        private static int _idCounter = 0;

        public float LeftFrequencyKhz => CentralFrequencyKhz - BandwidthKhz / 2;

        public float RightFrequencyKhz => CentralFrequencyKhz + BandwidthKhz / 2;

        private readonly ConcurrentTimeQueue<SourceInfo> _dataQueue;

        public RadioSource(ISignal signal, int id)
            : this(signal, Config.Instance.StoragesSettings.MaxRadioSourceDataCount, Config.Instance.StoragesSettings.MaxRadioSourceDataTimeSpan)
        {
            Id = id;
            // restore counter value after the other constructor
            Interlocked.Decrement(ref _idCounter);
        }

        public RadioSource(ISignal signal)
            : this(signal, Config.Instance.StoragesSettings.MaxRadioSourceDataCount, Config.Instance.StoragesSettings.MaxRadioSourceDataTimeSpan)
        { }

        public RadioSource(ISignal signal, int maxHistoryRadioSourceCount, TimeSpan maxHistoryRadioSourceTimeSpan)
        {
            Id = Interlocked.Increment(ref _idCounter);
            FrequencyKhz = signal.FrequencyKhz;
            CentralFrequencyKhz = signal.CentralFrequencyKhz;
            Direction = signal.Direction;
            StandardDeviation = signal.StandardDeviation;
            BandwidthKhz = signal.BandwidthKhz;
            Amplitude = signal.Amplitude;
            Reliability = signal.Reliability;
            DisardedDirectionsPart = signal.DisardedDirectionsPart;
            Modulation = signal.Modulation;
            PhaseDeviation = signal.PhaseDeviation;
            IsActive = true;
            IsNew = true;

            var now = DateTime.Now;
            FirstBroadcastStartTime = now;
            BroadcastStartTime = now;
            BroadcastTimeSpan = TimeSpan.Zero;
            Phases = signal.Phases;
            
            SourceType = RadioSourceType.Normal;

            _dataQueue = new ConcurrentTimeQueue<SourceInfo>(maxHistoryRadioSourceTimeSpan, maxHistoryRadioSourceCount);
            Update(signal);
        }

        public RadioSource(ISignal signal, int id, float linkedDirection, double latitude, double longitude, DateTime broadcastStartTime)
            : this(signal, Config.Instance.StoragesSettings.MaxRadioSourceDataCount, Config.Instance.StoragesSettings.MaxRadioSourceDataTimeSpan)
        {
            Id = id;
            LinkedDirection = linkedDirection;
            Latitude = latitude;
            Longitude = longitude;
            BroadcastStartTime = broadcastStartTime;
            // restore counter value after the other constructor
            Interlocked.Decrement(ref _idCounter);
        }

        public bool IsSameSource(ISignal signal)
        {
            var config = Config.Instance.DirectionFindingSettings;

            if (SignalExtensions.AreRangesIntersected(this, signal, config.SignalsGapKhz))
            {
                if (signal.IsDirectionReliable() && this.IsDirectionReliable())
                {
                    return PhaseMath.Angle(Direction, signal.Direction) < config.SignalsMergeDirectionDeviation;
                }
                return true;
            }
            return false;
        }

        public bool IsSameLinkedStationSource(ISignal signal)
        {
            var config = Config.Instance.DirectionFindingSettings;
            return signal.IsDirectionReliable() && SignalExtensions.AreRangesIntersected(this, signal, config.SignalsGapKhz);
        }

        public void Update(ISignal signal)
        {
            if (!IsSameSource(signal))
            {
                return;
            }
            var sourceInfo = new SourceInfo
            {
                Direction = signal.Direction,
                BandwidthKhz = signal.BandwidthKhz,
                CentralFrequencyKhz = signal.CentralFrequencyKhz,
                FrequencyKhz = signal.FrequencyKhz,
                Reliabilty = signal.Reliability,
                StandardDeviation = signal.StandardDeviation,
                Amplitude = signal.Amplitude,
                DisardedDirectionsPart = signal.DisardedDirectionsPart,
                Time = DateTime.Now,
                PhasesDeviation = signal.PhaseDeviation,
                Modulation = signal.Modulation
            };
            _dataQueue.Add(sourceInfo, update: true);
            UpdateFields();
        }

        private SignalModulation GetAverageModulation()
        {
            if (_dataQueue.Count == 1)
            {
                return _dataQueue.First().Modulation;
            }
            var modulations = _dataQueue.Select(d => (int) d.Modulation).ToArray();
            Array.Sort(modulations);
            return (SignalModulation) modulations[modulations.Length / 2];
        }

        private void UpdateFields()
        {
            var now = DateTime.Now;

            void UpdateWhenQueueIsEmpty()
            {
                IsNew = (now - FirstBroadcastStartTime) < Config.Instance.StoragesSettings.NewSignalTimeSpan;
                IsActive = (now - BroadcastStartTime) < Config.Instance.StoragesSettings.ActiveSignalTimeSpan;
            }

            if (_dataQueue.Count == 0)
            {
                UpdateWhenQueueIsEmpty();
                return;
            }
            SourceInfo[] sourceInfos;
            try
            {
                // just for case when _dataQueue suddenly updated to empty state from another thread
                sourceInfos = _dataQueue.ToArray();
            }
            catch (Exception)
            {
                UpdateWhenQueueIsEmpty();
                return;
            }
            var lastSourceInfo = sourceInfos.Last();
            var wasActive = IsActive;

            var phases = sourceInfos.Select(d => new PhaseFactor(SignalExtensions.GetMergeFactor(d.Reliabilty), d.Direction)).ToArray();
            Direction = PhaseMath.CalculatePhase(phases);
            BandwidthKhz = GetBandwidth(sourceInfos);
            try
            {
                StandardDeviation = PhaseMath.StandardDeviation(phases);
            }
            catch { } //error occurs for phases data :[0] = phase 2666 or smht, factor = 1
            Amplitude = lastSourceInfo.Amplitude;
            FrequencyKhz = sourceInfos.Average(s => s.FrequencyKhz);
            CentralFrequencyKhz = sourceInfos.Average(s => s.CentralFrequencyKhz);
            DisardedDirectionsPart = lastSourceInfo.DisardedDirectionsPart;
            PhaseDeviation = lastSourceInfo.PhasesDeviation;
            Modulation = GetAverageModulation();
            Reliability = sourceInfos.Average(s => s.Reliabilty);

            IsNew = (now - FirstBroadcastStartTime) < Config.Instance.StoragesSettings.NewSignalTimeSpan;
            IsActive = (now - lastSourceInfo.Time) < Config.Instance.StoragesSettings.ActiveSignalTimeSpan;

            if (IsActive && !wasActive)
            {
                BroadcastStartTime = lastSourceInfo.Time;
            }
            if (IsActive)
            {
                BroadcastTimeSpan = lastSourceInfo.Time - BroadcastStartTime;
            }
        }

        private float GetBandwidth(SourceInfo[] sourceInfos)
        {
            var minLeftFrequency = sourceInfos.Min(s => s.CentralFrequencyKhz - s.BandwidthKhz * 0.5f);
            var minRightFrequency = sourceInfos.Max(s => s.CentralFrequencyKhz + s.BandwidthKhz * 0.5f);
            return minRightFrequency - minLeftFrequency;
        }

        public void Update()
        {
            _dataQueue.Update();
            UpdateFields();
            UpdateLocation();
        }

        public void UpdateLinkedDirection(float linkedDirection)
        {
            if (linkedDirection == -1)
            {
                LinkedReliability = 0;
            }
            else
            {
                LinkedDirection = linkedDirection;
                LinkedReliability = 1;
            }
            Update();
        }

        public void UpdateLinkedStationDirection(float newDirection, float newReliability)
        {             
            if (newReliability < Constants.ReliabilityThreshold)
            {
                // current direction is very unreliable, so just ignore it
                return;
            }
            if (LinkedReliability < Constants.ReliabilityThreshold)
            {
                // previous information is unreliable, so just overwrite all
                LinkedDirection = newDirection;
                LinkedReliability = newReliability;
            }
            else
            {
                var maxDirectionsGap = Config.Instance.DirectionFindingSettings.SignalsMergeDirectionDeviation;
                if (PhaseMath.Angle(LinkedDirection, newDirection) < maxDirectionsGap)
                {
                    // all information is reliable so let's average it!
                    LinkedDirection = PhaseMath.AveragePhase(new[] {LinkedDirection, newDirection});
                    LinkedReliability = (LinkedReliability + newReliability) / 2;
                }
                else
                {
                    LinkedDirection = newDirection;
                    LinkedReliability = newReliability;
                }
            }
            Update();
        }

        private void UpdateLocation()
        {
            if (Reliability < Constants.ReliabilityThreshold || LinkedReliability < Constants.ReliabilityThreshold)
            {
                return;
            }
            var stationConfig = Config.Instance.StationPosition;
            var linkedStationConfig = Config.Instance.LinkedStationPosition;

            if (stationConfig == null || linkedStationConfig == null)
            {
                return;
            }
            if (!stationConfig.AreCoordinatesDefined() || !linkedStationConfig.AreCoordinatesDefined())
            {
                return;
            }

            (Longitude, Latitude) = LocationCalculator.DefineCoordinates(
                stationConfig.Longitude,
                stationConfig.Latitude,
                Direction,

                linkedStationConfig.Longitude,
                linkedStationConfig.Latitude,
                LinkedDirection);
        }

        private struct SourceInfo
        {
            public float Direction;
            public float BandwidthKhz;
            public float CentralFrequencyKhz;
            public float FrequencyKhz;
            public float StandardDeviation;
            public float Reliabilty;
            public float Amplitude;
            public float DisardedDirectionsPart;
            public float PhasesDeviation;
            public SignalModulation Modulation;
            public DateTime Time;
        }
    }
}

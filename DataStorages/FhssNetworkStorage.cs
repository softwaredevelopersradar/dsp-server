﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using DataProcessor.Fhss;
using DspDataModel;
using DspDataModel.Storages;
using SharpExtensions;

namespace DataStorages
{
    public class FhssNetworkStorage : IFhssNetworkStorage
    {
        public IReadOnlyCollection<IFhssNetwork> HidedFhssNetworks => _hidedFhssNetworks;

        private HashSet<IFhssNetwork> _hidedFhssNetworks;

        private readonly HashSet<int> _hidedIds;
        private readonly ConcurrentDictionary<int, IFhssNetwork> _data;
        private readonly IFilterManager _filterManager;

        public FhssNetworkStorage(IFilterManager filterManager) : this(filterManager, Config.Instance.StoragesSettings.OldRadioSourcesCollectTimeSpan)
        { }

        public FhssNetworkStorage(IFilterManager filterManager, TimeSpan oldRadioSourcesCollectTimeSpan)
        {
            _filterManager = filterManager;
            _data = new ConcurrentDictionary<int, IFhssNetwork>();
            _hidedIds = new HashSet<int>();
            _hidedFhssNetworks = new HashSet<IFhssNetwork>();

            PeriodicTask.Run(CollectOldRadioSources, oldRadioSourcesCollectTimeSpan);
        }

        public void PerformAction(SignalAction action, int[] signalsId)
        {
            switch (action)
            {
                case SignalAction.Hide:
                    foreach (var id in signalsId)
                    {
                        _hidedIds.Add(id);
                    }
                    break;
                case SignalAction.Restore:
                    foreach (var id in signalsId)
                    {
                        _hidedIds.Remove(id);
                    }
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(action), action, null);
            }

            _hidedFhssNetworks = new HashSet<IFhssNetwork>(
                GetFhssNetworksInner()
                    .Where(r => _hidedIds.Contains(r.Id))
            );
        }

        /// <summary>
        /// This method is running in additional thread endlessly in cycle with Config.Instance.OldRadioSourcesCollectTimeSpan delay
        /// </summary>
        private void CollectOldRadioSources()
        {
            var now = DateTime.Now;
            try
            {
                _data.Where(network => now - network.Value.LastUpdateTime > Config.Instance.StoragesSettings.OldRadioSourceTimeSpan)
                    .Do(network =>
                    {
                        _data.TryRemove(network.Key, out _);
                    });
            }
            catch (Exception e)
            {
                MessageLogger.Error(e, "Collecting fhss network error.");
            }

        }

        public void Put(IEnumerable<IFhssNetwork> networks)
        {
            try
            {
                foreach (var network in networks)
                {
                    Put(network);
                }
            }
            catch (Exception e)
            {
                MessageLogger.Error(e, "Adding fhss network error.");
            }
        }

        public void Clear()
        {
            _data.Clear();
            _hidedIds.Clear();
            _hidedFhssNetworks = new HashSet<IFhssNetwork>();
        }

        public IEnumerable<IFhssNetwork> GetFhssNetworks()
        {
            return GetFhssNetworksInner()
                .Where(n => !_hidedIds.Contains(n.Id));
        }

        private IEnumerable<IFhssNetwork> GetFhssNetworksInner()
        {
            var bands = _filterManager.Bands;
            if (bands.IsNullOrEmpty())
            {
                return new IFhssNetwork[0];
            }
            return _data
                .Where(pair => _filterManager.IsInFilter(pair.Value))
                .Select(valuePair => valuePair.Value);
        }

        public void Put(IFhssNetwork network)
        {
            var existingNetwork = _data
                .Where(valuePair => FhssNetwork.AreSameNetworks(network, valuePair.Value))
                .Select(valuePair => valuePair.Value)
                .FirstOrDefault();
            if (existingNetwork == null)
            {
                _data.TryAdd(network.Id, network);
            }
            else
            {
                existingNetwork.Update(network);
            }
        }
    }
}

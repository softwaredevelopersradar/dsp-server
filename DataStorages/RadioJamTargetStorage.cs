﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using DspDataModel.RadioJam;

namespace DataStorages
{
    public class RadioJamTargetStorage : IRadioJamTargetStorage
    {
        public IReadOnlyList<IRadioJamTarget> FrsTargets => _frsTargets;
        public IReadOnlyList<IRadioJamFhssTarget> FhssTargets => _fhssTargets;

        private ImmutableList<IRadioJamTarget> _frsTargets;
        private ImmutableList<IRadioJamFhssTarget> _fhssTargets;

        public event EventHandler<IReadOnlyList<IRadioJamTarget>> FrsTargetsUpdateEvent;//todo : find out if this event is being used or not ? add for fhss : remove dat nigga

        public RadioJamTargetStorage()
        {
            _frsTargets = ImmutableList.Create<IRadioJamTarget>();
            _fhssTargets = ImmutableList.Create<IRadioJamFhssTarget>();
        }

        public bool UpdateFrsTargets(IReadOnlyList<IRadioJamTarget> targets)
        {
            var sortedTargets = targets.ToList();//todo : figure this shit out
            sortedTargets.Sort((t1, t2) => t1.Id.CompareTo(t2.Id));
            var hasTargetsUpdated = !AreSimilarTargets(sortedTargets);
            var targetList = UpdateInputTargets(targets);
            targetList.Sort((t1, t2) => t1.Id.CompareTo(t2.Id));

            var hasTargetChanged = !AreSimilarTargets(targetList);
            _frsTargets = targetList.ToImmutableList();
            if (hasTargetChanged)
            {
                FrsTargetsUpdateEvent?.Invoke(this, _frsTargets);
            }

            if (hasTargetsUpdated)
                hasTargetChanged = true;
            return hasTargetChanged;
        }

        public bool UpdateFhssTargets(IReadOnlyList<IRadioJamFhssTarget> targets)
        {
            var targetList = targets.ToList();
            targetList.Sort((t1, t2) => t1.Id.CompareTo(t2.Id));

            var hasTargetChanged = !AreSimilarTargets(targetList, _fhssTargets);
            _fhssTargets = targetList.ToImmutableList();
            return hasTargetChanged;
        }

        /// <summary>
        /// Update input targets array by using the same target when it already exists in the storage with it's update
        /// </summary>
        private List<IRadioJamTarget> UpdateInputTargets(IReadOnlyList<IRadioJamTarget> inputTargets)
        {
            var result = new List<IRadioJamTarget>(inputTargets.Count);
            for (var i = 0; i < inputTargets.Count; i++)
            {
                var inputTarget = inputTargets[i];
                var currentTarget = _frsTargets.FirstOrDefault(t => t.Id == inputTarget.Id);
                if (currentTarget != null)
                {
                    currentTarget.UpdateTarget(inputTarget);
                    result.Add(currentTarget);
                }
                else
                {
                    result.Add(inputTarget);
                }
            }
            return result;
        }

        public void AddFrsTargets(IEnumerable<IRadioJamTarget> targets)
        {
            if (!targets.Any())
            {
                return;
            }
            
            _frsTargets = _frsTargets.AddRange(targets);

            FrsTargetsUpdateEvent?.Invoke(this, _frsTargets);
        }

        private bool AreSimilarTargets(IReadOnlyList<IRadioJamTarget> newTargets)
        {
            if (newTargets.Count != _frsTargets.Count)
            {
                return false;
            }

            for (var i = 0; i < _frsTargets.Count; ++i)
            {
                if (!newTargets[i].Equals(_frsTargets[i]))
                {
                    return false;
                }
            }
            return true;
        }

        private bool AreSimilarTargets(IReadOnlyList<IRadioJamFhssTarget> newTargets, IReadOnlyList<IRadioJamFhssTarget> oldTargets)
        {
            if (newTargets.Count != oldTargets.Count)
            {
                return false;
            }

            for (var i = 0; i < oldTargets.Count; ++i)
            {
                if (!newTargets[i].Equals(oldTargets[i]))
                {
                    return false;
                }
            }
            return true;
        }
    }
}

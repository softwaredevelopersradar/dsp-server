﻿using ClassLibraryPeleng;

namespace DataStorages
{
    public static class LocationCalculator
    {
        private static readonly Peleng Calculator = new Peleng("");

        private const double XCoordDatum = 25;
        private const double YCoordDatum = -141;
        private const double ZCoordDatum = -80;

        public static (double x, double y) Wgs84ToSk42(double longitude, double latitude)
        {
            double x = 0;
            double y = 0;

            double latitude42 = 0;
            double longitude42 = 0;
            double addLat = 0;
            double addLong = 0;

            Calculator.f_dLat(latitude, longitude, 0, XCoordDatum, YCoordDatum, ZCoordDatum, ref addLat);
            Calculator.f_dLong(latitude, longitude, 0, XCoordDatum, YCoordDatum, ZCoordDatum, ref addLong);
            Calculator.f_WGS84_SK42_Lat_Long(latitude, longitude, 0, addLat, addLong, ref latitude42, ref longitude42);
            Calculator.f_SK42_Krug(latitude42, longitude42, ref x, ref y);

            return (x, y);
        }

        public static (double longitude, double latitude) Sk42ToWgs84(double x, double y)
        {
            double latitude42 = 0;
            double longitude42 = 0;

            double addLat = 0;
            double addLong = 0;
            double latitude = 0;
            double longitude = 0;

            Calculator.f_Krug_SK42(x, y, ref latitude42, ref longitude42);
            Calculator.f_dLong(latitude42, longitude42, 0, XCoordDatum, YCoordDatum, ZCoordDatum, ref addLong);
            Calculator.f_dLat(latitude42, longitude42, 0, XCoordDatum, YCoordDatum, ZCoordDatum, ref addLat);
            Calculator.f_SK42_WGS84_Lat_Long(latitude42, longitude42, 0, addLat, addLong, ref latitude, ref longitude);

            return (longitude, latitude);
        }

        public static (double longitude, double latitude) DefineCoordinates(
            double longitude, double latitude, float direction, 
            double linkedLongitude, double linkedLatitude, float linkedDirection)
        {
            const float maxDistanceKm = 100;

            double x42Source = 0;
            double y42Source = 0;

            var (x42Own, y42Own) = Wgs84ToSk42(longitude, latitude);
            var (x42Linked, y42Linked) = Wgs84ToSk42(linkedLongitude, linkedLatitude);

            Calculator.f_Triang2(
                x42Own, y42Own, 
                x42Linked, y42Linked, 
                direction, linkedDirection, 
                maxDistanceKm, 
                ref x42Source, ref y42Source);

            var (resultLongitude, resultLatitude) = Sk42ToWgs84(x42Source, y42Source);

            return (resultLongitude, resultLatitude);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using Settings;
using DspDataModel;
using DspDataModel.Data;
using DspDataModel.Storages;
using SharpExtensions;
using System.Threading;

namespace DataStorages
{
    public class SpectrumHistoryStorage : ISpectrumHistoryStorage
    {
        public TimeSpan Rate
        {
            get => _rate;
            set
            {
                _rate = value;
                UpdateQueueMaxCapacity();
            }
        }

        public TimeSpan HistoryLength
        {
            get => _historyLength;
            set
            {
                _historyLength = value;
                UpdateQueueMaxCapacity();
            }
        }

        private readonly ISpectrumStorage _spectrumStorage;
        private readonly Queue<IAmplitudeBand> _spectrumQueue;
        private TimeSpan _historyLength;
        private TimeSpan _rate;

        private int _queueMaxCapacity;

        private int _minFrequencyKhz;
        private int _maxFrequencyKhz;

        private readonly CancellationTokenSource _cancellationTokenSource;

        public SpectrumHistoryStorage(ISpectrumStorage spectrumStorage, IFilterManager filterManager, TimeSpan rate, TimeSpan historyLength)
        {
            _spectrumStorage = spectrumStorage;
            filterManager.FiltersUpdatedEvent += OnFiltersChanged;
            _spectrumQueue = new Queue<IAmplitudeBand>();
            Rate = rate;
            HistoryLength = historyLength;
            _cancellationTokenSource = new CancellationTokenSource();
            PeriodicTask.Run((Action) UpdateSpectrum, Rate, _cancellationTokenSource.Token);
        }

        public void StopSpectrumUpdate()
        {
            _cancellationTokenSource.Cancel();
        }

        private void OnFiltersChanged(object sender, IReadOnlyList<Filter> e)
        {
            lock (_spectrumQueue)
            {
                if (e.Count == 0)
                {
                    _minFrequencyKhz = Constants.FirstBandMinKhz;
                    _maxFrequencyKhz = Constants.FirstBandMinKhz;
                }
                else
                {
                    _minFrequencyKhz = e.Min(filter => filter.MinFrequencyKhz);
                    _maxFrequencyKhz = e.Max(filter => filter.MaxFrequencyKhz);
                }
                _spectrumQueue.Clear();
            }
        }

        private void UpdateQueueMaxCapacity()
        {
            _queueMaxCapacity = (int) (HistoryLength.TotalMilliseconds / Rate.TotalMilliseconds);
        }

        private void UpdateSpectrum()
        {
            if (_minFrequencyKhz == _maxFrequencyKhz)
            {
                return;
            }
            var pointCount = Utilities.GetSamplesCount(_minFrequencyKhz, _maxFrequencyKhz);
            var amplitudeBand = _spectrumStorage.GetSpectrum(_minFrequencyKhz, _maxFrequencyKhz, pointCount);
            lock (_spectrumQueue)
            {
                _spectrumQueue.Enqueue(amplitudeBand);
                while (_spectrumQueue.Count > _queueMaxCapacity)
                {
                    _spectrumQueue.Dequeue();
                }
            }
        }

        public SpectrumHistory GetSpectrumHistory(float startFrequencyKhz, float endFrequencyKhz, int pointCount, TimeSpan timeSpan)
        {
            Contract.Assert(startFrequencyKhz < endFrequencyKhz);
            Contract.Assert(pointCount > 0);
            Contract.Assert(timeSpan > TimeSpan.Zero);

            lock (_spectrumQueue)
            {
                var bandsCount = (int) (timeSpan.TotalMilliseconds / Rate.TotalMilliseconds);

                if (startFrequencyKhz > _maxFrequencyKhz || endFrequencyKhz < _minFrequencyKhz)
                {
                    return SpectrumHistory.CreateEmpty(bandsCount, pointCount, Rate);
                }
                if (_spectrumQueue.IsNullOrEmpty())
                {
                    return SpectrumHistory.CreateEmpty(bandsCount, pointCount, Rate);
                }

                var bands = new IAmplitudeBand[bandsCount];

                if (bandsCount > _spectrumQueue.Count)
                {
                    var emptySpectrum = new AmplitudeBand(Enumerable.Repeat(Constants.ReceiverMinAmplitude, pointCount).ToArray());
                    for (var i = 0; i < bandsCount; ++i)
                    {
                        bands[i] = emptySpectrum;
                    }
                }

                var index = bandsCount - _spectrumQueue.Count;
                if (index < 0)
                {
                    index = 0;
                }

                var multiplierLeft = (_minFrequencyKhz - startFrequencyKhz) / (endFrequencyKhz - startFrequencyKhz);
                var multiplierRight = (endFrequencyKhz - _maxFrequencyKhz) / (endFrequencyKhz - startFrequencyKhz);
                if (multiplierLeft < 0)
                {
                    multiplierLeft = 0;
                }
                if (multiplierRight < 0)
                {
                    multiplierRight = 0;
                }
                var leftCount = (int) (multiplierLeft * pointCount);
                var rightCount = (int) (multiplierRight * pointCount);
                var centerCount = pointCount - leftCount - rightCount;

                var leftSpectrum = Enumerable.Repeat(Constants.ReceiverMinAmplitude, leftCount).ToArray();
                var rightSpectrum = Enumerable.Repeat(Constants.ReceiverMinAmplitude, rightCount).ToArray();

                var leftFrequency = Math.Max(startFrequencyKhz, _minFrequencyKhz);
                var rightFrequency = Math.Min(endFrequencyKhz, _maxFrequencyKhz);

                foreach (var amplitudeBand in _spectrumQueue)
                {
                    var count = Utilities.GetSamplesCount(leftFrequency, rightFrequency);
                    var offset = Utilities.GetSamplesCount(_minFrequencyKhz, startFrequencyKhz);
                    if (offset < 0)
                    {
                        offset = 0;
                    }
                    var segment = new ArraySegment<float>(amplitudeBand.Amplitudes, offset, count);
                    var spectrum = segment.StrechSpectrum(centerCount);
                    var aggregator = new ListAggregator<float>(leftSpectrum, spectrum.Amplitudes, rightSpectrum);

                    bands[index++] = new AmplitudeBand(aggregator.ToArray());

                    if (index == bandsCount)
                    {
                        break;
                    }
                }
                return new SpectrumHistory(bands, Rate);
            }
        }

    }
}

namespace Protocols
{
	extern const Settings.Constants.BandSampleCount

	extern enum DspDataModel.Hardware.ReceiverChannel
	extern enum DspDataModel.Tasks.DspServerMode
	extern enum DspDataModel.LinkedStation.StationRole
	extern enum DspDataModel.LinkedStation.ConnectionState
	extern enum DspDataModel.FftResolution
	extern enum DspDataModel.RangeType
	extern enum DspDataModel.TargetStation
	extern enum DspDataModel.StorageType
	extern enum DspDataModel.SignalAction
	extern enum DspDataModel.FrequencyType

	struct MessageHeader
	{
		byte SenderAddress
		byte ReceiverAddress
		byte Code
		byte ErrorCode
		int InformationLength
	}

	struct FrequencyRange
	{
		int StartFrequency
		int EndFrequency
	}

	class DefaultMessage
	{
		MessageHeader Header
	}

	class SpecialFrequenciesMessage
	{
		MessageHeader Header
		DspDataModel.FrequencyType FrequencyType
		DspDataModel.TargetStation Station
		FrequencyRange[(Header.InformationLength - 1) / FrequencyRange.BinarySize] Frequencies
	}

	class GetSpecialFrequenciesRequest
	{
		MessageHeader Header
		DspDataModel.FrequencyType FrequencyType
		DspDataModel.TargetStation Station
	}

	class FiltersMessage
	{
		MessageHeader Header
		byte Threshold
		short StandardDeviation
		int Bandwidth
		int Duration
	}

	class ModeMessage
	{
		MessageHeader Header
		DspDataModel.Tasks.DspServerMode Mode
	}

	struct RangeSector
	{
		int StartFrequency
		int EndFrequency
	 	short StartDirection
	 	short EndDirection
	}

	class GetSectorsAndRangesRequest
	{
		MessageHeader Header
		DspDataModel.RangeType RangesType
		DspDataModel.TargetStation Station
	}

	class SectorsAndRangesMessage
	{
		MessageHeader Header
		DspDataModel.RangeType RangesType
		DspDataModel.TargetStation Station
		RangeSector[Header.InformationLength / RangeSector.BinarySize] RangeSectors
	}

	struct AttenuatorSetting
	{
		byte BandNumber
		byte AttenuatorValue
		byte IsConstAttenuatorEnabled
	}

	class AttenuatorsMessage
	{
		MessageHeader Header
		AttenuatorSetting[Header.InformationLength / AttenuatorSetting.BinarySize] Settings
	}

	struct FRSJammingSetting
	{
		int Frequency
		byte ModulationCode
		byte DeviationCode
		byte ManipulationCode
		byte DurationCode
		byte Priority
		byte Threshold
		short Direction 
		int Id
		byte Liter
	}

	struct FhssJammingSetting
	{
		int StartFrequency
		int EndFrequency
		byte Threshold
		byte ModulationCode
		byte DeviationCode
		byte ManipulationCode
		int Id
		int FixedRadioSourceCount
		FhssFixedRadioSource[FixedRadioSourceCount] FixedRadioSources
	}

	class FrsJammingMessage
	{
		MessageHeader Header 
		DspDataModel.TargetStation Station
		FRSJammingSetting[Header.InformationLength / FRSJammingSetting.BinarySize] Settings
	}

	class GetFrsJammingRequest
	{
		MessageHeader Header
		DspDataModel.TargetStation Station
	}

	class FhssJammingMessage
	{
		MessageHeader Header
		int Duration
		DspDataModel.FftResolution FftResolutionCode
		DspDataModel.TargetStation Station
		byte TargetsCount
		FhssJammingSetting[TargetsCount] Settings
	}

	class GetFhssJammingRequest
	{
		MessageHeader Header
		DspDataModel.TargetStation Station
	}

	class GetAttenuatorsRequest
	{
		MessageHeader Header
		byte[Header.InformationLength] BandNumbers
	}

	class GetAttenuatorsResponse
	{
		MessageHeader Header
		AttenuatorSetting[Header.InformationLength / AttenuatorSetting.BinarySize] Settings
	}

	class GetAmplifiersRequest
	{
		MessageHeader Header
		byte[Header.InformationLength] BandNumbers
	}

	class GetAmplifiersResponse
	{
		MessageHeader Header
		byte[Header.InformationLength] Settings
	}

	class GetTemperatureRequest
	{
		MessageHeader Header
	}

	class GetTemperatureResponse
	{
		MessageHeader Header
		short Temperature
	}

	struct DetectionTime
	{
		byte Hour
		byte Minute
		byte Second
		short Millisecond
	}

	struct RadioSource
	{
		int Id
		bool IsNew
		bool IsActive
		int Frequency
		short Direction
		short Direction2
		double Latitude
		double Longitude
		short StandardDeviation
		int Bandwidth
		DetectionTime Time
		byte Modulation
		byte Amplitude
		int Duration
		byte BroadcastCount 
	}

	class GetRadioSourcesRequest
	{
		MessageHeader Header
	}

	class GetRadioSourcesResponse
	{
		MessageHeader Header
		RadioSource[Header.InformationLength / RadioSource.BinarySize] RadioSources
	}

	class GetSpectrumRequest
	{
		MessageHeader Header
		int StartFrequency
		int EndFrequency
		int PointCount
	}

	class GetSpectrumResponse
	{
		MessageHeader Header
		byte FirstPointOffset
		byte[Header.InformationLength - 1] Spectrum
	}

	class GetSpectrumFromReceiverRequest
	{
		MessageHeader Header
		byte BandNumber
		byte ReceiverNumber
	}

	class GetSpectrumFromReceiverResponse
	{
		MessageHeader Header
		byte[Header.InformationLength] Spectrum
	}

	class GetPhasesRequest
	{
		MessageHeader Header
		byte BandNumber
		byte ReceiverNumber
	}

	class GetPhasesResponse
	{
		MessageHeader Header
		short[Header.InformationLength / 2] Phases
	}

	class GetSpectrumWithPhasesRequest
	{
		MessageHeader Header
		int StartFrequency
		int EndFrequency
	}

	struct PointData
	{
		byte Amplitude
		short Direction
	}

	class GetSpectrumWithPhasesResponse
	{
		MessageHeader Header
		byte FirstPointOffset
		PointData[(Header.InformationLength - 1) / PointData.BinarySize] Spectrum
	}

	class ExecutiveDFRequest
	{
		MessageHeader Header
		int StartFrequency
		int EndFrequency
		byte PhaseAveragingCount
		byte DirectionAveragingCount
	}

	class ExecutiveDFResponse
	{
		MessageHeader Header
		int Frequency
		short Direction
		short StandardDeviation
		short DiscardedDirectionPercent
		byte[360] CorrelationHistogram
	}

	class QuasiSimultaneouslyDFRequest
	{
		MessageHeader Header
		int StartFrequency
		int EndFrequency
		byte PhaseAveragingCount
		byte DirectionAveragingCount
	}

	class QuasiSimultaneouslyDFResponse
	{
		MessageHeader Header
		RadioSource Source
	}

	class TechAppSpectrumRequest
	{
		MessageHeader Header
		byte BandNumber
		byte AveragingCount
	}

	class Scan
	{
		byte[Settings.Constants.BandSampleCount] Amplitudes
		short[Settings.Constants.BandSampleCount] Phases
	}

	class TechAppSpectrumResponse
	{
		MessageHeader Header
		Scan[6] Scans
	}

	class SetReceiversChannelRequest
	{
		MessageHeader Header
		DspDataModel.Hardware.ReceiverChannel Channel
	}

	class GetCalibrationProgressRequest
	{
		MessageHeader Header
	}

	class GetCalibrationProgressResponse
	{
		MessageHeader Header
		byte Progress
		byte[Header.InformationLength - 1] BandPhaseDeviations
	}

	class HeterodyneRadioSourcesRequest
	{
		MessageHeader Header
		int StartFrequency
		int EndFrequency
		byte StepMhz
		byte PhaseAveragingCount
		byte DirectionAveragingCount
	}

	struct HeterodyneRadioSouce
	{
		int Frequency
		byte Amplitude
		short Direction
		short StandardDeviation
		byte Reliability
		byte SignalToNoiseRatio
		byte PhasesDeviation
		short[10] Phases
	}

	class HeterodyneRadioSourcesResponse
	{
		MessageHeader Header
		HeterodyneRadioSouce[Header.InformationLength / HeterodyneRadioSouce.BinarySize] RadioSources
	}

	class GetBandAmplitudeLevelsRequest
	{
		MessageHeader Header
		byte[Header.InformationLength] BandNumbers
	}

	struct BandAmplitudeLevel
	{
		byte BandNumber
		short Level
	}

	class GetBandAmplitudeLevelsResponse
	{
		MessageHeader Header
		BandAmplitudeLevel[Header.InformationLength / BandAmplitudeLevel.BinarySize] BandLevels
	}

	class GetAmplitudeTimeSpectrumRequest
	{
		MessageHeader Header
		int StartFrequency
		int EndFrequency
		int PointCount
		byte TimeLength
	}

	class GetAmplitudeTimeSpectrumResponse
	{
		MessageHeader Header
		byte[Header.InformationLength] Spectrum
	}

	class StartRecordingRequest
	{
		MessageHeader Header
	}

	class StopRecordingRequest
	{
		MessageHeader Header
	}

	class StopRecordingResponse
	{
		MessageHeader Header
		short RecordId
	}

	class SetCorrelationTypeRequest
	{
		MessageHeader Header
		byte CorrelationTypeNumber // 0 - correlation, 1 - linear deviation, 2 - sd
	}

	class DirectionCorrectionMessage
	{
		MessageHeader Header
		short DirectionCorrection
		bool UseCorrection
	}

	class SetSynchronizationShiftRequest
	{
		MessageHeader Header
		short Shift
	}

	struct CalibrationCorrectionSignal
	{
		int Frequency
		short Direction
		short[10] Phases
	}

	class CalculateCalibrationCorrectionRequest
	{
		MessageHeader Header
		CalibrationCorrectionSignal[Header.InformationLength / CalibrationCorrectionSignal.BinarySize] Signals
	}

	class GetAdaptiveThresholdRequest
	{
		MessageHeader Header
		byte BandNumber
	}

	class GetAdaptiveThresholdResponse
	{
		MessageHeader Header
		byte ThresholdLevel
	}

	class SetFrsRadioJamSettingsRequest
	{
		MessageHeader Header
		int EmitionDuration
		int LongWorkingSignalDuration
		byte ChannelsInLiter
	}

	class SetAfrsRadioJamSettingsRequest
	{
		MessageHeader Header
		int EmitionDuration
		int LongWorkingSignalDuration
		byte ChannelsInLiter
		int SearchBandwidth
		short DirectionSearchSector
		byte Threshold
	}

	class SetFrsAutoRadioJamSettingsRequest
	{
		MessageHeader Header
		int EmitionDuration
		int LongWorkingSignalDuration
		byte ChannelsInLiter
		int SignalBandwidthThreshold
		byte Threshold
	}

	class RadioJamTargetState
	{
		int Id
		int Frequency
		byte Amplitude
		byte ControlState
		byte RadioJamState
		byte EmitState
	}

	class RadioJamStateUpdateEvent
	{
		MessageHeader Header
		byte ErrorCode
		RadioJamTargetState[(Header.InformationLength - 1) / RadioJamTargetState.BinarySize] TargetStates
	}

	class ShaperStateUpdateEvent
	{
		MessageHeader Header
		bool IsShaperConnected
	}

	class FhssRadioJamUpdateEvent
	{
		MessageHeader Header
		byte FhssNetworkCount
		FhssNetworkJammingState[FhssNetworkCount] JammingStates
		int JammedFrequenciesCount
		int[JammedFrequenciesCount] Frequencies
	}

	struct FhssNetworkJammingState
	{
		bool IsJammed
		int Id
	}

	class MasterSlaveStateChangedEvent
	{
		MessageHeader Header
		DspDataModel.LinkedStation.StationRole Role
		DspDataModel.LinkedStation.ConnectionState State
	}

	struct RadioSourceLocation
	{
		short Direction1
		short Direction2
		double Latitude
		double Longitude
	}

	struct FhssFixedRadioSource
	{
		int Frequency
		int Bandwidth
	}

	class FhssRadioNetwork
	{
		int StartFrequency
		int EndFrequency
		int Bandwidth
		int Step
		byte Amplitude
		int Id
		DetectionTime Time
		int IsActive
		int LocationsCount
		RadioSourceLocation[LocationsCount] Locations
		int FixedRadioSourceCount
		FhssFixedRadioSource[FixedRadioSourceCount] FixedRadioSources
		int ImpulseDuration
	}

	class GetFhssNetworksRequest
	{
		MessageHeader Header
	}

	class GetFhssNetworksResponse
	{
		MessageHeader Header
		int NetworksCount
		FhssRadioNetwork[NetworksCount] FhssNetworks
	}

	class GetScanSpeedResponse
	{
		MessageHeader Header
		int ScanSpeed
	}

	class StationLocationMessage
	{
		MessageHeader Header
		double Latitude
		double Longitude
		short Altitude
		bool UseLocation
		DspDataModel.TargetStation Station
	}

	class GetStationLocationRequest
	{
		MessageHeader Header
		DspDataModel.TargetStation Station
	}

	class GetRadioControlSpectrumRequest
	{
		MessageHeader Header
		byte BandNumber
	}

	class GetRadioControlSpectrumResponse
	{
		MessageHeader Header
		byte[Settings.Constants.BandSampleCount] Spectrum
	}

	class SetStationRoleRequest
	{
		MessageHeader Header
		DspDataModel.LinkedStation.StationRole Role
		byte OwnAddress
		byte LinkedAddress
	}

	class InitMasterSlaveRequest
	{
		MessageHeader Header
		string Address
	}

	class DisconnectFromMasterSlaveStationRequest
	{
		MessageHeader Header
	}

	class SendTextMessage
	{
		MessageHeader Header
		string Text
	}

	class SetTimeRequest
	{
		MessageHeader Header
		byte Hour
		byte Minute
		byte Second
		short Millisecond
	}

	class StorageActionMessage
	{
		MessageHeader Header
		DspDataModel.StorageType Storage
		DspDataModel.SignalAction Action
		int[(Header.InformationLength - 2) / 4] SignalsId
	}

	struct BearingPanoramaSignal
	{
		int Frequency
		short Direction
	}

	class GetBearingPanoramaSignalsRequest
	{
		MessageHeader Header
		int StartFrequency
		int EndFrequency
	}

	class GetBearingPanoramaSignalsResponse
	{
		MessageHeader Header
		int ImpulseSignalsCount
		BearingPanoramaSignal[ImpulseSignalsCount] ImpulseSignals
		int FixedSignalsCount
		BearingPanoramaSignal[FixedSignalsCount] FixedSignals
	}

	class SetPrecisePhasesRequest
	{
		MessageHeader Header
		int Frequency
		short Direction
		short[10] Phases
	}

	class SearchFhssMessage
	{
		MessageHeader Header
		byte SearchFhss
	}

	class AntennaDirectionsMessage
	{
		MessageHeader Header
		AntennaDirections antennaDirections
	}

	struct AntennaDirections
	{
		short Ard1
		short Ard2
		short Ard3
		short Compass
		short Lpa1_3
		short Lpa2_4
	}

	class NumberOfSatellitesUpdateEvent
	{
		MessageHeader Header
		byte NumberOfSatellites
	}

	class SetGpsSyncIntervalRequest
	{
		MessageHeader Header
		int SyncIntervalSec
	}

	class SetGpsPositionErrorRequest
	{
		MessageHeader Header
		short PositionErrorMeters
	}

	class ShaperModeUpdateEvent
	{
		MessageHeader Header
		byte Mode
	}

	class ShaperVoltageUpdateEvent
	{
		MessageHeader Header
		short LiterCount
		float[LiterCount] Voltage
	}

	class ShaperPowerUpdateEvent
	{
		MessageHeader Header
		short LiterCount
		short[LiterCount] Power
	}

	class ShaperTemperatureUpdateEvent
	{
		MessageHeader Header
		short LiterCount
		short[LiterCount] Temperature
	}

	class ShaperAmperageUpdateEvent
	{
		MessageHeader Header
		short LiterCount
		float[LiterCount] Amperage
	}

	class SetShaperAntennaRequest
	{
		MessageHeader Header
		byte AntennaOrEquivalent
	}

	class ShaperStartFrsJammingRequest
	{
		MessageHeader Header
		FRSJammingSetting[Header.InformationLength / FRSJammingSetting.BinarySize] Targets
	}

	class ShaperStartFhssJammingRequest
	{
		MessageHeader Header
		short TargetCount
		FhssJammingSetting[TargetCount] Targets
	}

	class SetShaperStateUpdateIntervalRequest
	{
		MessageHeader Header
		short IntervalSec
	}

	class SetShaperSettingsUpdateIntervalRequest
	{
		MessageHeader Header
		short IntervalSec
	}
}
namespace SimulatorProtocols
{
	extern const Settings.Constants.BandSampleCount
	
	extern enum DspDataModel.Hardware.ReceiverChannel

	typeheader struct DataRequest
	{
		byte[6] BandNumbers
	}

	class ChannelData
	{
		byte[Settings.Constants.BandSampleCount] Amplitudes
		short[Settings.Constants.BandSampleCount] Phases
	}

	class DataResponse
	{
		ChannelData[6] Channels
		bool IsPlayingRecord
	}

	typeheader class SetGeneratorSignalRequest
	{
		int Frequency
		int Level
		bool IsActive
	}

	typeheader class SetReceiversChannel
	{
		DspDataModel.Hardware.ReceiverChannel Channel
	}
}
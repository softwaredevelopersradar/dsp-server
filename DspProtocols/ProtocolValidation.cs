﻿using System.Linq;
using DspDataModel;
using DspDataModel.LinkedStation;
using Protocols;
using Settings;

namespace DspProtocols
{
    public static class ProtocolValidation
    {
        private const int MaxEmitionDuration = 10000;
        private const int MaxLongWorkingSignalDuration = 10000;

        public static bool IsValid(this CalculateCalibrationCorrectionRequest request)
        {
            if (request.Signals.Length == 0)
            {
                return false;
            }
            foreach (var signal in request.Signals)
            {
                if (signal.Direction < 0 || signal.Direction > 3600)
                {
                    return false;
                }
                if (signal.Frequency / 10 < Constants.FirstBandMinKhz ||
                    signal.Frequency / 10 > Config.Instance.BandSettings.LastBandMaxKhz)
                {
                    return false;
                }
                if (signal.Phases.Any(phase => phase < 0 || phase > 3600))
                {
                    return false;
                }
            }
            return true;
        }

        public static bool IsValid(this ExecutiveDFRequest request)
        {
            if (request.StartFrequency > request.EndFrequency)
            {
                return false;
            }
            if (request.StartFrequency / 10 < Constants.FirstBandMinKhz)
            {
                return false;
            }
            if (request.EndFrequency / 10 > Config.Instance.BandSettings.LastBandMaxKhz)
            {
                return false;
            }
            if (request.PhaseAveragingCount * request.DirectionAveragingCount <= 0)
            {
                return false;
            }
            return true;
        }

        public static bool IsValid(this QuasiSimultaneouslyDFRequest request)
        {
            if (request.StartFrequency > request.EndFrequency)
            {
                return false;
            }
            if (request.StartFrequency / 10 < Constants.FirstBandMinKhz)
            {
                return false;
            }
            if (request.EndFrequency / 10 > Config.Instance.BandSettings.LastBandMaxKhz)
            {
                return false;
            }
            if (request.PhaseAveragingCount * request.DirectionAveragingCount <= 0)
            {
                return false;
            }
            return true;
        }

        public static bool IsValid(this GetRadioControlSpectrumRequest request)
        {
            if (request.BandNumber >= Config.Instance.BandSettings.BandCount)
            {
                return false;
            }
            return true;
        }

        public static bool IsValid(this SetFrsRadioJamSettingsRequest request)
        {
            if (request.EmitionDuration <= 0 || request.EmitionDuration > MaxEmitionDuration)
            {
                return false;
            }
            if (request.LongWorkingSignalDuration <= 0 || request.LongWorkingSignalDuration > MaxLongWorkingSignalDuration)
            {
                return false;
            }
            return true;
        }

        public static bool IsValid(this SetAfrsRadioJamSettingsRequest request)
        {
            if (request.EmitionDuration <= 0 || request.EmitionDuration > MaxEmitionDuration)
            {
                return false;
            }
            if (request.LongWorkingSignalDuration <= 0 || request.LongWorkingSignalDuration > MaxLongWorkingSignalDuration)
            {
                return false;
            }
            if (request.DirectionSearchSector < 0 || request.DirectionSearchSector > 3600)
            {
                return false;
            }
            if (request.SearchBandwidth <= 0)
            {
                return false;
            }
            return true;
        }

        public static bool IsValid(this SetFrsAutoRadioJamSettingsRequest request)
        {
            if (request.EmitionDuration <= 0 || request.EmitionDuration > MaxEmitionDuration)
            {
                return false;
            }
            if (request.LongWorkingSignalDuration <= 0 || request.LongWorkingSignalDuration > MaxLongWorkingSignalDuration)
            {
                return false;
            }
            if (request.SignalBandwidthThreshold <= 0)
            {
                return false;
            }
            return true;
        }

        public static bool IsValid(this FrsJammingMessage request)
        {
            foreach (var target in request.Settings)
            {
                if (target.Frequency / 10 < Constants.FirstBandMinKhz)
                {
                    return false;
                }
                if (target.Frequency / 10 > Config.Instance.BandSettings.LastBandMaxKhz)
                {
                    return false;
                }
                if (target.Direction < 0 || target.Direction > 3600)
                {
                    return false;
                }
            }
            return true;
        }

        public static bool IsValid(this FhssJammingMessage request)
        {
            if (request.Duration <= 0)
            {
                return false;
            }
            foreach (var fhssJammingSetting in request.Settings)
            {
                if (fhssJammingSetting.StartFrequency < Constants.FirstBandMinKhz * 10 ||
                    fhssJammingSetting.EndFrequency < Constants.FirstBandMinKhz * 10)
                {
                    return false;
                }
                if (fhssJammingSetting.StartFrequency > Config.Instance.BandSettings.LastBandMaxKhz * 10 ||
                    fhssJammingSetting.EndFrequency > Config.Instance.BandSettings.LastBandMaxKhz * 10)
                {
                    return false;
                }
                if (fhssJammingSetting.StartFrequency >= fhssJammingSetting.EndFrequency)
                {
                    return false;
                }
            }

            return true;
        }

        public static bool IsValid(this GetSpectrumRequest request)
        {
            if (request.StartFrequency > request.EndFrequency)
            {
                return false;
            }
            if (request.StartFrequency / 10 < Constants.FirstBandMinKhz)
            {
                return false;
            }
            if (request.EndFrequency / 10 > Config.Instance.BandSettings.LastBandMaxKhz)
            {
                return false;
            }
            if (request.PointCount <= 0 || request.PointCount > 2 * 1024 * 1024)
            {
                return false;
            }
            return true;
        }

        public static bool IsValid(this GetAdaptiveThresholdRequest request)
        {
            if (request.BandNumber >= Config.Instance.BandSettings.BandCount)
            {
                return false;
            }
            return true;
        }

        public static bool IsValid(this GetAmplitudeTimeSpectrumRequest request)
        {
            if (request.StartFrequency > request.EndFrequency)
            {
                return false;
            }
            if (request.StartFrequency / 10 < Constants.FirstBandMinKhz)
            {
                return false;
            }
            if (request.EndFrequency / 10 > Config.Instance.BandSettings.LastBandMaxKhz)
            {
                return false;
            }
            if (request.PointCount <= 0 || request.PointCount > 65536)
            {
                return false;
            }
            if (request.TimeLength == 0)
            {
                return false;
            }
            return true;
        }

        public static bool IsValid(this HeterodyneRadioSourcesRequest request)
        {
            if (request.StartFrequency > request.EndFrequency)
            {
                return false;
            }
            if (request.StartFrequency / 10 < Constants.FirstBandMinKhz)
            {
                return false;
            }
            if (request.EndFrequency / 10 > Config.Instance.BandSettings.LastBandMaxKhz)
            {
                return false;
            }
            return true;
        }

        public static bool IsValid(this GetBearingPanoramaSignalsRequest request)
        {
            if (request.StartFrequency > request.EndFrequency)
            {
                return false;
            }
            if (request.StartFrequency / 10 < Constants.FirstBandMinKhz)
            {
                return false;
            }
            if (request.EndFrequency / 10 > Config.Instance.BandSettings.LastBandMaxKhz)
            {
                return false;
            }
            return true;
        }

        public static bool IsValid(this GetAttenuatorsRequest request)
        {
            return request.BandNumbers.All(bandNumber => bandNumber < Config.Instance.BandSettings.BandCount);
        }

        public static bool IsValid(this SetSynchronizationShiftRequest request)
        {
            return request.Shift >= 0 && request.Shift <= 522;
        }

        public static bool IsValid(this AttenuatorsMessage request)
        {
            return request.Settings.All(setup => setup.BandNumber < Config.Instance.BandSettings.BandCount &&
                                                 setup.IsConstAttenuatorEnabled <= 1 && setup.AttenuatorValue < 61);
        }

        public static bool IsValid(this SetStationRoleRequest request)
        {
            switch (request.Role)
            {
                case StationRole.Master:
                    return true;
                case StationRole.Slave:
                    return true;
                case StationRole.Standalone:
                    return true;
                default:
                    return false;
            }
        }

        public static bool IsValid(this GetAmplifiersRequest request)
        {
            return request.BandNumbers.All(bandNumber => bandNumber < Config.Instance.BandSettings.BandCount);
        }

        public static bool IsValid(this GetBandAmplitudeLevelsRequest request)
        {
            return request.BandNumbers.All(bandNumber => bandNumber < Config.Instance.BandSettings.BandCount);
        }

        public static bool IsValid(this SpecialFrequenciesMessage request)
        {
            foreach (var range in request.Frequencies)
            {
                if (range.StartFrequency > range.EndFrequency)
                {
                    return false;
                }
                if (range.StartFrequency / 10 < Constants.FirstBandMinKhz)
                {
                    return false;
                }
                if (range.EndFrequency / 10 > Config.Instance.BandSettings.LastBandMaxKhz)
                {
                    return false;
                }
            }
            return true;
        }

        public static bool IsValid(this FiltersMessage request)
        {
            if (request.Bandwidth < 0)
            {
                return false;
            }
            if (request.Duration < 0)
            {
                return false;
            }
            if (request.StandardDeviation < 0)
            {
                return false;
            }
            return true;
        }

        public static bool IsValid(this SectorsAndRangesMessage request)
        {
            foreach (var range in request.RangeSectors)
            {
                if (range.StartFrequency > range.EndFrequency)
                {
                    return false;
                }
                if (range.StartFrequency / 10 < Constants.FirstBandMinKhz)
                {
                    return false;
                }
                if (range.EndFrequency / 10 > Config.Instance.BandSettings.LastBandMaxKhz)
                {
                    return false;
                }
                if (range.StartDirection < 0 || range.StartDirection / 10 > 360)
                {
                    return false;
                }
                if (range.EndDirection < 0 || range.EndDirection / 10 > 360)
                {
                    return false;
                }
            }
            return true;
        }

        public static bool IsValid(this TechAppSpectrumRequest request)
        {
            return request.BandNumber < Config.Instance.BandSettings.BandCount;
        }

        public static bool IsValid(this AntennaDirectionsMessage request)
        {
            if (request.antennaDirections.Ard1 < 0 || request.antennaDirections.Ard1 > 360)
                return false;
            if (request.antennaDirections.Ard2 < 0 || request.antennaDirections.Ard2 > 360)
                return false;
            if (request.antennaDirections.Ard3 < 0 || request.antennaDirections.Ard3 > 360)
                return false;
            if (request.antennaDirections.Compass < 0 || request.antennaDirections.Compass > 360)
                return false;
            if (request.antennaDirections.Lpa1_3 < 0 || request.antennaDirections.Lpa1_3 > 360)
                return false;
            if (request.antennaDirections.Lpa2_4 < 0 || request.antennaDirections.Lpa2_4 > 360)
                return false;
            return true;
        }

        public static bool IsValid(this SetGpsSyncIntervalRequest request)
        {
            if (request.SyncIntervalSec < 0)
                return false;
            return true;
        }

        public static bool IsValid(this SetGpsPositionErrorRequest request)
        {
            if (request.PositionErrorMeters < 0)
                return false;
            return true;
        }
    }
}

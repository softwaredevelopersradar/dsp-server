﻿using System.IO;
using DspDataModel;
using NUnit.Framework;

namespace Tests
{
    [TestFixture]
    public class DataModelUnitTests
    {
        [OneTimeSetUp]
        public void Init()
        {
            Directory.SetCurrentDirectory(TestContext.CurrentContext.TestDirectory);
        }

        [Test]
        public void TestBands()
        {
            var band1 = new Band(0);
            var band2 = new Band(0);
            Assert.IsTrue(band1.Equals(band2));
            
            var band3 = new Band(1);
            Assert.IsFalse(band1.Equals(band3));
            Assert.AreEqual(band1.FrequencyToKhz, band3.FrequencyFromKhz);
        }

        [Test]
        public void TestFilters()
        {
            var filter1 = new Filter(100, 200, 0, 360);

            for (int frequency = filter1.MinFrequencyKhz; frequency <= filter1.MaxFrequencyKhz; frequency += 10)
            {
                for (int angle = 0; angle < 360; angle += 30)
                {
                    Assert.IsTrue(filter1.IsInFilter(frequency, angle));
                }
            }

            Assert.IsFalse(filter1.IsInFilter(250, 10));
            Assert.IsFalse(filter1.IsInFilter(50, 10));

            var filter2 = new Filter(100, 200, 0, 360);
            Assert.IsTrue(filter1.Equals(filter2));

            filter2 = new Filter(100, 250, 100, 200);
            Assert.IsFalse(filter1.Equals(filter2));

            Assert.IsFalse(filter2.IsInFilter(150, 50));
            Assert.IsFalse(filter2.IsInFilter(150, 250));
        }
    }
}

﻿using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Correlator;
using DataProcessor;
using DataStorages;
using DspDataModel;
using NUnit.Framework;

namespace Tests
{
    [TestFixture]
    public class RadioSourceStorageUnitTests
    {
        [OneTimeSetUp]
        public void Init()
        {
            Directory.SetCurrentDirectory(TestContext.CurrentContext.TestDirectory);
        }

        [SetUp]
        public void Setup()
        {
            var directory = Path.GetDirectoryName(new Uri(typeof(CorrelatorUnitTests).Assembly.CodeBase).LocalPath);
            Environment.CurrentDirectory = directory;
        }

        [Test]
        public void TestRadioSourceStorageCreation()
        {
            var filterManager = new FilterManager();
            var storage = new RadioSourceStorage(filterManager, TimeSpan.MaxValue);
            Assert.AreEqual(storage.GetRadioSources().Count(), 0);
        }

        [Test]
        public void TestRadioSourceStorageGetMethod()
        {
            var filterManager = new FilterManager();
            var storage = new RadioSourceStorage(filterManager, TimeSpan.MaxValue);
            var signal = new Signal(30000, 30000, 230, 1, 10, -100, 0, 0, 0, 1, new float[360], SignalModulation.Unknown, TimeSpan.Zero);
            var filters = new[]
            {
                new Filter(25000, 40000, 100, 300),
                new Filter(50000, 55000, 100, 300)
            };

            storage.Put(signal);

            Assert.AreEqual(storage.GetRadioSources().Count(), 0);
            filterManager.SetFilters(RangeType.Intelligence, filters.Take(1));
            var signals = storage.GetRadioSources().ToArray();
            Assert.AreEqual(signals.Length, 1);
            Assert.IsTrue(signals[0].IsSameSource(signal));

            storage.Put(new Signal(50000, 50000, 230, 1, 10, -100, 0, 0, 0, 1, new float[360], SignalModulation.Unknown, TimeSpan.Zero));
            Assert.AreEqual(storage.GetRadioSources().Count(), 1);

            // putting signal that already persisted in the storage
            storage.Put(signal);
            Assert.AreEqual(storage.GetRadioSources().Count(), 1);

            filterManager.SetFilters(RangeType.Intelligence, filters);
            Assert.AreEqual(storage.GetRadioSources().Count(), 2);
        }

        [Test]
        public async Task TestRadioSourceStorageCleanup()
        {
            var filterManager = new FilterManager();
            filterManager.SetFilters(RangeType.Intelligence, new[] {new Filter(25000, 40000, 100, 300)});

            var storage = new RadioSourceStorage(filterManager, TimeSpan.FromMilliseconds(100));
            var signal = new Signal(30000, 30000, 230, 1, 10, -100, 0, 0, 0, 1, new float[360], SignalModulation.Unknown, TimeSpan.Zero);

            storage.Put(signal);
            Assert.AreEqual(storage.GetRadioSources().Count(), 1);

            await Task.Delay(Config.Instance.StoragesSettings.OldRadioSourceTimeSpan + TimeSpan.FromMilliseconds(500));
            Assert.AreEqual(storage.GetRadioSources().Count(), 0);
        }
    }
}

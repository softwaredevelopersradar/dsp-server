using System;
using System.Threading.Tasks;
using DspDataModel;
using Simulator;

namespace Tests
{
    internal class ClientServerPair
    {
        public DspServer.DspServer Server { get; }
        public DspClient.DspClient Client { get; }
        public SimulatorCore SimulatorCore { get; }

        private const string localhost = "127.0.0.1";

        private ClientServerPair(int serverPort, int simulatorPort)
        {
            if (!Config.TryLoad(Config.ConfigPath, out var config))
            {
                throw new Exception("Can't load config!");
            }
            config.SimulatorSettings.SimulatorPort = simulatorPort;

            SimulatorCore = new SimulatorCore(saveSimulatedSignals: false);
            SimulatorCore.Server.Start(localhost, simulatorPort);
            SimulatorCore.SignalSimulator.GeneratePhaseNoise = false;

            Server = new DspServer.DspServer();
            Server.Start(localhost, serverPort);
            Client = new DspClient.DspClient();
        }

        public static async Task<ClientServerPair> Create(int serverPort, int simulatorPort)
        {
            var pair = new ClientServerPair(serverPort, simulatorPort);
            await pair.Client.Connect(localhost, serverPort);
            return pair;
        }

        public async Task Stop()
        {
            Client.Stop();
            await Task.WhenAll(Server.Stop(), SimulatorCore.Stop());
        }
    }
}
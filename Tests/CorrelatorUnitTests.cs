﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Correlator;
using Correlator.CalibrationCorrection;
using DspDataModel;
using DspDataModel.Data;
using NUnit.Framework;
using Phases;
using RadioTables;
using Settings;
using SharpExtensions;

namespace Tests
{
    [TestFixture]
    public class CorrelatorUnitTests
    {
        [OneTimeSetUp]
        public void Init()
        {
            Directory.SetCurrentDirectory(TestContext.CurrentContext.TestDirectory);
        }

        [Test]
        public void TestTableGetPhasesMethod()
        {
            RadioPathTableDto radioPathTableDto;
            TheoreticalTableDto theoreticalTableDto;
            GenerateTables(out theoreticalTableDto, out radioPathTableDto);
            var table = new TheoreticalTable(theoreticalTableDto);

            TestGetPhases(table, theoreticalTableDto, 
               frequency: theoreticalTableDto.AntennaTables[0].MinFrequencyKhz, 
               antennaIndex: 0);

            TestGetPhases(table, theoreticalTableDto,
               frequency: theoreticalTableDto.AntennaTables[0].MinFrequencyKhz,
               antennaIndex: 0);

            TestGetPhases(table, theoreticalTableDto,
               frequency: theoreticalTableDto.AntennaTables[0].MaxFrequencyKhz,
               antennaIndex: 0);

            TestGetPhases(table, theoreticalTableDto,
               frequency: theoreticalTableDto.AntennaTables[1].MinFrequencyKhz + 1,
               antennaIndex: 1);
        }
        
        private void TestGetPhases(TheoreticalTable table, TheoreticalTableDto theoreticalTable, int frequency, int antennaIndex)
        {
            var phases = table.GetPhases(frequency);
            Assert.IsTrue(theoreticalTable.AntennaTables[antennaIndex].TheoreticalDifferences.Contains(phases));
        }

        [Test]
        public void TestCorrelator()
        {
            GenerateTables(out var theoreticalTableDto, out var radioPathTableDto);

            // generate multipliers
            TheoreticalTableGenerator.GenerateTable();
            var theoreticalTable = new TheoreticalTable(theoreticalTableDto);
            var radioPathTable = new RadioPathTable(radioPathTableDto);

            var correlator = new PhaseCorrelator(theoreticalTable, radioPathTable);
            for (var frequency = 80 * 1000; frequency < 120 * 1000; frequency += 5 * 10000)
            {
                for (var direction = 5; direction < 360; direction += 50)
                {
                    var di = correlator.CalculateDirection(frequency, GetPointPhaseDifferencies(direction));
                    Assert.AreEqual(di.Angle, direction, 2);
                    Assert.IsTrue(di.Correlation.ApproxEquals(1));
                    Assert.IsTrue(di.StandardDeviation.ApproxEquals(0));
                }
            }            
        }

        [Test]
        public void TestDirectionCorrection()
        {
            var correlator = new PhaseCorrelator("");

            var phases = Enumerable.Range(0, 10)
                .Select(n => (float) n)
                .ToArray();
            var frequency = Constants.FirstBandMinKhz;
            var angle = correlator.CalculateDirection(frequency, phases).Angle;

            for (var correction = -175; correction < 180; correction += 100)
            {
                Config.Instance.DirectionFindingSettings.DirectionCorrection = correction;
                var newAngle = correlator.CalculateDirection(frequency, phases).Angle;
                Assert.AreEqual(PhaseMath.Angle(angle, newAngle), Math.Abs(correction), delta: 1);
            }
        }

        private void GenerateTables(out TheoreticalTableDto theoreticalTable, out RadioPathTableDto radioPathTable)
        {
            var phases = GetArrays();

            theoreticalTable = new TheoreticalTableDto(1000, new List<AntennaTableDto>
            {
                new AntennaTableDto(90 * 1000, 30 * 1000, Enumerable.Repeat(phases, 60).ToList()),
                new AntennaTableDto(120 * 1000, 90 * 1000, Enumerable.Repeat(phases, 30).ToList())
            });

            radioPathTable = new RadioPathTableDto(30 * 1000, 120 * 1000, new List<BandRadioPath>
            {
                new BandRadioPath
                {
                    RadioPath = new List<PhaseArray>
                    {
                        new PhaseArray(Enumerable.Repeat(0, 10).Select(i => (float) i).ToArray())
                    }
                },
                new BandRadioPath
                {
                    RadioPath = new List<PhaseArray>
                    {
                        new PhaseArray(Enumerable.Repeat(0, 10).Select(i => (float) i).ToArray())
                    }
                },
                new BandRadioPath
                {
                    RadioPath = new List<PhaseArray>
                    {
                        new PhaseArray(Enumerable.Repeat(0, 10).Select(i => (float) i).ToArray())
                    }
                },
                new BandRadioPath
                {
                    RadioPath = new List<PhaseArray>
                    {
                        new PhaseArray(Enumerable.Repeat(0, 10).Select(i => (float) i).ToArray())
                    }
                }
            });
        }

        private PhaseArray[] GetArrays()
        {
            return Enumerable.Range(0, 360)
                .Select(i => GetArray(i))
                .ToArray();
        }


        /// <summary>
        /// direction from 0 to 359
        /// </summary>
        private PhaseArray GetArray(float direction)
        {
            var phases = new float[10];
            phases[0] = direction % 360;
            phases[1] = direction % 360;
            phases[2] = direction % 360;
            phases[3] = direction % 360;

            return new PhaseArray(phases);
        }
        
        private float[] GetPointPhaseDifferencies(float direction)
        {
            var phases = new[] {direction % 360, 0, 0, 0, 0};
            return phases.GetPhaseDifferences();
        }
    }
}

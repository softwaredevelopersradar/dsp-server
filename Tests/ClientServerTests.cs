﻿using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using DataProcessor;
using DataProcessor.Fhss;
using DataStorages;
using Settings;
using DspDataModel;
using DspDataModel.Hardware;
using DspDataModel.Server;
using DspDataModel.Tasks;
using NUnit.Framework;
using Phases;
using Protocols;
using SharpExtensions;
using Simulator;
using TasksLibrary.Modes;
using RadioSource = Protocols.RadioSource;
using Signal = Simulator.Signal;

namespace Tests
{
    // to pass this tests you need to turn off shadow-copying option in resharper
    [TestFixture]
    public class ClientServerTests
    {
        [OneTimeSetUp]
        public void Init()
        {
            Directory.SetCurrentDirectory(TestContext.CurrentContext.TestDirectory);
        }

        private DspClient.DspClient _client;
        private DspServer.DspServer _server;
        private SimulatorCore _simulator;

        [SetUp]
        public async Task Initialize()
        {
            Utils.CreateClientAndServer(out _client, out _server, out _simulator);
            await Utils.Connect(_client);
        }

        // executed after each test
        [TearDown]
        public async Task TearDown()
        {
            await Utils.StopClientAndServer(_client, _server, _simulator);
        }

        [Test]
        public async Task TestClearStorageRequest()
        {
            var radioSourceStorage = _server.TaskManager.RadioSourceStorage;
            var fhssStorage = _server.TaskManager.FhssNetworkStorage;
            
            _server.TaskManager.FilterManager.SetFilters(RangeType.Intelligence, new[] {new Filter(25_000, 35_000, 0, 360)});

            radioSourceStorage.Put(new[]
            {
                new DataStorages.RadioSource(new DataProcessor.Signal(30_000, -20)),
                new DataStorages.RadioSource(new DataProcessor.Signal(34_000, -20))
            });
            var radioSources = radioSourceStorage.GetRadioSources().ToArray();

            fhssStorage.Put(new[]
            {
                new FhssNetwork(25_000, 30_000, 1, 100, 0, 100, false, 128, new float[0], new FixedRadioSource[0]),
                new FhssNetwork(33_000, 35_000, 1, 100, 0, 100, false, 128, new float[0], new FixedRadioSource[0]),
            });
            var fhssNetworks = fhssStorage.GetFhssNetworks().ToArray();

            Assert.AreEqual(radioSourceStorage.GetRadioSources().Count(), 2);
            Assert.AreEqual(fhssStorage.GetFhssNetworks().Count(), 2);

            // hiding one frs radio source
            Assert.AreEqual(
                await _client.PerformStorageAction(StorageType.Frs, SignalAction.Hide, new[] {radioSources[0].Id}),
                RequestResult.Ok);

            Assert.AreEqual(radioSourceStorage.GetRadioSources().Count(), 1);
            Assert.AreEqual(fhssStorage.GetFhssNetworks().Count(), 2);

            // hiding one fhss network
            Assert.AreEqual(
                await _client.PerformStorageAction(StorageType.Fhss, SignalAction.Hide, new[] { fhssNetworks[0].Id }),
                RequestResult.Ok);

            Assert.AreEqual(radioSourceStorage.GetRadioSources().Count(), 1);
            Assert.AreEqual(fhssStorage.GetFhssNetworks().Count(), 1);

            // restore all
            Assert.AreEqual(
                await _client.PerformStorageAction(StorageType.Frs, SignalAction.Restore, new[] { radioSources[0].Id }),
                RequestResult.Ok);
            Assert.AreEqual(
                await _client.PerformStorageAction(StorageType.Fhss, SignalAction.Restore, new[] { fhssNetworks[0].Id }),
                RequestResult.Ok);

            Assert.AreEqual(radioSourceStorage.GetRadioSources().Count(), 2);
            Assert.AreEqual(fhssStorage.GetFhssNetworks().Count(), 2);

            Assert.AreEqual(await _client.ClearStorage(StorageType.Frs), RequestResult.Ok);

            Assert.AreEqual(radioSourceStorage.GetRadioSources().Count(), 0);
            Assert.AreEqual(fhssStorage.GetFhssNetworks().Count(), 2);

            Assert.AreEqual(await _client.ClearStorage(StorageType.Fhss), RequestResult.Ok);

            Assert.AreEqual(radioSourceStorage.GetRadioSources().Count(), 0);
            Assert.AreEqual(fhssStorage.GetFhssNetworks().Count(), 0);
        }

        [Test]
        public async Task TestSettingSectorsAndRanges()
        {
            var filterManager = _server.TaskManager.FilterManager;

            Assert.AreEqual(filterManager.Bands.Count, 0);
            Assert.AreEqual(filterManager.Filters.Count, 0);

            var requestResult = await _client.SetSectorsAndRanges(new[]
            {
                new RangeSector(Constants.FirstBandMinKhz * 10, Constants.FirstBandMinKhz * 10 + 100, 0, 3600)
            });
            Assert.IsTrue(requestResult == RequestResult.Ok);

            Assert.AreEqual(filterManager.Bands.Count, 1);
            Assert.AreEqual(filterManager.Bands[0].Number, 0);

            Assert.AreEqual(filterManager.Filters.Count, 1);
            var filter = filterManager.Filters[0];
            Assert.IsTrue(filter.DirectionFrom == 0 && filter.DirectionTo == 360);
            Assert.IsTrue(
                filter.MinFrequencyKhz == Constants.FirstBandMinKhz && 
                filter.MaxFrequencyKhz == Constants.FirstBandMinKhz + 10);

            const int bandwidth = Constants.BandwidthKhz;
            requestResult = await _client.SetSectorsAndRanges(new[]
            {
                new RangeSector(Constants.FirstBandMinKhz * 10, Constants.FirstBandMinKhz * 10 + bandwidth * 10, 100, 400),
                new RangeSector(Constants.FirstBandMinKhz * 10 + bandwidth * 30, Constants.FirstBandMinKhz * 10 + bandwidth * 50, 200, 500)
            });
            Assert.IsTrue(requestResult == RequestResult.Ok);

            Assert.AreEqual(filterManager.Bands.Count, 3);
            Assert.AreEqual(filterManager.Bands[0].Number, 0);
            Assert.AreEqual(filterManager.Bands[1].Number, 3);
            Assert.AreEqual(filterManager.Bands[2].Number, 4);

            Assert.AreEqual(filterManager.Filters.Count, 2);

            filter = filterManager.Filters[0];
            Assert.IsTrue(filter.DirectionFrom == 10 && filter.DirectionTo == 40);
            Assert.IsTrue(
                filter.MinFrequencyKhz == Constants.FirstBandMinKhz &&
                filter.MaxFrequencyKhz == Constants.FirstBandMinKhz + bandwidth);

            filter = filterManager.Filters[1];
            Assert.IsTrue(filter.DirectionFrom == 20 && filter.DirectionTo == 50);
            Assert.IsTrue(
                filter.MinFrequencyKhz == Constants.FirstBandMinKhz + bandwidth * 3 &&
                filter.MaxFrequencyKhz == Constants.FirstBandMinKhz + bandwidth * 5);

            requestResult = await _client.SetSectorsAndRanges(new[]
            {
                new RangeSector(Constants.FirstBandMinKhz * 10, Constants.FirstBandMinKhz * 10 - 1, 0, 3600)
            });
            Assert.IsTrue(requestResult == RequestResult.InvalidRequest);
            requestResult = await _client.SetSectorsAndRanges(new[]
            {
                new RangeSector(Constants.FirstBandMinKhz * 10 - 1, Constants.FirstBandMinKhz * 10 + 1000, 0, 3600)
            });
            Assert.IsTrue(requestResult == RequestResult.InvalidRequest);
            requestResult = await _client.SetSectorsAndRanges(new[]
            {
                new RangeSector(Constants.FirstBandMinKhz * 10, Constants.FirstBandMinKhz * 10 - 1, -200, 3600)
            });
            Assert.IsTrue(requestResult == RequestResult.InvalidRequest);
            requestResult = await _client.SetSectorsAndRanges(new[]
            {
                new RangeSector(Constants.FirstBandMinKhz * 10, Constants.FirstBandMinKhz * 10 - 1, -200, 3700)
            });
            Assert.IsTrue(requestResult == RequestResult.InvalidRequest);
        }

        [Test]
        public async Task TestSettingMode()
        {
            await _client.SetMode(DspServerMode.Stop);
            Assert.IsTrue(_server.TaskManager.CurrentMode == null);
            Assert.IsFalse(_server.RadioJamManager.IsJammingActive);

            Assert.AreEqual(await _client.SetMode(DspServerMode.RadioIntelligence), RequestResult.InvalidRequest);

            Assert.AreEqual(await _client.SetSectorsAndRanges(new[] {new RangeSector(250000, 300000, 0, 3600)}), RequestResult.Ok);

            Assert.AreEqual(await _client.SetMode(DspServerMode.RadioIntelligence), RequestResult.Ok);
            Assert.IsTrue(_server.TaskManager.CurrentMode is SequenceReceiversMode);
            Assert.IsFalse(_server.RadioJamManager.IsJammingActive);

            Assert.AreEqual(await _client.SetMode(DspServerMode.RadioIntelligenceWithDf), RequestResult.Ok);
            Assert.IsTrue(_server.TaskManager.CurrentMode is RdfMode);
            Assert.AreEqual(_server.TaskManager.Tasks.Count, 1);

            Assert.AreEqual(await _client.SetMode(DspServerMode.Calibration), RequestResult.Ok);
            Assert.IsTrue(_server.TaskManager.CurrentMode is CalibrationMode);
            Assert.AreEqual(_server.TaskManager.Tasks.Count, 1);

            Assert.AreEqual(await _client.SetMode(DspServerMode.Stop), RequestResult.Ok);
            Assert.IsTrue(_server.TaskManager.Tasks.Count == 0);

            // checking radio jam mode

            // mode setting problem because jamming setting are not setup
            Assert.AreEqual(await _client.SetMode(DspServerMode.RadioJammingFrs), RequestResult.InvalidRequest);
            Assert.IsFalse(_server.RadioJamManager.IsJammingActive);

            Assert.AreEqual(await _client.SetFrsRadioJamSettings(200, 200, 4), RequestResult.Ok);
            Assert.AreEqual(await _client.SetMode(DspServerMode.RadioJammingFrs), RequestResult.Ok);

            await Task.Delay(100);

            Assert.IsTrue(_server.RadioJamManager.IsJammingActive);
            Assert.IsTrue(_server.TaskManager.Tasks.Count <= 1);

            Assert.AreEqual(await _client.SetMode(DspServerMode.Stop), RequestResult.Ok);
        }

        [Test]
        public async Task TestSettingFrsTargets()
        {
            var targets = new[]
            {
                new FRSJammingSetting(25_000_0, 0, 1, 2, 3, 0, 100, 10, 0, 0),
                new FRSJammingSetting(35_000_0, 0, 0, 0, 0, 0, 100, 20, 1, 1)
            };

            var requestResult = await _client.SetFrsRadioJamTargets(targets);
            Assert.AreEqual(requestResult, RequestResult.Ok);

            var serverTargets = await _client.GetFrsJammingTargets();
            Assert.AreEqual(targets.Length, serverTargets.Length);

            for (var i = 0; i < targets.Length; ++i)
            {
                var bytes1 = targets[i].GetBytes();
                var bytes2 = serverTargets[i].GetBytes();
                Assert.IsTrue(bytes1.SequenceEqual(bytes2));
            }
        }

        [Test]
        public async Task GetRadioControlSpectrumTest()
        {
            var bands = new[] {0, 5};

            foreach (var band in bands)
            {
                var result = await _client.GetRadioControlSpectrum(band);
                Assert.IsTrue(result != null && result.Length == Constants.BandSampleCount);
            }
        }

        [Test]
        public async Task TestSettingFhssTarget()
        {
            var target = new FhssJammingSetting(35_000_0, 45_000_0, 10, 0, 1, 2, 3, 0, new FhssFixedRadioSource[0]);
            var requestResult = await _client.SetFhssRadioJamTargets(2, FftResolution.N4096, new[] {target});
            Assert.AreEqual(requestResult, RequestResult.Ok);

            var targets = await _client.GetFhssJammingTargets();
            Assert.AreEqual(targets.Length, 1);

            var serverTarget = targets[0];

            var bytes1 = target.GetBytes();
            var bytes2 = serverTarget.GetBytes();

            Assert.IsTrue(bytes1.SequenceEqual(bytes2));
        }

        [Test]
        public async Task TestServerEvents()
        {
            var client2 = new DspClient.DspClient();
            await Utils.Connect(client2);

            var attenuatorsEventFired = false;
            var fhssEventFired = false;
            var frsEventFired = false;
            var filtersEventFired = false;
            var modeEventFired = false;
            var sectorsAndRangesEventFired = false;
            var specialFrequenciesEventFired = false;

            client2.AttenuatorsUpdateEvent += (s, e) => attenuatorsEventFired = true;
            client2.FhssJammingTargetsUpdateEvent += (s, e) => fhssEventFired = true;
            client2.FrsJammingTargetsUpdateEvent += (s, e) => frsEventFired = true;
            client2.FiltersUpdateEvent += (s, e) => filtersEventFired = true;
            client2.ModeUpdateEvent += (s, e) => modeEventFired = true;
            client2.SectorsAndRangesUpdateEvent += (s, e) => sectorsAndRangesEventFired = true;
            client2.SpecialFrequenciesUpdateEvent += (s, e) => specialFrequenciesEventFired = true;

            await _client.SetAttenuatorsValue(0, 10, true);
            await _client.SetFhssRadioJamTargets(2, FftResolution.N4096, new[] {new FhssJammingSetting(25_000_0, 35_000_0, 10, 0, 0, 0, 0, 0, new FhssFixedRadioSource[0]) });
            await _client.SetFrsRadioJamTargets(new[]
            {
                new FRSJammingSetting(25_000_0, 0, 0, 0, 0, 0, 90, 0, 0, 0), 
            });
            await _client.SetFilters(-50, 0, 0, TimeSpan.Zero);
            await _client.SetSectorsAndRanges(new[]
            {
                new RangeSector(25_000_0, 35_000_0, 0, 3600), 
            });
            await _client.SetMode(DspServerMode.RadioIntelligence);
            await _client.SetMode(DspServerMode.Stop);

            await _client.SetSpecialFrequencies(FrequencyType.Forbidden, new[]
            {
                new Protocols.FrequencyRange(25_000_0, 26_000_0), 
            });

            await Task.Delay(100);

            Assert.IsTrue(attenuatorsEventFired);
            Assert.IsTrue(fhssEventFired);
            Assert.IsTrue(frsEventFired);
            Assert.IsTrue(filtersEventFired);
            Assert.IsTrue(modeEventFired);
            Assert.IsTrue(sectorsAndRangesEventFired);
            Assert.IsTrue(specialFrequenciesEventFired);

            client2.Stop();
        }

        [Test]
        public async Task TestAfrsMode()
        {
            var signals = new[]
            {
                new Signal(frequencyKhz: 52_000, bandwidthKhz: 30, direction: 50, amplitude: -55),
                new Signal(frequencyKhz: 71_000, bandwidthKhz: 10, direction: 0, amplitude: -60),
                new Signal(frequencyKhz: 83_000, bandwidthKhz: 40, direction: 180, amplitude: -20)
            };
            foreach (var signal in signals)
            {
                _simulator.SignalSimulator.Signals.Add(signal);
            }

            RadioJamTargetState[] targets = null;

            _client.FrsRadioJamStateUpdateEvent += (sender, e) =>
            {
                if (e.TargetStates.All(t => t.ControlState == 0))
                {
                    // skipping "zero targets" udpate
                    return;
                }
                targets = e.TargetStates;
            };

            await _client.SetAfrsRadioJamSettings(
                emitionDurationMs: 250,
                longWorkingSignalDurationMs: 2000,
                channelsInLiter: 4,
                searchBandwidthKhz: 50_000,
                searchSector: 6,
                threshold: -80);

            await _client.SetFrsRadioJamTargets(new[]
            {
                new FRSJammingSetting(52_000_0, 0, 0, 0, 0, 0, 80, 50_0, 0, 0),
                new FRSJammingSetting(71_000_0, 0, 0, 0, 0, 0, 80, 0, 1, 0),
                new FRSJammingSetting(83_000_0, 0, 0, 0, 0, 0, 80, 180_0, 2, 0),
            });
            await _client.SetMode(DspServerMode.RadioJammingAfrs);

            await Task.Delay(300);

            for (var i = 0; i < targets.Length; i++)
            {
                Assert.AreNotEqual(targets[i].ControlState, 0);
                Assert.AreEqual(targets[i].Frequency * 0.1f, _simulator.SignalSimulator.Signals[i].FrequencyKhz, 100);
            }

            _simulator.SignalSimulator.Signals[0].Direction = 90;
            _simulator.SignalSimulator.Signals[0].FrequencyKhz += 500;

            _simulator.SignalSimulator.Signals[1].Amplitude += 5;
            _simulator.SignalSimulator.Signals[1].FrequencyKhz += 1000;

            await Task.Delay(1000);

            Assert.AreEqual(targets[0].ControlState, 0);

            Assert.AreNotEqual(targets[1].ControlState, 0);
            Assert.AreEqual(-targets[1].Amplitude, _simulator.SignalSimulator.Signals[1].Amplitude, 1);
            Assert.AreEqual(targets[1].Frequency * 0.1f, _simulator.SignalSimulator.Signals[1].FrequencyKhz, 100);

            Assert.AreNotEqual(targets[2].ControlState, 0);
            Assert.AreEqual(targets[2].Frequency / 10, _simulator.SignalSimulator.Signals[2].FrequencyKhz);

            _simulator.SignalSimulator.Signals[0].Direction = 50;
            _simulator.SignalSimulator.Signals[0].FrequencyKhz = 50_000; // move signal to the previous band
            _simulator.SignalSimulator.Signals[1].FrequencyKhz = 74_000;
            _simulator.SignalSimulator.Signals[2].FrequencyKhz = 90_000; // move signal to the next band

            await Task.Delay(1000);

            for (var i = 0; i < targets.Length; i++)
            {
                Assert.AreNotEqual(targets[i].ControlState, 0);
                Assert.AreEqual(targets[i].Frequency * 0.1f, _simulator.SignalSimulator.Signals[i].FrequencyKhz, 100);
            }
        }

        [Test]
        public async Task TestCalibration()
        {
            // hack for avoiding radioPath rewriting
            Config.Instance.CalibrationSettings.RadioPathTableFilename = "temp.bin";

            // setup some attenuators to check whether calibration would return them to thier start state
            await _client.SetAttenuatorsValue(bandNumber: 0, attenuatorValue: 10, isConstAttenuatorEnabled: true);
            await _client.SetAttenuatorsValue(bandNumber: 10, attenuatorValue: 15, isConstAttenuatorEnabled: true);

            var progress = 0;

            // checking default state of calibration progress - it should be 0
            Assert.AreEqual((await _client.GetCalibrationProgress()).Progress, 0);

            await _client.SetMode(DspServerMode.Calibration);

            GetCalibrationProgressResponse response = null;
            while (progress != 255)
            {
                response = await _client.GetCalibrationProgress();
                progress = response.Progress;
            }
            // checking that we have all band deviations
            Assert.AreEqual(response.BandPhaseDeviations.Length, Config.Instance.BandSettings.BandCount);

            var attenuators = await _client.GetAttenuatorsValues(new[] { 0, 1, 10 });
            Assert.AreEqual(attenuators[0].AttenuatorValue / 2, 10);
            Assert.AreEqual(attenuators[1].AttenuatorValue, 0);
            Assert.AreEqual(attenuators[2].AttenuatorValue / 2, 15);

            Assert.AreEqual(attenuators[0].IsConstAttenuatorEnabled, 1);
            Assert.AreEqual(attenuators[1].IsConstAttenuatorEnabled, 0);
            Assert.AreEqual(attenuators[2].IsConstAttenuatorEnabled, 1);
        }

        [Test]
        public async Task TestHistoryStorage()
        {
            var requestResult = await _client.SetSectorsAndRanges(new RangeSector[2]
            {
                new RangeSector(250000, 1000000, 0, 3600), // from 25 to 100 mhz
                new RangeSector(29000000, 30000000, 1000, 2000), // from 2900 to 3000 mhz, sector: 100 - 200 degrees
            });
            Assert.AreEqual(RequestResult.Ok, requestResult);
            await _client.SetMode(DspServerMode.RadioIntelligenceWithDf);

            // after pause we should be able to get 1 second amplitude time spectrum
            await Task.Delay(2000);

            var response = await _client.GetAmplitudeTimeSpectrum(25_000, 35_000, 100, 1);
            Assert.AreEqual(response.Length, 100);

            response = await _client.GetAmplitudeTimeSpectrum(25_000, 3_000_000, 500, 1);
            Assert.AreEqual(response.Length, 500);

            await Task.Delay(1000);

            response = await _client.GetAmplitudeTimeSpectrum(25_000, 3_000_000, 200, 2);
            Assert.AreEqual(response.Length, 200 * 2);

            response = await _client.GetAmplitudeTimeSpectrum(25_000, 3_000_000, 200, 22);
            Assert.IsTrue(response.Length == 22 * 200);

            await _client.SetSectorsAndRanges(new RangeSector[1]
            {
                new RangeSector(250000, 1000000, 0, 3600) // from 25 to 100 mhz
            });

            await Task.Delay(1000);

            response = await _client.GetAmplitudeTimeSpectrum(25_000, 3_000_000, 200, 2);
            // checking request with bounds wider than working range
            Assert.AreEqual(response.Length, 200 * 2);
        }

        [Test]
        public async Task TestReceiverChannels()
        {
            bool IsNoiseGeneratorEnabled() => _server.TaskManager.HardwareController.ReceiverManager.IsNoiseGeneratorEnabled;

            Assert.IsFalse(IsNoiseGeneratorEnabled());

            Assert.IsTrue(await _client.SetReceiversChannel(ReceiverChannel.NoiseGenerator) == RequestResult.Ok);
            Assert.IsTrue(IsNoiseGeneratorEnabled());

            Assert.IsTrue(await _client.SetReceiversChannel(ReceiverChannel.Air) == RequestResult.Ok);
            Assert.IsFalse(IsNoiseGeneratorEnabled());
        }

        [Test]
        public async Task TestShaperEvent()
        {
            var isEventFired = false;
            var isShaperConnected = false;
            const int eventAwaitPauseMs = 200;

            _client.ShaperConnectionUpdateEvent += (sender, isConnected) =>
            {
                isEventFired = true;
                isShaperConnected = isConnected;
            };

            // in simulator mode shaper should connect to any host and port
            _server.RadioJamManager.Shaper.Connect("127.0.0.1", 0, "127.0.0.1", 0);

            await Task.Delay(eventAwaitPauseMs);

            Assert.AreEqual(isEventFired, true);
            Assert.AreEqual(isShaperConnected, true);

            isEventFired = false;
            _server.RadioJamManager.Shaper.Disconnect();

            await Task.Delay(eventAwaitPauseMs);

            Assert.AreEqual(isEventFired, true);
            Assert.AreEqual(isShaperConnected, false);
        }

        [Test]
        public async Task TestSendingFhssForJamming()
        {
            var targets = new[]
            {
                new FhssJammingSetting(25_000_0, 35_000_0, 50, 1, 2, 3, 0, 0, new FhssFixedRadioSource[0]),
                new FhssJammingSetting(45_000_0, 55_000_0, 40, 3, 2, 1, 1, 0, new FhssFixedRadioSource[0]),
            };
            await _client.SetFhssRadioJamTargets(123, FftResolution.N4096, targets);

            var serverTargets = _server.RadioJamManager.Storage.FhssTargets;

            Assert.AreEqual(serverTargets.Count, targets.Length);

            for (var i = 0; i < targets.Length; ++i)
            {
                Assert.AreEqual(targets[i].StartFrequency * 0.1f, serverTargets[i].MinFrequencyKhz, 1e-3f);
                Assert.AreEqual(targets[i].EndFrequency * 0.1f, serverTargets[i].MaxFrequencyKhz, 1e-3f);
                Assert.AreEqual(-targets[i].Threshold, serverTargets[i].Threshold, 1e-3f);

                Assert.AreEqual(targets[i].DeviationCode, serverTargets[i].DeviationCode);
                Assert.AreEqual(targets[i].ManipulationCode, serverTargets[i].ManipulationCode);
                Assert.AreEqual(targets[i].ModulationCode, serverTargets[i].ModulationCode);

                Assert.AreEqual(targets[i].Id, serverTargets[i].Id);
            }
        }

        [Test]
        public async Task TestGettingFhssJamStateUpdateEvent()
        {
            var isFired = false;
            _client.FhssRadioJamStateUpdateEvent += (sender, e) =>
            {
                isFired = true;
            };
            var target = new FhssJammingSetting(35_000_0, 45_000_0, 100, 0, 0, 0, 0, 0, new FhssFixedRadioSource[0]);

            Assert.AreEqual(await _client.SetFrsRadioJamSettings(500, 1000, 4), RequestResult.Ok);
            Assert.AreEqual(await _client.SetFhssRadioJamTargets(1000, FftResolution.N4096, new[] {target}), RequestResult.Ok);
            Assert.AreEqual(await _client.SetMode(DspServerMode.RadioJammingFhss), RequestResult.Ok);

            await Task.Delay(2000);

            Assert.AreEqual(isFired, true);
        }

        [Test]
        public async Task GetScanSpeedTest()
        {
            // scan speed should be zero at server start because server is not scanning anything
            Assert.AreEqual(await _client.GetScanSpeed(), 0, 1e-3);

            await _client.SetSectorsAndRanges(new[] {new RangeSector(25_000_0, 35_000_0, 0, 3600)});
            await _client.SetMode(DspServerMode.RadioIntelligenceWithDf);

            await Task.Delay(500);
            var dfScanSpeed = await _client.GetScanSpeed();
            Assert.IsTrue(dfScanSpeed > 0);

            await _client.SetMode(DspServerMode.RadioIntelligence);

            await Task.Delay(500);

            var sequenceScanSpeed = await _client.GetScanSpeed();
            Assert.IsTrue(sequenceScanSpeed > 0);
            Assert.IsTrue(sequenceScanSpeed > dfScanSpeed);
        }

        [Test]
        public async Task TestStationLocation()
        {
            Assert.AreEqual(await _client.SetStationLocation(10, 11, 3, true, TargetStation.Current), RequestResult.Ok);

            var response = await _client.GetStationLocation();
            Assert.AreEqual(response.Header.ErrorCode, (byte) RequestResult.Ok);
            Assert.AreEqual(response.Latitude, 10, 1e-3);
            Assert.AreEqual(response.Longitude, 11, 1e-3);
            Assert.AreEqual(response.Altitude, 3);
            Assert.AreEqual(response.UseLocation, true);
            Assert.AreEqual(response.Station, TargetStation.Current);
        }

        [Test]
        public async Task TestDirectionCorrection()
        {
            Assert.AreEqual(await _client.GetDirectionCorrection(), -1, 0.1f);

            Assert.AreEqual(await _client.SetDirectionCorrection(10.5f, false), RequestResult.Ok);
            Assert.AreEqual(await _client.GetDirectionCorrection(), -1, 0.1f);

            Assert.AreEqual(await _client.SetDirectionCorrection(10.5f, true), RequestResult.Ok);
            Assert.AreEqual(await _client.GetDirectionCorrection(), 10.5f, 0.1f);
        }

        [Test]
        public async Task TestAttenuators()
        {
            var bandSettings = _server.TaskManager.HardwareController.FpgaDeviceBandsSettings;

            Assert.IsTrue(await _client.SetAttenuatorsValue(0, 10, true) == RequestResult.Ok);
            Assert.IsTrue(bandSettings[0].IsConstantAttenuatorEnabled && bandSettings[0].AttenuatorLevel.ApproxEquals(10));

            Assert.IsTrue(await _client.SetAttenuatorsValue(0, 20, false) == RequestResult.Ok);
            Assert.IsTrue(!bandSettings[0].IsConstantAttenuatorEnabled && bandSettings[0].AttenuatorLevel.ApproxEquals(20));

            Assert.IsTrue(await _client.SetAttenuatorsValue(3, 20, false) == RequestResult.Ok);
            Assert.IsTrue(!bandSettings[3].IsConstantAttenuatorEnabled && bandSettings[3].AttenuatorLevel.ApproxEquals(20));

            Assert.IsTrue(await _client.SetAttenuatorsValue(3, -10, false) == RequestResult.InvalidRequest);
            Assert.IsTrue(await _client.SetAttenuatorsValue(3, 63.5f, false) == RequestResult.InvalidRequest);
        }

        [Test]
        public async Task GetPanoramaSignalsTest()
        {
            var response = await _client.GetPanoramaSignals(25_000, 35_000);
            Assert.AreEqual(response.FixedSignalsCount + response.ImpulseSignalsCount, 0);

            _simulator.SignalSimulator.AddFhssSignal(new FhssSignal(25_000, 35_000, 500, 25, 0, -50));

            await _client.SetSectorsAndRanges(new[] {new RangeSector(25_000_0, 55_000_0, 0, 3600)});
            await _client.SetMode(DspServerMode.RadioIntelligenceWithDf);

            // waiting some time while fhss signals are being loaded into signal storage 
            await Task.Delay(3000);

            response = await _client.GetPanoramaSignals(25_000, 55_000);
            Assert.IsTrue(response.FixedSignalsCount + response.ImpulseSignalsCount > 5);

            // check other frequency range
            response = await _client.GetPanoramaSignals(55_000, 65_000);
            Assert.AreEqual(response.FixedSignalsCount + response.ImpulseSignalsCount, 0);

            // check invalid arguments
            response = await _client.GetPanoramaSignals(75_000, 65_000);
            Assert.AreEqual(response.FixedSignalsCount + response.ImpulseSignalsCount, 0);

            response = await _client.GetPanoramaSignals(5_000, 65_000);
            Assert.AreEqual(response.FixedSignalsCount + response.ImpulseSignalsCount, 0);

            response = await _client.GetPanoramaSignals(5_000, Config.Instance.BandSettings.LastBandMaxKhz + 1);
            Assert.AreEqual(response.FixedSignalsCount + response.ImpulseSignalsCount, 0);
        }

        [Test]
        public async Task TestHeterodyneRequest()
        {
            void CheckRadioSource(HeterodyneRadioSouce radioSource, Signal signal)
            {
                Assert.IsTrue(Math.Abs(radioSource.Frequency / 10 - signal.FrequencyKhz) < 100);
                Assert.IsTrue(PhaseMath.Angle(radioSource.Direction / 10, signal.Direction) < 10);
                Assert.IsTrue(Math.Abs(signal.Amplitude + radioSource.Amplitude) < 2);
                Assert.IsTrue(radioSource.SignalToNoiseRatio >= 20);
                Assert.IsTrue(radioSource.Reliability > 128); // just to make sure that reliability is close to max value (255)
            }

            // avoiding situation where default threshold is lower than noise level
            _server.TaskManager.FilterManager.SetGlobalThreshold(_simulator.SignalSimulator.NoiseLevel +
                                                                _simulator.SignalSimulator.FuzzinessLevel + 5);

            var radioSources = await _client.GetHeterodyneRadioSources(230000, 250000, 1, 1, 1);
            Assert.AreEqual(radioSources.Length, 21);
            foreach (var radioSouce in radioSources)
            {
                Assert.IsTrue(Constants.ReceiverMinAmplitude.ApproxEquals(-radioSouce.Amplitude));
            }

            var signals = new[]
            {
                new Signal(frequencyKhz: 30000, bandwidthKhz: 30, direction: 100, amplitude: -50),
                new Signal(frequencyKhz: 31000, bandwidthKhz: 30, direction: 200, amplitude: -50),
                new Signal(frequencyKhz: 50000, bandwidthKhz: 10, direction: 300, amplitude: -40)
            };
            foreach (var signal in signals)
            {
                _simulator.SignalSimulator.Signals.Add(signal);
            }

            radioSources = await _client.GetHeterodyneRadioSources(30000, 50000, 1, 1, 1);
            Assert.AreEqual(radioSources.Length, 21);

            CheckRadioSource(radioSources[0], signals[0]);
            CheckRadioSource(radioSources[1], signals[1]);
            CheckRadioSource(radioSources.Last(), signals[2]);

            for (var i = 2; i < radioSources.Length - 1; ++i)
            {
                Assert.AreEqual(radioSources[i].Amplitude, (byte) -Constants.ReceiverMinAmplitude);
                Assert.AreEqual(radioSources[i].Frequency, 10 * (30000 + 1000 * i));
                Assert.AreEqual(radioSources[i].Reliability, 0);
            }
        }

        [Test]
        public async Task TestIntelligenceAndRadioJamFilters()
        {
            var intelligenceFilter = new RangeSector(25_000_0, 55_000_0, 0, 360_0);
            var radioJamFilter = new RangeSector(35_000_0, 55_000_0, 10_0, 260_0);
            var filterManager = _server.TaskManager.FilterManager;

            await _client.SetSectorsAndRanges(RangeType.Intelligence, TargetStation.Current, new[] {intelligenceFilter});
            await _client.SetSectorsAndRanges(RangeType.RadioJamming, TargetStation.Current, new[] {radioJamFilter});
            

            Assert.AreEqual(1, filterManager.IntelligenceFilters.Count);
            Assert.AreEqual(1, filterManager.RadioJamFilters.Count);

            Assert.AreEqual(filterManager.Filters[0], filterManager.IntelligenceFilters[0]);

            await _client.SetMode(DspServerMode.RadioIntelligence);
            Assert.AreEqual(filterManager.Filters[0], filterManager.IntelligenceFilters[0]);

            await _client.SetFrsRadioJamSettings(1000, 5000, 4);
            await _client.SetMode(DspServerMode.RadioJammingFrs);
            Assert.AreEqual(filterManager.Filters[0], filterManager.RadioJamFilters[0]);

            await _client.SetMode(DspServerMode.RadioIntelligenceWithDf);
            Assert.AreEqual(filterManager.Filters[0], filterManager.IntelligenceFilters[0]);

            await _client.SetFrsRadioJamSettings(1000, 5000, 4);
            await _client.SetMode(DspServerMode.RadioJammingAfrs);
            Assert.AreEqual(filterManager.Filters[0], filterManager.RadioJamFilters[0]);

            await _client.SetFrsRadioJamSettings(1000, 5000, 4);
            await _client.SetMode(DspServerMode.RadioJammingFrsAuto);
            Assert.AreEqual(filterManager.Filters[0], filterManager.RadioJamFilters[0]);
        }

        [Test]
        public async Task TestDfMode()
        {
            // error, no sectors and ranges set.
            //Assert.AreEqual(await client.SetMode(DspServerMode.RadioIntelligenceWithDF), RequestResult.InvalidServerMode);

            var requestResult = await _client.SetSectorsAndRanges(new[]
            {
                new RangeSector(250000, 1000000, 0, 3600), // from 25 to 100 mhz
                new RangeSector(2000000, 3000000, 1000, 2000), // from 200 to 300 mhz, sector: 100 - 200 degrees
            });
            Assert.AreEqual(requestResult, RequestResult.Ok);

            Assert.AreEqual(await _client.SetMode(DspServerMode.RadioIntelligenceWithDf), RequestResult.Ok);

            var radioSources = await _client.GetRadioSources();
            Assert.AreEqual(radioSources.Length, 0);

            // adding some radioSources
            var signals = new[]
            {
                new Signal(frequencyKhz: 26000, bandwidthKhz: 30, direction: 20, amplitude: -55),
                new Signal(frequencyKhz: 34000, bandwidthKhz: 50, direction: 0, amplitude: -45),
                new Signal(frequencyKhz: 30000, bandwidthKhz: 10, direction: 0, amplitude: _server.TaskManager.FilterManager.Thresholds[0]) // signal with amplitude lower then threshold
            };

            foreach (var signal in signals)
            {
                _simulator.SignalSimulator.Signals.Add(signal);
            }

            await WaitWhenAllSignalsAreScanned(_server, signalCount: 2);
            radioSources = await _client.GetRadioSources();
            Assert.AreEqual(radioSources.Length, 2);

            CheckRadioSource(radioSources[0], signals[0]);
            CheckRadioSource(radioSources[1], signals[1]);

            signals = new[]
            {
                new Signal(frequencyKhz: 120000, bandwidthKhz: 30, direction: 20, amplitude: -55), // signal that is not in working ranges
                new Signal(frequencyKhz: 260000, bandwidthKhz: 10, direction: 0, amplitude: -60), // signal that is in the range but not in the sector
                new Signal(frequencyKhz: 270000, bandwidthKhz: 40, direction: 180, amplitude: -20),
                new Signal(frequencyKhz: 270100, bandwidthKhz: 40, direction: 183, amplitude: -60) // this signal should be merged with the previous one
            };

            foreach (var signal in signals)
            {
                _simulator.SignalSimulator.Signals.Add(signal);
            }

            await WaitWhenAllSignalsAreScanned(_server, signalCount: 3);
            radioSources = await _client.GetRadioSources();
            Assert.AreEqual(radioSources.Length, 3);

            CheckRadioSource(radioSources[2], signals[2]);

            void CheckRadioSource(RadioSource radioSource, Signal signal)
            {
                Assert.IsTrue(Math.Abs(radioSource.Frequency / 10 - signal.FrequencyKhz) < 100);
                Assert.IsTrue(PhaseMath.Angle(radioSource.Direction / 10, signal.Direction) < 10);
                Assert.IsTrue(Math.Abs(signal.Amplitude + radioSource.Amplitude) < 2);
                Assert.IsTrue(radioSource.StandardDeviation < 300); // just to make sure that standard deviation is close to min value (less then 30 degrees)
            }

            async Task WaitWhenAllSignalsAreScanned(DspServer.DspServer dspServer, int signalCount)
            {
                while (dspServer.TaskManager.RadioSourceStorage.GetRadioSources().Count() != signalCount)
                {
                    await Task.Delay(10);
                }
            }
        }

        [Test]
        public async Task TestComplexRanges()
        {
            var rangeSectors = new[]
            {
                new RangeSector(250000, 300000, 0, 3600), // from 25 to 30 mhz
                new RangeSector(550000, 850000, 0, 3600), // from 55 to 85 mhz
                new RangeSector(1250000, 2250000, 0, 3600), // from 125 to 225 mhz
            };

            await _client.SetSectorsAndRanges(rangeSectors);
            Assert.AreEqual(await _client.SetMode(DspServerMode.RadioIntelligenceWithDf), RequestResult.Ok);

            var serverRangeSectors = await _client.GetSectorsAndRanges(RangeType.Intelligence);
            Assert.AreEqual(rangeSectors.Length, serverRangeSectors.Length);
            for (var i = 0; i < rangeSectors.Length; ++i)
            {
                Assert.AreEqual(rangeSectors[i].StartFrequency, serverRangeSectors[i].StartFrequency);
                Assert.AreEqual(rangeSectors[i].StartDirection, serverRangeSectors[i].StartDirection);
                Assert.AreEqual(rangeSectors[i].EndFrequency, serverRangeSectors[i].EndFrequency);
                Assert.AreEqual(rangeSectors[i].EndDirection, serverRangeSectors[i].EndDirection);
            }

            // waiting for whole working range scan completed
            await Task.Delay(3000);
            var spectrum1 = await _client.GetSpectrum(25000, 225000, 2048);

            // get one more spectrum in other mode
            // this two spectrum should be equal at non working range
            Assert.AreEqual(await _client.SetMode(DspServerMode.RadioIntelligence), RequestResult.Ok);

            await Task.Delay(1000);
            var spectrum2 = await _client.GetSpectrum(25000, 225000, 2048);

            const byte noiseLevel = (byte) -Constants.ReceiverMinAmplitude;
            for (var i = 0; i < 2048; ++i)
            {
                if (spectrum1[i] == noiseLevel && spectrum2[i] != noiseLevel)
                {
                    Assert.Fail();
                }
            }
        }

        [Test]
        public async Task TestSettingFrsRadioJamSettings()
        {
            var radioJamManager = _server.RadioJamManager;
            var requestResult = await _client.SetFrsRadioJamSettings(emitionDurationMs: 500, longWorkingSignalDurationMs: 200, channelsInLiter: 2);

            Assert.AreEqual(requestResult, RequestResult.Ok);
            Assert.AreEqual(radioJamManager.EmitDuration, 500);
            Assert.AreEqual(radioJamManager.LongWorkingSignalDurationMs, 200);
            Assert.AreEqual(radioJamManager.ChannelsInLiter, 2);
        }

        [Test]
        public async Task TestSettingAfrsRadioJamSettings()
        {
            var radioJamManager = _server.RadioJamManager;
            var requestResult = await _client.SetAfrsRadioJamSettings(
                emitionDurationMs: 500, longWorkingSignalDurationMs: 200, channelsInLiter: 2, searchBandwidthKhz: 1000, searchSector: 10, threshold: -70);

            Assert.AreEqual(requestResult, RequestResult.Ok);
            Assert.AreEqual(radioJamManager.EmitDuration, 500);
            Assert.AreEqual(radioJamManager.LongWorkingSignalDurationMs, 200);
            Assert.AreEqual(radioJamManager.ChannelsInLiter, 2);
            Assert.AreEqual(radioJamManager.Threshold, -70);
            Assert.AreEqual(radioJamManager.FrequencySearchBandwidthKhz, 1000);
            Assert.AreEqual(radioJamManager.DirectionSearchSector, 10);
        }

        [Test]
        public async Task TestSettingFrsAutoRadioJamSettings()
        {
            var radioJamManager = _server.RadioJamManager;
            var requestResult = await _client.SetFrsAutoRadioJamSettings(
                emitionDurationMs: 500, longWorkingSignalDurationMs: 200, channelsInLiter: 2, signalBandwitdhThresholdKhz: 5, threshold: - 70);

            Assert.AreEqual(requestResult, RequestResult.Ok);
            Assert.AreEqual(radioJamManager.EmitDuration, 500);
            Assert.AreEqual(radioJamManager.LongWorkingSignalDurationMs, 200);
            Assert.AreEqual(radioJamManager.ChannelsInLiter, 2);
            Assert.AreEqual(radioJamManager.Threshold, -70);
            Assert.AreEqual(radioJamManager.MinSignalBandwidthKhz, 5);
        }

        [Test]
        public async Task TestFrsRadioJamming()
        {
            var signals = new[]
            {
                new Signal(frequencyKhz: 26000, bandwidthKhz: 30, direction: 20, amplitude: -55),
                new Signal(frequencyKhz: 30000, bandwidthKhz: 50, direction: 0, amplitude: -45),
            };
            foreach (var signal in signals)
            {
                _simulator.SignalSimulator.Signals.Add(signal);
            }

            var targets = new[]
            {
                new FRSJammingSetting(26000 * 10, 0, 0, 0, 0, 0, 70, 0, Id: 0, Liter: 0), // signal that should be jammed
                new FRSJammingSetting(30000 * 10, 0, 0, 0, 0, 0, 30, 0, Id: 1, Liter: 0), // signal with too high threshold
                new FRSJammingSetting(34000 * 10, 0, 0, 0, 0, 0, 70, 0, Id: 2, Liter: 0), // non existing signal
            };
            int[] eventCallCount = {0};
            var areEventChecksOk = true;
            
            _client.FrsRadioJamStateUpdateEvent += (sender, e) =>
            {
                ++eventCallCount[0];
                if (e.TargetStates.Length != targets.Length)
                {
                    areEventChecksOk = false;
                    return;
                }
                for (var i = 0; i < targets.Length; ++i)
                {
                    if (e.TargetStates[i].Id != targets[i].Id || e.TargetStates[i].Frequency != targets[i].Frequency)
                    {
                        areEventChecksOk = false;
                        return;
                    }
                }
            };

            var requestResult = await _client.SetFrsRadioJamSettings(emitionDurationMs: 500, longWorkingSignalDurationMs: 200, channelsInLiter: 2);
            Assert.AreEqual(requestResult, RequestResult.Ok);
            Assert.AreEqual(await _client.SetFrsRadioJamTargets(targets), RequestResult.Ok);
            Assert.AreEqual(_server.RadioJamManager.Storage.FrsTargets.Count, 3);
            Assert.AreEqual(await _client.SetMode(DspServerMode.RadioJammingFrs), RequestResult.Ok);

            await Task.Delay(550); // 2 events should come

            Assert.IsTrue(eventCallCount[0] > 0);
            Assert.IsTrue(areEventChecksOk);

            await _client.SetMode(DspServerMode.Stop);

            // checking work with empty targets
            targets = new FRSJammingSetting[0];
            Assert.AreEqual(await _client.SetFrsRadioJamTargets(targets), RequestResult.Ok);
            Assert.AreEqual(_server.RadioJamManager.Storage.FrsTargets.Count, 0);
            Assert.AreEqual(await _client.SetMode(DspServerMode.RadioJammingFrs), RequestResult.Ok);
            eventCallCount[0] = 0;

            await Task.Delay(550); // events shouldn't come when there are no targets

            Assert.IsTrue(eventCallCount[0] == 0);
            Assert.IsTrue(areEventChecksOk);
        }
    }
}

﻿using System;
using System.Linq;
using DataProcessor;
using DspDataModel;
using StructTypeGroza;

namespace LinkedStationLibrary
{
    internal static class Utilities
    {
        //commented : stuff to keep/discard ?
        public static TDataScan GetLibrarySignal(ISignal signal)
        {
            var scan = new TDataScan
            { 
                //dtBroadcastStartTime = default(DateTime),
                //dtFirstBroadcastStartTime = default(DateTime),
                tsBroadcastTimeSpan = signal.BroadcastTimeSpan,
                iBandWidth = (int) (signal.BandwidthKhz * 10),
                iBearing = (int) (signal.Direction * 10),
                iCentralFreq = (int) (signal.CentralFrequencyKhz * 10),
                //iDisardedDirectionsPart = (int) (signal.DisardedDirectionsPart * 100),
                iFreq = (int) (signal.FrequencyKhz * 10),
                iId = default(int),
                iPhaseDeviation = (int) (signal.PhaseDeviation * 10),
                //iPhases = signal.Phases.Select(p => (int) (p * 10)).ToArray(),
                iReliability = (int) (signal.Reliability * 100),
                iStandardDeviation = (int) (signal.StandardDeviation * 10),
                sAmplitude = (short) signal.Amplitude,
                bModulation = (byte) signal.Modulation
            };
            return scan;
        }

        public static ISignal GetSignal(TDataScan scan)
        {
            return new Signal(
                scan.iFreq * 0.1f,
                scan.iCentralFreq * 0.1f,
                scan.iBearing * 0.1f,
                scan.iReliability * 0.01f,
                scan.iBandWidth * 0.1f,
                scan.sAmplitude,
                scan.iStandardDeviation * 0.1f,
                0,//scan.iDisardedDirectionsPart * 0.01f,
                scan.iPhaseDeviation * 0.1f,
                1,
                new float[1].ToList(),//scan.iPhases.Select(p => p * 0.1f).ToArray(),
                (SignalModulation) scan.bModulation,
                scan.tsBroadcastTimeSpan
            );
        }
    }
}

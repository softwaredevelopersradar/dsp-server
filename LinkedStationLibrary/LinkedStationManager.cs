﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using DspDataModel;
using DspDataModel.Data;
using DspDataModel.LinkedStation;
using DspDataModel.RadioJam;
using DspDataModel.Server;
using DspDataModel.Storages;
using DspDataModel.Tasks;
namespace LinkedStationLibrary
{
    public class LinkedStationManager : ILinkedStationManager
    {
        public int OwnAddress { get; private set; }
        public int LinkedAddress { get; private set; }

        public bool IsWorking => _activeController?.IsWorking ?? false;
        public bool Connected => _activeController?.Connected ?? false;
        public ConnectionTypes ConnectionType => _activeController?.ConnectionType ?? ConnectionTypes.NoConnection;

        public StationRole Role { get; set; } = StationRole.Standalone;

        public ConnectionState State
        {
            get
            {
                if (Connected)
                {
                    return ConnectionState.Connected;
                }
                if (_server?.IsWorking ?? false)
                {
                    return ConnectionState.Hosted;
                }
                return ConnectionState.NoConnection;
            }
        }

        private ILinkedStationClient _client;
        private ILinkedStationServer _server;

        private ILinkedStationBase _activeController;

        public bool SendText(string text) => _activeController?.SendText(text) ?? false;
        public bool SendSyncTime(DateTime time) => _activeController?.SendSyncTime(time) ?? false;

        public bool SendCoordinates(double latitude, double longitude, int altitude, TargetStation station) => _activeController?.SendCoordinates(latitude, longitude, altitude, station) ?? false;

        public event EventHandler<string> TextReceivedEvent;
        public event EventHandler<(IPosition, TargetStation)> CoordinatesReceivedEvent;
        public event EventHandler ConnectEvent;
        public event EventHandler<DspServerMode> SetModeEvent;
        public event EventHandler<(RangeType, IReadOnlyCollection<Filter>)> SetRangeSectorsEvent;
        public event EventHandler<IReadOnlyCollection<ISignal>> SignalsReceivedEvent;
        public event EventHandler DisconnectEvent;
        public event EventHandler<(FrequencyType, IReadOnlyCollection<FrequencyRange>)> SetSpecialRangesEvent;
        public event EventHandler<IReadOnlyCollection<IRadioJamTarget>> SetFrsJammingTargetsEvent;
        public event EventHandler<IReadOnlyCollection<IRadioJamFhssTarget>> SetFhssJammingTargetsEvent;
        public event EventHandler RequestFrsTargetsEvent;
        public event EventHandler<IReadOnlyCollection<IRadioSource>> ResponseFrsTargetsEvent;
        public event EventHandler<DateTime> SetSyncTimeEvent;


        private readonly IServerController _serverController;

        public LinkedStationManager(IServerController serverController)
        {
            _serverController = serverController;
        }

        public async Task<IReadOnlyCollection<ISignal>> WaitForSignals()
        {
            if (_activeController == null)
            {
                return null;
            }
            return await _activeController.WaitForSignals();
        }

        public bool SendSignals(IEnumerable<ISignal> signals)
        {
            if (_activeController == null)
            {
                return false;
            }
            return _activeController.SendSignals(signals);
        }

        public void Stop()
        {
            _activeController?.Stop();
            Unsubcsribe();

            _activeController = null;
            _server = null;
            _client = null;
        }

        public void SetupAddresses(int ownAddress, int linkedAddress)
        {
            OwnAddress = ownAddress;
            LinkedAddress = linkedAddress;
        }

        private void Subscribe()
        {
            _activeController.TextReceivedEvent += OnTextReceived;
            _activeController.CoordinatesReceivedEvent += OnCoordinatesReceived;
            _activeController.DisconnectEvent += OnActiveControllerDisconnect;
            _activeController.ConnectEvent += OnActiveControllerConnect;
            _activeController.SetModeEvent += OnSetModeRequest;
            _activeController.SetRangeSectorsEvent += OnSetFiltersRequest;
            _activeController.SignalsReceivedEvent += OnSignalsReceived;
            _activeController.SetSpecialRangesEvent += OnSetSpecialRangesRequest;
            _activeController.SetFrsJammingTargetsEvent += OnSetFrsJammingTargetsRequest;
            _activeController.SetFhssJammingTargetsEvent += OnSetFhssJammingTargetsRequest;
            _activeController.RequestFrsTargetsEvent += OnGetFrsTargetsRequest;
            _activeController.ResponseFrsTargetsEvent += OnGetFrsTargetsResponse;
            _activeController.SetSyncTimeEvent += OnSetSyncTimeRequest;
        }

        private void OnSetSyncTimeRequest(object sender, DateTime time)
        {
            SetSyncTimeEvent?.Invoke(this, time);
        }

        private void OnSignalsReceived(object sender, IReadOnlyCollection<ISignal> signals)
        {
            SignalsReceivedEvent?.Invoke(this, signals);
        }

        private void OnSetFiltersRequest(object sender, (RangeType, IReadOnlyCollection<Filter>) filters)
        {
            SetRangeSectorsEvent?.Invoke(this, filters);
        }

        private void OnSetModeRequest(object sender, DspServerMode dspServerMode)
        {
            SetModeEvent?.Invoke(this, dspServerMode);
        }

        private void OnSetSpecialRangesRequest(object sender, (FrequencyType rangeType, IReadOnlyCollection<FrequencyRange> ranges) specialRanges)
        {
            SetSpecialRangesEvent?.Invoke(this,(specialRanges.rangeType, specialRanges.ranges));
        }

        private void OnSetFrsJammingTargetsRequest(object sender, IReadOnlyCollection<IRadioJamTarget> targets)
        {
            SetFrsJammingTargetsEvent?.Invoke(this,targets);
        }

        private void OnSetFhssJammingTargetsRequest(object sender, IReadOnlyCollection<IRadioJamFhssTarget> targets)
        {
            SetFhssJammingTargetsEvent?.Invoke(this,targets);
        }

        private void OnGetFrsTargetsRequest(object sender, EventArgs args)
        {
            RequestFrsTargetsEvent?.Invoke(sender, EventArgs.Empty);
        }

        private void OnGetFrsTargetsResponse(object sender, IReadOnlyCollection<IRadioSource> targets)
        {
            ResponseFrsTargetsEvent?.Invoke(sender, targets);
        }

        private void OnCoordinatesReceived(object sender, (IPosition position, TargetStation station) stationLocation)
        {
            CoordinatesReceivedEvent?.Invoke(this, stationLocation);
        }

        private void Unsubcsribe()
        {
            if (_activeController == null)
            {
                return;
            }
            _activeController.TextReceivedEvent -= OnTextReceived;
            _activeController.CoordinatesReceivedEvent -= OnCoordinatesReceived;
            _activeController.DisconnectEvent -= OnActiveControllerDisconnect;
            _activeController.ConnectEvent -= OnActiveControllerConnect;
            _activeController.SetModeEvent -= OnSetModeRequest;
            _activeController.SetRangeSectorsEvent -= OnSetFiltersRequest;
            _activeController.SignalsReceivedEvent -= OnSignalsReceived;
            _activeController.SetSpecialRangesEvent -= OnSetSpecialRangesRequest;
            _activeController.SetFrsJammingTargetsEvent -= OnSetFrsJammingTargetsRequest;
            _activeController.SetFhssJammingTargetsEvent -= OnSetFhssJammingTargetsRequest;
            _activeController.RequestFrsTargetsEvent -= OnGetFrsTargetsRequest;
            _activeController.ResponseFrsTargetsEvent -= OnGetFrsTargetsResponse;
            _activeController.SetSyncTimeEvent -= OnSetSyncTimeRequest;
        }

        private void OnActiveControllerConnect(object sender, EventArgs eventArgs)
        {
            ConnectEvent?.Invoke(sender, eventArgs);
        }

        private void OnActiveControllerDisconnect(object sender, EventArgs eventArgs)
        {
            DisconnectEvent?.Invoke(sender, eventArgs);
        }

        private void OnTextReceived(object sender, string text)
        {
            TextReceivedEvent?.Invoke(this, text);
        }

        public bool StartServer(string hostname, int port)
        {
            if (IsWorking)
            {
                Stop();
            }
            _server = new LinkedStationServer(OwnAddress, LinkedAddress);
            InitializeController(_server, StationRole.Master);

            return _server.StartServer(hostname, port);
        }

        public bool StartServer(string comPortName)
        {
            if (IsWorking)
            {
                Stop();
            }
            _server = new LinkedStationServer(OwnAddress, LinkedAddress);
            InitializeController(_server, StationRole.Master);

            return _server.StartServer(comPortName);
        }

        private void InitializeController(ILinkedStationBase controller, StationRole role)
        {
            _activeController = controller;
            Role = role;
            Subscribe();
        }

        public async Task<bool> SetRangeSectors(RangeType rangeType, IEnumerable<Filter> filters)
        {
            if (_server == null)
            {
                return false;
            }
            return await _server.SetRangeSectors(rangeType, filters);
        }

        public async Task<bool> SetMode(DspServerMode mode)
        {
            if (_server == null)
            {
                return false;
            }
            return await _server.SetMode(mode);
        }

        public async Task<bool> SetSpecialRanges(FrequencyType rangeType, IEnumerable<FrequencyRange> ranges)
        {
            if (_server == null)
            {
                return false;
            }
            return await _server.SetSpecialRanges(rangeType, ranges);
        }

        public async Task<bool> SetFrsJammingTargets(IReadOnlyCollection<IRadioJamTarget> targets)
        {
            if (_server == null)
            {
                return false;
            }
            return await _server.SetFrsJammingTargets(targets);
        }

        public async Task<bool> SetFhssJammingTargets(IReadOnlyCollection<IRadioJamFhssTarget> targets)
        {
            if (_server == null)
            {
                return false;
            }
            return await _server.SetFhssJammingTargets(targets);
        }

        public async Task<bool> GetFrsTargetsRequest()
        {
            if (_server == null)
            {
                return false;
            }
            return await _server.GetFrsTargetsRequest();
        }

        public async Task<float?> PerformExecutiveDf(float startFrequencyKhz, float endFrequencyKhz, int phaseAveragingCount, int directionAveragingCount)
        {
            if (_activeController == null)
            {
                return null;
            }
            return await _activeController.PerformExecutiveDf(startFrequencyKhz, endFrequencyKhz, phaseAveragingCount, directionAveragingCount);
        }

        /// <summary>
        /// Connect from slave to master via etherner
        /// </summary>
        public bool Connect(string hostname, int port)
        {
            if (IsWorking)
            {
                Stop();
            }
            _client = new LinkedStationClient(_serverController, OwnAddress, LinkedAddress);
            InitializeController(_client, StationRole.Slave);
            return _client.Connect(hostname, port);
        }

        /// <summary>
        /// Connect from slave to master via com port
        /// </summary>
        public bool Connect(string comportName)
        {
            if (IsWorking)
            {
                Stop();
            }
            _client = new LinkedStationClient(_serverController, OwnAddress, LinkedAddress);
            InitializeController(_client, StationRole.Slave);
            return _client.Connect(comportName);
        }

        public async Task<bool> GetFrsTargetsResponse(IReadOnlyCollection<IRadioSource> targets)
        {
            if (_client == null)
                return false;
            return await _client.GetFrsTargetsResponse(targets);
        }
    }
}
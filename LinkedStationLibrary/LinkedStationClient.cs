using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataStorages;
using DspDataModel;
using DspDataModel.Data;
using DspDataModel.LinkedStation;
using DspDataModel.RadioJam;
using DspDataModel.Server;
using DspDataModel.Storages;
using DspDataModel.Tasks;
using GrozaLinkJammerDLL;
using Nito.AsyncEx;
using StructTypeGroza;
namespace LinkedStationLibrary
{
    public class LinkedStationClient : ILinkedStationClient
    {
        private readonly IExJmrAdd _client;

        public int OwnAddress { get; }
        public int LinkedAddress { get; }
        public bool IsWorking { get; private set; }
        public ConnectionTypes ConnectionType { get; private set; }

        private bool _connected;

        public bool Connected
        {
            get => _connected;
            private set
            {
                var isChanged = _connected != value;
                _connected = value;
                if (isChanged)
                {
                    if (_connected)
                    {
                        ConnectEvent?.Invoke(this, EventArgs.Empty);
                    }
                    else
                    {
                        DisconnectEvent?.Invoke(this, EventArgs.Empty);
                    }
                }
            }
        }

        public event EventHandler<string> TextReceivedEvent;
        public event EventHandler<(IPosition, TargetStation)> CoordinatesReceivedEvent;
        public event EventHandler DisconnectEvent;
        public event EventHandler ConnectEvent;
        public event EventHandler<DspServerMode> SetModeEvent;
        public event EventHandler<(RangeType, IReadOnlyCollection<Filter>)> SetRangeSectorsEvent;
        public event EventHandler<IReadOnlyCollection<ISignal>> SignalsReceivedEvent;
        public event EventHandler<(FrequencyType, IReadOnlyCollection<FrequencyRange>)> SetSpecialRangesEvent;
        public event EventHandler<IReadOnlyCollection<IRadioJamTarget>> SetFrsJammingTargetsEvent;
        public event EventHandler<IReadOnlyCollection<IRadioJamFhssTarget>> SetFhssJammingTargetsEvent;
        public event EventHandler RequestFrsTargetsEvent;
        public event EventHandler<IReadOnlyCollection<IRadioSource>> ResponseFrsTargetsEvent;
        public event EventHandler<DateTime> SetSyncTimeEvent;

        private readonly IServerController _serverController;
        

        private readonly AsyncAutoResetEvent _signalsReceivedEvent = new AsyncAutoResetEvent();
        private IReadOnlyCollection<ISignal> _receivedSignals;

        private readonly LinkedStationConfig _config;
        private readonly object _lockObject = new object();

        public LinkedStationClient(IServerController serverController, int ownAddress, int linkedAddress, LinkedStationConfig config = null)
        {
            _config = config ?? Config.Instance.LinkedStationSettings;
            _serverController = serverController;
            OwnAddress = ownAddress;
            LinkedAddress = linkedAddress;
            _client = new IExJmrAdd((byte)OwnAddress, (byte)LinkedAddress);
            _client.OnConnect += OnClientConnect;
            _client.OnDisconnect += OnDisconnect;
            _client.OnReceiveTextCmd += OnReceiveText;
            _client.OnRequestCoord += OnReceiveCoordinates;
            _client.OnRequestExecBear += OnExecutiveDfRequest;
            _client.OnRequestRegime += OnSetModeRequest;
            _client.OnRequestRangeSector += OnSetRangeSectorsRequest;
            _client.OnRequestDataScan += OnSignalsReceived;
            _client.OnRequestRangeSpec += OnSetSpecialRangesRequest;
            _client.OnRequestSupprFWS += OnSetFrsJammingTargetsRequest;
            _client.OnRequestSupprFHSS += OnSetFhssJammingTargetsRequest;
            _client.OnRequestReconFWS += OnGetFrsTargetsRequest;
            _client.OnRequestSynchTime += OnSetSyncTimeRequest;
        }

        private void OnSetSyncTimeRequest(object sender, TTimeMy tTime)
        {
            SetSyncTimeEvent?.Invoke(this, new DateTime(0, 0, 0, tTime.bHour, tTime.bMinute, tTime.bMinute));
        }

        private void OnSignalsReceived(object sender, TDataScan[] tDataScan)
        {
            var signals = tDataScan?.Select(Utilities.GetSignal).ToArray() ?? new ISignal[0];
            SignalsReceivedEvent?.Invoke(this, signals);
            _receivedSignals = signals;
            _signalsReceivedEvent.Set();
        }

        private void OnSetRangeSectorsRequest(object sender, byte type, TRangeSector[] rangeSectors)
        {
            var rangeType = (RangeType) type;
            var filters = rangeSectors.Select(r => new Filter(r.iFregMin / 10, r.iFregMax / 10, r.sAngleMin / 10, r.sAngleMax / 10)).ToArray();
            SetRangeSectorsEvent?.Invoke(this, (rangeType, filters));
            _client.SendRangeSector(0, 0);
        }

        private void OnSetModeRequest(object sender, byte bRegime)
        {
            try
            {
                var mode = (DspServerMode)bRegime;
                _serverController.RaiseSetLinkedStationSlaveModeEvent(mode);
                _client.SendRegime(0);
            }
            catch (Exception e)
            {
                MessageLogger.Error(e, "Linked station set mode request exception");
            }
        }

        private void OnSetSpecialRangesRequest(object sender, byte type, TRangeSpec[] rangeSectors)
        {
            var rangeType = (FrequencyType)type;
            var ranges = rangeSectors.Select(r => new FrequencyRange(r.iFregMin / 10, r.iFregMax / 10)).ToArray();
            SetSpecialRangesEvent?.Invoke(this, (rangeType, ranges));
            _client.SendRangeSpec(0, type);
        }

        private void OnSetFrsJammingTargetsRequest(object sender, int duration, TSupprFWS[] targets)
        {
            var radioJamTargets = targets.Select(
                t => new RadioJamTarget(
                    frequencyKhz : t.iFreq, 
                    priority: t.bPrioritet,
                    threshold:t.bThreshold, 
                    direction:t.sBearing,
                    useAdaptiveThreshold: false,
                    modulationCode: t.bModulation,
                    deviationCode: t.bDeviation, 
                    manipulationCode: t.bManipulation,
                    durationCode: t.bDuration, 
                    id : t.iID,
                    liter : Settings.Utilities.GetLiter(t.iFreq),
                    targetConfig: null //dont forget to initialize target config in server controller!
                    )).ToArray();
            SetFrsJammingTargetsEvent?.Invoke(this,radioJamTargets);
            _client.SendSupprFWS(0);
        }

        private void OnSetFhssJammingTargetsRequest(object sender, int duration, TSupprFHSS[] targets)
        {
            var radioJamFhssTargets = targets.Select(
                t => new RadioJamFhssTarget(
                    minFrequencyKhz: t.iFreqMin,
                    maxFrequencyKhz: t.iFreqMax,
                    threshold: 0, //TODO : idk
                    modulationCode: t.bModulation,
                    deviationCode: t.bDeviation,
                    manipulationCode: t.bManipulation,
                    forbiddenRanges: new List<FrequencyRange>(),
                    id: t.iID
                )).ToArray();
            SetFhssJammingTargetsEvent?.Invoke(this, radioJamFhssTargets);
            _client.SendSupprFHSS(0);
        }

        private void OnGetFrsTargetsRequest(object sender)
        {
            RequestFrsTargetsEvent?.Invoke(this, EventArgs.Empty);
            _client.SendReconFWS(0,new TReconFWS[]{});
        }

        private async void OnExecutiveDfRequest(object sender, int iId, int iFreqMin, int iFreqMax, byte bCountPhase, byte bCountPeleng)
        {
            var startFrequency = iFreqMin * 0.1f;
            var endFrequency = iFreqMax * 0.1f;
            var signal = await _serverController.GetSignal(startFrequency, endFrequency, bCountPhase, bCountPeleng);

            short direction = -1;
            if (signal?.IsDirectionReliable() ?? false)
            {
                direction = (short) (signal.Direction * 10);
            }
            _client.SendExecBear(0, 0, (int) (signal?.FrequencyKhz * 10 ?? 0), direction);
        }

        private void OnReceiveCoordinates(object sender, TCoord coords)
        {
            var station = (TargetStation) Math.Abs(coords.bSignLatitude - 1);// Reverse station type, because "linked" for master is "current" for slave
            var position = new Position(coords.fDegreeLatitude, coords.fDegreeLongitude, coords.sHeight);
            CoordinatesReceivedEvent?.Invoke(this, (position, station)); 
        }

        private void OnDisconnect(object sender)
        {
            Connected = false;
            IsWorking = false;
            ConnectionType = ConnectionTypes.NoConnection;
            DisconnectEvent?.Invoke(this, EventArgs.Empty);
        }

        private void OnReceiveText(object sender, string strText)
        {
            TextReceivedEvent?.Invoke(this, strText);
        }

        private void OnClientConnect(object sender)
        {
            Connected = true;
        }

        public async Task<IReadOnlyCollection<ISignal>> WaitForSignals()
        {
            await _signalsReceivedEvent.WaitAsync();
            return _receivedSignals;
        }

        public bool SendText(string text)
        {
            if (!Connected)
            {
                return false;
            }
            return _client.SendText(text);
        }

        public Task<float?> PerformExecutiveDf(float startFrequencyKhz, float endFrequencyKhz, int phaseAveragingCount, int directionAveragingCount)
        {
            return Task.FromResult<float?>(null);
        }

        public bool SendSyncTime(DateTime time)
        {
            if (!Connected)
            {
                return false;
            }
            return _client.SendSynchTime(0, new TTimeMy
            {
                bHour = (byte) time.Hour,
                bMinute = (byte) time.Minute,
                bSecond = (byte) time.Second
            });
        }

        public bool SendCoordinates(double latitude, double longitude, int altitude, TargetStation station)
        {
            if (!Connected)
            {
                return false;
            }

            return _client.SendCoord(0, new TCoord
            {
                fDegreeLatitude = latitude,
                fDegreeLongitude = longitude,
                sHeight = (short)altitude,
                bSignLatitude = (byte)station
            });
        }

        public bool SendSignals(IEnumerable<ISignal> signals)
        {
            lock (_lockObject)
            {
                var librarySignals = signals.Select(Utilities.GetLibrarySignal).ToArray();
                return _client.SendDataScan(0, librarySignals);
            }
        }

        public async Task<bool> GetFrsTargetsResponse(IReadOnlyCollection<IRadioSource> targets)
        {
            var frequencies = targets.Select(t => new TReconFWS()
            {
                iID = t.Id,
                iFreq = (int)t.FrequencyKhz,
                tTime = new TTimeMy()
                {
                    bHour = (byte)t.BroadcastStartTime.Hour,
                    bMinute = (byte)t.BroadcastStartTime.Minute,
                    bSecond = (byte)t.BroadcastStartTime.Second
                },
                sBearingOwn = (short)t.Direction,
                sBearingLinked = (short)t.LinkedDirection,
                tCoord = new TCoord()
                {
                    fDegreeLatitude = t.Latitude,
                    fDegreeLongitude = t.Longitude,
                },
                bLevelOwn = (byte)t.Amplitude,
                bBandWidth = (byte)t.BandwidthKhz, //TODO : this int to byte hmmm
                bModulation = (byte)t.Modulation,
                bSignAudio = (byte)(t.IsActive ? 1 : 0)
            }).ToArray();

            if (!_client.SendReconFWS(0, frequencies))
            {
                return false;
            }
            return true;
        }

        public void Stop()
        {
            _client.Disconnect();
            ConnectionType = ConnectionTypes.NoConnection;
            IsWorking = false;
            Connected = false;
        }

        public bool Connect(string hostname, int port)
        {
            if (IsWorking)
            {
                return true;
            }
            IsWorking = _client.Connect(hostname, port);
            if(IsWorking)
                ConnectionType = ConnectionTypes.TcpIp;
            return IsWorking;
        }

        public bool Connect(string comportName)
        {
            if (IsWorking)
            {
                return true;
            }
            var com = _config.ComPortSettings;
            _client.Connect(comportName, com.BaudRate, com.Parity, com.DataBits, com.StopBits);
            IsWorking = true;
            ConnectionType = ConnectionTypes.Rs232;
            // client connect event is not fired in the COM-connection mode
            OnClientConnect(null);
            return IsWorking;
        }
    }
}
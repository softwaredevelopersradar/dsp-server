﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataProcessor;
using DspDataModel;
using DspDataModel.Data;
using DspDataModel.LinkedStation;
using DspDataModel.Tasks;
using GrozaLinkJammerDLL;
using Nito.AsyncEx;
using StructTypeGroza;
using DspDataModel.RadioJam;
using DspDataModel.Storages;

namespace LinkedStationLibrary
{
    public class LinkedStationServer : ILinkedStationServer
    {
        private readonly IExJmrMain _server;
        private bool _connected;

        public int OwnAddress { get; }
        public int LinkedAddress { get; }
        public bool IsWorking { get; private set; }
        public ConnectionTypes ConnectionType { get; private set; }

        public bool Connected
        {
            get => _connected;
            private set
            {
                var isChanged = _connected != value;
                _connected = value;
                if (isChanged)
                {
                    if (_connected)
                    {
                        ConnectEvent?.Invoke(this, EventArgs.Empty);
                    }
                    else
                    {
                        DisconnectEvent?.Invoke(this, EventArgs.Empty);
                    }
                }
            }
        }

        public event EventHandler<string> TextReceivedEvent;
        public event EventHandler<(IPosition, TargetStation)> CoordinatesReceivedEvent;
        public event EventHandler DisconnectEvent;
        public event EventHandler ConnectEvent;
        public event EventHandler<DspServerMode> SetModeEvent;
        public event EventHandler<(RangeType, IReadOnlyCollection<Filter>)> SetRangeSectorsEvent;
        public event EventHandler<IReadOnlyCollection<ISignal>> SignalsReceivedEvent;
        public event EventHandler<(FrequencyType, IReadOnlyCollection<FrequencyRange>)> SetSpecialRangesEvent;
        public event EventHandler<IReadOnlyCollection<IRadioJamTarget>> SetFrsJammingTargetsEvent;
        public event EventHandler<IReadOnlyCollection<IRadioJamFhssTarget>> SetFhssJammingTargetsEvent;
        public event EventHandler RequestFrsTargetsEvent;
        public event EventHandler<IReadOnlyCollection<IRadioSource>> ResponseFrsTargetsEvent;
        public event EventHandler<DateTime> SetSyncTimeEvent;

        private TaskCompletionSource<(float frequency, float direction)?> _executiveDfResponseTask;
        private TaskCompletionSource _setModeResponseTask;
        private TaskCompletionSource _setRangeSectorsTask;
        private TaskCompletionSource _setRangeSpecTask;
        private TaskCompletionSource _setFrsJammingTask;
        private TaskCompletionSource _setFhssJammingTask;
        private TaskCompletionSource _getFrsTargetsTask;

        private readonly AsyncAutoResetEvent _signalsReceivedEvent = new AsyncAutoResetEvent();
        private IReadOnlyCollection<ISignal> _receivedSignals;

        private readonly AsyncLock _executiveDfLock = new AsyncLock();
        private readonly AsyncLock _setModeLock = new AsyncLock();
        private readonly AsyncLock _setRangeSectorsLock = new AsyncLock();
        private readonly AsyncLock _setSpecialRangesLock = new AsyncLock();
        private readonly AsyncLock _setFrsJammingTargetsLock = new AsyncLock();
        private readonly AsyncLock _setFhssJammingTargetsLock = new AsyncLock();
        private readonly AsyncLock _getFrsTargetsLock = new AsyncLock();

        private readonly LinkedStationConfig _config;
        private readonly object _lockObject = new object();

        public LinkedStationServer(int ownAddress, int linkedAddress, LinkedStationConfig config = null)
        {
            _config = config ?? Config.Instance.LinkedStationSettings;
            OwnAddress = ownAddress;
            LinkedAddress = linkedAddress;
            _server = new IExJmrMain((byte)OwnAddress, (byte)LinkedAddress);

            _server.OnConnectClient += OnClientConnect;
            _server.OnDisconnectClient += OnClientDisconnected;
            _server.OnTextCmd += OnReceiveText;
            _server.OnConfirmCoord += OnReceiveCoordinates;
            _server.OnConfirmExecBear += OnExecutiveDfResponse;
            _server.OnConfirmRangeSector += OnSetRangeSectorsResponse;
            _server.OnConfirmRegime += OnSetModeResponse;
            _server.OnConfirmDataScan += OnSignalsReceived;
            _server.OnConfirmRangeSpec += OnSetRangeSpecResponse;
            _server.OnConfirmSupprFWS += OnSetFrsTargetsResponse;
            _server.OnConfirmSupprFHSS += OnSetFhssTargetsResponse;
            _server.OnConfirmReconFWS += OnGetFrsTargetsResponse;
            _server.OnConfirmSynchTime += OnGetSyncTimeRequest;
        }

        private void OnGetSyncTimeRequest(object sender, byte bAddressSend, byte bCodeError, TTimeMy tTime)
        {
            SendSyncTime(DateTime.UtcNow);
        }

        private void OnSignalsReceived(object sender, byte bAddressSend, byte bCodeError, TDataScan[] tDataScan)
        {
            var signals = tDataScan?.Select(Utilities.GetSignal).ToArray() ?? new ISignal[0];
            SignalsReceivedEvent?.Invoke(this, signals);
            _receivedSignals = signals;
            _signalsReceivedEvent.Set();
        }

        private void OnSetRangeSectorsResponse(object sender, byte bAddressSend, byte bCodeError, byte bTypeRange)
        {
            _setRangeSectorsTask.SetResult();
        }

        private void OnSetModeResponse(object sender, byte bAddressSend, byte bCodeError)
        {
            _setModeResponseTask.SetResult();
        }

        private void OnSetRangeSpecResponse(object sender, byte bAddressSend, byte bCodeError, byte bTypeRange)
        {
            _setRangeSpecTask.SetResult();
        }

        private void OnSetFrsTargetsResponse(object sender, byte bAddressSend, byte bCodeError)
        {
            _setFrsJammingTask.SetResult();
        }

        private void OnSetFhssTargetsResponse(object sender, byte bAddressSend, byte bCodeError)
        {
            _setFhssJammingTask.SetResult();
        }

        private void OnGetFrsTargetsResponse(object sender, byte bAddressSend, byte bCodeError, TReconFWS[] targets)
        {
            if (targets == null || targets.Length == 0)
            {
                if(_getFrsTargetsTask.Task.IsCompleted == false)
                    _getFrsTargetsTask.SetResult();
                return;
            }
            var now = DateTime.Now;
            var radioSourceTargets = targets.Select(
                    t => new DataStorages.RadioSource(
                        new Signal(
                            frequencyKhz: t.iFreq, 
                            centralFrequencyKhz: t.iFreq, 
                            direction: t.sBearingOwn, 
                            reliability: 0, 
                            bandwidthKhz: t.bBandWidth, 
                            amplitude: t.bLevelOwn, 
                            standardDeviation: 1800, 
                            disardedDirectionsPart: 1, 
                            phaseDeviation: 0, 
                            relativeSubScanCount: 1, 
                            phases: new float[10], 
                            modulation: (SignalModulation)t.bModulation, 
                            broadcastTimeSpan: TimeSpan.FromMilliseconds(0)
                            ),
                        id: t.iID,
                        linkedDirection: t.sBearingLinked,
                        latitude: t.tCoord.fDegreeLatitude,
                        longitude: t.tCoord.fDegreeLongitude,
                        broadcastStartTime: new DateTime(now.Year,now.Month,now.Day, t.tTime.bHour, t.tTime.bMinute, t.tTime.bSecond)
                            )).ToList();
                ResponseFrsTargetsEvent?.Invoke(this, radioSourceTargets);
        }

        private void OnExecutiveDfResponse(object sender, byte bAddressSend, byte bCodeError, int iID, int iFreq, short sOwnBearing)
        {
            if (sOwnBearing == -1)
            {
                _executiveDfResponseTask.SetResult(null);
            }
            else
            {
                _executiveDfResponseTask.SetResult((iFreq * 0.1f, sOwnBearing * 0.1f));
            }
        }

        private void OnClientDisconnected(object sender, string strIPAddress, byte bAddressPC)
        {
            Connected = false;
        }

        private void OnReceiveText(object sender, byte _, string strText)
        {
            TextReceivedEvent?.Invoke(this, strText);
        }

        private void OnClientConnect(object sender, string strIpAddress, byte addressPc)
        {
            Connected = true;
        }

        private void OnReceiveCoordinates(object sender, byte bAddressSend, byte bCodeError, TCoord coords)
        {
            coords.bSignLatitude = (byte)Math.Abs(coords.bSignLatitude - 1);// Reverse station type, because "linked" for master is "current" for slave
            var position = new Position(coords.fDegreeLatitude, coords.fDegreeLongitude, coords.sHeight);
            CoordinatesReceivedEvent?.Invoke(this, (position, (TargetStation)coords.bSignLatitude));
        }

        public async Task<IReadOnlyCollection<ISignal>> WaitForSignals()
        {
            await _signalsReceivedEvent.WaitAsync();
            return _receivedSignals;
        }

        public bool StartServer(string hostname, int port)
        {
            if (IsWorking)
            {
                return true;
            }
            IsWorking = _server.CreateServerRRC(hostname, port);
            if(IsWorking)
                ConnectionType = ConnectionTypes.TcpIp;
            return IsWorking;
        }

        public bool StartServer(string comPortName)
        {
            if (IsWorking)
            {
                return true;
            }
            var com = _config.ComPortSettings;
            IsWorking = _server.ConnectModem(comPortName, com.BaudRate, com.Parity, com.DataBits, com.StopBits, (byte) OwnAddress, (byte) LinkedAddress);
            
            // client connect event is not fired in the COM-connection mode
            OnClientConnect(null, "", 0);
            ConnectionType = ConnectionTypes.Rs232;
            return IsWorking;
        }

        public async Task<float?> PerformExecutiveDf(float startFrequencyKhz, float endFrequencyKhz, int phaseAveragingCount, int directionAveragingCount)
        {
            using (await _executiveDfLock.LockAsync().ConfigureAwait(false))
            {
                _executiveDfResponseTask = new TaskCompletionSource<(float frequency, float direction)?>();

                var startFrequency = (int) (startFrequencyKhz * 10);
                var endFrequency = (int)(endFrequencyKhz * 10);
                if (!_server.SendExecBear((byte) LinkedAddress, 0, startFrequency, endFrequency, (byte) phaseAveragingCount, (byte) directionAveragingCount))
                {
                    return null;
                }
                var result = await _executiveDfResponseTask.Task.ConfigureAwait(false);
                return result?.direction;
            }
        }

        public async Task<bool> SetRangeSectors(RangeType rangesType, IEnumerable<Filter> filters)
        {
            using (await _setRangeSectorsLock.LockAsync().ConfigureAwait(false))
            {
                _setRangeSectorsTask = new TaskCompletionSource();

                var rangeSectors = filters.Select(f => new TRangeSector
                {
                    iFregMin = f.MinFrequencyKhz * 10,
                    iFregMax = f.MaxFrequencyKhz * 10,
                    sAngleMin = (short) (f.DirectionFrom * 10),
                    sAngleMax = (short) (f.DirectionTo * 10)
                }).ToArray();
                if (!_server.SendRangeSector((byte) rangesType, rangeSectors))
                {
                    return false;
                }
                if (ConnectionType == ConnectionTypes.TcpIp)
                    await _setRangeSectorsTask.Task.ConfigureAwait(false);
                return true;
            }
        }

        public async Task<bool> SetMode(DspServerMode mode)
        {
            using (await _setModeLock.LockAsync().ConfigureAwait(false))
            {
                if (!Connected)
                {
                    return false;
                }
                _setModeResponseTask = new TaskCompletionSource();

                if (!_server.SendRegime((byte) mode))
                {
                    return false;
                }
                if (ConnectionType == ConnectionTypes.TcpIp)
                    await _setModeResponseTask.Task.ConfigureAwait(false);
                return true;
            }
        }

        public async Task<bool> SetSpecialRanges(FrequencyType rangeType, IEnumerable<FrequencyRange> ranges)
        {
            using (await _setSpecialRangesLock.LockAsync().ConfigureAwait(false))
            {
                _setRangeSpecTask = new TaskCompletionSource();

                var rangeFrequencies = ranges.Select(f => new TRangeSpec()
                {
                    iFregMin = (int)(f.StartFrequencyKhz * 10),
                    iFregMax = (int)(f.EndFrequencyKhz * 10)
                }).ToArray();
                if (!_server.SendRangeSpec((byte)rangeType, rangeFrequencies))
                {
                    return false;
                }
                if (ConnectionType == ConnectionTypes.TcpIp)
                    await _setRangeSpecTask.Task.ConfigureAwait(false);
                return true;
            }
        }

        public async Task<bool> SetFrsJammingTargets(IReadOnlyCollection<IRadioJamTarget> targets)
        {
            using (await _setFrsJammingTargetsLock.LockAsync().ConfigureAwait(false))
            {
                _setFrsJammingTask = new TaskCompletionSource();
                
                var targetsFrequencies = targets.Select(t => new TSupprFWS()
                {
                    bDeviation = t.DeviationCode,
                    bDuration = t.DurationCode,
                    bManipulation = t.ManipulationCode,
                    bModulation = t.ModulationCode,
                    bPrioritet = (byte)t.Priority,
                    bThreshold = (byte)t.Threshold,
                    sBearing = (short)t.Direction,
                    iFreq = (int)t.FrequencyKhz,
                    iID = t.Id
                }).ToArray();
                if (!_server.SendSupprFWS(0, targetsFrequencies)) //  TODO : Ask Ira for duration not 0, cause I don't understand what is this
                {
                    return false;
                }
                if (ConnectionType == ConnectionTypes.TcpIp)
                    await _setFrsJammingTask.Task.ConfigureAwait(false);
                return true;
            }
        }

        public async Task<bool> SetFhssJammingTargets(IReadOnlyCollection<IRadioJamFhssTarget> targets)
        {
            using (await _setFhssJammingTargetsLock.LockAsync().ConfigureAwait(false))
            {
                _setFhssJammingTask = new TaskCompletionSource();

                var targetsFrequencies = targets.Select(t => new TSupprFHSS()
                {
                    bModulation = t.ModulationCode,
                    bManipulation = t.ManipulationCode,
                    bDeviation = t.DeviationCode,
                    iFreqMin = (int)t.MinFrequencyKhz,
                    iFreqMax = (int)t.MaxFrequencyKhz,
                    iID = t.Id
                }).ToArray();

                if (!_server.SendSupprFHSS(0, targetsFrequencies)) // TODO :  Ask Ira for duration not 0, cause I don't understand what is this
                {
                    return false;
                }
                if (ConnectionType == ConnectionTypes.TcpIp)
                    await _setFhssJammingTask.Task.ConfigureAwait(false);
                return true;
            }
        }

        public async Task<bool> GetFrsTargetsRequest()
        {
            using (await _getFrsTargetsLock.LockAsync().ConfigureAwait(false))
            {
                _getFrsTargetsTask = new TaskCompletionSource();
                
                if (!_server.SendReconFWS()) 
                {
                    return false;
                }
                if(ConnectionType == ConnectionTypes.TcpIp)
                    await _getFrsTargetsTask.Task.ConfigureAwait(false);
                return true;
            }
        }

        public bool SendSyncTime(DateTime time)
        {
            if (!Connected)
            {
                return false;
            }
            return _server.SendSynchTime(new TTimeMy
            {
                bHour = (byte) time.Hour,
                bMinute = (byte) time.Minute,
                bSecond = (byte) time.Second
            });
        }

        public bool SendCoordinates(double latitude, double longitude, int altitude, TargetStation station)
        {
            if (!Connected)
            {
                return false;
            }

            return _server.SendCoord(new TCoord
            {
                fDegreeLatitude = latitude,
                fDegreeLongitude = longitude,
                sHeight = (short) altitude,
                bSignLatitude = (byte) station
            });
        }

        public bool SendText(string text)
        {
            if (!Connected)
            {
                return false;
            }
            return _server.SendText(text);
        }

        public bool SendSignals(IEnumerable<ISignal> signals)
        {
            lock (_lockObject)
            {
                var librarySignals = signals.Select(Utilities.GetLibrarySignal).ToArray();
                return _server.SendDataScan(librarySignals);
            }
        }

        public void Stop()
        {
            _server.DisconnectModem();
            _server.DestroyServerRRC();

            Connected = false;
            IsWorking = false;
            ConnectionType = ConnectionTypes.NoConnection;
        }
    }
}
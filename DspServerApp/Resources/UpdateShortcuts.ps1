$startupPath = $env:APPDATA + "\Microsoft\Windows\Start Menu\Programs\Startup\Пеленгатор.lnk"
$desktopPath = "$Home\Desktop\Пеленгатор.lnk"


$WshShell = New-Object -comObject WScript.Shell

Remove-Item $startupPath
$Shortcut = $WshShell.CreateShortcut($startupPath)
$Shortcut.TargetPath = (Resolve-Path .\).Path + "\DspServerApp.exe"
$Shortcut.Save()

Remove-Item $desktopPath
$Shortcut = $WshShell.CreateShortcut($desktopPath)
$Shortcut.TargetPath = (Resolve-Path .\).Path + "\DspServerApp.exe"
$Shortcut.Save()
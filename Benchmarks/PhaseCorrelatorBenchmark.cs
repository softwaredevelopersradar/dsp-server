﻿using System;
using System.Linq;
using BenchmarkDotNet.Attributes;
using BenchmarkDotNet.Attributes.Jobs;
using Settings;
using Correlator;
using DspDataModel.Data;

namespace Benchmarks
{
    [ShortRunJob]
    public class PhaseCorrelatorBenchmark
    {
        private PhaseCorrelator _phaseCorrelator;

        private float[][] _phases;

        private int _index;

        private IDataScan _dataScan;

        [Setup]
        public void Setup()
        {
            _phaseCorrelator = new PhaseCorrelator("");
            var random = new Random(DateTime.Now.Millisecond);

            _phases = new float[1000][];
            for (var i = 0; i < _phases.Length; ++i)
            {
                _phases[i] = Enumerable.Repeat(0, 10).Select(_ => (float) random.Next(0, 359)).ToArray();
            }

            _dataScan = Utils.CreateDataScan();
        }

        private void Increment()
        {
            ++_index;
            if (_index == _phases.Length)
            {
                _index = 0;
            }
        }

        [Benchmark]
        public void TestSimpleCalculateDirection()
        {
            Increment();
            _phaseCorrelator.CalculateDirection(Constants.FirstBandMinKhz + _index * 1000, _phases[_index]);
        }

        [Benchmark]
        public void TestSimpleDataScanCalculateDirection()
        {
            Increment();
            _phaseCorrelator.CalculateDirection(Constants.DefaultThresholdValue, _dataScan, _index * 9);
        }

        [Benchmark]
        public void TestCalculateDirection()
        {
            Increment();
            _phaseCorrelator.CalculateDirection(Constants.DefaultThresholdValue, _dataScan, _index * 9, _index * 9 + 10);
        }
    }
}

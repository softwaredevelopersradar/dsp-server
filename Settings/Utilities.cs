﻿using System;

namespace Settings
{
    public static class Utilities
    {
        public static float GetFrequencyKhz(int bandNumber, int sampleNumber)
        {
            Contract.Assert(bandNumber >= 0, $"Band number value is {bandNumber}, but it can't be less than zero");
            return Constants.FirstBandMinKhz + Constants.BandwidthKhz * bandNumber + Constants.SamplesGapKhz * sampleNumber;
        }

        public static int GetBandNumber(int frequencyKhz)
        {
            Contract.Assert(frequencyKhz >= Constants.FirstBandMinKhz, $"Frequency is {frequencyKhz} KHz, but it can't be less than {Constants.FirstBandMinKhz}");
            return (frequencyKhz - Constants.FirstBandMinKhz) / Constants.BandwidthKhz;
        }

        public static int GetBandNumber(float frequencyKhz)
        {
            Contract.Assert(frequencyKhz >= Constants.FirstBandMinKhz, $"Frequency is {frequencyKhz} KHz, but it can't be less than {Constants.FirstBandMinKhz}");
            return (int) ((frequencyKhz - Constants.FirstBandMinKhz) / Constants.BandwidthKhz);
        }

        public static int GetBandNumber(float frequencyKhz, BandBorderSelectionPolicy bandSelectionPolicy, int bandCount)
        {
            var bandNumber = GetBandNumber(frequencyKhz);
            var sampleNumber = GetSampleNumber(frequencyKhz, bandNumber);

            // handle boundary cases
            if (sampleNumber == 0 && bandNumber == 0)
            {
                return 0;
            }
            if (sampleNumber == 0 && bandNumber == bandCount)
            {
                return bandCount - 1;
            }

            // current sample is on the bands border
            if (sampleNumber == 0 && bandSelectionPolicy == BandBorderSelectionPolicy.TakeLeft)
            {
                return bandNumber - 1;
            }
            if (sampleNumber == Constants.BandSampleCount - 1 && bandSelectionPolicy == BandBorderSelectionPolicy.TakeRight)
            {
                return bandNumber + 1;
            }

            return bandNumber;
        }

        public static int GetBandSamplesCount(int receiverSamplesCount)
        {
            return (Constants.BandwidthKhz * (receiverSamplesCount - 1) - receiverSamplesCount + 1) / Constants.ReceiverBandwidthKhz + 2;
        }

        public static int GetBandMinFrequencyKhz(int bandNumber)
        {
            Contract.Assert(bandNumber >= 0, $"Band number value is {bandNumber}, but it can't be less than zero");
            return Constants.FirstBandMinKhz + Constants.BandwidthKhz * bandNumber;
        }

        public static int GetBandMaxFrequencyKhz(int bandNumber)
        {
            Contract.Assert(bandNumber >= 0, $"Band number value is {bandNumber}, but it can't be less than zero");
            return Constants.FirstBandMinKhz + Constants.BandwidthKhz * (bandNumber + 1);
        }

        public static int GetSampleNumber(float frequencyKhz)
        {
            var bandNumber = GetBandNumber(frequencyKhz);
            return GetSampleNumber(frequencyKhz, bandNumber);
        }

        public static int GetSampleNumber(float frequencyKhz, int bandNumber)
        {
            return GetSamplesCount(GetBandMinFrequencyKhz(bandNumber), frequencyKhz);
        }

        /// <summary>
        /// calculates count of samples between two frequencies (for frequencies F and F+sampleGap result is 1)
        /// </summary>
        public static int GetSamplesCount(float startFrequencyKhz, float endFrequencyKhz, float samplesGapKhz = Constants.SamplesGapKhz)
        {
            return (int) ((endFrequencyKhz - startFrequencyKhz + samplesGapKhz - 1) / samplesGapKhz);
        }

        private static readonly int[] LiterEdgesKhz =
        {
            50_000,
            90_000,
            160_000,
            290_000,
            512_000,
            860_000,
            1215_000,
            2000_000,
            3000_000
        };

        /// <summary>
        /// Get liter for radio jamming
        /// </summary>
        public static int GetLiter(float frequencyKhz)
        {
            Contract.Assert(
                frequencyKhz >= Constants.MinRadioJamFrequencyKhz, 
                $"Frequency is {frequencyKhz} KHz can't be used in radio jamming (min radio jam frequency is {Constants.MinRadioJamFrequencyKhz})");

            if (frequencyKhz < LiterEdgesKhz[0])
            {
                return 1;
            }

            for (var i = 0; i < LiterEdgesKhz.Length; ++i)
            {
                if (frequencyKhz < LiterEdgesKhz[i])
                {
                    return i + 1;
                }
            }
            if (frequencyKhz <= LiterEdgesKhz[LiterEdgesKhz.Length - 1])
            {
                return LiterEdgesKhz.Length;
            }
            throw new ArgumentException($"frequency {frequencyKhz} KHz is to high for radio jam frequency");
        }
    }

    /// <summary>
    /// Selects what band to choose when a sample is lying right on the bands border
    /// </summary>
    public enum BandBorderSelectionPolicy
    {
        TakeLeft, TakeRight
    }
}

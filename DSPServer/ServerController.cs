﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataStorages;
using DspDataModel;
using DspDataModel.Data;
using DspDataModel.LinkedStation;
using DspDataModel.RadioJam;
using DspDataModel.Server;
using DspDataModel.Tasks;
using Nito.AsyncEx;
using TasksLibrary;
using TasksLibrary.Modes;
using TasksLibrary.Tasks;
using System.Diagnostics.Contracts;
using DspDataModel.Storages;
using FrequencyRange = DspDataModel.FrequencyRange;
using FrequencyType = DspDataModel.FrequencyType;

namespace DspServer
{
    public class ServerController : IServerController
    {
        public ITaskManager TaskManager { get; }
        public IRadioJamManager RadioJamManager { get; }
        public ILinkedStationManager LinkedStationManager { get; }

        public DspServerMode CurrentMode { get; private set; } = DspServerMode.Stop;
        public DspServerMode PreDurationMeasurementMode { get; private set; } = DspServerMode.Stop;

        public event EventHandler<IServerCommandResult> ModeChangedEvent;
        public event EventHandler<SectorAndRangesChangedEventArgs> SectorsAndRangesChangedEvent;
        public event EventHandler<RadioJamTargetsChangedEventArgs> RadioJamFrsTargetsChangedEvent;
        public event EventHandler<SpecialRangesChangedEventArgs> SpecialRangesChangedEvent;
        public event EventHandler<DspServerMode> SetLinkedStationSlaveModeEvent;
        public event EventHandler<RadioJamFhssTargetsChangedEventArgs> RadioJamFhssTargetsChangedEvent;
        public event EventHandler RequestFrsTargetsEvent;
        public event EventHandler<IReadOnlyCollection<IRadioSource>> ResponseFrsTargetsEvent;
        public event EventHandler<string> TextFromLinkedStationReceivedEvent;
        public event EventHandler<StationLocationChangedEventArgs> CoordinatesFromLinkedStationReceivedEvent;
        public event EventHandler<DateTime> LinkedStationSyncTimeEvent;

        private readonly AsyncLock _setModeLock = new AsyncLock();

        private readonly Config _config;

        public ServerController(UpdateFrsJamStateDelegate updateFrsJamState, UpdateFhssJamStateDelegate updateFhssJamState)
            : this(updateFrsJamState, updateFhssJamState, Config.Instance)
        { }

        public ServerController(UpdateFrsJamStateDelegate updateFrsJamState, UpdateFhssJamStateDelegate updateFhssJamState, Config config)
        {
            _config = config;
            TaskManager = new TaskManager(_config);
            LinkedStationManager = new LinkedStationLibrary.LinkedStationManager(this);
            SubscribeOnLinkedStationEvents();
            
            var radioJamStorage = new RadioJamTargetStorage();
            RadioJamManager = new RadioJamManager(this, radioJamStorage, updateFrsJamState, updateFhssJamState);
            
            TaskManager.FhssDurationMeasuringStartEvent += OnFhssDurationMeasuringStart;
        }

        private void OnFhssDurationMeasuringStart(object sender, EventArgs e)
        {
            SetMode(DspServerMode.FhssDurationMeasuring).Wait(10000);//todo : check wait
        }

        public bool Initialize() => TaskManager.Initialize();

        private void SubscribeOnLinkedStationEvents()
        {
            LinkedStationManager.CoordinatesReceivedEvent += OnCoordinatesReceivedFromLinkedStation;
            LinkedStationManager.SetRangeSectorsEvent += OnLinkedStationRangeSectorsReceived;
            LinkedStationManager.ConnectEvent += OnLinkedStationConnected;
            LinkedStationManager.SetModeEvent += OnLinkedStationSetMode;
            LinkedStationManager.SetSpecialRangesEvent += OnLinkedStationSetSpecialRanges;
            LinkedStationManager.SetFrsJammingTargetsEvent += OnLinkedStationSetFrsJammingTargets;
            LinkedStationManager.SetFhssJammingTargetsEvent += OnLinkedStationSetFhssJammingTargets;
            LinkedStationManager.RequestFrsTargetsEvent += OnLinkedStationGetFrsTargetsRequest;
            LinkedStationManager.ResponseFrsTargetsEvent += OnLinkedStationGetFrsTargetsResponse;
            LinkedStationManager.TextReceivedEvent += OnLinkedStationTextReceivedEvent;
            LinkedStationManager.SetSyncTimeEvent += OnLinkedStationSyncTimeEvent;
        }

        private void OnLinkedStationSyncTimeEvent(object sender, DateTime time)
        {
            LinkedStationSyncTimeEvent?.Invoke(sender, time);
        }

        private void OnLinkedStationSetMode(object sender, DspServerMode mode)
        {
            SetMode(mode);
        }

        private void OnLinkedStationSetSpecialRanges(object sender, (FrequencyType rangeType, IReadOnlyCollection<FrequencyRange> ranges) specialRanges)
        {
            SetSpecialFrequencies(specialRanges.ranges,specialRanges.rangeType,TargetStation.Current);
        }

        private void OnLinkedStationSetFrsJammingTargets(object sender, IReadOnlyCollection<IRadioJamTarget> targets)
        {
            var targetsWithConfig = targets.Select(t => new RadioJamTarget(
                frequencyKhz: t.FrequencyKhz,
                priority: t.Priority,
                threshold: t.Threshold,
                direction: t.Direction,
                useAdaptiveThreshold: false,
                modulationCode: t.ModulationCode,
                deviationCode: t.DeviationCode,
                manipulationCode: t.ManipulationCode,
                durationCode: t.DurationCode,
                id: t.Id,
                liter: t.Liter,
                targetConfig: RadioJamManager
            )).ToArray();

            SetFrsJammingTargets(targetsWithConfig, TargetStation.Current);
        }

        private void OnLinkedStationSetFhssJammingTargets(object sender, IReadOnlyCollection<IRadioJamFhssTarget> targets)
        { 
            SetFhssJammingTargets(targets, TargetStation.Current);
        }

        private void OnLinkedStationGetFrsTargetsRequest(object sender, EventArgs e)
        {
            RequestFrsTargetsEvent?.Invoke(sender, e);
        }

        private void OnLinkedStationGetFrsTargetsResponse(object sender, IReadOnlyCollection<IRadioSource> targets)
        {
            ResponseFrsTargetsEvent?.Invoke(sender,targets);
        }

        private async void OnLinkedStationConnected(object sender, EventArgs e)
        {
            var position = Config.Instance.StationPosition;
            if (position.AreCoordinatesDefined())
            {
                SetStationLocation(position.Latitude, position.Longitude, position.Altitude, TargetStation.Current);
            }
            await LinkedStationManager.SetRangeSectors(RangeType.Intelligence, TaskManager.FilterManager.IntelligenceFilters).ConfigureAwait(false);
            await LinkedStationManager.SetRangeSectors(RangeType.RadioJamming, TaskManager.FilterManager.RadioJamFilters).ConfigureAwait(false);
            await LinkedStationManager.SetMode(CurrentMode).ConfigureAwait(false);
        }

        private async void OnLinkedStationRangeSectorsReceived(object sender, (RangeType, IReadOnlyCollection<Filter>) filters)
        {
            var (ranges, rangeType) = filters;
            await SetSectorsAndRanges(rangeType, ranges, TargetStation.Current);
        }

        private void OnCoordinatesReceivedFromLinkedStation(object sender, (IPosition position, TargetStation station) stationLocation)
        {
            var stationConfig = stationLocation.station == TargetStation.Current ?
                _config.StationPosition : _config.LinkedStationPosition;
            var isLocationChanged = Math.Abs(stationConfig.Latitude - stationLocation.position.Latitude) > 1e-7 ||
                                    Math.Abs(stationConfig.Longitude - stationLocation.position.Longitude) > 1e-7 ||
                                    Math.Abs(stationConfig.Altitude - stationLocation.position.Altitude) > 1;
            if (isLocationChanged)
            {
                stationConfig.Latitude = stationLocation.position.Latitude;
                stationConfig.Longitude = stationLocation.position.Longitude;
                stationConfig.Altitude = stationLocation.position.Altitude;

                var eventArgs = new StationLocationChangedEventArgs(stationLocation.position, stationLocation.station, RequestResult.Ok, -1, true);
                CoordinatesFromLinkedStationReceivedEvent?.Invoke(this, eventArgs);
            }
        }

        private void OnLinkedStationTextReceivedEvent(object sender, string text)
        {
            TextFromLinkedStationReceivedEvent?.Invoke(sender, text);
        }

        public void RaiseSetLinkedStationSlaveModeEvent(DspServerMode mode)
        {
            SetLinkedStationSlaveModeEvent?.Invoke(this, mode);
        }

        public bool StartLinkedStationServer(string address)
        {
            if (address.StartsWith("COM"))
            {
                return LinkedStationManager.StartServer(address);
            }
            else
            {
                var (host, port) = SplitAddress(address);
                return LinkedStationManager.StartServer(host, port);
            }
        }

        public void UpdateFrsRadioJamTargets(IReadOnlyList<IRadioJamTarget> targets, int clientId = -1)
        {
            var areTargetsChanged = RadioJamManager.Storage.UpdateFrsTargets(targets);
            if (areTargetsChanged)
            {
                var eventArgs = new RadioJamTargetsChangedEventArgs(targets, TargetStation.Current, RequestResult.Ok,  clientId, true);
                RadioJamFrsTargetsChangedEvent?.Invoke(this, eventArgs);
            }
        }

        public bool ConnectToLinkedStation(string address)
        {
            if (address.StartsWith("COM"))
            {
                return LinkedStationManager.Connect(address);
            }
            else
            {
                var (host, port) = SplitAddress(address);
                return LinkedStationManager.Connect(host, port);
            }
        }

        private (string, int) SplitAddress(string address)
        {
            var parts = address.Split(':');
            Contract.Assert(parts.Length == 2);
            var host = parts[0];
            var port = int.Parse(parts[1]);
            return (host, port);
        }

        public async Task<RequestResult> SetMode(DspServerMode serverMode, int clientId = -1)
        {
            if (CurrentMode == serverMode && serverMode != DspServerMode.Calibration)
            {
                return RequestResult.Ok;
            }

            using (await _setModeLock.LockAsync())
            {
                if (!serverMode.IsRadioJamMode() && RadioJamManager.IsJammingActive)
                {
                    await RadioJamManager.Stop();
                }
                if (serverMode == DspServerMode.Stop)
                {
                    TaskManager.ClearTasks();
                }
                else if (serverMode.IsRadioJamMode())
                {
                    //TODO : remove, test only
                    if (serverMode == DspServerMode.RadioJammingFrsAuto && Config.Instance.FhssSearchSettings.FhssDurationMeasureTest)
                        serverMode = DspServerMode.FhssDurationMeasuring;

                    TaskManager.FilterManager.WorkingRangeType = RangeType.RadioJamming;
                    var jammingMode = ServerModeToRadioJamMode(serverMode);
                    if ((jammingMode != RadioJamMode.Fhss && jammingMode != RadioJamMode.FhssDurationMeasurement) 
                        && RadioJamManager.EmitDuration <= 0)
                    {
                        return RequestResult.InvalidRequest;
                    }
                    TaskManager.ClearTasks();
                    RadioJamManager.Start(jammingMode);
                }
                else
                {
                    // Intelligence
                    if (CurrentMode != DspServerMode.FhssDurationMeasuring)//no clears after measuring
                    {
                        TaskManager.SignalStorage.Clear();
                        TaskManager.FhssNetworkStorage.Clear();
                        TaskManager.RadioSourceStorage.Clear();
                    }

                    if (serverMode != DspServerMode.Calibration && TaskManager.FilterManager.Filters.Count == 0)
                    {
                        return RequestResult.InvalidRequest;
                    }
                    TaskManager.FilterManager.WorkingRangeType = RangeType.Intelligence;
                    var modeController = ServerModeToModeController(serverMode);
                    await modeController.Initialize();
                    TaskManager.SetMode(modeController);
                }
                if (serverMode == DspServerMode.FhssDurationMeasuring &&
                    PreDurationMeasurementMode == DspServerMode.Stop)
                    PreDurationMeasurementMode = CurrentMode;
                else
                    PreDurationMeasurementMode = DspServerMode.Stop; //nullifying preMode just in case of something

                var sendEvent = serverMode != DspServerMode.FhssDurationMeasuring && CurrentMode != DspServerMode.FhssDurationMeasuring;
                CurrentMode = serverMode;

                if(sendEvent)
                    ModeChangedEvent?.Invoke(this, new ServerCommandResult(RequestResult.Ok, true, clientId));

                if(LinkedStationManager.Role == StationRole.Master && CurrentMode != DspServerMode.FhssDurationMeasuring)
                    await LinkedStationManager.SetMode(CurrentMode);
                return RequestResult.Ok;
            }

            RadioJamMode ServerModeToRadioJamMode(DspServerMode mode)
            {
                switch (mode)
                {
                    case DspServerMode.RadioJammingFrs:
                        return RadioJamMode.Frs;
                    case DspServerMode.RadioJammingAfrs:
                        return RadioJamMode.Afrs;
                    case DspServerMode.RadioJammingFrsAuto:
                        return RadioJamMode.FrsAuto;
                    case DspServerMode.RadioJammingFhss:
                        return RadioJamMode.Fhss;
                    case DspServerMode.FhssDurationMeasuring:
                        return RadioJamMode.FhssDurationMeasurement;
                    default:
                        throw new ArgumentOutOfRangeException(nameof(mode), mode, null);
                }
            }

            IModeController ServerModeToModeController(DspServerMode mode)
            {
                switch (mode)
                {
                    case DspServerMode.RadioIntelligence:
                        return new SequenceReceiversMode(TaskManager);
                    case DspServerMode.RadioIntelligenceWithDf:
                        return new RdfMode(TaskManager, LinkedStationManager);
                    case DspServerMode.Calibration:
                        return new CalibrationMode(TaskManager);
                    default:
                        throw new ArgumentOutOfRangeException(nameof(mode), mode, null);
                }
            }
        }

        public bool PerformStorageAction(StorageType storage, SignalAction action, int[] signalIds)
        {
            var isStorageChanged = false;

            if (action == SignalAction.Hide && signalIds.Length == 0)
            {
                ClearStorage();
            }
            else
            {
                PerformAction();
            }

            return isStorageChanged;

            void PerformAction()
            {
                if (signalIds.Length == 0)
                {
                    return;
                }
                switch (storage)
                {
                    case StorageType.Frs:
                        TaskManager.RadioSourceStorage.PerformAction(action, signalIds);
                        break;
                    case StorageType.Fhss:
                        TaskManager.FhssNetworkStorage.PerformAction(action, signalIds);
                        break;
                    default:
                        MessageLogger.Error("perform action storage request error: unkwown storage type!");
                        throw new ArgumentOutOfRangeException();
                }

                isStorageChanged = true;
            }
            void ClearStorage()
            {
                switch (storage)
                {
                    case StorageType.Frs:
                        isStorageChanged = TaskManager.RadioSourceStorage.GetRadioSources().Any();
                        TaskManager.RadioSourceStorage.Clear();
                        break;
                    case StorageType.Fhss:
                        isStorageChanged = TaskManager.FhssNetworkStorage.GetFhssNetworks().Any();
                        TaskManager.FhssNetworkStorage.Clear();
                        break;
                    default:
                        MessageLogger.Error("Clear storage request error: unknown storage type!");
                        throw new ArgumentOutOfRangeException();
                }
            }
        }

        public async Task<RequestResult> SetSectorsAndRanges(IReadOnlyCollection<Filter> filters, RangeType rangeType, TargetStation station,  int clientId = -1)
        {
            IFilterManager manager = null;
            switch (station)
            {
                case TargetStation.Current:
                    manager = TaskManager.FilterManager;
                    break;
                case TargetStation.Linked:
                    manager = TaskManager.LinkedStationFilterManager;
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(station), station, null);
            }

            if (AreWrongIntelligenceFilters() || AreWrongFrsAutoFilters())
            {
                // we can't work at intelligence mode without sectors and ranges
                await SetMode(DspServerMode.Stop).ConfigureAwait(false);
            }

            var hasChanged = manager.SetFilters(rangeType, filters);
            if (station == TargetStation.Linked)
            {
                await LinkedStationManager.SetRangeSectors(rangeType, filters);
            }

            if (hasChanged)
            {
                var eventArgs = new SectorAndRangesChangedEventArgs(
                    rangeType, station, filters, RequestResult.Ok, clientId, true);

                SectorsAndRangesChangedEvent?.Invoke(this, eventArgs);
            }

            return RequestResult.Ok;

            bool AreWrongIntelligenceFilters() =>
                station == TargetStation.Current &&
                rangeType == RangeType.Intelligence &&
                filters.Count == 0 &&
                CurrentMode.IsIntelligenceMode();

            bool AreWrongFrsAutoFilters() =>
                station == TargetStation.Current &&
                rangeType == RangeType.RadioJamming &&
                filters.Count == 0 &&
                CurrentMode == DspServerMode.RadioJammingFrsAuto;
        }

        public async Task<RequestResult> SetSpecialFrequencies(IReadOnlyCollection<FrequencyRange> ranges, FrequencyType rangeType, TargetStation station, int clientId = -1)
        {
            SetForCurrent();
            switch (station)
            {
                case TargetStation.Current:
                    return RequestResult.Ok;
                case TargetStation.Linked:
                    if (LinkedStationManager.Role == StationRole.Slave)
                        throw new ArgumentOutOfRangeException(nameof(LinkedStationManager.Role), LinkedStationManager.Role, null);

                    var result = await LinkedStationManager.SetSpecialRanges(rangeType, ranges)? 
                        RequestResult.Ok : RequestResult.LinkedStationConnectionError;
                    return result;
                default:
                    throw new ArgumentOutOfRangeException(nameof(station), station, null);
            }

            void SetForCurrent()
            {
                var filterManager = 
                    station == TargetStation.Current ? 
                    TaskManager.FilterManager : TaskManager.LinkedStationFilterManager;

                var hasChanged = false;
                switch (rangeType)
                {
                    case FrequencyType.Forbidden:
                        hasChanged = filterManager.SetForbiddenFrequencies(ranges);
                        break;
                    case FrequencyType.Known:
                        hasChanged = filterManager.SetKnownFrequencies(ranges);
                        break;
                    case FrequencyType.Important:
                        hasChanged = filterManager.SetImportantFrequencies(ranges);
                        break;
                    default:
                        throw new ArgumentOutOfRangeException(nameof(rangeType), rangeType, null);
                }

                if (hasChanged)
                {
                    var eventArgs = new SpecialRangesChangedEventArgs(rangeType, station, ranges, RequestResult.Ok, clientId, true);
                    SpecialRangesChangedEvent?.Invoke(this, eventArgs);
                }
            }
        }

        public async Task<RequestResult> SetFrsJammingTargets(IReadOnlyCollection<IRadioJamTarget> targets, TargetStation station, int clientId = -1)
        {
            SetTargets();
            switch (station)
            {
                case TargetStation.Current:
                    return RequestResult.Ok;
                case TargetStation.Linked:
                    if (LinkedStationManager.Role == StationRole.Slave)
                        throw new ArgumentOutOfRangeException(nameof(LinkedStationManager.Role), LinkedStationManager.Role, null);

                    var result = await LinkedStationManager.SetFrsJammingTargets(targets)?
                        RequestResult.Ok : RequestResult.LinkedStationConnectionError;
                    return result;
                default:
                    throw new ArgumentOutOfRangeException(nameof(station), station, null);
            }

            void SetTargets()
            {
                var storage =
                    station == TargetStation.Current ?
                    RadioJamManager.Storage : RadioJamManager.LinkedStationStorage;
                var areTargetsChanged = storage.UpdateFrsTargets(targets.ToList());
                if (areTargetsChanged)
                {
                    var eventArgs = new RadioJamTargetsChangedEventArgs(targets.ToList(), station, RequestResult.Ok,  clientId, true);
                    RadioJamFrsTargetsChangedEvent?.Invoke(this, eventArgs);
                }
            }
        }

        public async Task<RequestResult> SetFhssJammingTargets(IReadOnlyCollection<IRadioJamFhssTarget> targets, TargetStation station, int clientId = -1)
        {
            SetTargets();
            switch (station)
            {
                case TargetStation.Current:
                    return RequestResult.Ok;
                case TargetStation.Linked:
                    if (LinkedStationManager.Role == StationRole.Slave)
                        throw new ArgumentOutOfRangeException(nameof(LinkedStationManager.Role), LinkedStationManager.Role, null);

                    var result = await LinkedStationManager.SetFhssJammingTargets(targets)?
                        RequestResult.Ok : RequestResult.LinkedStationConnectionError;
                    return result;
                default:
                    throw new ArgumentOutOfRangeException(nameof(station), station, null);
            }

            void SetTargets()
            {
                var storage = 
                    station == TargetStation.Current ? 
                    RadioJamManager.Storage : RadioJamManager.LinkedStationStorage;
                var areTargetsChanged = storage.UpdateFhssTargets(targets.ToList());
                if (areTargetsChanged)
                {
                    var eventArgs = new RadioJamFhssTargetsChangedEventArgs(targets.ToList(), station, RequestResult.Ok, clientId, true);
                    RadioJamFhssTargetsChangedEvent?.Invoke(this, eventArgs);
                }
            }
        }

        public RequestResult SetStationLocation(double latitude, double longitude, int altitude, TargetStation station, int clientId = -1)
        {
            var stationConfig = station == TargetStation.Current ?
                _config.StationPosition : _config.LinkedStationPosition;
            var isLocationChanged = Math.Abs(stationConfig.Latitude - latitude) > 1e-7 ||
                                    Math.Abs(stationConfig.Longitude - longitude) > 1e-7 ||
                                    Math.Abs(stationConfig.Altitude - altitude) > 1;
            var result = true;
            if (isLocationChanged)
            {
                stationConfig.Latitude = latitude;
                stationConfig.Longitude = longitude;
                stationConfig.Altitude = altitude;
                result = LinkedStationManager.SendCoordinates(latitude, longitude, altitude, station);
            }
            return result ? RequestResult.Ok : RequestResult.LinkedStationConnectionError;
        }

        public async Task<IReadOnlyList<ISignal>> GetSignals(IReadOnlyList<FrequencyRange> frequencyRanges, int phaseAveragingCount, int directionAveragingCount, bool calculateHighQualityPhases = false)
        {
            var taskSetup = new DirectionFindingTaskSetup
            {
                TaskManager = TaskManager,
                FrequencyRanges = frequencyRanges,
                Priority = 1,
                ReceiversIndexes = new[] { 0, 1, 2, 3, 4 },
                ScanAveragingCount = phaseAveragingCount,
                BearingAveragingCount = directionAveragingCount,
                IsEndless = false,
                AutoUpdateObjectives = false,
                CalculateHighQualityPhases = calculateHighQualityPhases
            };
            var task = new DirectionFindingTask(taskSetup);
            TaskManager.AddTask(task);
            await task.WaitForResult().ConfigureAwait(false);

            return task.TypedResult.Signals;
        }

        public async Task<ISignal> GetSignal(float startFrequencyKhz, float endFrequencyKhz, int phaseAveragingCount, int directionAveragingCount, bool calculateHighQualityPhases = false)
        {
            var frequencyRanges = new[] {new FrequencyRange(startFrequencyKhz, endFrequencyKhz)};
            var signals = await GetSignals(frequencyRanges, phaseAveragingCount, directionAveragingCount, calculateHighQualityPhases);
            return signals?.OrderByDescending(s => s.Amplitude).FirstOrDefault();
        }

        public async Task<RequestResult> GetFrsTargetsResponse(IReadOnlyCollection<IRadioSource> targets)
        {
            var result = await LinkedStationManager.GetFrsTargetsResponse(targets.ToList());

            if (result == false)
                return RequestResult.LinkedStationConnectionError;

            return RequestResult.Ok;
        }

        public async Task<RequestResult> GetFrsTargetsRequest()
        {
            var result = await LinkedStationManager.GetFrsTargetsRequest();

            if (result == false)
                return RequestResult.LinkedStationConnectionError;

            return RequestResult.Ok;
        }
    }
}

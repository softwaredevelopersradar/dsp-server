﻿using System;
using System.IO;
using Settings;
using YamlDotNet.Serialization;

namespace CycleSyncronization
{
    public class Config
    {
        private static Config _instance;

        public const string ConfigPath = "config.yaml";

        public string ServerHost { get; private set; }

        public int ServerPort { get; private set; }

        public int WorkingFrequencyKhz { get; private set; }

        public int WorkingRangeKhz { get; private set; }

        public int ShiftUpdatePauseMs { get; private set; }

        public int ScanCount { get; private set; }

        public int MaxArgument { get; private set; }
        
        public string ServerConfigPath { get; private set; }

        [YamlIgnore]
        public int WorkingBandNumber { get; private set; }

        /// <returns> returns true if load is successful </returns>
        public static bool Load(string filename)
        {
            try
            {
                var deserializer = new DeserializerBuilder()
                        .Build();
                _instance = deserializer.Deserialize<Config>(File.ReadAllText(filename));
            }
            catch (Exception)
            {
                return false;
            }

            var left = Instance.WorkingFrequencyKhz - Instance.WorkingRangeKhz / 2;
            var right = Instance.WorkingFrequencyKhz + Instance.WorkingRangeKhz / 2;

            if (Utilities.GetBandNumber(left) != Utilities.GetBandNumber(right))
            {
                throw new Exception("Working range is in the different band numbers!");
            }

            Instance.WorkingBandNumber = Utilities.GetBandNumber(left);
            return true;
        }

        public static Config Instance
        {
            get
            {
                if (_instance != null)
                    return _instance;
                if (Load(ConfigPath))
                    return _instance;
                throw new ArgumentException("Can't load config from " + ConfigPath);
            }
        }
    }
}

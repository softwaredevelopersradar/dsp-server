﻿using LiveCharts;
using LiveCharts.Wpf;
using Settings;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using DspDataModel.Hardware;
using LiveCharts.Defaults;
using Protocols;

namespace CycleSyncronization
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public SeriesCollection SeriesCollection { get; set; }

        public int CurrentShift { get; set; }

        private readonly DspClient.DspClient _client;

        private bool _isWorking;

        private readonly List<float> _shiftDeviations = new List<float>();

        public int Threshold => 40;

        private const int StartShift = 5;

        public MainWindow()
        {
            InitializeComponent();

            SeriesCollection = new SeriesCollection
            {
                new LineSeries
                {
                    Title = "Deviations",
                    Values = new ChartValues<ObservablePoint>()
                }
            };
            Chart.Series = SeriesCollection;
            DataContext = this;

            CurrentShift = StartShift;
            _isWorking = false;
            _client = new DspClient.DspClient();
            OnStart();
        }

        private async Task OnStart()
        {
            await _client.Connect(Config.Instance.ServerHost, Config.Instance.ServerPort);
            Title = "Cycle synchronizer: connected";
            WorkingCycle();
        }

        private async Task WorkingCycle()
        {
            if (_isWorking)
            {
                return;
            }
            await _client.SetReceiversChannel(ReceiverChannel.NoiseGenerator);
            _isWorking = true;

            var halfRange = Config.Instance.WorkingRangeKhz / 2;
            var startIndex = Utilities.GetSampleNumber(Config.Instance.WorkingFrequencyKhz - halfRange);
            var endIndex = Utilities.GetSampleNumber(Config.Instance.WorkingFrequencyKhz + halfRange);
            
            for (var i = 0; i < CurrentShift; ++i)
            {
                SeriesCollection[0].Values.Add(new ObservablePoint(i, 0));
            }
            
            for (CurrentShift = StartShift; CurrentShift <= Config.Instance.MaxArgument; ++CurrentShift)
            {
                await _client.SetSynchronizationShift(CurrentShift);
                await Task.Delay(Config.Instance.ShiftUpdatePauseMs);
                var deviation = await ProcessShift();

                _shiftDeviations.Add(deviation);
                SeriesCollection[0].Values.Add(new ObservablePoint(CurrentShift, deviation));
            }
            
            var peaks = CreatePeaksList();
            if (peaks.Count < 1)
            {
                Title = "Can't find phase deviation peaks";
                return;
            }
            var maxDistanceIndex = GetMaxPeakDistanceIndex();
            var resultShift = GetResultShift();

            ResultPanel.Visibility = Visibility.Visible;
            ResultShift.Text = resultShift.ToString();

            // inner methods

            int GetResultShift()
            {
                var shift = (peaks[maxDistanceIndex] + peaks[maxDistanceIndex - 1]) / 2;
                var center = _shiftDeviations[shift];
                var left = _shiftDeviations[shift - 1];
                var right = _shiftDeviations[shift + 1];

                // peaking left or right index when it has lower deviations than the current one
                if (center < left && center < right)
                {
                    return shift + StartShift;
                }
                if (left < right)
                {
                    --shift;
                }
                else
                {
                    ++shift;
                }

                return shift + StartShift;
            }

            List<int> CreatePeaksList()
            {
                var result = new List<int>();
                for (var i = 0; i < _shiftDeviations.Count; ++i)
                {
                    if (_shiftDeviations[i] < Threshold)
                    {
                        continue;
                    }
                    var j = i + 1;
                    var maxIndex = i;
                    for (; j < _shiftDeviations.Count; ++j)
                    {
                        if (_shiftDeviations[j] > _shiftDeviations[maxIndex])
                        {
                            maxIndex = j;
                        }
                        if (_shiftDeviations[j] < Threshold)
                        {
                            break;
                        }
                    }
                    i = j - 1;
                    result.Add(maxIndex);
                }
                return result;
            }

            int GetMaxPeakDistanceIndex()
            {
                var maxIndex = 1;
                var maxDistance = peaks[1] - peaks[0];
                for (var i = 2; i < peaks.Count; ++i)
                {
                    var peakDistance = peaks[i] - peaks[i - 1];
                    if (peakDistance > maxDistance)
                    {
                        maxIndex = i;
                        maxDistance = peakDistance;
                    }
                }

                return maxIndex;
            }

            async Task<float> ProcessShift()
            {
                var phases = new float[10][];
                for (var i = 0; i < 10; ++i)
                {
                    phases[i] = new float[Config.Instance.ScanCount];
                }
                for (var i = 0; i < Config.Instance.ScanCount; ++i)
                {
                    var currentPhases = await ProcessScan();
                    for (var j = 0; j < 10; ++j)
                    {
                        phases[j][i] = currentPhases[j];
                    }
                }
                return phases.Select(Phases.PhaseMath.StandardDeviation).Max();
            }

            async Task<float[]> ProcessScan()
            {
                var scans = await _client.GetTechAppSpectrum(Config.Instance.WorkingBandNumber, 1);
                var maxAmplitude = float.MinValue;
                var maxIndex = 0;
                for (var i = startIndex; i < endIndex; ++i)
                {
                    var amplitude = Amplitude(scans, i);
                    if (amplitude > maxAmplitude)
                    {
                        maxAmplitude = amplitude;
                        maxIndex = i;
                    }
                }
                return GetPhaseDifferences(scans, maxIndex);
            }

            float Amplitude(Protocols.Scan[] scans, int index)
            {
                var amplitude = 0f;
                for (var i = 0; i < 5; ++i)
                {
                    amplitude += scans[i].Amplitudes[index];
                }
                return -amplitude / 5;
            }

            float[] GetPhaseDifferences(Protocols.Scan[] scans, int index)
            {
                var phases = new float[10];
                int k = 0;

                for (var i = 0; i < 5; ++i)
                {
                    for (var j = i + 1; j < 5; ++j)
                    {
                        phases[k++] = Phases.PhaseMath
                            .NormalizePhase(scans[i].Phases[index] * 0.1f - scans[j].Phases[index] * 0.1f);
                    }
                }
                return phases;
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (int.TryParse(ResultShift.Text, out var resultShift))
            {
                if (!DspDataModel.Config.TryLoad(Config.Instance.ServerConfigPath, out _))
                {
                    Title = "Can't find config file!";
                    return;
                }
                DspDataModel.Config.Instance.HardwareSettings.TactingCycleLengthMs = resultShift;
                DspDataModel.Config.Save(Config.Instance.ServerConfigPath);
            }
        }
    }
}

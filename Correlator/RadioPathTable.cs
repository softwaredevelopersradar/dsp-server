﻿using System;
using System.Collections.Generic;
using System.Linq;
using DspDataModel;
using Settings;
using RadioTables;

namespace Correlator
{
    public class RadioPathTable : IVisualTable
    {
        private readonly RadioPathTableDto _table;

        public int MinFrequencyKhz => _table.MinFrequencyKhz;
        public int MaxFrequencyKhz => _table.MaxFrequencyKhz;

        public int ValuesCount { get; }

        public int CurrentAngle { get; set; }

        private readonly float _step;

        public RadioPathTable(RadioPathTableDto table)
        {
            _table = table;
            ValuesCount = _table.BandRadioPathCalibrations.Sum(b => b.RadioPath.Count);
        }

        /// <summary>
        /// Creates RadioPathTable with zero coefficents
        /// </summary>
        public RadioPathTable() : this(Config.Instance.BandSettings)
        { }

        /// <summary>
        /// Creates RadioPathTable with zero coefficents
        /// </summary>
        public RadioPathTable(BandConfig config)
        {
            var bandPath = new BandRadioPath(
                new List<PhaseArray> {new PhaseArray(new float[Constants.PhasesDifferencesCount])},
                new List<AmplitudeArray> {new AmplitudeArray(new float[Constants.DfReceiversCount])},
                PhaseDeviation: 0
            );

            var radioPath = Enumerable.Repeat(bandPath, config.BandCount).ToList();
            _table = new RadioPathTableDto(Constants.FirstBandMinKhz, config.LastBandMaxKhz, radioPath);
            ValuesCount = _table.BandRadioPathCalibrations.Sum(b => b.RadioPath.Count);
        }

        public RadioPathTable(string radioPathTableFilename)
        {
            _table = new RadioPathTableDto();
            _table.LoadFromFile(radioPathTableFilename);
            _step = 1f * Constants.BandwidthKhz / _table.BandRadioPathCalibrations[0].RadioPath.Count;
            ValuesCount = _table.BandRadioPathCalibrations.Sum(b => b.RadioPath.Count);
        }

        public PhaseArray GetPhases(int bandNumber, int sampleNumber)
        {
            var bandRadioPath = _table.BandRadioPathCalibrations[bandNumber];
            if (bandRadioPath.RadioPath.Count == Constants.BandSampleCount)
            {
                return bandRadioPath.RadioPath[sampleNumber];
            }
            else
            {
                var index = bandRadioPath.RadioPath.Count * sampleNumber / Constants.BandSampleCount;
                return bandRadioPath.RadioPath[index];
            }
        }

        public AmplitudeArray GetAmplitudes(int bandNumber, int sampleNumber)
        {
            var bandRadioPath = _table.BandRadioPathCalibrations[bandNumber];
            if (bandRadioPath.RadioPath.Count == Constants.BandSampleCount)
            {
                return bandRadioPath.Amplitudes[sampleNumber];
            }
            else
            {
                var index = bandRadioPath.Amplitudes.Count * sampleNumber / Constants.BandSampleCount;
                return bandRadioPath.Amplitudes[index];
            }
        }

        public float GetPhaseDeviations(int bandNumber)
        {
            return _table.BandRadioPathCalibrations[bandNumber].PhaseDeviation;
        }

        public float[] FrequencyToPhases(float frequencyKhz)
        {
            return GetPhases(frequencyKhz).Phases;
        }

        public float[] FrequencyToAmplitudes(float frequencyKhz)
        {
            return GetAmplitudes(frequencyKhz).Amplitudes;
        }

        public float GetNearestTableFrequency(float frequency)
        {
            var index = (int) ((frequency - MinFrequencyKhz) / _step);
            return MinFrequencyKhz + _step * index;
        }
        
        public PhaseArray GetPhases(float frequencyKhz)
        {
            return GetPhases(Utilities.GetBandNumber(frequencyKhz), Utilities.GetSampleNumber(frequencyKhz));
        }

        public AmplitudeArray GetAmplitudes(float frequencyKhz)
        {
            return GetAmplitudes(Utilities.GetBandNumber(frequencyKhz), Utilities.GetSampleNumber(frequencyKhz));
        }

        public void Save(string filename)
        {
            _table.SaveToFile(filename);
        }
    }
}

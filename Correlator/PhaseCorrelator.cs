﻿using System;
using System.Collections.Generic;
using System.Linq;
using DspDataModel;
using Settings;
using DspDataModel.Correlator;
using DspDataModel.Data;
using Phases;

namespace Correlator
{
    public class PhaseCorrelator : IPhaseCorrelator
    {
        private readonly TheoreticalTable _theoreticalTable;
        private RadioPathTable _radioPathTable;
        
        /// <summary>
        /// Relative threshold to max amplitude in current signal, from 0 to 1
        /// </summary>
        public float AmplitudeRelativeThreshold { get; set; }

        /// <summary>
        /// Correlation threshold of signal, from 0 to 1
        /// </summary>
        public float CorrelationThreshold { get; set; }

        /// <summary>
        /// Final direction is selecting by maximazing rating: Correlation + StandardDeviationWeigth * StandardDeviation
        /// value should be in range [0..1]
        /// </summary>
        public float StandardDeviationWeigth
        {
            get => _standardDeviationWeigth;
            set
            {
                Contract.Assert(0 <= value && value <= 1);
                _standardDeviationWeigth = value;
                CorrelationWeigth = 1 - StandardDeviationWeigth;
            }
        }

        public float CorrelationWeigth { get; private set; }

        public CorrelationType CorrelationProviderType
        {
            get => _correlationProviderType;
            set
            {
                var isChanged = _correlationProviderType != value;
                _correlationProviderType = value;
                if (isChanged)
                {
                    switch (value)
                    {
                        case CorrelationType.Correlation:
                            _correlationProvider = new CorrelationWithSdProvider(this);
                            return;
                        case CorrelationType.LinearDeviation:
                            _correlationProvider = new LinearDeviationProvider();
                            return;
                        case CorrelationType.StandardDeviation:
                            _correlationProvider = new StandardDeviationProvider();
                            return;
                        case CorrelationType.QuadroDeviation:
                            _correlationProvider = new QuadroDeviationProvider();
                            break;
                        case CorrelationType.QuadroFactorCorrelation:
                            _correlationProvider = new FactorQuadroCorrelationProvider();
                            break;
                        case CorrelationType.QuadroFactorDeviation:
                            _correlationProvider = new QuadroFactorDeviationProvider();
                            break;
                        default:
                            throw new ArgumentOutOfRangeException(nameof(value), value, null);
                    }
                }
            }
        }

        private float _standardDeviationWeigth;

        private readonly int _sector = 30;

        private readonly int _broadbandSignalSampleWidth;

        private ICorrelationProvider _correlationProvider;
        private CorrelationType _correlationProviderType;

        private readonly Config _config;

        private PhaseCorrelator(Config config)
        {
            _config = config ?? Config.Instance;

            AmplitudeRelativeThreshold = _config.CorrelatorSettings.AmplitudeRelativeThreshold;
            CorrelationThreshold = _config.CorrelatorSettings.CorrelationThreshold;
            StandardDeviationWeigth = _config.CorrelatorSettings.StandardDeviationWeigth;
            _broadbandSignalSampleWidth = (int)
                (_config.DirectionFindingSettings.BroadbandSignalWidthKhz / Constants.SamplesGapKhz);

            _correlationProvider = new FactorQuadroCorrelationProvider();
            _correlationProviderType = CorrelationType.QuadroFactorCorrelation;
        }

        public PhaseCorrelator(TheoreticalTable theoreticalTable, RadioPathTable radioPathTable, Config config = null) : this(config)
        {
            _theoreticalTable = theoreticalTable;
            _radioPathTable = radioPathTable;
        }

        public PhaseCorrelator(RadioPathTable radioPathTable, Config config = null) : this(config)
        {
            _theoreticalTable = new TheoreticalTable();
            _theoreticalTable.AddCorrectionTable(_config.CalibrationSettings.CalibrationCorrectionFilename);

            _radioPathTable = radioPathTable;
        }

        public PhaseCorrelator(string radioPathTableFilename, Config config = null) : this(config)
        {
            _theoreticalTable = new TheoreticalTable();
            _theoreticalTable.AddCorrectionTable(_config.CalibrationSettings.CalibrationCorrectionFilename);

            LoadRadioPathTable(radioPathTableFilename);
        }

        private void LoadRadioPathTable(string filename)
        {
            try
            {
                _radioPathTable = new RadioPathTable(filename);
            }
            catch (Exception)
            {
                MessageLogger.Warning("Can't load radio path table! Loading empty table.");
                _radioPathTable = new RadioPathTable();
            }
        }

        public void ReloadRadioPathTable(string filename)
        {
            LoadRadioPathTable(filename);
        }

        private static int CalculateMaxAmplitudeIndex(IAmplitudeBand scan, int startIndex, int endIndex)
        {
            var maxAmplitude = scan.Amplitudes[startIndex];
            var maxAmplitudeIndex = startIndex;
            for (var i = startIndex; i <= endIndex; i++)
            {
                if (scan.Amplitudes[i] > maxAmplitude)
                {
                    maxAmplitudeIndex = i;
                    maxAmplitude = scan.Amplitudes[i];
                }
            }
            return maxAmplitudeIndex;
        }

        /// <summary>
        /// Calculates direction for range of points
        /// </summary>
        public DirectionInfo CalculateDirection(float threshold, IDataScan scan, int startIndex, int endIndex)
        {
            Contract.Assert(startIndex <= endIndex && startIndex >= 0);
            Contract.Assert(scan != null);

            var maxAmplitudeIndex = CalculateMaxAmplitudeIndex(scan, startIndex, endIndex);
            var thresholdAmplitude = (scan.Amplitudes[maxAmplitudeIndex] - threshold) * AmplitudeRelativeThreshold;

            return endIndex - startIndex < _broadbandSignalSampleWidth 
                ? CalculateDirectionForStraightSignal()
                : CalculateDirectionForBroadbandSignal();

            DirectionInfo CalculateDirectionForStraightSignal()
            {
                var maxCorrelation = float.MinValue;

                var phases = new List<PhaseFactor>();
                var directions = new List<DirectionInfo>();

                for (var i = startIndex; i <= endIndex; i++)
                {
                    // passing all frequencies with low amplitude
                    if (scan.Amplitudes[i] - threshold < thresholdAmplitude)
                        continue;
                    var diretionInfo = CalculateDirection(threshold, scan, i);
                    // passing all frequencies with low correlation
                    if (diretionInfo.Correlation < CorrelationThreshold)
                        continue;

                    var phaseFactor = new PhaseFactor(diretionInfo.Correlation, diretionInfo.Angle);
                    phases.Add(phaseFactor);
                    directions.Add(diretionInfo);
                    if (diretionInfo.Correlation > maxCorrelation)
                    {
                        maxCorrelation = diretionInfo.Correlation;
                    }
                }
                // if all phases have low amplitudes and correlations lets just calculate phase for point with max amplitude
                if (phases.Count == 0)
                {
                    return CalculateDirection(threshold, scan, maxAmplitudeIndex);
                }

                var phase = PhaseMath.CalculatePhase(phases, _sector, out var discardedPhasesPart);
                return new DirectionInfo(phase, 0, discardedPhasesPart, maxCorrelation, directions.CalculatePhases(), directions.CalculatePhaseDeviation());
            }

            DirectionInfo CalculateDirectionForBroadbandSignal()
            {
                var maxCorrelation = float.MinValue;
                var samples = new int[endIndex - startIndex + 1];

                var phases = new List<PhaseFactor>();
                var directions = new List<DirectionInfo>();

                for (var i = 0; i < samples.Length; ++i)
                {
                    samples[i] = startIndex + i;
                }
                // sorting samples by descending by amplitude
                Array.Sort(samples, (i, j) => scan.Amplitudes[i].CompareTo(scan.Amplitudes[j]));
                for (var i = 0; i < _broadbandSignalSampleWidth; ++i)
                {
                    var index = samples[i];
                    if (scan.Amplitudes[index] - threshold < thresholdAmplitude)
                        continue;
                    var diretionInfo = CalculateDirection(threshold, scan, index);
                    // passing all frequencies with low correlation
                    if (diretionInfo.Correlation < CorrelationThreshold)
                        continue;

                    var phaseFactor = new PhaseFactor(diretionInfo.Correlation, diretionInfo.Angle);
                    phases.Add(phaseFactor);
                    directions.Add(diretionInfo);
                    if (diretionInfo.Correlation > maxCorrelation)
                    {
                        maxCorrelation = diretionInfo.Correlation;
                    }
                }
                // if all phases have low amplitudes and correlations lets just calculate phase for point with max amplitude
                if (phases.Count == 0)
                {
                    return CalculateDirection(threshold, scan, maxAmplitudeIndex);
                }

                var phase = PhaseMath.CalculatePhase(phases, _sector, out var discardedPhasesPart);
                return new DirectionInfo(phase, 0, discardedPhasesPart, maxCorrelation, directions.CalculatePhases(), directions.CalculatePhaseDeviation());
            }
        }

        /// <summary>
        /// Calculates direction for one point
        /// </summary>
        public DirectionInfo CalculateDirection(float frequencyKhz, float[] phases)
        {
            var correctedPhases = new float[phases.Length];
            var radioPathCorrection = _radioPathTable.GetPhases(frequencyKhz);

            for (var i = 0; i < correctedPhases.Length; ++i)
            {
                correctedPhases[i] = PhaseMath.NormalizePhase(phases[i] - radioPathCorrection.Phases[i]);
            }

            var correlationPhases = _theoreticalTable.GetPhases(frequencyKhz);

            var resultAngle = 0;
            var bestCorrelation = float.MinValue;

            // calculating correlation and standard deviation for each angle from 0 to 360
            for (var i = 0; i < 360; ++i)
            {
                var correlation = _correlationProvider.Correlation(correctedPhases, correlationPhases[i].Phases, i);
                if (correlation > bestCorrelation)
                {
                    bestCorrelation = correlation;
                    resultAngle = i;
                }
            }
            var correction = GetCorrection(frequencyKhz);
            var phase = PhaseMath.NormalizePhase(resultAngle + correction);
            return new DirectionInfo(phase, 0, 0, bestCorrelation, correctedPhases, 0);
        }

        private float GetCorrection(float frequencyKhz)
        {
            var theoreticalTableConfig = _config.TheoreticalTableSettings;
            var antennaIndex = theoreticalTableConfig.GetAntennaIndex(frequencyKhz);
            var antennaCorrection = theoreticalTableConfig.AntennaTableSettings[antennaIndex].DirectionCorrection;
            var factor = _config.DirectionFindingSettings.DirectionCorrectionFactor;
            return antennaCorrection + factor * _config.DirectionFindingSettings.DirectionCorrection;
        }

        /// <summary>
        /// Phases should already be corrected with radiopath (all signals or radiosources containts such phases)
        /// </summary>
        public float[] GetCorrelationCurve(float frequencyKhz, IReadOnlyList<float> phases)
        {
            var phasesArray = phases.ToArray();
            var correlationPhases = _theoreticalTable.GetPhases(frequencyKhz);
            var correlations = new float[360];
            var correction = (int) PhaseMath.NormalizePhase(GetCorrection(frequencyKhz));
            
            for (var i = 0; i < 360; ++i)
            {
                correlations[(i + correction) % 360] = _correlationProvider.Correlation(phasesArray, correlationPhases[i].Phases, i);
            }
            return correlations;
        }

        public float[] GetCorrelationCurve(ISignal signal)
        {
            return GetCorrelationCurve(signal.FrequencyKhz, signal.Phases);
        }

        /// <summary>
        /// Calculates direction for one point
        /// </summary>
        public DirectionInfo CalculateDirection(float threshold, IDataScan scan, int index)
        {
            return CalculateDirection(
                Utilities.GetFrequencyKhz(scan.BandNumber, index), 
                scan.GetPhaseDifferences(threshold, index));
        }

        public void SaveTheoreticalTable(string filename)
        {
            try
            {
                _theoreticalTable.Save(filename);
            }
            catch { }
        }

        #region InnerClasses

        public enum CorrelationType
        {
            Correlation, LinearDeviation, StandardDeviation, QuadroDeviation, QuadroFactorDeviation, QuadroFactorCorrelation
        }

        private interface ICorrelationProvider
        {
            float Correlation(float[] phases, float[] theoreticalPhases, int direction);
        }

        private class CorrelationWithSdProvider : ICorrelationProvider
        {
            private readonly PhaseCorrelator _correlator;

            public CorrelationWithSdProvider(PhaseCorrelator correlator) => _correlator = correlator;

            public float Correlation(float[] phases, float[] theoreticalPhases, int direction)
            {
                var inversedSd = PhaseMath.InversedSd(phases, theoreticalPhases);
                var correlation = PhaseMath.Correlation(phases, theoreticalPhases);
                if (float.IsNaN(correlation))
                {
                    correlation = 0;
                }
                // standard deviation integration
                correlation = _correlator.CorrelationWeigth * correlation + inversedSd * _correlator.StandardDeviationWeigth;
                return correlation;
            }
        }

        private class LinearDeviationProvider : ICorrelationProvider
        {
            public float Correlation(float[] phases, float[] theoreticalPhases, int direction)
            {
                var sum = 0f;
                for (var i = 0; i < phases.Length; ++i)
                {
                    sum += PhaseMath.Angle(phases[i], theoreticalPhases[i]);
                }
                return 1 - sum / 1800;
            }
        }

        private class StandardDeviationProvider : ICorrelationProvider
        {
            public float Correlation(float[] phases, float[] theoreticalPhases, int direction)
            {
                var sum = 0f;
                for (var i = 0; i < phases.Length; ++i)
                {
                    var anlge = PhaseMath.Angle(phases[i], theoreticalPhases[i]);
                    sum += anlge * anlge;
                }
                return (float) (1 - Math.Sqrt(sum * 0.1f) / 180);
            }
        }

        private class QuadroDeviationProvider : ICorrelationProvider
        {
            public float Correlation(float[] phases, float[] theoreticalPhases, int direction)
            {
                var sum = 0f;
                for (var i = 0; i < phases.Length; ++i)
                {
                    var anlge = PhaseMath.Angle(phases[i], theoreticalPhases[i]);
                    sum += anlge * anlge * anlge * anlge;
                }
                return (float)(1 - Math.Pow(sum * 0.1f, 0.25) / 180);
            }
        }

        private class QuadroFactorDeviationProvider : ICorrelationProvider
        {
            public float Correlation(float[] phases, float[] theoreticalPhases, int direction)
            {
                var sum = 0f;
                var factors = TheoreticalTableGenerator.Factors[direction];

                for (var i = 0; i < phases.Length; ++i)
                {
                    var anlge = PhaseMath.Angle(phases[i], theoreticalPhases[i]);
                    sum += factors[i] * anlge * anlge * anlge * anlge;
                }
                return (float)(1 - Math.Pow(sum, 0.25) / 180);
            }
        }

        private class FactorQuadroCorrelationProvider : ICorrelationProvider
        {
            public float Correlation(float[] phases, float[] theoreticalPhases, int direction)
            {
                var sum = 0f;
                var factors = TheoreticalTableGenerator.Factors[direction];
                var phasesCount = phases.Length;
                float sum1 = 0, sum2 = 0, sum11 = 0, sum22 = 0, sum12 = 0;

                for (var i = 0; i < phasesCount; ++i)
                {
                    var p1 = phases[i];
                    var p2 = theoreticalPhases[i];
                    var anlge = PhaseMath.Angle(p1, p2);
                    if (Math.Abs(p1 - p2) >= 180)
                    {
                        if (p1 >= 180)
                            p1 = 360 - p1;
                        else
                            p2 = 360 - p2;
                    }
                    sum += factors[i] * anlge * anlge * anlge * anlge;

                    sum1 += p1;
                    sum2 += p2;
                    sum11 += p1 * p1;
                    sum12 += p1 * p2;
                    sum22 += p2 * p2;
                }
                var correlation = (phasesCount * sum12 - sum1 * sum2) * (phasesCount * sum12 - sum1 * sum2) /
                                  ((phasesCount * sum11 - sum1 * sum1) * (phasesCount * sum22 - sum2 * sum2));
                var quadroDeviation = (float)(1 - Math.Pow(sum, 0.25) / 180);

                return correlation * quadroDeviation;
            }
        }

        #endregion
    }
}

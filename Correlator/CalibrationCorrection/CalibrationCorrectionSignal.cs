using System.Linq;

namespace Correlator.CalibrationCorrection
{
    public class CalibrationCorrectionSignal
    {
        public int FrequencyKhz { get; set; }
        public int Direction { get; set; }
        public float[] Phases { get; set; }

        public CalibrationCorrectionSignal(int frequencyKhz, int direction, float[] phases)
        {
            FrequencyKhz = frequencyKhz;
            Direction = direction;
            Phases = phases;
        }

        public CalibrationCorrectionSignal Copy()
        {
            return new CalibrationCorrectionSignal(FrequencyKhz, Direction, Phases.ToArray());
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using DataStorages;
using DspDataModel;
using DspDataModel.RadioJam;
using DspDataModel.Server;
using DspDataModel.Tasks;
using HardwareLibrary.RadioJam;
using Settings;
using TasksLibrary.JamStrategies;

namespace TasksLibrary
{
    public delegate Task UpdateFrsJamStateDelegate(IReadOnlyList<IRadioJamTarget> targets);
    public delegate Task UpdateFhssJamStateDelegate(IReadOnlyList<(int id, bool isJammed)> targets, IReadOnlyList<float> jammedFrequencies);

    public class RadioJamManager : IRadioJamManager
    {
        public IRadioJamTargetStorage Storage { get; }
        public IRadioJamTargetStorage LinkedStationStorage { get; }
        public IRadioJamShaper Shaper { get; }

        public RadioJamMode JamMode { get; private set; }

        public bool IsJammingActive => !_jammingTask?.IsCompleted ?? false;

        public int EmitDuration { get; set; }
        public int Threshold { get; set; }
        public int DirectionSearchSector { get; set; }
        public int FrequencySearchBandwidthKhz { get; set; }
        public float MinSignalBandwidthKhz { get; set; }
        public int FhssJamDuration { get; set; }
        public FftResolution FhssFftResoultion { get; set; }
        public int ChannelsInLiter { get; set; }
        public int LongWorkingSignalDurationMs { get; set; }

        private readonly RadioJamConfig _config;

        private readonly ITaskManager _taskManager;
        private readonly IServerController _serverController;
        private IRadioJamStrategy _radioJamStrategy; 

        private readonly object _lockObject;

        private CancellationTokenSource _cancellationTokenSource;

        private Task _jammingTask;

        public int CheckEmitDelayMs => 10;
        public int MinPowerLevel => 100;

        private readonly UpdateFrsJamStateDelegate _updateFrsJamStateFunction;

        private readonly UpdateFhssJamStateDelegate _updateFhssJamStateFunction;

        private readonly object _createTargetLockObject = new object();

        public RadioJamManager(
            IServerController serverController,
            IRadioJamTargetStorage storage, 
            UpdateFrsJamStateDelegate updateFrsJamStateFunction,
            UpdateFhssJamStateDelegate updateFhssJamStateFunction)
        {
            Storage = storage;
            LinkedStationStorage = new RadioJamTargetStorage();
            Shaper = Config.Instance.HardwareSettings.JammingConfig.SimulateJamShaper
                ? new RadioJamShaperSimulator() as IRadioJamShaper
                : new RadioJamShaper();

            _serverController = serverController;
            _taskManager = serverController.TaskManager;
            _config = Config.Instance.HardwareSettings.JammingConfig;
            _updateFrsJamStateFunction = updateFrsJamStateFunction;
            _updateFhssJamStateFunction = updateFhssJamStateFunction;
            _lockObject = new object();
        }

        public bool Connect()
        {
            MessageLogger.Trace("Radio jam manager: connect");

            return Shaper.Connect(
                _config.ShaperHost, _config.ShaperPort,
                _config.ClientHost, _config.ClientPort);
        }

        public void Disconnect()
        {
            MessageLogger.Trace("Radio jam manager: disconnect");

            Shaper.Disconnect();
        }

        public IRadioJamTarget CreateRadioJamTarget(ISignal signal)
        {
            const int thresholdAdditionalLevel = -10;
            lock (_createTargetLockObject)
            {
                var id = Storage.FrsTargets.Count == 0
                    ? 0
                    : Storage.FrsTargets.Max(t => t.Id) + 1;

                var threshold = signal.Amplitude + thresholdAdditionalLevel;
                var liter = Utilities.GetLiter(signal.FrequencyKhz);
                var priority = 1;

                // default jam settings
                const int modulationCode = 1;
                const int deviationCode = 1;
                const int manipulationCode = 0;
                const int durationCode = 1;

                return new RadioJamTarget(signal.CentralFrequencyKhz, priority, threshold, signal.Direction, false,
                    modulationCode, deviationCode, manipulationCode, durationCode, id, liter, this);
            }
        }

        public void Start(RadioJamMode mode)
        {
            MessageLogger.Trace("Radio jam manager: start");

            lock (_lockObject)
            {
                if (IsJammingActive)
                {
                    return;
                }
                if (!Connect())
                {
                    throw new Exception("Can't connect to radio jam shaper");
                }
                _cancellationTokenSource = new CancellationTokenSource();
                JamMode = mode;
                ClearStateFlagsInRadioJamTargets();
                switch (JamMode)
                {
                    case RadioJamMode.Frs:
                        _radioJamStrategy = new FrsJamStrategy(this, _taskManager, _updateFrsJamStateFunction);
                        break;
                    case RadioJamMode.Afrs:
                        _radioJamStrategy = new AfrsJamStrategy(this, _taskManager, _updateFrsJamStateFunction);
                        break;
                    case RadioJamMode.FrsAuto:
                        _radioJamStrategy = new FrsAutoJamStrategy(_serverController, _updateFrsJamStateFunction);
                        break;
                    case RadioJamMode.Fhss:
                        var updateTimeSpan = TimeSpan.FromMilliseconds(Config.Instance.HardwareSettings.JammingConfig.FhssJamUpdateEventPeriodMs);
                        var maxPeakCount = Config.Instance.HardwareSettings.JammingConfig.FhssJamMaxPeakCount;
                        _radioJamStrategy = new FhssJamStrategy(this, _taskManager, updateTimeSpan, maxPeakCount, _updateFhssJamStateFunction);
                        break;
                    case RadioJamMode.FhssDurationMeasurement:
                        var controlTimeMs = Config.Instance.FhssSearchSettings.FhssDurationMeasureTimeMs;
                        _radioJamStrategy = new FhssDurationMeasureStrategy(controlTimeMs, this, _taskManager, _serverController);
                        break;
                    default:
                        throw new ArgumentException($"Unknown RadioJamMode value: {JamMode}");
                }
                _jammingTask = Task.Run(() => _radioJamStrategy.JammingTask(_cancellationTokenSource.Token));
            }
        }

        private void ClearStateFlagsInRadioJamTargets()
        {
            foreach (var target in Storage.FrsTargets)
            {
                target.ClearStateFlags();
            }
        }

        public async Task Stop()
        {
            MessageLogger.Trace("Radio jam manager: stop");

            lock (_lockObject)
            {
                if (_cancellationTokenSource != null && !_cancellationTokenSource.IsCancellationRequested)
                {
                    _cancellationTokenSource.Cancel();
                }
            }
            if (!_jammingTask.IsCompleted)
            {
                try
                {
                    await _jammingTask;
                }
                catch
                {
                    // supress cancellation exception in Task.Delay
                }
            }
            Disconnect();
        }
    }
}

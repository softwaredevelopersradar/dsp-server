﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using DataProcessor.Fhss;
using DataStorages;
using DspDataModel;
using DspDataModel.DataProcessor;
using DspDataModel.Hardware;
using DspDataModel.Storages;
using DspDataModel.Tasks;
using HardwareLibrary;
using Nito.AsyncEx;
using TasksLibrary.Modes;

namespace TasksLibrary
{
    public class TaskManager : ITaskManager
    {
        public IModeController CurrentMode { get; private set; }

        public IHardwareController HardwareController { get; }
        public IDataProcessor DataProcessor { get; }
        public IFilterManager FilterManager { get; }
        public IFilterManager LinkedStationFilterManager { get; }
        public IRadioSourceStorage RadioSourceStorage { get; }
        public ISignalStorage SignalStorage { get; }
        public IFhssNetworkStorage FhssNetworkStorage { get; }    
        public ISpectrumStorage SpectrumStorage { get; }
        public ISpectrumHistoryStorage SpectrumHistoryStorage { get; }

        public IReadOnlyList<IReceiverTask> Tasks => _tasks;

        /// <summary>
        /// returns scan speed according to current mode
        /// </summary>
        public float ScanSpeed => CurrentMode?.ScanSpeed ?? 0;

        private readonly ManualResetEvent _workCircuitStoppedEvent = new ManualResetEvent(false);
        private readonly object _lockObject = new object();

        /// <summary>
        /// all tasks are sorted by priority, from max to min
        /// </summary>
        private readonly List<IReceiverTask> _tasks;
        private readonly Config _config;
        private readonly FhssProcessor _fhssProcessor;
        private readonly AsyncAutoResetEvent _taskAddEvent = new AsyncAutoResetEvent();

        public event EventHandler FhssDurationMeasuringStartEvent;

        public TaskManager() : this(Config.Instance)
        { }

        public TaskManager(Config config)
        {
            _config = config;

            MessageLogger.Trace("Task manager constructor");

            _tasks = new List<IReceiverTask>();
            
            FilterManager = new FilterManager();
            LinkedStationFilterManager = new FilterManager();
            DataProcessor = new DataProcessor.DataProcessor();
            HardwareController = new HardwareController(DataProcessor, _config);

            SpectrumStorage = new SpectrumStorage();
            RadioSourceStorage = new RadioSourceStorage(FilterManager);
            SignalStorage = new SignalStorage(FilterManager);
            FhssNetworkStorage = new FhssNetworkStorage(FilterManager);
            SpectrumHistoryStorage = new SpectrumHistoryStorage(SpectrumStorage, FilterManager,
                _config.StoragesSettings.SpectrumHistoryStorageUpdateRate, _config.StoragesSettings.SpectrumHistoryStorageHistoryLength);

            _fhssProcessor = new FhssProcessor(RadioSourceStorage);
            FilterManager.BandsUpdatedEvent += OnObjectivesUpdated;
            Task.Run(FhssSearchCircuit);
            Task.Run(WorkCircuit);
        }

        private void OnObjectivesUpdated(object sender, IReadOnlyList<Band> e)
        {
            var frequencyRanges = FilterManager.Filters.Select(f => f.ToFrequencyRange()).ToArray();
            lock (_tasks)
            {
                for (var i = 0; i < _tasks.Count; ++i)
                {
                    if (_tasks[i].AutoUpdateObjectives)
                    {
                        _tasks[i].UpdateObjectives(frequencyRanges);
                    }
                }
            }
        }

        public IFilterManager GetFilterManager(TargetStation station)
        {
            switch (station)
            {
                case TargetStation.Current:
                    return FilterManager;
                case TargetStation.Linked:
                    return LinkedStationFilterManager;
                default:
                    throw new ArgumentOutOfRangeException(nameof(station), station, null);
            }
        }

        public bool Initialize()
        {
            return HardwareController.Initialize();
        }

        public void SetMode<TMode>(TMode modeController) where TMode : IModeController
        {
            lock (_lockObject)
            {
                ClearTasks();
                CurrentMode = modeController;
                var newTasks = modeController.CreateTasks();
                foreach (var task in newTasks)
                {
                    AddTask(task);
                }
                modeController.OnActivated();
            }
        }

        public void AddTask(IReceiverTask task)
        {
            if (task.IsReady)
            {
                return;
            }
            lock (_tasks)
            {
                _tasks.Add(task);
                _tasks.Sort((t1, t2) => t2.Priority.CompareTo(t1.Priority));
                _taskAddEvent.Set();
            }
        }

        public void AddTasks(IEnumerable<IReceiverTask> tasks)
        {
            lock (_tasks)
            {
                _tasks.AddRange(tasks.Where(t => !t.IsReady));
                _tasks.Sort((t1, t2) => t2.Priority.CompareTo(t1.Priority));
                _taskAddEvent.Set();
            }
        }

        public void RemoveTask(IReceiverTask task)
        {
            lock (_tasks)
            {
                if (!_tasks.Contains(task))
                {
                    return;
                }
                task.Cancel();
                _tasks.Remove(task);
            }
        }

        public void ClearTasks()
        {
            lock (_tasks)
            {
                foreach (var task in Tasks)
                {
                    task.Cancel();
                }
                _tasks.Clear();
            }
        }

        /// <summary>
        /// returns if at least one task is setted up
        /// </summary>
        private bool SetBandAndTaskNumbers(IReceiverTask[] localTasks, int[] bandNumbers, int[] taskNumbers)
        {
            for (var i = 0; i < taskNumbers.Length; ++i)
            {
                bandNumbers[i] = -1;
                taskNumbers[i] = -1;
            }

            var taskIndex = 0;
            var result = false;
            for (var i = 0; i < localTasks.Length; i++)
            {
                if (localTasks[i].IsReady)
                {
                    continue;
                }
                var iterationTasks = localTasks[i].IterationTasks;
                // Checking if current task is trying to access receiver that already in use
                if (iterationTasks.Any(task => bandNumbers[task.ReceiverNumber] != -1))
                {
                    continue;
                }
                result = true;
                foreach (var iterationTask in iterationTasks)
                {
                    bandNumbers[iterationTask.ReceiverNumber] = iterationTask.BandNumber;
                    taskNumbers[taskIndex++] = i;
                }
            }
            return result;
        }

        // this circuit is always running
        private async Task FhssSearchCircuit()
        {
            while (true)
            {
                try
                {
                    var config = _config.FhssSearchSettings;
                    
                    await Task.Delay(config.FhssAcumulationDuration);

                    if (!(CurrentMode is RdfMode))
                    {
                        continue;
                    }
                    if (FilterManager.Bands.Count == 0)
                    {
                        continue;
                    }
                    if (config.SearchFhss != 1)
                    {
                        continue;
                    }

                    var threshold = FilterManager.GetThreshold(FilterManager.Bands[0].Number);
                    var signals = SignalStorage.GetSignals()
                        .Where(s => s.Signal.Amplitude > threshold)
                        .ToList();
                    
                    if (signals.Count == 0)
                        continue;
                    var networks = _fhssProcessor.FindNetworks(signals);
                    if (networks.Count != 0)
                    {
                        PostProcessing(networks);
                        FhssNetworkStorage.Put(networks);
                    }
                    //send event to measure duration
                    //TODO:remove
                    var notMeasuredCount = FhssNetworkStorage.
                        GetFhssNetworks().
                        Count(n =>n.IsDurationMeasured == false);
                    if (Config.Instance.FhssSearchSettings.FhssDurationMeasureTest == false && notMeasuredCount != 0)
                        FhssDurationMeasuringStartEvent?.Invoke(this, EventArgs.Empty);
                }
                catch (Exception e)
                {
                    MessageLogger.Error(e, "Fhss search error occurred");
                }
            }

            void PostProcessing(List<IFhssNetwork> networks)
            {
                var counter = 0;
                while (counter != networks.Count - 1)
                {
                    counter = 0;
                    for (int i = 0; i < networks.Count - 1; i++)
                    {
                        if (Math.Abs(networks[i].Directions[0] - networks[i + 1].Directions[0]) < 10
                        ) //todo: this !10 degree direction difference
                        {
                            if (Math.Abs(networks[i].MaxFrequencyKhz - networks[i + 1].MinFrequencyKhz) < 2000)//2Mhz gap 
                            {
                                networks[i].Update(
                                    new FhssNetwork(
                                        networks[i].MinFrequencyKhz,
                                        networks[i+1].MaxFrequencyKhz,
                                        networks[i].BandwidthKhz,
                                        networks[i].StepKhz,
                                        networks[i].Amplitude,
                                        networks[i].ImpulseDurationMs,
                                        networks[i].IsDurationMeasured,
                                        networks[i].FrequenciesCount + networks[i+1].FrequenciesCount,
                                        networks[i].Directions,
                                        networks[i].FixedRadioSources.Concat(networks[i+1].FixedRadioSources)));
                                networks.RemoveAt(i+1);
                                break;
                            }
                        }
                        counter++;
                    }
                }
            }
        }

        private async Task WorkCircuit()
        {
            // i-element of array contains band number for i-receiver
            var bandNumbers = new int[6];
            var taskNumbers = new int[6];

            try
            {
                while (true)
                {
                    IReceiverTask[] localTasks;
                    lock (_tasks)
                    {
                        localTasks = Tasks.ToArray();
                    }
                    if (localTasks.Length == 0)
                    {
                        await _taskAddEvent.WaitAsync();
                        // this blank scan is needed to exclude situations when band is not changed after previous task
                        // but some vital changes have occured awaiting some task
                        // with this blank scan, first real scan in the task is going to be from the current time moment
                        HardwareController.DeviceManager.GetBlankScan();
                        continue;
                    }
                    if (!SetBandAndTaskNumbers(localTasks, bandNumbers, taskNumbers))
                    {
                        RemoveCompletedTasks(localTasks);
                        continue;
                    }
                    var scan = HardwareController.GetScan(bandNumbers);
                    if (scan == null)
                    {
                        MessageLogger.Warning("Scan is null");
                        continue;
                    }

                    for (var i = 0; i < localTasks.Length; ++i)
                    {
                        if (!localTasks[i].IsReady && taskNumbers.Contains(i))
                        {
                            await localTasks[i].HandleScan(scan);
                        }
                    }
                    RemoveCompletedTasks(localTasks);
                }
            }
            catch (IOException e)
            {
                // simulator connection lost
                MessageLogger.Warning(e.Message);
            }
            catch (Exception e)
            {
                MessageLogger.Error(e, "Work circuit error occurred");
            }
            _workCircuitStoppedEvent.Set();
        }

        void RemoveCompletedTasks(IReceiverTask[] localTasks)
        {
            foreach (var task in localTasks)
            {
                if (task.IsReady)
                {
                    RemoveTask(task);
                }
            }
        }
    }
}

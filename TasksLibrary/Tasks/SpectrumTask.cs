﻿using System.Collections.Generic;
using System.Linq;
using DspDataModel;
using DspDataModel.Data;
using DspDataModel.Storages;
using DspDataModel.Tasks;

namespace TasksLibrary.Tasks
{
    public class SpectrumTask : ReceiverTask<IAmplitudeBand>
    {
        private readonly ISpectrumStorage _spectrumStorage;

        private readonly List<float> _amplitudes;

        public SpectrumTask(SpectrumTaskSetup setup)
            : base(setup.TaskManager.DataProcessor, setup.Objectives, setup.ReceiversIndexes, setup.AveragingCount, true, setup.Priority)
        {
            _spectrumStorage = setup.TaskManager.SpectrumStorage;
            IsEndless = setup.IsEndless;
            _amplitudes = new List<float>();
        }

        protected override void ProcessData(IDataAggregator aggregator, IReadOnlyList<IterationTask> objectives)
        {
            var bandNumber = objectives[0].BandNumber;
            var data = aggregator.GetSpectrum();
            if (IsEndless)
            {
                _spectrumStorage.Put(data);
            }
            else
            {
                _amplitudes.AddRange(data.Amplitudes);
                if (bandNumber != Objectives.Last().Number)
                {
                    return;
                }
                TypedResult = new AmplitudeBand(_amplitudes.ToArray());
            }
        }
    }

    public class SpectrumTaskSetup
    {
        public ITaskManager TaskManager;
        public IReadOnlyList<Band> Objectives;
        public int[] ReceiversIndexes;
        public int Priority = 0;
        public int AveragingCount = Config.Instance.DirectionFindingSettings.SpectrumScanCount;
        public bool IsEndless = true;
    }
}

using System.Collections.Generic;
using System.Linq;
using DspDataModel;
using DspDataModel.Data;
using DspDataModel.DataProcessor;
using DspDataModel.RadioJam;
using DspDataModel.Storages;
using DspDataModel.Tasks;
using Settings;
using SharpExtensions;
using TasksLibrary.JamStrategies;

namespace TasksLibrary.Tasks
{
    public class RadioSourcesControlTask : ReceiverTask<RadioSourcesControlTask.TaskResult>
    {
        private readonly ITaskManager _taskManager;

        private readonly IReadOnlyList<IRadioJamTarget> _targets;

        private readonly TaskResult _result;

        private readonly HashSet<int> _processedBands;

        private readonly float _controlFrequencyDeviationKhz;

        private readonly ISpectrumStorage _spectrumStorage;

        private RadioSourcesControlTask(ITaskManager taskManager, IReadOnlyList<IRadioJamTarget> targets, IReadOnlyList<Band> objectives)
            : base(taskManager.DataProcessor, objectives, new[] { 0, 1, 2, 3, 4 }, 1, false, 1, moveReceiversSynchronously: true)
        {
            _taskManager = taskManager;
            _spectrumStorage = taskManager.SpectrumStorage;
            _targets = targets;
            _result = new TaskResult();
            _processedBands = new HashSet<int>();
            IsEndless = false;

            var config = Config.Instance.HardwareSettings.JammingConfig;
            _controlFrequencyDeviationKhz = config.ControlFrequencyDeviationKhz;

            if (targets.IsNullOrEmpty())
            {
                TypedResult = new TaskResult();
                return;
            }

            Objectives = objectives;

            UpdateIterationsTasks();
        }

        public static RadioSourcesControlTask Create(ITaskManager taskManager, IReadOnlyList<IRadioJamTarget> targets)
        {
            var objectives = targets
                .Select(s => s.FrequencyKhz)
                .Select(Utilities.GetBandNumber)
                .OrderBy(b => b)
                .Distinct()
                .Select(b => new Band(b))
                .ToArray();

            return new RadioSourcesControlTask(taskManager, targets, objectives);
        }

        protected override void ProcessData(IDataAggregator aggregator, IReadOnlyList<IterationTask> objectives)
        {
            var scans = aggregator.GetSpectrums();

            var isLastScan = false;
            for (var i = 0; i < scans.Count; i++)
            {
                var scan = scans[i];
                if (objectives[i].BandNumber != -1)
                {
                    ProcessScan(scan);
                }
            }
            if (isLastScan)
            {
                TypedResult = _result;
            }

            void ProcessScan(IDataScan scan)
            {
                if (_processedBands.Contains(scan.BandNumber))
                {
                    return;
                }
                _spectrumStorage.Put(scan);
                if (scan.BandNumber == Objectives.Last().Number)
                {
                    isLastScan = true;
                }
                var scanTargets = _targets.Where(t => Utilities.GetBandNumber(t.FrequencyKhz) == scan.BandNumber).ToArray();
                _processedBands.Add(scan.BandNumber);

                _taskManager.FilterManager.UpdateNoiseLevel(scan);
                var adaptiveThreshold = _taskManager.FilterManager.GetAdaptiveThreshold(scan.BandNumber);

                foreach (var target in scanTargets)
                {
                    ProcessSignal(scan, target, adaptiveThreshold);
                }
            }

            // Trying to find signal corresponding to current target
            void ProcessSignal(IDataScan scan, IRadioJamTarget target, float adaptiveThreshold)
            {
                var threshold = target.UseAdaptiveThreshold ? adaptiveThreshold : target.Threshold;

                var frequencyRange = new FrequencyRange(target.FrequencyKhz - _controlFrequencyDeviationKhz, target.FrequencyKhz + _controlFrequencyDeviationKhz);
                var processConfig = ScanProcessConfig.CreateConfigWithoutDf(threshold, new [] {frequencyRange});

                var processResult = DataProcessor.GetSignals(scan, processConfig);
                var scannedSignals = processResult.Signals;

                if (scannedSignals.IsNullOrEmpty())
                {
                    return;
                }
                var result = new TargetSignalPair(target, scannedSignals.MaxArg(s => s.Amplitude));
                _result.Add(result);
            }
        }

        public class TaskResult
        {
            public IReadOnlyList<TargetSignalPair> Signals => _signals;

            private readonly List<TargetSignalPair> _signals;

            public TaskResult()
            {
                _signals = new List<TargetSignalPair>();
            }

            internal void Add(TargetSignalPair signal)
            {
                _signals.Add(signal);
            }
        }
    }
}
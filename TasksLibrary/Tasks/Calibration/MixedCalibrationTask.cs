﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DspDataModel;
using DspDataModel.Data;
using DspDataModel.Hardware;
using DspDataModel.Tasks;
using Settings;

namespace TasksLibrary.Tasks.Calibration
{
    public class MixedCalibrationTask : CalibrationTask
    {
        public override float StepKhz { get; protected set; }

        private readonly List<float> _phaseDeviations;

        public override float Progress { get; protected set; }

        public override IReadOnlyList<float> PhaseDeviations => _phaseDeviations;

        private readonly int _signalAveragingCount;

        private readonly CalibrationConfig _config;

        private readonly NoiseCalibrationTask _noiseCalibrationTask;

        private SignalCalibrationTask _signalCalibrationTask;

        private readonly ITaskManager _taskManager;

        public MixedCalibrationTask(ITaskManager taskManager, IReadOnlyList<Band> objectives, float stepKhz, int averagingCount, int signalAveragingCount, int priority = 0)
            : base(taskManager.DataProcessor, objectives, new[] { 0, 1, 2, 3, 4 }, signalAveragingCount, true, priority)
        {
            _noiseCalibrationTask = new NoiseCalibrationTask(taskManager, objectives, stepKhz, averagingCount, useRecalibration: false);
            _taskManager = taskManager;
            StepKhz = stepKhz;
            IsEndless = false;
            _phaseDeviations = new List<float>();
            _signalAveragingCount = signalAveragingCount;

            taskManager.HardwareController.ReceiverManager.SetReceiversChannel(ReceiverChannel.ExternalCalibration);

            _config = Config.Instance.CalibrationSettings;

            // We use averaging "signalAveragingCount" times for each signal in the band
            AveragingCount = (int)(Constants.BandwidthKhz / stepKhz) * signalAveragingCount;
        }

        internal override bool IsDataReadyForProcessing()
        {
            return _noiseCalibrationTask.IsReady
                ? base.IsDataReadyForProcessing()
                : _noiseCalibrationTask.IsDataReadyForProcessing();
        }

        public override async Task HandleScan(IFpgaDataScan scan)
        {
            var totalCalibrateBandCount =
                _noiseCalibrationTask.Objectives.Count + _noiseCalibrationTask.BandCountToRecalibrate;
            if (!_noiseCalibrationTask.IsReady)
            {
                await _noiseCalibrationTask.HandleScan(scan).ConfigureAwait(false);

                var bandIndex = _noiseCalibrationTask.Progress * _noiseCalibrationTask.Objectives.Count;
                Progress = 1f * bandIndex / totalCalibrateBandCount;
            }
            else if (!_signalCalibrationTask.IsReady)
            {
                await _signalCalibrationTask.HandleScan(scan).ConfigureAwait(false);

                var bandIndex = _noiseCalibrationTask.Objectives.Count + _signalCalibrationTask.Progress * _signalCalibrationTask.Objectives.Count;
                Progress = 1f * bandIndex / totalCalibrateBandCount;
            }
        }

        private void CreateSignalCalibrationTask()
        {
            var noiseCalibration = _noiseCalibrationTask.TypedResult.RadioPathBandCalibrations;
            var signalCalibrationObjectives = noiseCalibration
                .Where(cal => cal.BandPhaseDeviation > MaxPhaseDeviation)
                .Select(cal => new Band(cal.BandNumber))
                .ToArray();
            _signalCalibrationTask = new SignalCalibrationTask(_taskManager, signalCalibrationObjectives, StepKhz, _signalAveragingCount);
        }

        private CalibrationTaskResult MixCalibrationResults()
        {
            var result = new CalibrationTaskResult();
            var noiseCalibration = _noiseCalibrationTask.TypedResult.RadioPathBandCalibrations;
            var signalCalibration = _signalCalibrationTask.TypedResult.RadioPathBandCalibrations;

            foreach (var objective in Objectives)
            {
                var noiseBandCalibration = noiseCalibration.First(cal => cal.BandNumber == objective.Number);
                var signalBandCalibration = signalCalibration.FirstOrDefault(cal => cal.BandNumber == objective.Number);

                if (signalBandCalibration == null)
                {
                    result.AddBandRadioPathCalibration(noiseBandCalibration);
                }
                else
                {
                    var bandCalibration = noiseBandCalibration.BandPhaseDeviation < signalBandCalibration.BandPhaseDeviation
                        ? noiseBandCalibration
                        : signalBandCalibration;
                    result.AddBandRadioPathCalibration(bandCalibration);
                }
            }

            return result;
        }

        protected override void ProcessData(IDataAggregator aggregator, IReadOnlyList<IterationTask> objectives)
        {
            if (_noiseCalibrationTask.IsReady && _signalCalibrationTask == null)
            {
                CreateSignalCalibrationTask();
            }
            if (_signalCalibrationTask?.IsReady ?? false)
            {
                TypedResult = MixCalibrationResults();
            }
        }
    }
}

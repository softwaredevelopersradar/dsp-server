﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using DspDataModel;
using DspDataModel.Data;
using DspDataModel.Hardware;
using DspDataModel.Tasks;
using Settings;

namespace TasksLibrary.Tasks.Calibration
{
    public class SignalCalibrationTask : CalibrationTask
    {
        public override float StepKhz { get; protected set; }

        private readonly CalibrationTaskResult _calibrationResult;

        private readonly List<float> _phaseDeviations;

        public override float Progress { get; protected set; }

        public override IReadOnlyList<float> PhaseDeviations => _phaseDeviations;

        private int _bandIndex = 0;

        private readonly int _signalSignalAveragingCount;

        private readonly ISignalGenerator _signalGenerator;

        private const int MaxSkippedSignalCount = 200;

        private const int ResetSignalCount = 100;

        private int _skippedSignalCount = 0;

        private readonly CalibrationConfig _config;

        private List<CalibrationRangesConfig> CalibrationRanges;

        public SignalCalibrationTask(ITaskManager taskManager, IReadOnlyList<Band> objectives, float stepKhz, int signalAveragingCount, int priority = 0)
            : base(taskManager.DataProcessor, objectives, new[] { 0, 1, 2, 3, 4 }, signalAveragingCount, true, priority)
        {
            StepKhz = stepKhz;
            IsEndless = false;
            _phaseDeviations = new List<float>();
            _calibrationResult = new CalibrationTaskResult();
            _signalSignalAveragingCount = signalAveragingCount;
            _signalGenerator = taskManager.HardwareController.SignalGenerator;

            taskManager.HardwareController.ReceiverManager.SetReceiversChannel(ReceiverChannel.ExternalCalibration);

            _config = Config.Instance.CalibrationSettings;
            ValidateRangeSettings(_config.SignalGeneratorSettings.CalibrationRangesSettings);
            _signalGenerator.EmitionEnabled = true;
            _signalGenerator.Level = _config.SignalGeneratorSettings.DefaultCalibrationLevel;
            _signalGenerator.FrequencyKhz = GetSignalFrequency(objectives[0].Number, 0);

            // We use averaging "signalAveragingCount" times for each signal in the band
            AveragingCount = (int)(Constants.BandwidthKhz / stepKhz) * signalAveragingCount;

            // Signal generator is not responding after initialization for a while, so just small pause here
            Thread.Sleep(100);
        }

        private void ValidateRangeSettings(CalibrationRangesConfig[] rangesConfig)
        {
            var validRanges = rangesConfig.Where(range => range.IsValidRange()).ToList();
            if (validRanges.Count != rangesConfig.Length)
            {
                rangesConfig = validRanges.ToArray();
                Config.Save();//to remove incorrect values for future uses
            }
            
            CalibrationRanges = validRanges;
        }

        private float GetSignalFrequency(int bandNumber, int index)
        {
            var startFrequency = Utilities.GetBandMinFrequencyKhz(bandNumber) + StepKhz / 2;
            var outputFrequency = startFrequency + StepKhz * index;
            var rangeLevel = _config.SignalGeneratorSettings.DefaultCalibrationLevel;
            
            foreach (var range in CalibrationRanges)
            {
                if (range.ContainsFrequency(outputFrequency))
                {
                    rangeLevel = range.CalibrationLevel;
                    break;
                }
            }

            if (_signalGenerator.Level != rangeLevel)
            {
                _signalGenerator.Level = rangeLevel;
                Thread.Sleep(500);//todo : 50 ms is not enough for generator to change level, so give him more time
            }

            return outputFrequency;
        }

        public override async Task HandleScan(IFpgaDataScan scan)
        {
            if (!ScanContainsSignal(scan, _signalGenerator.FrequencyKhz))
            {
                ++_skippedSignalCount;
                if (_skippedSignalCount == ResetSignalCount)
                {
                    _signalGenerator.Reset();
                }
                if (_skippedSignalCount == MaxSkippedSignalCount)
                {
                    FormErrorResult(scan);
                }
                return;
            }
            if (DataAggregator.DataCount % _signalSignalAveragingCount == _signalSignalAveragingCount - 1)
            {
                _skippedSignalCount = 0;
                var index = 1 + DataAggregator.DataCount / _signalSignalAveragingCount;
                var bandNumber = CurrentObjective.Number;
                var frequency = GetSignalFrequency(bandNumber, index);

                _signalGenerator.FrequencyKhz = frequency;
            }
            await base.HandleScan(scan);
        }

        private void FormErrorResult(IFpgaDataScan scan)
        {
            var errorAmplitudes = Enumerable
                .Repeat(Constants.ReceiverMinAmplitude, Constants.DfReceiversCount)
                .ToArray();
            var errorPhases = Enumerable.Repeat(0f, Constants.PhasesDifferencesCount)
                .ToArray();

            var pointsCount = (int)(Constants.BandwidthKhz / StepKhz);

            while (_calibrationResult.RadioPathBandCalibrations.Count < Objectives.Count)
            {
                var bandNumber = Objectives[_calibrationResult.RadioPathBandCalibrations.Count].Number;

                _calibrationResult.RadioPathBandCalibrations.Add(
                    new BandRadioPathCalibration(bandNumber,
                        Enumerable.Repeat(errorPhases, pointsCount).ToList(),
                        Enumerable.Repeat(errorAmplitudes, pointsCount).ToList(), 180)
                );

                _phaseDeviations.Add(180);
            }
            MessageLogger.Error($"Calibration error! Can't find generator's signal at {_signalGenerator.FrequencyKhz}!" +
                                $"{Environment.NewLine} {GetMaxAmplitudeErrorString()}");

            _signalGenerator.EmitionEnabled = false;
            Progress = 1;
            TypedResult = _calibrationResult;

            string GetMaxAmplitudeErrorString()
            {
                var mergedScan = scan.Scans.Take(5).ToArray().MergeSpectrum();
                var maxAmplitude = mergedScan.Amplitudes.Max();
                var index = Array.IndexOf(mergedScan.Amplitudes, maxAmplitude);
                var frequency = Utilities.GetFrequencyKhz(mergedScan.BandNumber, index);
                var noiseLevel = scan.Scans[0].Amplitudes.GetNoiseLevel();
                return $"Max amplitude is {maxAmplitude} dB at frequency {frequency} KHz, noise level is {noiseLevel} dB, " +
                       $"signal generator's level should be {_signalGenerator.Level}";
            }
        }

        private bool ScanContainsSignal(IFpgaDataScan scan, float frequency)
        {
            var pointNumber = Utilities.GetSampleNumber(frequency);

            var start = Math.Max(0, pointNumber - 5);
            var end = Math.Min(Constants.BandSampleCount, pointNumber + 5);
            var threshold = scan.Scans[0].Amplitudes.GetNoiseLevel() + _config.SignalCalibrationRelativeThreshold;

            for (var i = start; i < end; ++i)
            {
                for (var j = 0; j < Constants.DfReceiversCount; ++j)
                {
                    if (scan.Scans[j].Amplitudes[i] > threshold)
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        protected override void ProcessData(IDataAggregator aggregator, IReadOnlyList<IterationTask> objectives)
        {
            var bandNumber = objectives[0].BandNumber;
            var startFrequency = Utilities.GetBandMinFrequencyKhz(bandNumber) + StepKhz / 2;
            var calibration = aggregator.GetCalibrationDataBySignal(startFrequency, StepKhz, _signalSignalAveragingCount);

            _bandIndex++;

            CalibrateBand();
            Progress = 1f * _bandIndex / Objectives.Count;

            if (bandNumber == Objectives.Last().Number)
            {
                Progress = 1;
                _signalGenerator.EmitionEnabled = false;
                TypedResult = _calibrationResult;
            }

            void CalibrateBand()
            {
                var phaseDeviation = calibration.PhaseDeviations.Max();
                var bandCalibration = new BandRadioPathCalibration(bandNumber, calibration.Phases, calibration.Amplitudes, phaseDeviation);
                _calibrationResult.AddBandRadioPathCalibration(bandCalibration);
                //updating progress
                _phaseDeviations.Add(phaseDeviation);
                LogBandCalibration(bandNumber, phaseDeviation);
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DspDataModel;
using DspDataModel.Data;
using DspDataModel.DataProcessor;
using DspDataModel.Tasks;
using Settings;

namespace TasksLibrary.Tasks
{
    public class DirectionFindingTask : ReceiverTask<ProcessResult>
    {
        private IReadOnlyList<FrequencyRange> _frequencyRanges;

        /// <summary>
        /// fires when new portion of data (set of scans) is handled
        /// </summary>
        public event EventHandler<SignalsProcessedEventArg> SignalsProcessedEvent;

        private readonly List<ISignal> _signals;
        private readonly List<ISignal> _rawSignals;

        private readonly ITaskManager _taskManager;

        private readonly DirectionFindingTaskSetup _setup;

        public DirectionFindingTask(DirectionFindingTaskSetup setup)
            : base(setup.TaskManager.DataProcessor, setup.GetObjectives(), setup.ReceiversIndexes, setup.ScanAveragingCount * setup.BearingAveragingCount, setup.AutoUpdateObjectives, setup.Priority)
        {
            _taskManager = setup.TaskManager;
            _frequencyRanges = setup.GetFrequencyRanges();
            _setup = setup;
            IsEndless = setup.IsEndless;
            _signals = new List<ISignal>();
            _rawSignals = new List<ISignal>();
            _spectrumBands = GetSpectrumBandNumbers();
        }

        public override void UpdateObjectives(IReadOnlyList<FrequencyRange> frequencyRanges)
        {
            base.UpdateObjectives(frequencyRanges);
            _frequencyRanges = frequencyRanges;
        }

        private int _spectrumBandIndex = 0;
        private List<int> _spectrumBands;
        private bool flag;
        protected override void ProcessData(IDataAggregator aggregator, IReadOnlyList<IterationTask> objectives)
        {
            flag = true;
            /*if (IsEndless && Config.Instance.FhssSearchSettings.SearchFhss == 1)
                Task.Run(async () =>
                {
                    while (flag)
                    {
                        var bands = GetRangeCycled(_spectrumBandIndex, Constants.ReceiversCount);
                        _taskManager.HardwareController.ReceiverManager.SetBandNumbers(bands);
                        var sca = _taskManager.HardwareController.DeviceManager.GetScan(bands);
                        for (int i = 0; i < Constants.DfReceiversCount; i++)
                        {
                            _taskManager.SpectrumStorage.Put(sca.Scans[i]);
                        }
                        _spectrumBandIndex += Constants.DfReceiversCount;
                        if (_spectrumBandIndex >= _spectrumBands.Count)
                        {
                            _spectrumBandIndex = 0;
                        }
                        await Task.Delay(0);
                    }
                });*/
            var bandNumber = objectives[0].BandNumber;
            var scan = aggregator.GetDataScan();
            _taskManager.FilterManager.UpdateNoiseLevel(scan);
            var threshold = _taskManager.FilterManager.GetThreshold(bandNumber);
            var dfConfig = ScanProcessConfig.CreateDfConfig(threshold, _setup.BearingAveragingCount, _setup.ScanAveragingCount, _frequencyRanges, _setup.CalculateHighQualityPhases);
            var result = DataProcessor.GetSignals(scan, dfConfig);
            if (IsEndless)
            {
                flag = false;
                SignalsProcessedEvent?.Invoke(this, new SignalsProcessedEventArg(result, scan));
            }
            else
            {
                _signals.AddRange(result.Signals.Where(IsSignalInFrequencyRange));
                _rawSignals.AddRange(result.RawSignals.Where(IsSignalInFrequencyRange));
                if (bandNumber != Objectives.Last().Number)
                {
                    return;
                }
                TypedResult = new ProcessResult(_signals, _rawSignals);
            }
        }

        private List<int> GetSpectrumBandNumbers()
        {
            var spectrumBands = new List<int>();
            foreach (var range in _frequencyRanges)
            {
                var startBand = Utilities.GetBandNumber(range.StartFrequencyKhz);
                var endBand = Utilities.GetBandNumber(range.EndFrequencyKhz);
                spectrumBands.AddRange(Enumerable.Range(startBand, endBand - startBand + 1));
            }
            return spectrumBands;
        }

        private List<int> GetRangeCycled(int startIndex, int count)
        {
            if (startIndex + count < _spectrumBands.Count)
            {
                return _spectrumBands.GetRange(startIndex, count);
            }

            var bands = new List<int>();
            var firstPartCount = _spectrumBands.Count - startIndex;
            bands.AddRange(_spectrumBands.GetRange(startIndex, firstPartCount));
            bands.AddRange(GetRangeCycled(0, count - firstPartCount));
            return bands;
        }

        private bool IsSignalInFrequencyRange(ISignal signal)
        {
            var signalFrequencyRange = signal.GetFrequencyRange();
            return _frequencyRanges.Any(range => FrequencyRange.AreIntersected(range, signalFrequencyRange));
        }

        protected override void UpdateSpectrumStorage(IFpgaDataScan scan)
        {
            //_taskManager.SpectrumStorage.Put(scan.Scans[0]);
        }
    }

    public struct SignalsProcessedEventArg
    {
        public readonly IReadOnlyList<ISignal> Signals;
        public readonly IReadOnlyList<ISignal> RawSignals;
        public readonly IDataScan Scan;

        public SignalsProcessedEventArg(ProcessResult result, IDataScan scan)
        {
            Signals = result.Signals;
            RawSignals = result.RawSignals;
            Scan = scan;
        }
    }

    public class DirectionFindingTaskSetup
    {
        /// you should use either FrequencyRanges or Objectives, not both
        public IReadOnlyList<FrequencyRange> FrequencyRanges
        {
            private get => _frequencyRanges;
            set
            {
                _frequencyRanges = value.OrderBy(f => f.StartFrequencyKhz).ToArray();
            }
        }

        public IReadOnlyList<Band> Objectives
        {
            private get => _objectives;
            set
            {
                _objectives = value.OrderBy(b => b.Number).ToArray(); 
            }
        }

        public ITaskManager TaskManager;
        public int[] ReceiversIndexes;
        public int Priority = 0;
        public int ScanAveragingCount = Config.Instance.DirectionFindingSettings.DefaultDirectionScanCount;
        public int BearingAveragingCount = Config.Instance.DirectionFindingSettings.DefaultBearingScanCount;
        public bool IsEndless;
        public bool CalculateHighQualityPhases = false;
        public bool AutoUpdateObjectives;
        private IReadOnlyList<FrequencyRange> _frequencyRanges;
        private IReadOnlyList<Band> _objectives;

        public IReadOnlyList<Band> GetObjectives()
        {
            return Objectives ?? FrequencyRange.SplitInBands(FrequencyRanges).ToArray();
        }

        internal IReadOnlyList<FrequencyRange> GetFrequencyRanges()
        {
            return Objectives == null
                ? FrequencyRanges
                : new[] {new FrequencyRange(Objectives[0].FrequencyFromKhz, Objectives.Last().FrequencyToKhz)};
        }
    }
}

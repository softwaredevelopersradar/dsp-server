﻿using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Settings;
using Correlator;
using DspDataModel;
using DspDataModel.Tasks;
using RadioTables;
using DspDataModel.Hardware;
using TasksLibrary.Tasks.Calibration;

namespace TasksLibrary.Modes
{
    public class CalibrationMode : IModeController
    {
        public ITaskManager TaskManager { get; }
        public float ScanSpeed => 0;

        public int BandIterationCount { get; }

        public float StepKhz { get; }

        private CalibrationTask _task;

        private readonly ISignalGenerator _signalGenerator;

        public float Progress
        {
            get
            {

                if (_isCalibrationReady)
                    return 1;
                var progress = _task?.Progress ?? 0;
                return progress * 0.98f;
            }
        }

        // little hack to prevent returning Progress = 1, while some background operations are not ready (setting attenuators and so on)
        private bool _isCalibrationReady = false;

        private const string OldCalibrationsDirectory = "OldCalibrations";

        private const int MaxCalibrationFiles = 32;

        public IReadOnlyList<float> PhaseDeviations => _task.PhaseDeviations;

        private IReadOnlyList<FpgaDeviceBandSettings> _savedBandsSettings;

        private IReceiverManager ReceiverManager => TaskManager.HardwareController.ReceiverManager;

        public CalibrationMode(ITaskManager taskManager)
            : this(taskManager, Config.Instance.CalibrationSettings.CalibrationStepKhz, Config.Instance.CalibrationSettings.CalibrationMaxScanCount)
        { }

        public CalibrationMode(ITaskManager taskManager, float stepKhz, int bandIterationCount)
        {
            MessageLogger.Trace("Calibration mode: constructor");

            Contract.Assert(bandIterationCount > 0);
            Contract.Assert(stepKhz > 0);

            TaskManager = taskManager;
            StepKhz = stepKhz;
            BandIterationCount = bandIterationCount;
            _signalGenerator = TaskManager.HardwareController.SignalGenerator;
        }

        private async Task InitializeSignalGenerator()
        {
            if (Config.Instance.CalibrationSettings.UseNoiseGeneratorCalibration)
            {
                return;
            }
            var hostname = Config.Instance.CalibrationSettings.SignalGeneratorSettings.Host;
            var result = await _signalGenerator.Initialize(hostname);
            if (!result)
            {
                throw new Exception("Can't start calibration - signal generator is not initialized.");
            }

            // after first initialization signal generator is not responding to first commands
            // so let's try to send several commands to it
            _signalGenerator.FrequencyKhz = Constants.FirstBandMinKhz;
            _signalGenerator.EmitionEnabled = true;
            _signalGenerator.Level = -20;
            _signalGenerator.EmitionEnabled = false;
        }

        public async Task Initialize()
        {
            await InitializeSignalGenerator();
        }

        public void OnActivated()
        {
            MessageLogger.Trace("Calibration mode: on activated");

            _savedBandsSettings = ReceiverManager.BandsSettingsSource.Settings.Select(s => s.Copy()).ToArray();

            // turning off all attenuators
            for (var i = 0; i < _savedBandsSettings.Count; ++i)
            {
                ReceiverManager.SetAttenuatorValue(i, false, 0);
            }
        }

        public void OnStop()
        { }

        public IEnumerable<IReceiverTask> CreateTasks()
        {
            var bands = Enumerable.Range(0, Config.Instance.BandSettings.BandCount);
            if (TaskManager.FilterManager.CalibrationBands.Count != 0)
                bands = TaskManager.FilterManager.CalibrationBands;

            var objectives = bands.Select(number => new Band(number))
                .ToArray();
            if (Config.Instance.CalibrationSettings.UseNoiseGeneratorCalibration)
            {
                _task = new NoiseCalibrationTask(TaskManager, objectives, StepKhz, BandIterationCount);
            }
            else
            {
                _task = new SignalCalibrationTask(TaskManager, objectives, StepKhz, Config.Instance.CalibrationSettings.SignalCalibrationScanCount);
            }
            _task.TaskEndEvent += OnTaskEnd;
            yield return _task;
        }

        void OnTaskEnd(object sender, EventArgs e)
        {
            MessageLogger.Trace("Calibration mode: on task end");

            var table = new RadioPathTable(CreateRadioPathDto());
            DeleteOldCalibrationFiles();
            MoveOldCalibrationFile();
            table.Save(Config.Instance.CalibrationSettings.RadioPathTableFilename);

            TaskManager.DataProcessor.ReloadRadioPathTable(Config.Instance.CalibrationSettings.RadioPathTableFilename);
            ReceiverManager.SetReceiversChannel(ReceiverChannel.Air);
            _signalGenerator?.Close();

            // restore band attenuators settings
            for (var i = 0; i < _savedBandsSettings.Count; ++i)
            {
                var settings = _savedBandsSettings[i];
                ReceiverManager.SetAttenuatorValue(i, settings.IsConstantAttenuatorEnabled, settings.AttenuatorLevel);
            }

            _isCalibrationReady = true;
            MessageLogger.Log("Calibration complete.");
        }

        private void DeleteOldCalibrationFiles()
        {
            try
            {
                if (!Directory.Exists(OldCalibrationsDirectory))
                {
                    return;
                }

                var oldCalibrationFiles = Directory
                    .EnumerateFiles(OldCalibrationsDirectory)
                    .ToArray();

                if (oldCalibrationFiles.Length > MaxCalibrationFiles)
                {
                    foreach (var oldCalibrationFile in oldCalibrationFiles)
                    {
                        File.Delete(oldCalibrationFile);
                    }
                }
            }
            catch (Exception e)
            {
                MessageLogger.Warning($"Can't delete old calibration files: {e.Message}");
            }
        }

        private void MoveOldCalibrationFile()
        {
            var filename = Config.Instance.CalibrationSettings.RadioPathTableFilename;

            if (!File.Exists(filename))
            {
                return;
            }
            if (!Directory.Exists(OldCalibrationsDirectory))
            {
                Directory.CreateDirectory(OldCalibrationsDirectory);
            }
            
            var date = File.GetLastWriteTime(filename).ToString("yyyy-MM-dd.HH-mm");
            var filenameWithoutExtension = Path.GetFileNameWithoutExtension(filename);
            var extension = Path.GetExtension(filename);
            try
            {
                File.Move(filename, Path.Combine(OldCalibrationsDirectory, filenameWithoutExtension + date + extension));
            }
            catch (Exception e)
            {
                MessageLogger.Warning("Can't move old calibration file! " + e.Message);
            }
        }

        private RadioPathTableDto CreateRadioPathDto()
        {
            var radioPathTable = new RadioPathTableDto
            {
                MinFrequencyKhz = Constants.FirstBandMinKhz,
                MaxFrequencyKhz = Constants.FirstBandMinKhz + Config.Instance.BandSettings.BandCount * Constants.BandwidthKhz
            };

            var updateRadioPathFile = false;

            if (TaskManager.FilterManager.CalibrationBands.Count != 0)
            {
                MessageLogger.Warning("Calibration table is going to be created, but it's only a partial calibration.");
                if (File.Exists(Config.Instance.CalibrationSettings.RadioPathTableFilename))
                {
                    radioPathTable.LoadFromFile(Config.Instance.CalibrationSettings.RadioPathTableFilename);
                    if (radioPathTable.BandRadioPathCalibrations.Count == Config.Instance.BandSettings.BandCount)
                    {
                        updateRadioPathFile = true;
                        MessageLogger.Warning("Previous radiopath table is going to be updated.");
                    }
                }
                if(updateRadioPathFile == false)
                    MessageLogger.Warning("Please run full calibration and then partial for correct result");
            }

            foreach (var bandCalibration in _task.TypedResult.RadioPathBandCalibrations)
            {
                var phaseArrays = new PhaseArray[bandCalibration.RadioPathPhaseDifferencies.Count];
                for (var i = 0; i < phaseArrays.Length; ++i)
                {
                    phaseArrays[i] = new PhaseArray(bandCalibration.RadioPathPhaseDifferencies[i]);
                }
                var amplitudeArrays = new AmplitudeArray[bandCalibration.RadioPathPhaseDifferencies.Count];
                for (var i = 0; i < phaseArrays.Length; ++i)
                {
                    amplitudeArrays[i] = new AmplitudeArray(bandCalibration.Amplitudes[i]);
                }

                var bandRadioPathDto = new BandRadioPath(
                    new List<PhaseArray>(phaseArrays),
                    new List<AmplitudeArray>(amplitudeArrays),
                    bandCalibration.BandPhaseDeviation
                );

                if (updateRadioPathFile)
                {
                    radioPathTable.BandRadioPathCalibrations[bandCalibration.BandNumber] = bandRadioPathDto;
                }
                else
                {
                    radioPathTable.BandRadioPathCalibrations.Add(bandRadioPathDto);
                }
            }
            return radioPathTable;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DspDataModel;
using DspDataModel.LinkedStation;
using DspDataModel.Tasks;
using TasksLibrary.Tasks;

namespace TasksLibrary.Modes
{
    public class RdfMode : IModeController
    {
        public ITaskManager TaskManager { get; }
        public float ScanSpeed { get; private set; }
        public ILinkedStationManager LinkedStationManager { get; }
        
        private DirectionFindingTask _dfTask;
        private readonly DirectionFindingConfig _config;

        private readonly List<ISignal> _signalsToSent = new List<ISignal>();

        private bool _ownScansReady = false;
        private DateTime _lastCycleEndTime;


        public RdfMode(ITaskManager taskManager, ILinkedStationManager linkedStationManager)
        {
            TaskManager = taskManager;
            LinkedStationManager = linkedStationManager;
            _config = Config.Instance.DirectionFindingSettings;
        }

        public Task Initialize() => Task.CompletedTask;

        public void OnActivated()
        {
            _lastCycleEndTime = DateTime.UtcNow;
        }

        public void OnStop()
        {
            _dfTask?.Cancel();
        }

        public IEnumerable<IReceiverTask> CreateTasks()
        {
            var dfSetup = new DirectionFindingTaskSetup
            {
                FrequencyRanges = TaskManager.FilterManager.Filters.Select(f => f.ToFrequencyRange()).ToArray(),
                TaskManager = TaskManager,
                Priority = 0,
                ReceiversIndexes = new[] {0, 1, 2, 3, 4},
                IsEndless = true,
                AutoUpdateObjectives = true
            };

            var dfTask = new DirectionFindingTask(dfSetup);
            dfTask.SignalsProcessedEvent += OnNewSignalsProcessed;
            _dfTask = dfTask;

            return new [] {_dfTask};
        }

        private void OnNewSignalsProcessed(object sender, SignalsProcessedEventArg e)
        {
            if (LinkedStationManager.Connected && LinkedStationManager.ConnectionType == ConnectionTypes.TcpIp)
            {
                ProcessSignalsInLinkedMode(e);
            }
            else
            {
                ProcessSignalsInStandaloneMode(e);
            }
            if (e.Scan.BandNumber == _dfTask.Objectives.LastOrDefault().Number)
            {
                UpdateScanSpeed();
            }
        }

        private void UpdateScanSpeed()
        {
            ScanSpeed = this.UpdateScanSpeed(_dfTask.Objectives.Count, ref _lastCycleEndTime);
        }

        private async void ProcessSignalsInLinkedMode(SignalsProcessedEventArg e)
        {
            ProcessSignalsInStandaloneMode(e);
            if (_ownScansReady)
            {
                return;
            }
            _signalsToSent.AddRange(RadioSourceSignals(e.Signals));

            if (e.Scan.BandNumber == _dfTask.Objectives.Last().Number && !_ownScansReady)
            {
                LinkedStationManager.SendSignals(_signalsToSent);
                _signalsToSent.Clear();

                // pause task waiting for linked station signals
                TaskManager.RemoveTask(_dfTask);
                _ownScansReady = true;

                var receivedSignals = await LinkedStationManager.WaitForSignals();
                TaskManager.RadioSourceStorage.PutLinkedStationSignals(receivedSignals);

                // resume task
                TaskManager.AddTask(_dfTask);
                _ownScansReady = false;
            }
            else
            {
                ProcessSignalsInStandaloneMode(e);
            }
        }

        private IEnumerable<ISignal> RadioSourceSignals(IReadOnlyList<ISignal> signals)
        {
            var subScanRelativeThreshold = _config.RadioSourceRelativeSubScanCount;
            return signals.Where(s => s.RelativeSubScanCount > subScanRelativeThreshold);
        }

        private IEnumerable<ISignal> ImpulseSignals(IReadOnlyList<ISignal> signals)
        {
            var subScanRelativeThreshold = _config.RadioSourceRelativeSubScanCount;
            return signals.Where(s => s.RelativeSubScanCount <= subScanRelativeThreshold);
        }

        private void ProcessSignalsInStandaloneMode(SignalsProcessedEventArg e)
        {
            TaskManager.SpectrumStorage.Put(e.Scan);
            TaskManager.RadioSourceStorage.Put(RadioSourceSignals(e.Signals));
            TaskManager.SignalStorage.Put(ImpulseSignals(e.RawSignals));
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DspDataModel;
using DspDataModel.Storages;
using DspDataModel.Tasks;
using Settings;
using TasksLibrary.Tasks;

namespace TasksLibrary.Modes
{
    public class SequenceReceiversMode : IModeController
    {
        public ITaskManager TaskManager { get; }
        public float ScanSpeed { get; private set; }

        private ReceiversSequenceTask _task;

        private readonly ISpectrumStorage _spectrumStorage;
        private readonly IRadioSourceStorage _radioSourceStorage;
        private readonly DirectionFindingConfig _config;

        private DateTime _lastCycleEndTime;

        public SequenceReceiversMode(ITaskManager taskManager)
        {
            TaskManager = taskManager;
            _radioSourceStorage = taskManager.RadioSourceStorage;
            _spectrumStorage = taskManager.SpectrumStorage;

            _config = Config.Instance.DirectionFindingSettings;
        }

        public Task Initialize() => Task.CompletedTask;

        public void OnActivated()
        {
            _lastCycleEndTime = DateTime.UtcNow;
        }

        public void OnStop()
        {
            _task.Cancel();
        }

        public IEnumerable<IReceiverTask> CreateTasks()
        {
            _task = new ReceiversSequenceTask(TaskManager, 
                averagingCount: Config.Instance.DirectionFindingSettings.SpectrumScanCount,
                findSignals: true,
                isEndless: true);
            _task.DataProcessedEvent += OnNewDataProcessed;
            yield return _task;
        }

        private void OnNewDataProcessed(object sender, ReceiversSequenceTask.TaskResult data)
        {
            var lastBandNumber = _task.Objectives.LastOrDefault().Number;

            foreach (var scan in data.Scans)
            {
                if (scan.BandNumber == lastBandNumber)
                {
                    UpdateScanSpeed();   
                }
                _spectrumStorage.Put(scan);
            }
            var subScanRelativeThreshold = _config.RadioSourceRelativeSubScanCount;
            _radioSourceStorage.Put(data.Signals.Where(s => s.RelativeSubScanCount > subScanRelativeThreshold));
        }

        private void UpdateScanSpeed()
        {
            ScanSpeed = this.UpdateScanSpeed(_task.Objectives.Count, ref _lastCycleEndTime);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using DataStorages;
using DspDataModel;
using DspDataModel.RadioJam;
using DspDataModel.Tasks;
using SharpExtensions;
using TasksLibrary.Tasks;

namespace TasksLibrary.JamStrategies
{
    public abstract class JamStrategy : IRadioJamStrategy
    {
        protected readonly IRadioJamManager RadioJamManager;
        protected readonly ITaskManager TaskManager;
        protected readonly UpdateFrsJamStateDelegate UpdateStateEventFunction;

        protected JamStrategy(IRadioJamManager radioJamManager, ITaskManager taskManager, UpdateFrsJamStateDelegate updateStateEventFunction)
        {
            RadioJamManager = radioJamManager;
            TaskManager = taskManager;
            UpdateStateEventFunction = updateStateEventFunction;
        }

        public async Task JammingTask(CancellationToken token)
        {
            try
            {
                while (!token.IsCancellationRequested)
                {
                    var activeSources = await FindRadioSources().ConfigureAwait(false);
                    activeSources = await UpdateActiveSources(activeSources).ConfigureAwait(false);

                    var waitDuration =
                        TimeSpan.FromMilliseconds(RadioJamManager.EmitDuration - RadioJamManager.CheckEmitDelayMs);
                    var duration = TimeSpan.FromMilliseconds(RadioJamManager.EmitDuration);

                    if (token.IsCancellationRequested)
                    {
                        break;
                    }
                    if (activeSources.Count == 0)
                    {
                        IdleCycle();
                        continue;
                    }

                    var sourcesToJam = GetSourcesToJam(activeSources);
                    UpdateTargetsControlState(activeSources);
                    await StartJamming(sourcesToJam, duration, token).ConfigureAwait(false);
                    UpdateEmitionFlag(sourcesToJam);

                    if (token.IsCancellationRequested)
                    {
                        break;
                    }

                    await FireJamStateEvents(waitDuration, token).ConfigureAwait(false);
                }
            }
            catch (TaskCanceledException e)
            {
                // ignore, exception is thrown by Task.Delay method
            }
            catch (Exception e)
            {
                MessageLogger.Error(e);
            }
            finally
            {
                RadioJamManager.Shaper.StopJamming();
            }
        }

        protected virtual async Task<IReadOnlyList<TargetSignalPair>> UpdateActiveSources(IReadOnlyList<TargetSignalPair> activeSources)
        {
            return activeSources;
        }

        protected async Task FireJamStateEvents(TimeSpan waitDuration, CancellationToken token)
        {
            UpdateStateEventFunction(RadioJamManager.Storage.FrsTargets);
            await Task.Delay(waitDuration, token);

            var zeroTargets = RadioJamManager.Storage.FrsTargets
                .Select(t => new RadioJamTarget(t.FrequencyKhz, t.Id))
                .ToArray();

            UpdateStateEventFunction(zeroTargets);
        }

        protected async Task StartJamming(TargetSignalPair[] sourcesToJam, TimeSpan emitionDuration, CancellationToken token)
        {
            var commandResult = RadioJamManager.Shaper.StartFrsJamming(emitionDuration, sourcesToJam.Select(s => s.Target));
            // checking real emition level
            await Task.Delay(RadioJamManager.CheckEmitDelayMs, token);

            if (commandResult.IsOk)
            {
                foreach (var target in RadioJamManager.Storage.FrsTargets)
                {
                    target.UpdateJamState(sourcesToJam.Any(s => ReferenceEquals(s.Target, target)));
                }
            }
        }

        protected void UpdateEmitionFlag(TargetSignalPair[] sourcesToJam)
        {
            var powers = RadioJamManager.Shaper.GetPower();
            foreach (var target in RadioJamManager.Storage.FrsTargets)
            {
                target.UpdateEmitionState(false);
            }
            for (var i = 0; i < powers.Result?.Length; i++)
            {
                if (powers.Result[i] > RadioJamManager.MinPowerLevel)
                {
                    var liter = i + 1;
                    sourcesToJam.Where(s => s.Target.Liter == liter && s.Target.JamState.IsActive())
                        .Do(s => s.Target.UpdateEmitionState(true));
                }
            }
        }

        protected void IdleCycle()
        {
            var isSomeStateChanged = false;
            foreach (var target in RadioJamManager.Storage.FrsTargets)
            {
                isSomeStateChanged |= target.UpdateJamState(false);
                isSomeStateChanged |= target.UpdateControlState(false);
                isSomeStateChanged |= target.UpdateEmitionState(false);
            }

            if (isSomeStateChanged)
            {
                UpdateStateEventFunction(RadioJamManager.Storage.FrsTargets);
            }
        }

        protected virtual TargetSignalPair[] GetSourcesToJam(IReadOnlyList<TargetSignalPair> sources)
        {
            return sources
                .OrderByDescending(s => s.Target.Priority)
                .GroupBy(s => s.Target.Liter)
                .Select(g => g
                    .OrderBy(s => s.Target.Id)
                    .Take(RadioJamManager.ChannelsInLiter))
                .SelectMany(_ => _)
                .ToArray();
        }

        protected void UpdateTargetsControlState(IReadOnlyList<TargetSignalPair> sources)
        {
            foreach (var target in RadioJamManager.Storage.FrsTargets)
            {
                var controlledSignal = sources.FirstOrDefault(s => ReferenceEquals(s.Target, target));
                if (controlledSignal != null)
                {
                    target.Amplitude = controlledSignal.Signal.Amplitude;
                }
                target.UpdateControlState(controlledSignal != null);
            }
        }

        protected virtual async Task<IReadOnlyList<TargetSignalPair>> FindRadioSources()
        {
            var controlSources = RadioJamManager.Storage.FrsTargets.ToArray();
            if (controlSources.Length == 0)
            {
                return new TargetSignalPair[0];
            }
            var task = RadioSourcesControlTask.Create(TaskManager, controlSources);
            TaskManager.AddTask(task);
            await task.WaitForResult();
            return task.TypedResult?.Signals ?? new TargetSignalPair[0];
        }
    }
}

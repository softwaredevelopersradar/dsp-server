﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataProcessor;
using DspDataModel;
using DspDataModel.RadioJam;
using DspDataModel.Storages;
using DspDataModel.Tasks;
using Phases;
using Settings;
using SharpExtensions;
using TasksLibrary.Tasks;

namespace TasksLibrary.JamStrategies
{
    // Подавление АПРЧ
    public class AfrsJamStrategy : JamStrategy
    {
        private readonly IEnumerable<float> _initialFrs;
        private readonly float _gap = Config.Instance.DirectionFindingSettings.SignalsMergeGapKhz;

        public AfrsJamStrategy(IRadioJamManager radioJamManager, ITaskManager taskManager,
            UpdateFrsJamStateDelegate updateStateEventFunction)
            : base(radioJamManager, taskManager, updateStateEventFunction)
        {
            var initialTargets = RadioJamManager.Storage.FrsTargets;
            _initialFrs = TaskManager.RadioSourceStorage.GetRadioSources()
                .Where(rs => rs.IsActive && !initialTargets.Any(
                    t => Math.Abs(t.FrequencyKhz - rs.FrequencyKhz) < _gap))
                .Select(s => s.FrequencyKhz).ToList();
        }

        protected override async Task<IReadOnlyList<TargetSignalPair>> UpdateActiveSources(IReadOnlyList<TargetSignalPair> activeSources)
        {
            var leftSources = RadioJamManager.Storage.FrsTargets
                .Where(t => activeSources.All(s => !ReferenceEquals(s.Target, t)))
                .ToArray();

            var resultSources = (await FindDfRadioSources(leftSources)).ToList();
            foreach (var source in resultSources)
            {
                source.Target.FrequencyKhz = source.Signal.FrequencyKhz;
            }

            resultSources.AddRange(activeSources.Select(r => new TargetSignalPair(r.Target, r.Signal)));
            return resultSources;
        }

        private IEnumerable<Band> GetSignalSearchRange(IRadioJamTarget signal)
        {
            var searchRange = RadioJamManager.FrequencySearchBandwidthKhz / 2;

            var startFrequency = Math.Max(signal.FrequencyKhz - searchRange, Constants.FirstBandMinKhz);
            var endFrequency = Math.Min(signal.FrequencyKhz + searchRange, Config.Instance.BandSettings.LastBandMaxKhz);

            var startBandNumber = Utilities.GetBandNumber(startFrequency);
            var endBandNumber = Utilities.GetBandNumber(endFrequency);

            return Enumerable
                .Range(startBandNumber, endBandNumber - startBandNumber + 1)
                .Select(b => new Band(b));
        }

        /// <summary>
        /// finds new frequencies of not found targets
        /// </summary>
        private async Task<List<TargetSignalPair>> FindDfRadioSources(IReadOnlyList<IRadioJamTarget> targets)
        {
            if (targets.IsNullOrEmpty())
            {
                return new List<TargetSignalPair>();    
            }

            var bands = targets
                .SelectMany(GetSignalSearchRange)
                .Distinct()
                .ToArray();

            var taskSetup = new DirectionFindingTaskSetup
            {
                TaskManager = this.TaskManager,
                Objectives = bands,
                Priority = 1,
                ReceiversIndexes = new[] {0, 1, 2, 3, 4},
                IsEndless = false,
                AutoUpdateObjectives = false
            };

            var task = new DirectionFindingTask(taskSetup);
            TaskManager.AddTask(task);
            await task.WaitForResult();

            var result = new List<TargetSignalPair>();

            foreach (var target in targets)
            {
                var signal = task.TypedResult.Signals.FirstOrDefault(s =>
                {
                    if (result.Any(pair => pair.Signal == s))
                        return false;

                    if (PhaseMath.Angle(target.Direction, s.Direction) > RadioJamManager.DirectionSearchSector)
                    {
                        return false;
                    }
                    
                    if (s.Amplitude < RadioJamManager.Threshold)
                    {
                        return false;
                    }
                    
                    var searchRange = RadioJamManager.FrequencySearchBandwidthKhz / 2;
                    var startFrequency = Math.Max(target.FrequencyKhz - searchRange, Constants.FirstBandMinKhz);
                    var endFrequency = Math.Min(target.FrequencyKhz + searchRange, Config.Instance.BandSettings.LastBandMaxKhz);

                    if (s.FrequencyKhz > endFrequency || s.FrequencyKhz < startFrequency)
                        return false;

                    var otherTargets = RadioJamManager.Storage.FrsTargets.Where(t => !ReferenceEquals(t,target));
                    if (otherTargets.Any(
                        t => Math.Abs(s.FrequencyKhz - t.FrequencyKhz) < _gap))
                        return false;

                    if (_initialFrs.Any(rs=> Math.Abs(rs - s.FrequencyKhz) < _gap))
                        return false;
                    
                    return true;
                });
                if (signal != null)
                {
                    result.Add(new TargetSignalPair(target, signal));
                }
            }
            return result;
        }
    }
}

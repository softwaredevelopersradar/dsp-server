﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using DataProcessor.Fhss;
using DataStorages;
using DspDataModel;
using DspDataModel.Hardware;
using DspDataModel.RadioJam;
using DspDataModel.Server;
using DspDataModel.Tasks;
using HardwareLibrary.FpgaDevices;
using Settings;

namespace TasksLibrary.JamStrategies
{
    /// <summary>
    /// Not a jamming strategy though, because we only measure the duration without radiation.
    /// </summary>
    public class FhssDurationMeasureStrategy : IRadioJamStrategy
    {
        private readonly int _controlTimeMs;
        private readonly IFpgaDeviceManager _deviceManager;
        private readonly IRadioJamManager _radioJamManager;
        private readonly ITaskManager _taskManager;
        private readonly IRadioJamShaper _shaper;
        private readonly IServerController _serverController;
        private IReadOnlyList<IRadioJamFhssTarget> _fhssTargets; //add readonly
        private FhssSetup _fhssSetup;
        public FhssDurationMeasureStrategy(
            int controlTimeMs,
            IRadioJamManager radioJamManager,
            ITaskManager taskManager,
            IServerController serverController)
        {
            _controlTimeMs = controlTimeMs;
            _taskManager = taskManager;
            _radioJamManager = radioJamManager;
            _deviceManager = taskManager.HardwareController.DeviceManager;
            _shaper = radioJamManager.Shaper;
            _serverController = serverController;
            _fhssTargets = taskManager.FhssNetworkStorage.GetFhssNetworks()
                .Where(n => n.IsDurationMeasured == false)
                .Select(n => new RadioJamFhssTarget(
                    minFrequencyKhz: n.MinFrequencyKhz,
                    maxFrequencyKhz: n.MaxFrequencyKhz,
                    threshold: taskManager.FilterManager.GetThreshold(taskManager.FilterManager.Bands[0].Number),
                    modulationCode: 0,
                    manipulationCode: 0,
                    deviationCode: 0,
                    forbiddenRanges: new List<FrequencyRange>(),
                    id: n.Id)
            ).ToList();
        }

        private FhssSetup GenerateFhssSetup(IRadioJamFhssTarget target)
        {
            var threshold = new[] {target.Threshold};
            var bandSettings = _taskManager.HardwareController.DeviceManager.BandsSettingsSource.Settings;
            var fpgaReceiverSettings = _fhssTargets
                .Select(t => new FhssReceiverSetup(
                    (int)(t.MinFrequencyKhz / 1000) * 1000,
                    bandSettings[Utilities.GetBandNumber(t.MinFrequencyKhz + Constants.BandwidthKhz * 0.5f)].ShouldBeReversed))
                .ToArray();
            return new FhssSetup(FftResolution.N16384, threshold, _fhssTargets, fpgaReceiverSettings);//maximum precision for duration measurement
        }

        private void SetupReceivers(IRadioJamFhssTarget target)
        {
            var receiverCentralFrequencyKhz = target.MinFrequencyKhz + Constants.BandwidthKhz * 0.5f;
            _taskManager.HardwareController.ReceiverManager.Receivers[0].SetFrequency((int)(receiverCentralFrequencyKhz / 1000));
        }

        private bool StartJamming(IRadioJamFhssTarget target)
        {
            _fhssSetup = GenerateFhssSetup(target);
            SetupReceivers(target);
            if (!_taskManager.HardwareController.DeviceManager.StartFhssMode(_fhssSetup))
            {
                throw new Exception("Device manager can't start fhss mode");
            }

            var response = _shaper.StartFhssDurationMeasurement(_controlTimeMs, target);
            if (response.ErrorCode == -1)
                return false;
            return true;
        }

        public Task JammingTask(CancellationToken token)
        {
            //todo : use token for forced stop.
            return Task.Run(() => JammingTaskInner());
        }

        private void JammingTaskInner()
        {
            IFpgaDevice device;
            if (Config.Instance.FhssSearchSettings.FhssDurationMeasureTest)
                _fhssTargets = _radioJamManager.Storage.FhssTargets;
            foreach (var target in _fhssTargets)
            {
                if (StartJamming(target) == false)
                {
                    MessageLogger.Warning($"Error occured while trying to measure duration of {target.MinFrequencyKhz} - {target.MaxFrequencyKhz}," +
                                          " measurement is aborted. Check shaper state");
                    continue;
                }

                device = _deviceManager.Devices[0];
                try
                {
                    MeasuringCycle(target);
                }
                catch (Exception e)
                {
                    MessageLogger.Error($"Couldn' measure {target.MinFrequencyKhz} - {target.MaxFrequencyKhz} network duration "+e);
                }
                finally
                {
                    _deviceManager.StopFhssMode();
                    _shaper.StopJamming();
                }
            }
            _serverController.SetMode(_serverController.PreDurationMeasurementMode);

            void MeasuringCycle(IRadioJamFhssTarget target)
            {
                // is needed for simulation only
                var isSimulation = device is FakeFpgaDevice;
                var bandNumbers = _taskManager.HardwareController.ReceiverManager.Receivers
                    .Select(r => r.BandNumber)
                    .ToArray();

                var previousScanIndex = -1;
                var peakIndecies = new List<float>();
                const int noPeakFoundCode = 16383;

                CancellationTokenSource source = new CancellationTokenSource();
                CancellationToken token;
                token = source.Token;
                
                var indexTask = new Task(() =>
                {
                    while (token.IsCancellationRequested == false)
                    {
                        if (isSimulation)
                        {
                            Array.Copy(
                                _fhssTargets.Select(t => Utilities.GetBandNumber(t.MinFrequencyKhz)).ToArray(),
                                bandNumbers,
                                _fhssTargets.Count
                            );
                            _deviceManager.GetScan(bandNumbers);
                        }
                        else
                        {
                            var scanIndex = _deviceManager.Devices[0].GetScanIndex();
                            if (scanIndex == previousScanIndex)
                            {
                                continue;
                            }
                            previousScanIndex = scanIndex;
                        }

                        for (var i = 0; i < _fhssSetup.ReceiverSettings.Count; ++i)
                        {
                            var peakIndex = device.GetFhssPeakIndex(i);
                            if (peakIndex == noPeakFoundCode)
                            {
                                continue;
                            }
                            peakIndecies.Add(peakIndex);
                        }
                    }
                }, token);

                while (_shaper.GetState().Result == 4 && token.IsCancellationRequested == false) //todo : 4 -> to shaper states enum
                {
                    if (indexTask.Status == TaskStatus.Created)
                        indexTask.Start();
                    Thread.Sleep(1000);//TODO : add to config
                }

                if (isSimulation)
                {
                    if (indexTask.Status == TaskStatus.Created)
                        indexTask.Start();//todo : remove?
                    Thread.Sleep(_controlTimeMs);
                }

                source.Cancel(); //ending the task
                
                var duration = MeasureDuration(peakIndecies);

                if (isSimulation)
                    duration = new Random().Next(0, 11_000_000);

                MessageLogger.Warning(
                    $"Measuring result : {target.MinFrequencyKhz} - {target.MaxFrequencyKhz}, duration : {duration}");

                if (duration < 50 || duration > 10_000_000)
                {
                    MessageLogger.Warning($"Measured duration {duration} mcs is not going to be applied, since it is too small or big");
                    return;
                }

                var network = _taskManager.FhssNetworkStorage.GetFhssNetworks().FirstOrDefault(n => n.Id == target.Id);
                network?.Update(
                    new FhssNetwork(
                        minFrequencyKhz: network.MinFrequencyKhz,
                        maxFrequencyKhz: network.MaxFrequencyKhz,
                        bandwidthKhz: network.BandwidthKhz,
                        stepKhz: network.StepKhz,
                        amplitude: network.Amplitude,
                        impulseDurationMs: duration,
                        isDurationMeasured: true,
                        frequenciesCount: network.FrequenciesCount,
                        directions: network.Directions,
                        fixedRadioSources: network.FixedRadioSources
                        ));
                }
        }

        private float MeasureDuration(IList<float> peakIndecies)
        {
            //clear index copies, from 111222333 -> 123
            while (true)
            {
                var indexToRemove = -1;
                for (int i = 0; i < peakIndecies.Count - 1; i++)
                {
                    if (peakIndecies[i] == peakIndecies[i+1])//todo : numbers to config or some-sort of?
                    {
                        indexToRemove = i;
                        break;
                    }
                }
                if (indexToRemove != -1)
                {
                    peakIndecies.RemoveAt(indexToRemove);
                    continue;
                }
                break;
            }
            var boundariesCount = 2;
            if (peakIndecies.Count <= 50) //when indecies count is small, boundaries are included to result 
                boundariesCount = 0;
            var listWithout4097 = peakIndecies.Where(i => i != 4097).ToList();//todo : 4097?
            return (float) _controlTimeMs * 1000 / (listWithout4097.Count - boundariesCount);
        }
    }
}

